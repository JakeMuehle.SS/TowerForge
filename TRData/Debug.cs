//using DevExpress.Xpo.Logger;

//https://stackoverflow.com/questions/2743260/is-it-possible-to-write-to-the-console-in-colour-in-net
public class Debug
{
    public static object _lock = new object();
        
    public static void Log(string log)
    {
        System.Console.WriteLine("LOG: " + log);
    }

    public static void LogWarning(string log)
    {
        lock (_lock)
        {
            System.Console.BackgroundColor = System.ConsoleColor.Yellow;
            System.Console.ForegroundColor = System.ConsoleColor.Black;
            System.Console.WriteLine("WARNING: " + log);
            System.Console.ResetColor();

        }
    }

    public static void LogError(string log)
    {
        lock (_lock)
        {
            System.Console.BackgroundColor = System.ConsoleColor.DarkRed;
            System.Console.WriteLine("ERROR: " + log);
            System.Console.ResetColor();

        }
    }
}
