﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Linq;

public class ActionCardsTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private int COL_ACTIVE;
	public ActionCardsTree() : base()
	{
		AddColumn("Name", 100, 100);
		AddColumn("Cost",75,75,Editor_Cost,Validator_Cost);
        AddColumn("Rarity",80,80,Editor_Rarity,Validator_Rarity,cellPipFunc:RarityPip);
		COL_ACTIVE = AddColumn("Active",50,50,Editor_Active,Validator_Active).ColumnIndex;
		AddColumn("Arena",60,60,Editor_Arena,Validator_Arena);

		Signals.ActionCardChangedLists.Register(ActionCardChangedLists);

		OnNodeSelect += (id) =>
		{
			Signals.ActionCardSelected.Send(id, this);
		};
	}

	public void ActionCardChangedLists(string id)
	{
		if (!DataManager.Inst.ActionCardIsRunnerPower(id))
		{
			if (!_nodes.ContainsKey(id))
			{
				InitialPopulateAction(id);
			}
		}
		else
		{
			if (_nodes.ContainsKey(id))
			{
				_nodes[id].Remove();
				_nodes.Remove(id);
			}
		}
	}

	private object[] GetData(string id)
	{
		CardRarity rarity = CardRarity.None;
		int arena = -1;
		if(DataManager.Inst.TryGetEnumIntValue(id,out int intId))
        {
            rarity = FirebaseManager.Inst.GetRarity(intId);
			arena = FirebaseManager.Inst.GetArenaForCardUnlock(intId);
        }

        List<ObjectType> actionCardObjectTypes = DataManager.Inst.GetActionCardObjectTypes().ToList();
		object[] data = new object[5 + actionCardObjectTypes.Count()];

		data[0] = DataManager.Inst.DisplayName(id);
		data[1] = DataManager.Inst.GetActionCardCost(id);
		data[2] = rarity;
		data[3] = DataManager.Inst.GetIsCardActive(id);
		data[4] = arena;
		for(int i = 0; i < actionCardObjectTypes.Count; i++)
        {
			data[i + 5] = DataManager.Inst.GetActionCardAsset(id, actionCardObjectTypes[i]);
        }

		return data;
	}

	private CellPip RarityPip(string id, object value)
	{
		if (!_nodes.ContainsKey(id) || value == null)
		{
			return new CellPip(false);
		}
		CardRarity rarity = GetEnum<CardRarity>(value);
		return Utils.GetRarityPip(rarity);
	}


	#region editors
	private RepositoryItem Editor_Cost(string id)
    {
		if (!_nodes.ContainsKey(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
    }

	private RepositoryItem Editor_Rarity(string id)
    {
        if (!_nodes.ContainsKey(id)) { return null; }
		return CreateEditorDropDown<CardRarity>();
    }

	private RepositoryItem Editor_Active(string id)
    {
		if (!_nodes.ContainsKey(id)) { return null; }
		return CreateEditorCheckEdit();
    }

	private RepositoryItem Editor_Arena(string id)
    {
		if (!_nodes.ContainsKey(id)) { return null; }

		return CreateEditorDropDown(() =>
		{
			List<string> toReturn = new List<string>();
			for(int i = -1; i <  FirebaseManager.Inst.GetArenaCount(); i++)
            {
				toReturn.Add(i.ToString());
            }
			return toReturn;
		});
    }

	#endregion editors
	#region validators
	private bool Validator_Cost(string id, object value)
    {
        if (!_nodes.ContainsKey(id))
        { return false; }
		return DataManager.Inst.SetActionCardCost(id, (int)value);
    }

	private bool Validator_Rarity(string id, object value)
    {
		if(!DataManager.Inst.TryGetEnumIntValue(id, out int intId))
        {
			return false;
        }
		return FirebaseManager.Inst.SetRarity(intId, GetEnum<CardRarity>(value));
    }

	private bool Validator_Active(string id, object value)
    {
        if (!_nodes.ContainsKey(id)) { return false; }
		return DataManager.Inst.SetActionCardActive(id, (bool)value);
    }

	private bool Validator_Arena(string id, object value)
    {
        if (!int.TryParse(value.ToString(), out int arenaIndex))
        { return false; }
		if(!DataManager.Inst.TryGetEnumIntValue(id,out int intId))
		{ return false; }
		return FirebaseManager.Inst.SetCardUnlockedArena(intId, arenaIndex);
    }
    #endregion

    protected override bool SpeedRender(int column, TreeListNode node)
    {
		return column != COL_ACTIVE;
    }
    protected override string GetTreeName() { return nameof(ActionCardsTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
    protected override void RuntimeInit()
    { }
    protected override void PrePopulate() 
	{
		foreach (ObjectType otype in DataManager.Inst.GetActionCardObjectTypes())
		{
			AddColumn(otype.ToString(), 120, 120, (id) => { return CreateEditorTextEdit(); }, (id, value) =>
			{
				return DataManager.Inst.SetActionCardAsset(id, otype, (string)value);
			},
			cellPipFunc: (id, value) =>
			{
				if (DataManager.Inst.CheckActionCardAssetDefault(id, otype))
				{
					return new CellPip(3, System.Drawing.Color.DarkRed);
				}
				return new CellPip(false);
			});
		}
	}

	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		List<string> ids = new List<string>();
		foreach (string id in DataManager.Inst.GetIds<ActionCardType>())
		{
			if (!DataManager.Inst.ActionCardIsRunnerPower(id))
			{
				ids.Add(id);
			}
		}
		return ids;
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}


	public void DropAction(string id, List<string> droppingIds)
    {
		foreach (string did in droppingIds)
		{
			DataManager.Inst.MoveActionCard(did, false);
		}
	}
}