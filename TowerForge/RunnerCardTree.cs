﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class RunnerCardTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private int _level;
	private int _rank;
	private int COL_RARITY;
	private int COL_COMPS;
	private int COL_COST;
	private int COL_ACTIVE;

	private Dictionary<string, HashSet<string>> _categories = new Dictionary<string, HashSet<string>>();
	public RunnerCardTree() : base()
	{
		_rank = 0;
		AddColumn("ID", 100, 100);
		COL_RARITY = AddColumn("Rarity",100,100,Editor_Rarity,Validator_Rarity,cellPipFunc:RarityPip).ColumnIndex;
		COL_ACTIVE = AddColumn("Active",60,60,Editor_IsCardActive,Validator_IsActive).ColumnIndex;
		COL_COST = AddColumn("Cost",60,60,Editor_Cost,Validator_Cost).ColumnIndex;
		AddColumn("Distribution",100,100,Editor_Distribution,Validator_Distribution);
		AddColumn("Interval",75,75,Editor_Interval,Validator_Interval);
		AddColumn("Card Art", 220, 220, Editor_CardArt, Validator_CardArt, cellPipFunc: CardArtPip);
		AddColumn("Threat/Cost");
		COL_COMPS = AddColumn("Compositions").ColumnIndex;

		OnNodeSelect += (id) =>
		{
			foreach(HashSet<string> cards in _categories.Values)
            {
				if (cards.Contains(id))
				{ Signals.RunnerCardSelected.Send(id); }
            }
		};

		Signals.LibraryUpdated.Register(UpdateLibrary);
		Signals.RunnerCardUpdated.Register(UpdateCardData);
	}

    protected override bool SpeedRender(int column, TreeListNode node)
    {
		return column != COL_ACTIVE;
    }

	private CellPip RarityPip(string id, object value)
	{
		if (_categories.ContainsKey(id) || value == null)
		{
			return new CellPip(false);
		}
		CardRarity rarity = GetEnum<CardRarity>(value);
		return Utils.GetRarityPip(rarity);
	}

	private void UpdateLibrary()
    {
		foreach(string nodeId in _nodes.Keys)
        {
            if (_categories.ContainsKey(nodeId)) { continue; }
			if(!DataManager.Inst.TryGetEnumIntValue(nodeId, out int intId))
            {
				continue;
            }
			_nodes[nodeId].SetValue(COL_RARITY, FirebaseManager.Inst.GetRarity(intId).ToString());
        }
    }

	private object[] GetData(string id, string category)
    {
		if(category == RunnerCardCategory.None.ToString())
        {
			Debug.LogError($"Hey you can't pass {RunnerCardCategory.None} as a Runner Card category!");
			return new object[] { id };
        }

		int cost = DataManager.Inst.GetRunnerCardCost(id);
		float totalThreat = 0;
		for (int i = 0; i < DataManager.Inst.GetRunnerCardUnitCountForRank(id, _rank); i++)
		{
			string unitType = DataManager.Inst.GetRunnerCardUnitInRankComposition(id, _rank, i);
			totalThreat += DataManager.Inst.GetRunnerThreat(unitType,_level,_rank);
		}

		//string distribution = .ToString();
		//string interval = .ToString();
		string threatString = (totalThreat / cost).ToString();
		int compositions = DataManager.Inst.GetRunnerCardUniqueCompositionRanks(id).Count;
		string rarity = CardRarity.None.ToString();
		if(DataManager.Inst.TryGetEnumIntValue(id,out int intId))
        {
			rarity = FirebaseManager.Inst.GetRarity(intId).ToString();
		}

		object[] data = new object[]
		{
			DataManager.Inst.DisplayName(id),
			rarity,
			DataManager.Inst.GetIsRunnerCardActive(id),
			cost,
			DataManager.Inst.GetRunnerCardDistribution(id),
			DataManager.Inst.GetRunnerCardInterval(id),
			DataManager.Inst.GetRunnerCardArt(id),
			threatString,
			compositions
		};
		return data;
	}

	private void UpdateCardData(string id)
    {
        string cat = DataManager.Inst.GetRunnerCardCategory(id).ToString();
		object[] data = GetData(id, cat);
		if (!_nodes.ContainsKey(id))
		{
			TreeListNode node = AppendNode(data, _nodes[cat], id);
			_nodes.Add(id, node);
			_categories[cat].Add(id);
		}
		else
		{
			for (int i = 0; i < data.Length; i++)
			{
				_nodes[id].SetValue(i, data[i]);
			}
		}
    }

	protected override List<string> GetErrors(string id, bool exitEarly)
    {
		List<string> errors = new List<string>();
        if (!IsCard(id)) { return errors; }
		TreeListNode node = _nodes[id];
        if (GetEnum<CardRarity>(node.GetValue(COL_RARITY)) == CardRarity.None)
        {
            errors.Add("Card rarity must be assigned!");
            if (exitEarly) { return errors; }
        }
		object cost = node.GetValue(COL_COST);
        if (cost == null || (int)cost == 0)
        {
            errors.Add("Card must cost more than 0!");
            if (exitEarly) { return errors; }
        }
		object comps = node.GetValue(COL_COMPS);
		if(comps == null ||  (int) comps == 0)
        {
			errors.Add("Card must have at least one composition!");
            if (exitEarly) { return errors; }
        }

        return errors;
    }

    protected override bool IsHeader(string id)
    {
		return _categories.ContainsKey(id);
    }

	private CellPip CardArtPip(string id, object value)
    {
		if (IsCard(id) && DataManager.Inst.IsRunnerCardArtDefault(id))
		{
			return new CellPip(3, System.Drawing.Color.DarkRed);
		}
		return new CellPip(false);
	}

    #region setEditors
    private RepositoryItem Editor_Rarity(string id)
    {
		if(IsCard(id))
        {
			return CreateEditorDropDown<CardRarity>();
        }
		return null;
    }

	private RepositoryItem Editor_IsCardActive(string id)
    {
		if (IsCard(id))
		{
			return CreateEditorCheckEdit();
		}
		return null;
    }

	private RepositoryItem Editor_Cost(string id)
    {
		if(IsCard(id))
        {
			return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
        }
		return null;
    }

	private RepositoryItem Editor_Distribution(string id)
    {
		if(IsCard(id))
        {
			return CreateEditorDropDown<RunnerDistribution>();
        }
		return null;
    }

	private RepositoryItem Editor_Interval(string id)
    {
		if(IsCard(id))
        {
			return CreateEditorTextEdit(DataEditorType.FloatZeroOrPositive);
        }
		return null;
    }

	private RepositoryItem Editor_CardArt(string id)
    {
		if(IsCard(id))
        {
			return CreateEditorTextEdit();
        }
		return null;
    }

	private bool IsCard(string id)
    {
		return (!_categories.ContainsKey(id) && _nodes.ContainsKey(id));

	}
	#endregion setEditors
	#region validators
	private bool Validator_Rarity(string id, object value)
    {
		if(!IsCard(id))
        {
			return false;
        }
		if(!DataManager.Inst.TryGetEnumFromId<RunnerCardType>(id,out RunnerCardType type))
        {
			return false;
        }
		return FirebaseManager.Inst.SetRarity(type, GetEnum<CardRarity>(value));
    }

	private bool Validator_IsActive(string id, object value)
    {
		if(!IsCard(id))
        {
			return false;
        }
		return DataManager.Inst.SetIsRunnerCardActive(id, (bool)value);
    }

	private bool Validator_Cost(string id, object value)
    {
		if(!IsCard(id))
        {
			return false;
        }
		return DataManager.Inst.SetRunnerCardCost(id, (int)value);
    }

    private bool Validator_Distribution(string id, object value)
    {
		if(!IsCard(id))
        {
			return false;
        }
		return DataManager.Inst.SetRunnerCardDistribution(id, GetEnum<RunnerDistribution>(value));
    }

	private bool Validator_Interval(string id, object value)
    {
		if(!IsCard(id))
        {
			return false;
        }
		return DataManager.Inst.SetRunnerCardInterval(id, (float)value);
    }

	private bool Validator_CardArt(string id, object value)
    {
        if (!IsCard(id))
        { return false; }
		return DataManager.Inst.SetRunnerCardArt(id, (string)value);
    }
    #endregion



    protected override string GetTreeName() { return "RunnerCardTree"; }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}

    
    protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
    protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType)
    {
        if (targetNode == null) { return; }
        if (insertType == InsertType.Before)
        {
            targetNode = targetNode.PrevVisibleNode;
        }
        if (targetNode == null) { return; }

		string nodeId = targetNode.Tag.ToString();
		while (!_categories.ContainsKey(nodeId))
        {
            if (!_nodes.ContainsKey(nodeId) || _nodes[nodeId].ParentNode == null)
            { return; }
			nodeId = _nodes[nodeId].ParentNode.Tag.ToString();
        }

		if(!System.Enum.TryParse<RunnerCardCategory>(nodeId, out RunnerCardCategory category))
        {
			return;
        }
		if(!DataManager.Inst.SetRunnerCardCategory(droppingID, category))
		{ return; }

		//_nodes[droppingID].Remove();
		//_nodes.Remove(droppingID);
		RefreshAll();
    }

    protected override bool AllowCopy(string targetId, string sourceId)
    {
		return false;
    }
    protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return new List<string>()
		{
			RunnerCardCategory.Runner.ToString(),
			RunnerCardCategory.Boss.ToString(),
			RunnerCardCategory.DefaultRunner.ToString(),
			RunnerCardCategory.Inactive.ToString()
		};
	}
	protected override void InitialPopulateAction(string category)
	{
		TreeListNode node = AppendNode(new object[] {category}, GetRoot(), category);
		_nodes.Add(category, node);
		_categories.Add(category, new HashSet<string>());
		PopulateCategory(category);
	}

	private void PopulateCategory(string category)
    {
        List<string> cards = DataManager.Inst.GetRunnerCardsForCategory(category);
		foreach(string id in cards)
        {
			TreeListNode node = AppendNode(GetData(id, category), _nodes[category], id);
			_nodes.Add(id, node);
			_categories[category].Add(id);
        }
    }

	private void RefreshAll()
    {
		// it's best to clear all nodes out first, since it's likely that theres a node that has recently transferred.
		// if you don't clear every node out first, it's very possible that a similar node is already registered in the _node dictionary.
		foreach (string catId in _categories.Keys)
		{
			foreach (string nodeId in _categories[catId])
            {
				_nodes[nodeId].Remove();
				_nodes.Remove(nodeId);
            }
			_categories[catId].Clear();
        }
		foreach(string catId in _categories.Keys)
        {
			PopulateCategory(catId);
        }
    }

}
