﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using System.Drawing;

public class Utils
{

    public static bool Approximately(float a, float b)
    {
        return Math.Abs(a - b) < 0.00001;
    }

    public static string ListString<T>(HashSet<T> items)
    {
        StringBuilder sb = new StringBuilder();
        foreach (T item in items)
        {
            if (sb.Length > 0)
            { sb.Append(", "); }
            sb.Append(item.ToString());
        }

        return sb.ToString();
    }

    public static List<T> GetEnumList<T>()
    {
        T[] vals = (T[])Enum.GetValues(typeof(T));
        return vals.ToList();
    }

    public static List<string> GetEnumStringList<T>()
    {
        List<string> vals = new List<string>();
        foreach(T item in Enum.GetValues(typeof(T)))
        {
            vals.Add(item.ToString());
        }
        return vals;
    }

    public static string SplitLast(string s, char c)
    {
        return SplitFromLast(s, c, 0);
    }

    public static string SplitIndex(string s, char c, int i)
    {
        string[] split = s.Split(c);
        return split[i];
    }

    public static string SplitFirst(string s, char c)
    {
        return SplitIndex(s, c, 0);
    }

    public static string SplitFromLast(string s, char c, int fromLast)
    {
        string[] split = s.Split(c);
        if (split.Length <= fromLast) { return string.Empty; }
        return split[split.Length - (1 + fromLast)];
    }

    public static SunTree.CellPip GetRankablePip(RankableFloat rf)
    {
        int scales = !Approximately(rf.Scalar, 1) ? 1 : 0;
        int ranks = !Approximately(rf.BaseValue, rf.FinalValue) && rf.Curve != CurveType.None ? 2 : 0;
        int flags = scales | ranks;
        switch (flags)
        {
            case 1: // scale only.
                return new SunTree.CellPip(0, System.Drawing.Color.CornflowerBlue);
            case 2: // rank only.
                return new SunTree.CellPip(0, System.Drawing.Color.Orange);
            case 3: // both rank and scale
                return new SunTree.CellPip(3, System.Drawing.Color.DarkViolet);
        }
        return new SunTree.CellPip(0, System.Drawing.Color.Transparent);
    }

    public static SunTree.CellPip GetRarityPip(CardRarity rarity)
    {
        switch (rarity)
        {
            case CardRarity.None:
                return new SunTree.CellPip(4, Color.Red);
            case CardRarity.Common:
                return new SunTree.CellPip(0, Color.CornflowerBlue);
            case CardRarity.Rare:
                return new SunTree.CellPip(0, Color.DarkOrange);
            case CardRarity.Epic:
                return new SunTree.CellPip(0, Color.Purple);
            case CardRarity.Legendary:
                return new SunTree.CellPip(2, Color.LavenderBlush);
        }
        return new SunTree.CellPip(false);
    }

    public static SunTree.CellPip GetRarityPip(ChestType rarity)
    {
        switch (rarity)
        {
            //case ChestType.None:
            //    return new SunTree.CellPip(4, Color.Red);
            case ChestType.Common:
                return new SunTree.CellPip(0, Color.CornflowerBlue);
            case ChestType.Rare:
                return new SunTree.CellPip(0, Color.DarkOrange);
            case ChestType.Epic:
                return new SunTree.CellPip(0, Color.Purple);
            case ChestType.Legendary:
                return new SunTree.CellPip(2, Color.LavenderBlush);
        }
        return new SunTree.CellPip(false);
    }
}
