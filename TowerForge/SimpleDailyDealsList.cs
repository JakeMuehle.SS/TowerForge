﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;
using System.Linq;
public class SimpleDailyDealsList : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private HashSet<string> _arenas = new HashSet<string>();
	private HashSet<string> _dealSets = new HashSet<string>();
	private HashSet<string> _deals = new HashSet<string>();

	private const string MENU_ADD_SET = "Add deal set";
	private const string MENU_REMOVE_SET = "Remove deal set";
	private const string MENU_ADD_DEAL = "Add deal";
	private const string MENU_REMOVE_DEAL = "Remove deal";
	private const string MENU_REROLL_ALL = "Reroll all";
	private const string MENU_REROLL_ARENA = "Reroll this arena";

	//private const string MENU_ADD_CHEST = "Add Chest";
	//private const string MENU_REMOVE_CHEST = "Remove Chest";
	//private const string MENU_ADD_CARD = "Add card";
	//private const string MENU_REMOVE_CARD = "Remove card";

	//private const string CARDS = "Deal Cards";
	//private const string CHESTS = "Deal Chests";

	private const string COMMON = "Gold";
	private const string PREMIUM = "Gems";
	private Random _random = new Random();



	private int COL_NAME;
	private int COL_THING;
	private int COL_GEMPACK;
	private int COL_PRICE;
	private int COL_VALUE;
	private int COL_COMPLEXITY;


	public SimpleDailyDealsList() : base()
	{
		//return;
		COL_NAME = AddColumn("Name", 100, 100, Editor_NameColumn, Validator_NameColumn).ColumnIndex;
		COL_THING = AddColumn("Item",100,100,Editor_Item,Validator_Thing,cellPipFunc:ThingPip).ColumnIndex;
		COL_VALUE = AddColumn("Value", 80, 80, Editor_Value, Validator_DealValue).ColumnIndex;
		COL_PRICE = AddColumn("Price", 80, 80, Editor_Currency, Validator_Price).ColumnIndex;
		COL_GEMPACK = AddColumn("CostsGems", 80, 80, Editor_GemPack, Validator_GemPack).ColumnIndex;
		COL_COMPLEXITY = AddColumn("Complexity").ColumnIndex;
		AddColumn("id");

		Signals.RefreshArenaDailyDeals.Register(RefreshDailyDeals);
	}

	private object[] GetDealData(int arenaIndex, int setIndex, string dealKey, string id)
	{
		ServerData_StorePack pack = FirebaseManager.Inst.GetDailyDeal(arenaIndex, setIndex, dealKey);

		DetermineThing(pack,out string thing, out int amount,out int complexity);
		return new object[]
		{
			dealKey,
			thing,
			amount,
			pack.price,
			pack.isGemPack,
			complexity,
			id
		};
	}

	private object[] GetArenaData(int arenaIndex, string id)
	{
		return new object[]
		{
			"Arena " + arenaIndex,
			"",
			"",
			"",
			"",
			"",
			id
		};
	}

	private object[] GetSetData(int arenaIndex, int setIndex, string id)
	{
		return new object[]
		{
			"Set " + setIndex,
			"",
			"",
			"",
			"",
			"",
			id
		};
	}

	private CellPip ThingPip(string id, object value)
    {
		if(!_deals.Contains(id))
        {
			return null;
        }
        RewardedCard card = GetRewardedCard(id);
		if(card.id != 0)
        {
			return Utils.GetRarityPip(FirebaseManager.Inst.GetRarity(card.id));
        }
		if(Enum.TryParse<ChestType>((string)value,out ChestType ctype))
        {
			return Utils.GetRarityPip(ctype);
        }
		return null;
    }

    private bool GetPackWithThing(string id, string thing, object value, out ServerData_StorePack pack)
    {
		pack = new ServerData_StorePack();

		
		if (thing == PREMIUM)
		{
			pack.PremiumCurrency = (int)value;
			return true;
		}
		if(thing == COMMON)
		{
			pack.CommonCurrency = (int)value;
		}
		if(Enum.TryParse<ChestType>(thing,out ChestType chestType))
        {
			pack.Chests.Add(chestType.ToString());
			return true;
        }
		RewardedCard card = GetRewardedCard(id, thing);
		if(card.id != 0)
        {
			card.amount = (int)value;
			pack.Cards.Add(card);
			return true;
        }
		return false;
	}

	private bool CompletePack(string id, ref ServerData_StorePack pack)
    {
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal))
		{
			Debug.LogError("Could not parse id: " + id);
			return false; 
		}
		if(!_nodes.ContainsKey(id) || !_deals.Contains(id))
		{ return false; }

		TreeListNode node = _nodes[id];
        pack.name = (string)node.GetValue(COL_NAME);
		pack.price = (int)node.GetValue(COL_PRICE);
		pack.isGemPack = (bool)node.GetValue(COL_GEMPACK);

		return true;
    }

    private bool DetermineThing(ServerData_StorePack pack, out string thing, out int amount, out int complexity)
    {
		complexity = 0;
		thing = "";
		amount = 0;
		if(pack.Cards.Count > 0)
        {
			complexity += pack.Cards.Count;
			thing = pack.Cards[0].name;
			amount = pack.Cards[0].amount;
        }
		if(pack.Chests.Count > 0)
        {
			complexity += pack.Chests.Count;
			thing = pack.Chests[0];
			amount = 1;
        }
		if(pack.PremiumCurrency > 0)
        {
			complexity++;
			thing = PREMIUM;
			amount = pack.PremiumCurrency;
        }
		if(pack.CommonCurrency > 0)
        {
			complexity++;
			thing = COMMON;
			amount = pack.CommonCurrency;
        }

		return true;
    }

	private bool Validator_DealValue(string id, object value)
    {
        if (!_nodes.ContainsKey(id)) { return false; }
        if (!_deals.Contains(id)) { return false; }
        int complexity = (int)_nodes[id].GetValue(COL_COMPLEXITY);
		if(complexity > 1)
        {
			Debug.LogError("Cannot edit complex rewards in the simple deals list!");
			return false;
        }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal))
		{ return false; }
		string thing = (string)_nodes[id].GetValue(COL_THING);
		if (!GetPackWithThing(id,thing,value,out ServerData_StorePack pack))
        {
			return false;
        }
		if(!CompletePack(id,ref pack))
        {
			return false;
        }
		return FirebaseManager.Inst.SetWholeDailyDeal(arenaIndex, dealSetIndex,deal,pack);

    }

	private bool Validator_Thing(string id, object thing)
    {
		if (!_nodes.ContainsKey(id)) { return false; }
		if (!_deals.Contains(id)) { return false; }
		int complexity = (int)_nodes[id].GetValue(COL_COMPLEXITY);
		if (complexity > 1)
		{
			Debug.LogError("Cannot edit complex rewards in the simple deals list!");
			return false;
		}
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal))
		{ return false; }
		if (!GetPackWithThing(id, (string)thing, _nodes[id].GetValue(COL_VALUE), out ServerData_StorePack pack))
		{
			return false;
		}
		if(!CompletePack(id,ref pack))
        {
			return false;
        }
		return FirebaseManager.Inst.SetWholeDailyDeal(arenaIndex, dealSetIndex, deal, pack);
	}






	#region columnfunctions

	private RepositoryItem Editor_NameColumn(string id)
	{
		if (_deals.Contains(id))
		{
			return CreateEditorTextEdit();
		}
		return null;
	}

	private bool Validator_NameColumn(string id, object value)
	{
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal))
		{ return false; }
		if (_deals.Contains(id))
		{ return FirebaseManager.Inst.ChangeDailyDealKey(arenaIndex, dealSetIndex, deal, (string)value); }
		return false;
	}

	private int EnumInt(string enumName)
	{
		if (string.IsNullOrEmpty(enumName)) { return 0; }
		if (Enum.IsDefined(typeof(RunnerCardType), enumName))
		{
			return (int)Enum.Parse(typeof(RunnerCardType), enumName);
		}
		if (Enum.IsDefined(typeof(ActionCardType), enumName))
		{
			return (int)Enum.Parse(typeof(ActionCardType), enumName);
		}
		return 0;
	}

	private RepositoryItem Editor_Item(string id)
    {
		if (!_nodes.ContainsKey(id) || !_deals.Contains(id))
        {
			return null;
        }
		return CreateEditorDropDown(() =>
		{
			List<string> things = DataManager.Inst.GetAllCards(true).Select(x => x.ToString()).ToList();
			foreach (string ctype in Utils.GetEnumStringList<ChestType>())
			{
				if (ctype == ChestType.None.ToString()) { continue; }
				things.Add(ctype);
			}
			things.Add(COMMON);
			things.Add(PREMIUM);
			return things;
		});
    }

	private RepositoryItem Editor_Value(string id)
	{
		if (_deals.Contains(id))
		{ return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive); }
		return null;
	}


	private bool Validator_Price(string id, object value)
	{
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey))
		{ return false; }

        if (!GetPackWithThing(id, (string)_nodes[id].GetValue(COL_THING), _nodes[id].GetValue(COL_VALUE),out ServerData_StorePack pack))
        {
			return false;
        }
		if(!CompletePack(id,ref pack))
        {
			return false;
        }
		pack.price = (int)value;
		return FirebaseManager.Inst.SetWholeDailyDeal(arenaIndex, dealSetIndex, dealkey, pack);
	}

	private RepositoryItem Editor_GemPack(string id)
	{
		if (!_deals.Contains(id)) { return null; }
		return CreateEditorCheckEdit();
	}

	private bool Validator_GemPack(string id, object value)
	{
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey))
		{ return false; }

		if (!GetPackWithThing(id, (string)_nodes[id].GetValue(COL_THING), _nodes[id].GetValue(COL_VALUE), out ServerData_StorePack pack))
		{
			return false;
		}
		if (!CompletePack(id, ref pack))
		{
			return false;
		}
		pack.isGemPack = (bool)value;
		return FirebaseManager.Inst.SetWholeDailyDeal(arenaIndex, dealSetIndex, dealkey, pack);
	}

	private RepositoryItem Editor_Currency(string id)
	{
		if (!_deals.Contains(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
	}

	private RewardedCard GetRewardedCard(string id, string cardName = null)
	{
		if (!_deals.Contains(id) || !_nodes.ContainsKey(id))
		{
			return default;
		}

		TreeListNode node = _nodes[id];
		RewardedCard card = new RewardedCard();
		card.amount = (int)node.GetValue(COL_VALUE);
		if (string.IsNullOrEmpty(cardName))
		{
			card.name = Convert.ToString(node.GetValue(COL_THING));
		}
		else
        {
			card.name = cardName;
        }
        card.id = EnumInt(card.name);
		return card;
	}

	#endregion columnFunctions

	protected override string GetTreeName() { return nameof(SimpleDailyDealsList); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		menu.Add(MENU_ADD_SET);
		menu.Add(MENU_REROLL_ALL);
		menu.Add(MENU_REROLL_ARENA);
		if (_dealSets.Contains(id))
		{
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_ADD_DEAL);
		}
		if (_deals.Contains(id))
		{
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_REMOVE_DEAL);
			menu.Add(MENU_ADD_DEAL);
		}
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes)
	{
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal))
		{ return; }
		switch (operation)
		{
			case MENU_ADD_DEAL:
				FirebaseManager.Inst.AddDailyDeal(arenaIndex, dealSetIndex);
				break;
			case MENU_REMOVE_DEAL:
				FirebaseManager.Inst.RemoveDailyDeal(arenaIndex, dealSetIndex, deal);
				break;
			case MENU_ADD_SET:
				FirebaseManager.Inst.AddDailyDealSet(arenaIndex);
				break;
			case MENU_REMOVE_SET:
				FirebaseManager.Inst.RemoveDailyDealSet(arenaIndex, dealSetIndex);
				break;
			case MENU_REROLL_ALL:
				RerollAll();
				break;
			case MENU_REROLL_ARENA:
				RerollArena(arenaIndex);
				break;
		}
	}

	private void RerollAll()
    {
		for (int i = 0; i < FirebaseManager.Inst.GetArenaCount(); i++)
        {
			RerollArena(i);
        }
    }

	private void RerollArena(int arenaIndex)
    {
		Signals.LargeOperationBegin.Send("RerollAll");
		Dictionary<CardRarity, List<int>> arenaCards = FirebaseManager.Inst.GetAllCardsUnlockedByArena(arenaIndex);
		FirebaseManager.Inst.ClearArenaDeals(arenaIndex);
		List<KeyValuePair<float, CardRarity>> rarityChances = new List<KeyValuePair<float, CardRarity>>()
		{
			new KeyValuePair<float, CardRarity>(.5f,CardRarity.Common),
			new KeyValuePair<float, CardRarity>(.9f,CardRarity.Rare),
			new KeyValuePair<float, CardRarity>(.99f,CardRarity.Epic),
			new KeyValuePair<float, CardRarity>(1f,CardRarity.Legendary)
		};
		List<Dictionary<CardRarity, int>> _dealSizes = new List<Dictionary<CardRarity, int>>()
		{
			new Dictionary<CardRarity, int>()
			{
				{CardRarity.Common, 10},
				{CardRarity.Rare, 2},
				{CardRarity.Epic,1 },
				{CardRarity.Legendary,1 }
			},
			new Dictionary<CardRarity, int>()
			{
				{CardRarity.Common, 15},
				{CardRarity.Rare, 3},
				{CardRarity.Epic,1 },
				{CardRarity.Legendary,1 }
			},
			new Dictionary<CardRarity, int>()
			{
				{CardRarity.Common, 20},
				{CardRarity.Rare, 4},
				{CardRarity.Epic, 2 },
				{CardRarity.Legendary,1 }
			},
			new Dictionary<CardRarity, int>()
			{
				{CardRarity.Common, 30},
				{CardRarity.Rare, 8},
				{CardRarity.Epic, 3 },
				{CardRarity.Legendary,1 }
			},
		};
		Dictionary<CardRarity, int> _goldPerCard = new Dictionary<CardRarity, int>()
		{
			{CardRarity.Common,10 },
			{CardRarity.Rare,50 },
			{CardRarity.Epic,250 },
			{CardRarity.Legendary,20000 }
		};
		float gemConversionRate = 0.1f;
		float arenaIncrease = 1 + (arenaIndex * 0.5f);

		for (int s = 0; s < 10; s++)
		{
			FirebaseManager.Inst.AddDailyDealSet(arenaIndex);
			bool bump = false;
			HashSet<int> setCards = new HashSet<int>();
			for (int dd = 0; dd < 3; dd++)
			{
				if (!bump && _random.NextDouble() > 0.8)
				{
					bump = true;
				}
				int rewardSize = dd + (bump ? 1 : 0);
				//float rarity = (float)_random.NextDouble() - (rewardSize < 1? 0.2f : rewardSize < 2? 0.1f:0);
				float rarity = (float)_random.NextDouble() - (rewardSize < 1 ? 0.35f : 0);
				CardRarity packRarity = CardRarity.None;
				foreach (KeyValuePair<float, CardRarity> chance in rarityChances)
				{
					if (chance.Key > rarity) // the first rarity to be bigger is the winner.
					{
						packRarity = chance.Value;
						break;
					}
				}
				if (!arenaCards.ContainsKey(packRarity)) { packRarity = CardRarity.Common; }
				if (!arenaCards.ContainsKey(packRarity)) { packRarity = CardRarity.Rare; }

				bool isGemPack = false;
				int amount = (int)(_dealSizes[rewardSize][packRarity] * arenaIncrease);
				List<int> validCards = new List<int>(arenaCards[packRarity]);
				foreach(int alreadyChosen in setCards)
                {
					validCards.Remove(alreadyChosen);
                }
				int whichCard = _random.Next(0, validCards.Count);
				int cardId = validCards[whichCard];
				setCards.Add(cardId);
				string cardName = "";
				if (cardId != 0)
				{
					if (Enum.IsDefined(typeof(ActionCardType), cardId))
					{
						cardName = ((ActionCardType)cardId).ToString();
					}
					if (Enum.IsDefined(typeof(RunnerCardType), cardId))
					{
						cardName = ((RunnerCardType)cardId).ToString();
					}
				}
                else
                {
					Debug.LogError("Tried to award None!");
                }
				int cost = _goldPerCard[packRarity] * amount;
				if(rewardSize == 3)
                {
					isGemPack = true;
					cost = (int)(cost * gemConversionRate);
                }
				FirebaseManager.Inst.AddDailyDeal(arenaIndex, s, out string dealId);
				ServerData_StorePack pack = new ServerData_StorePack();
				RewardedCard card = new RewardedCard();
				card.id = cardId;
				card.name = cardName;
				card.amount = amount;
				pack.Cards.Add(card);
				pack.price = cost;
				pack.isGemPack = isGemPack;
				FirebaseManager.Inst.SetWholeDailyDeal(arenaIndex, s, dealId,pack);

			}
        }
		Signals.LargeOperationEnd.Send("RerollAll");
    }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate()
	{
		List<string> ids = new List<string>();
		for (int i = 0; i < FirebaseManager.Inst.GetArenaCount(); i++)
		{
			ids.Add(GetId(i));
		}
		return ids;
	}

	private void RefreshDailyDeals(int i)
	{
		PopulateArena(GetId(i));
		MakeDirty(_nodes[GetId(i)]);
	}

    protected override bool SpeedRender(int column, TreeListNode node)
    {
		return column != COL_GEMPACK;
    }

    protected override bool IsHeader(string id)
    {
		return _arenas.Contains(id);
    }

    protected override bool IsSubHeader(string id)
    {
		return _dealSets.Contains(id);
    }

    private string GetId(int arenaIndex, int dealSetIndex = -1, string deal = "")
	{
		string id = arenaIndex.ToString();
		if (dealSetIndex >= 0)
		{
			id += $".{dealSetIndex}";
		}
		if (!string.IsNullOrEmpty(deal))
		{
			id += $".{deal}";
		}
		return id;
	}

	private bool ParseId(string id, out int arenaIndex, out int dealSetIndex, out string deal)
	{
		dealSetIndex = -1;
		deal = "";
		string[] segs = id.Split('.');
		if (!int.TryParse(segs[0], out arenaIndex))
		{
			return false;
		}
		if (segs.Length > 1)
		{
			if (!int.TryParse(segs[1], out dealSetIndex))
			{ return false; }
		}
		if (segs.Length > 2)
		{
			deal = segs[2];
		}
		return true;
	}

	private void PopulateArena(string arenaId)
	{
		if (!ParseId(arenaId, out int arenaIndex, out _, out _))
		{ return; }
		ClearArena(arenaId);
		TreeListNode arena = AppendNode(GetArenaData(arenaIndex, arenaId), GetRoot(), arenaId);
		_nodes.Add(arenaId, arena);
		_arenas.Add(arenaId);
		for (int s = 0; s < FirebaseManager.Inst.GetDailyDealSetCount(arenaIndex); s++)
		{
			string setId = GetId(arenaIndex, s);
			TreeListNode set = AppendNode(GetSetData(arenaIndex, s, setId), arena, setId);
			_nodes.Add(setId, set);
			_dealSets.Add(setId);
			foreach (string ddkey in FirebaseManager.Inst.GetDailyDealKeys(arenaIndex, s))
			{
				string dealId = GetId(arenaIndex, s, ddkey);
				TreeListNode deal = AppendNode(GetDealData(arenaIndex, s, ddkey, dealId), set, dealId);
				_nodes.Add(dealId, deal);
				_deals.Add(dealId);
			}
		}
		arena.ExpandAll();
	}

	protected override List<string> GetErrors(string id, bool exitEarly)
    {
        List<string> errors = new List<string>();
        if (!_nodes.ContainsKey(id))
        { return errors; }
        if ((int)_nodes[id].GetValue(COL_COMPLEXITY) > 1)
        {
			errors.Add("Daily deals aught not be complex!");
			if(exitEarly) { return errors; }
        }
        return errors;
    }

	private void ClearArena(string arenaId)
	{
		if (!_nodes.ContainsKey(arenaId))
		{
			return;
		}
		HashSet<TreeListNode> children = new HashSet<TreeListNode>(_nodes[arenaId].Nodes);
		foreach (TreeListNode child in children)
		{
			ClearArena(child.Tag.ToString());
		}
		_arenas.Remove(arenaId);
		_dealSets.Remove(arenaId);
		_nodes[arenaId].Remove();
		_deals.Remove(arenaId);
		_nodes.Remove(arenaId);
	}
	protected override void InitialPopulateAction(string id)
	{
		PopulateArena(id);
	}

	private enum RewardSubType
	{
		None,
		Card,
		Chest
	}
}

