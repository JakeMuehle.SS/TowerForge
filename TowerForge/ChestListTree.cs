﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class ChestListTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	public ChestListTree() : base()
	{
		AddColumn("i",20,40);
		AddColumn("ChestType", 100, 100,Editor_FirstColumn,Validator_FirstColumn, cellPipFunc: RarityPip);
	}

	private CellPip RarityPip(string id, object value)
	{
		if (!_nodes.ContainsKey(id) || value == null)
		{
			return new CellPip(false);
		}
		ChestType rarity = GetEnum<ChestType>(value);
		return Utils.GetRarityPip(rarity);
	}


	private RepositoryItem Editor_FirstColumn(string id)
    {
		return CreateEditorDropDown<ChestType>();
    }

	private bool Validator_FirstColumn(string id, object value)
    {
		return true;
    }
	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id
		};
		return data;
	}
	protected override string GetTreeName() { return nameof(ChestListTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{ 
		return new List<string>() { "oneBogusCall" };
	}
	protected override void InitialPopulateAction(string id)
	{
		// this intentionally left blank.
		
		int index = 0;
		foreach(string chest in FirebaseManager.Inst.GetChestList())
        {
			string nid = System.Guid.NewGuid().ToString();
			TreeListNode node = AppendNode(new object[] { index++, chest }, null, nid);
			_nodes.Add(nid, node);
        }
	}

}