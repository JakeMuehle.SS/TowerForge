﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Linq;
public class StatusTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	public StatusTree() : base()
	{
		AddColumn("Status", 100, 100);
		AddColumn("ReconcileType", 100, 100,Editor_Reconcile,Validator_Reconcile,cellPipFunc:GetPip);
	}

	private CellPip GetPip(string id, object value)
    {
		if (GetEnum<ReconcileType>(value) == ReconcileType.None)
        {
			return new CellPip(3, System.Drawing.Color.DarkRed);
		}
		return new CellPip(false);
    }

	private RepositoryItem Editor_Reconcile(string id)
    {
		return CreateEditorDropDown<ReconcileType>();
    }

	private bool Validator_Reconcile(string id, object value)
    {
		return DataManager.Inst.SetStatusReconcileType(GetEnum<UnitStatus>(id), GetEnum<ReconcileType>(value));
    }

	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id,
			DataManager.Inst.GetStatusReconcileType(GetEnum<UnitStatus>(id))
		};
		return data;
	}
	protected override string GetTreeName() { return nameof(StatusTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return (Utils.GetEnumList<UnitStatus>().Select(x => x.ToString())).ToList();
	}
	protected override void InitialPopulateAction(string id)
	{
		if(id == UnitStatus.None.ToString()) { return; }
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}