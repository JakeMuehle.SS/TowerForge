﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class ArenaCardsTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();

	private Dictionary<string,int> _arenaNodes = new Dictionary<string,int>();
	private string NO_ARENA = "No Arena";
	private const string MENU_NEW = "New Arena";

	private const string TYPE_ACTION = "ActionCard";
	private const string TYPE_RUNNER = "RunnerCard";
	public ArenaCardsTree() : base()
	{
		AddColumn("Name", 100, 100);
		AddColumn("Type", 100, 100, cellPipFunc: TypePip);
		AddColumn("Rarity", 100, 100,Editor_Rarity,Validator_Rarity,cellPipFunc:RarityPip);
		AddColumn("ELO",50,50,Editor_ELO,Validator_ELO);

		Signals.LibraryUpdated.Register(RefreshLibrary);
	}

	private CellPip RarityPip(string id, object value)
    {
		if (_arenaNodes.ContainsKey(id) || value == null)
        {
			return new CellPip(false);
        }
        CardRarity rarity = GetEnum<CardRarity>(value);
		return Utils.GetRarityPip(rarity);
    }

	private CellPip TypePip(string id, object value)
    {
		if (_arenaNodes.ContainsKey(id) || value == null)
		{
			return new CellPip(false);
		}
		switch((string) value)
        {
			case TYPE_ACTION:
				return new CellPip(0, System.Drawing.Color.Purple);
			case TYPE_RUNNER:
				return new CellPip(0, System.Drawing.Color.SeaGreen);
			default:
				return new CellPip(false);
        }
	}

	private RepositoryItem Editor_ELO(string id)
    {
		if(_arenaNodes.ContainsKey(id))
        {
			return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
        }
		return null;
    }

	private bool Validator_ELO(string id, object value)
    {
		if(!_arenaNodes.ContainsKey(id))
		{ return false;}
		return FirebaseManager.Inst.SetELO(_arenaNodes[id], (int)value);
    }

	private RepositoryItem Editor_Rarity(string id)
    {
		if(!_arenaNodes.ContainsKey(id) && _nodes.ContainsKey(id))
        {
			return CreateEditorDropDown<CardRarity>();
        }
		return null;
    }

	private bool Validator_Rarity(string id, object value)
    {
		if(!DataManager.Inst.TryGetEnumIntValue(id,out int intId))
        {
			return false;
        }
		return FirebaseManager.Inst.SetRarity(intId, GetEnum<CardRarity>(value));			
    }
	private object[] GetArenaData(string arenaId)
	{
		object[] data = new object[]
		{
			arenaId,
			"",
			"",
			FirebaseManager.Inst.GetELO(_arenaNodes[arenaId])
		};
		return data;
	}

	private object[] GetCardData(string id)
    {
        string type = DataManager.Inst.TryGetEnumFromId<ActionCardType>(id, out ActionCardType _) ? TYPE_ACTION : TYPE_RUNNER;
		string rarity = "???";
        if (DataManager.Inst.TryGetEnumIntValue(id,out int intId))
		{
			rarity = FirebaseManager.Inst.GetRarity(intId).ToString();
		}
		object[] data = new object[]
		{
			DataManager.Inst.DisplayName(id),
			type,
			rarity
		};
		return data;
    }
    protected override bool IsHeader(string id)
    {
		return _arenaNodes.ContainsKey(id);
    }
    protected override string GetTreeName() { return nameof(ArenaCardsTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		//menu.Add(MENU_SAVE);
		menu.Add(MENU_NEW);
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{ 
		switch(operation)
        {
			//case MENU_SAVE:
			//	SetArenaData();
			//	break;
			case MENU_NEW:
				FirebaseManager.Inst.AddArena(out int arena);
				AddArena(arena);
				break;
        }
	}
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) 
	{
		if (_arenaNodes.ContainsKey(droppingID) || !_nodes.ContainsKey(droppingID))
        {
			return;
        }
		if(targetNode == null) 
		{ 
			return; 
		}
		TreeListNode parentNode = targetNode;
		if (insertType == InsertType.Before)
		{
			parentNode = targetNode.PrevVisibleNode;
		}

		while ( parentNode != null && !_arenaNodes.ContainsKey(parentNode.Tag.ToString()))
        {
			parentNode = parentNode.ParentNode;
        }
        
		if(parentNode != null && _arenaNodes.ContainsKey(parentNode.Tag.ToString()))
        {
			//MoveNode(_nodes[droppingID], parentNode);
			DataManager.Inst.TryGetEnumIntValue(droppingID, out int intID);
			FirebaseManager.Inst.SetCardUnlockedArena(intID, _arenaNodes[parentNode.Tag.ToString()]);
        }
	}
	protected override bool AllowCopy(string targetId, string sourceId)
    {
		return false;
    }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate()
    {
        List<string> ids = DataManager.Inst.GetIds<RunnerCardType>();
		ids.AddRange(DataManager.Inst.GetIds<ActionCardType>());
		return ids;
    }
    protected override void InitialPopulateAction(string id)
	{
		AddCard(id);
	}

	private void AddCard(string cardId)
    {
		
        if(!DataManager.Inst.TryGetEnumIntValue(cardId, out int intId))
		{ return; }

		int arenaIndex = FirebaseManager.Inst.GetArenaForCardUnlock(intId);
		AddArena(arenaIndex);

		TreeListNode cardNode = AppendNode(GetCardData(cardId), _nodes[GetArenaId(arenaIndex)], cardId);
		_nodes.Add(cardId, cardNode);

    }

	protected override void PostPopulate()
	{
		for(int i = -1; i < FirebaseManager.Inst.GetArenaCount(); i++) // -1 for the "NO ARENA" option.
        {
			AddArena(i);
        }
	}

	private void AddArena(int id)
    {
		string arenaId = GetArenaId(id);
		if (!_arenaNodes.ContainsKey(arenaId))
		{
			_arenaNodes.Add(arenaId, id);
			TreeListNode arenaNode = AppendNode(GetArenaData(arenaId), GetRoot(), arenaId);
			_nodes.Add(arenaId, arenaNode);
		}
	}

	private void RefreshLibrary()
    {
		foreach(string nodeId in _nodes.Keys)
        {
			_nodes[nodeId].Remove();
        }
		_nodes.Clear();
		_arenaNodes.Clear();

		List<string> ids = DataManager.Inst.GetIds<RunnerCardType>();
		ids.AddRange(DataManager.Inst.GetIds<ActionCardType>());

		foreach(string id in ids)
        {
			AddCard(id);
        }
		PostPopulate();
		ExpandAll();
	}

	//private bool TryGetCardId(string cardId, out int intId, out bool isAction)
	//{
	//	intId = -1;
	//	isAction = false;
	//	if (DataManager.Inst.TryGetEnumFromId<RunnerCardType>(cardId, out RunnerCardType runnerType))
	//	{
	//		intId = (int)runnerType;
	//		return true;
	//	}
	//	else if (DataManager.Inst.TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType actionType))
	//    {
	//        intId = (int)actionType;
	//		isAction = true;
	//        return true;
	//	}
	//	Debug.LogError($"CardId {cardId} was neither a RunnerCardType or an ActionCardType!");
	//	return false;
	//}

	//private void SetArenaData()
    //{
	//	Dictionary<int, List<int>> arenaUnlocks = new Dictionary<int, List<int>>();
	//	foreach(string arenaId in _arenaNodes.Keys)
    //    {
    //        arenaUnlocks.Add(_arenaNodes[arenaId], new List<int>());
    //        foreach(TreeListNode node in _nodes[arenaId].Nodes)
    //        {
	//			if(DataManager.Inst.TryGetEnumIntValue(node.Tag.ToString(),out int intId))
    //            {
    //                arenaUnlocks[_arenaNodes[arenaId]].Add(intId);
    //            }
    //        }
    //    }
	//	FirebaseManager.Inst.SetArenaUnlockedCards(arenaUnlocks);
    //}

    

    private string GetArenaId(int index)
    {
		if (index < 0)
		{ return NO_ARENA; }
		return "Arena " + index.ToString();
    }

}