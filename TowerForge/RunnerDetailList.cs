﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class RunnerDetailList : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private string _currentRunner;

	private const string ABILITIES = "abilities";
	private const string ABILITY_EFFECTS = "abilityEffects";
	private const string ABILITY_CONDITIONS = "abilityConditions";
	private const string DEFAULTS = "defaults";
	//private HashSet<string> _effects = new HashSet<string>();
	private HashSet<string> _abilities = new HashSet<string>();
	private HashSet<string> _conditions = new HashSet<string>();
	private HashSet<string> _effects = new HashSet<string>();
	private HashSet<string> _rankables = new HashSet<string>();

	private const string MENU_REMOVE_CONDITION = "Remove Condition";
	private const string MENU_ADD_CONDITION = "Add Condition";
	private const string MENU_REMOVE_ABILITY = "Remove Ability";
	private const string MENU_ADD_ABILITY = "Add Ability";
    private const string MENU_ADD_EFFECT = "Add ability effect";
	private const string MENU_REMOVE_EFFECT = "Remove ability effect";

    private int COL_BASE;
	private int COL_FINAL;
	private int COL_SCALAR;
	private int COL_RANDMAX;
	private int COL_CURVE;
	

	private string _selectedId;
	public RunnerDetailList() : base()
    {
		//return;
        AddColumn("Parameter", 100, 100,Editor_FirstColumn,Validator_FirstColumn);
		COL_BASE = AddColumn("BaseValue",100,100,Editor_BaseValue,Validator_BaseValue).ColumnIndex;
		COL_FINAL = AddColumn("FinalValue",75,75,Editor_FinalValue,Validator_FinalValue).ColumnIndex;
		COL_SCALAR = AddColumn("Scalar", 75, 75, Editor_RankableFloat,Validator_ScalarValue).ColumnIndex;
		COL_CURVE = AddColumn("CurveType", 75, 75, Editor_RankableCurve,Validator_CurveValue).ColumnIndex;
		COL_RANDMAX = AddColumn("RandMax", 75, 75, Editor_RankableFloat, Validator_RandCapValue).ColumnIndex;
		AddColumn("ValueType",100,100,Editor_ValueType,Validator_ValueType);
		AddColumn("Comparator",100,100,Editor_Comparator,Validator_Comparator);

        Signals.RunnerUnitSelected.Register(SetRunner);
		Signals.RunnerUnitUpdated.Register(SetRunner);

		OnNodeSelect += (id) =>
		{
			if(_effects.Contains(id))
            {
				if(ParseId(id,out int abilityIndex, out int effectIndex))
                {
					if (IsSpawnPower(id))
					{

						Signals.ActionCardSelected.Send(DataManager.Inst.GetStringId(DataManager.Inst.GetRunnerAbilityPower(_currentRunner, abilityIndex, effectIndex)), this);
					}
					else
					{
						Signals.RunnerAbilityEffectSelected.Send(_currentRunner, abilityIndex,effectIndex);
					}
                }
            }
			else
            {
				Signals.RunnerAbilityEffectSelected.Send("", -1,-1);
            }
		};
    }

    #region getdata
    private object[] GetRankableData(string runnerId, string rankableFieldName)
	{
		return GetRankableData(rankableFieldName, DataManager.Inst.GetRunnerRankableFloat(runnerId, rankableFieldName));
	}

	private object[] GetRankableData(string rankableFieldName,RankableFloat rf)
	{
		object[] data = new object[]
		{
			rankableFieldName,
			rf.BaseValue,
			rf.FinalValue,
			rf.Scalar,
			rf.Curve,
			rf.RandomCap
		};
		return data;
	}

	private object[] GetAbilityConditionData(string runner, int abilityIndex, int conditionIndex)
	{
		RankableFloat rf = DataManager.Inst.GetRunnerAbilityConditionCompareTo(runner, abilityIndex, conditionIndex);
		return new object[]
		{
			DataManager.Inst.GetRunnerAbilityConditionTrigger(runner, abilityIndex, conditionIndex),
			rf.BaseValue,
			rf.FinalValue,
			rf.Scalar,
			rf.Curve,
			rf.RandomCap,
			DataManager.Inst.GetRunnerAbilityConditionValueType(runner, abilityIndex, conditionIndex),
			DataManager.Inst.GetRunnerAbilityConditionComparator(runner, abilityIndex, conditionIndex)
		};
	}

	public object[] GetEffectData(string id, int abilityIndex, int effectIndex)
	{
		RunnerAbilityType type = DataManager.Inst.GetRunnerAbilityEffectType(id, abilityIndex, effectIndex);
		switch (type)
		{
			case RunnerAbilityType.SpawnPower:
				ActionCardType powerType = DataManager.Inst.GetRunnerAbilityPower(id, abilityIndex, effectIndex);
				string typeString = powerType == ActionCardType.None ? "" : powerType.ToString();
				return new object[] { type, typeString };
			default:
				return new object[] { type };

		}
	}
    #endregion getdata

    #region editors
    private RepositoryItem Editor_FirstColumn(string id)
    {
		if(_conditions.Contains(id))
        {
			return CreateEditorDropDown<RunnerAbilityTrigger>();
        }
		if(_effects.Contains(id))
        {
			return CreateEditorDropDown<RunnerAbilityType>();
        }
		return null;
    }

	private RepositoryItem Editor_BaseValue(string id)
    {
		if(_effects.Contains(id))
        {
			if(IsSpawnPower(id))
            {
				return CreateEditorDropDown<ActionCardType>();
            }
        }
		if(_rankables.Contains(id))
        {
			return Quick_CreateFloatEditor(id);
        }
		if(id == nameof(RunnerDefaults.DefaultColor))
        {
			return CreateEditorColor();
        }
		if(id == nameof(RunnerDefaults.DefaultScale))
        {
			return CreateEditorTextEdit(DataEditorType.FloatZeroOrPositive);
        }
		if(id == nameof(RunnerDefaults.DefaultString))
        {
			return CreateEditorTextEdit();
        }
		return null;
    }

	private RepositoryItem Editor_FinalValue(string id)
    {
		if (IsSpawnPower(id))
		{
			return CreateEditorDropDown<ActionCardType>();
		}
		if (_rankables.Contains(id))
		{
			return Quick_CreateFloatEditor(id);
		}
		return null;
	}

	private RepositoryItem Editor_RankableFloat(string id)
    {
		if(_rankables.Contains(id))
        {
			return Quick_CreateFloatEditor(id);
        }
		return null;
    }
	private RepositoryItem Editor_RankableCurve(string id)
	{
		if (_rankables.Contains(id))
		{
			return CreateEditorDropDown<CurveType>();
		}
		return null;
	}

	private RepositoryItem Editor_ValueType(string id)
    {
		if(_conditions.Contains(id))
        {
			return CreateEditorDropDown<RunnerAbilityConditionValueType>();
        }
		return null;
    }

	private RepositoryItem Editor_Comparator(string id)
    {
		if(_conditions.Contains(id))
        {
			return CreateEditorDropDown<ConditionComparator>();
        }
		return null;
    }
	#endregion editors

	#region validators
	private bool Validator_FirstColumn(string id, object value)
    {
		_selectedId = id;
		if (!TryParseId(id, out int abilityIndex, out int secondIndex, out NodeType nodeType))
		{ return false; }
		if (_conditions.Contains(id) && nodeType == NodeType.ConditionNode)
        {
            RunnerAbilityTrigger trigger = GetEnum<RunnerAbilityTrigger>(value);
			return DataManager.Inst.SetRunnerAbilityConditionTrigger(_currentRunner, abilityIndex, secondIndex, trigger);
        }
		if(_effects.Contains(id) && nodeType == NodeType.EffectNode)
        {
			RunnerAbilityType type = GetEnum<RunnerAbilityType>(value);
			return DataManager.Inst.SetRunnerAbilityEffect(_currentRunner, abilityIndex, secondIndex, type);
		}

        return false;
    }

	private bool IsSpawnPower(string id)
    {
		if (!_effects.Contains(id) || !_nodes.ContainsKey(id))
		{ return false; }
		object value = _nodes[id].GetValue(1);
		if(value == null) { return false; }
		RunnerAbilityType type = GetEnum<RunnerAbilityType>(value);
		return type == RunnerAbilityType.SpawnPower;
    }

	private bool Validator_BaseValue(string id, object value)
    {
		_selectedId = id;
		if(_rankables.Contains(id))
        {
			RankableFloat rf = GetRankableFloat(id, out string name);
			rf.BaseValue = (float)value;
			return TrySetRankableFloat(id, rf, name);
        }
		RunnerDefaults defaults = GetDefaults();
		if (id == nameof(RunnerDefaults.DefaultColor))
		{
			defaults.DefaultColor = System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color) value);
			return DataManager.Inst.SetRunnerDefaults(_currentRunner, defaults);
		}
		if (id == nameof(RunnerDefaults.DefaultScale))
		{
			defaults.DefaultScale = (float)value;
			return DataManager.Inst.SetRunnerDefaults(_currentRunner, defaults);
		}
		if (id == nameof(RunnerDefaults.DefaultString))
		{
			defaults.DefaultString = (string)value;
			return DataManager.Inst.SetRunnerDefaults(_currentRunner, defaults);
		}
		return false;
    }

	private bool Validator_FinalValue(string id, object value)
    {
		_selectedId = id;
		if (_rankables.Contains(id))
		{
			RankableFloat rf = GetRankableFloat(id, out string name);
			rf.FinalValue = (float)value;
			return TrySetRankableFloat(id, rf, name);
		}
		if(IsSpawnPower(id))
        {
			//int abilityIndex = GetAbilityIndex(id);
			if(!TryParseId(id,out int abilityIndex, out int effectIndex, out NodeType nodeType))
			{ return false; }
			if (nodeType == NodeType.EffectNode)
			{
				ActionCardType type = GetEnum<ActionCardType>(value);
				return DataManager.Inst.SetRunnerAbilityPower(_currentRunner, abilityIndex, effectIndex, type);
			}
        }
        return false;
	}

	private bool Validator_ScalarValue(string id, object value)
	{
		_selectedId = id;
		if (_rankables.Contains(id))
		{
			RankableFloat rf = GetRankableFloat(id, out string name);
			rf.Scalar = (float)value;
			return TrySetRankableFloat(id, rf, name);
		}
		return false;
	}

	private bool Validator_RandCapValue(string id, object value)
	{
		_selectedId = id;
		if (_rankables.Contains(id))
		{
			RankableFloat rf = GetRankableFloat(id, out string name);
			rf.RandomCap = (float)value;
			return TrySetRankableFloat(id, rf, name);
		}
		return false;
	}

	private bool Validator_CurveValue(string id, object value)
	{
		_selectedId = id;
		if (_rankables.Contains(id))
		{
			RankableFloat rf = GetRankableFloat(id, out string name);
			rf.Curve = GetEnum<CurveType>(value);
			return TrySetRankableFloat(id, rf, name);
		}
		return false;
	}

	private RankableFloat GetRankableFloat(string id,out string name)
    {
		name = null;
        if (!_nodes.ContainsKey(id)) { return null; }
		TreeListNode node = _nodes[id];
		if (node == null) { return null; }
		RankableFloat rf = new RankableFloat();

		rf.BaseValue = (float)node.GetValue(COL_BASE);
		rf.Scalar = (float)node.GetValue(COL_SCALAR);
		rf.FinalValue = (float)node.GetValue(COL_FINAL);
		rf.RandomCap = (float)node.GetValue(COL_RANDMAX);
		rf.Curve = GetEnum<CurveType>(node.GetValue(COL_CURVE));
		name = node.GetValue(0).ToString();

		return rf;
	}

	private bool Validator_Comparator(string id, object value)
    {
		_selectedId = id;
		if (_conditions.Contains(id))
        {
			ConditionComparator comparator = GetEnum<ConditionComparator>(value);
			if (!TryParseId(id, out int abilityIndex, out int conditionIndex, out NodeType nodeType))
			{ return false; }
			if (nodeType == NodeType.ConditionNode)
			{
				return DataManager.Inst.SetRunnerAbilityConditionComparator(_currentRunner, abilityIndex, conditionIndex, comparator);
			}
        }
        return false;
    }

	private bool Validator_ValueType(string id, object value)
    {
		_selectedId = id;
		if (_conditions.Contains(id))
		{
			RunnerAbilityConditionValueType valueType = GetEnum<RunnerAbilityConditionValueType>(value);
			if (!TryParseId(id, out int abilityIndex, out int conditionIndex, out NodeType nodeType))
			{ return false; }
			if (nodeType == NodeType.ConditionNode)
			{
				return DataManager.Inst.SetRunnerAbilityConditionValueType(_currentRunner, abilityIndex, conditionIndex, valueType);
			}
        }
        return false;
	}

	private bool TryParseId(string id, out int abilityIndex, out int secondIndex, out NodeType nodeType)
    {
		secondIndex = -1;
		nodeType = NodeType.None;
		string[] segs = id.Split('.');
        if (!int.TryParse(segs[0],out abilityIndex))
        {
			abilityIndex = -1;
			return false;
        }
		if(segs.Length < 2)
        {
			return true;
        }
        switch(segs[1])
        {
			case ABILITY_CONDITIONS:
				nodeType = NodeType.ConditionNode;
				break;
			case ABILITY_EFFECTS:
				nodeType = NodeType.EffectNode;
				break;
			default:
				return true;
        }
		if(segs.Length < 3)
        {
			return true;
        }
		if(!int.TryParse(segs[2],out secondIndex))
        {
			return false;
        }
		return true;
    }

	private bool ParseId(string id, out int abilityIndex, out int effectIndex)
	{
		effectIndex = -1;
		string[] segs = id.Split('.');
		if (!int.TryParse(segs[0], out abilityIndex))
		{ return false; }
		if (segs.Length < 2 || !int.TryParse(segs[2], out effectIndex))
		{ return false; }
		return true;
	}
	#endregion validators

	#region helpers

	private bool IsAbility(string id)
	{
		if (!_nodes.ContainsKey(id))
		{
			return false;
		}
		if (_effects.Contains(id))
		{
			return true;
		}
		if (_conditions.Contains(id))
		{
			return true;
		}
		TreeListNode parent = _nodes[id].ParentNode;
		while (parent != null)
		{
			if (_abilities.Contains(parent.Tag.ToString()))
			{
				return true;
			}
			parent = parent.ParentNode;
		}
		return false;
	}
	private bool TrySetRankableFloat(string id, RankableFloat rf, string propertyName)
	{
		TryParseId(id, out int abilityIndex, out int conditionIndex, out NodeType nodeType);

		if (nodeType == NodeType.ConditionNode)
		{
			return DataManager.Inst.SetRunnerAbilityConditionCompareTo(_currentRunner, abilityIndex, conditionIndex, rf);
		}

		if (abilityIndex == -1)
		{
			return DataManager.Inst.SetRunnerRankableFloat(_currentRunner, propertyName, rf);
		}
		return DataManager.Inst.SetRunnerAbilityCooldown(_currentRunner, abilityIndex, rf);
	}

	private string GetId(int abilityIndex, int secondIndex, bool isEffectNotCondition)
	{
		string id = $"{abilityIndex}.{(isEffectNotCondition ? ABILITY_EFFECTS : ABILITY_CONDITIONS)}";
		if (secondIndex >= 0)
		{
			id = id + "." + secondIndex;
		}
		return id;
	}

	private void AddDefaultValue(string property, object value)
	{
		TreeListNode pnode = AppendNode(new object[] { property, value }, _nodes[DEFAULTS], property);
		_nodes.Add(property, pnode);
	}

	private RunnerDefaults GetDefaults()
	{
		RunnerDefaults defaults = new RunnerDefaults();
		defaults.DefaultScale = (float)(_nodes[nameof(RunnerDefaults.DefaultScale)].GetValue(1));
		object colorValue = _nodes[nameof(RunnerDefaults.DefaultColor)].GetValue(1);
		defaults.DefaultColor = (string)colorValue;
		//defaults.DefaultColor = System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)(colorValue));
		defaults.DefaultString = (string)(_nodes[nameof(RunnerDefaults.DefaultString)].GetValue(1));

		return defaults;
	}

	private void AddRankableField(string fieldName)
	{
		TreeListNode node = AppendNode(GetRankableData(_currentRunner, fieldName), GetRoot(), fieldName);
		_nodes.Add(fieldName, node);
		_rankables.Add(fieldName);
		MakeDirty(node, true);
	}

	private int GetAbilityIndex(string id)
	{
		if (!_nodes.ContainsKey(id))
		{
			Debug.LogError("You shouldn't be calling GetAbilityID if you don't already know you belong to an ability! Call IsAbility() first!");
			return -1;
		}

		TreeListNode testNode = _nodes[id];
		do
		{
			if (_effects.Contains(testNode.Tag.ToString()))
			{
				if (int.TryParse(testNode.Tag.ToString(), out int abilityIndex))
				{
					return abilityIndex;
				}
			}
			testNode = testNode.ParentNode;
		}
		while (testNode != null);

		Debug.LogError("You shouldn't be calling GetAbilityID if you don't already know you belong to an ability! Call IsAbility() first!");
		return -1;
	}

    #endregion helpers

    #region populate
    public void SetRunner(string id)
	{
        _currentRunner = id;
		foreach (string nodeID in _nodes.Keys)
        {
            Nodes.Remove(_nodes[nodeID]);
        }
		_nodes.Clear();
		//_effects.Clear();
		_conditions.Clear();
		_effects.Clear();
		_rankables.Clear();
		_abilities.Clear();
		Flush();
		GetRoot();
		RootName = DataManager.Inst.DisplayName(id);
		AddRankableField("Health");
		AddRankableField("HitStrength");
		AddRankableField("Speed");
		int abilitiesCount = DataManager.Inst.GetRunnerAbilityCount(id);
        TreeListNode abilitiesNode = AppendNode(new object[] { ABILITIES, abilitiesCount }, GetRoot(), ABILITIES);
		_nodes.Add(ABILITIES, abilitiesNode);


		for (int a = 0; a < abilitiesCount; a++)
        {
			string abilityId = a.ToString();
			TreeListNode abilityNode = AppendNode(new object[] { "Ability #" + a }, abilitiesNode, abilityId);
			_nodes.Add(abilityId, abilityNode);
			_abilities.Add(abilityId);
			string cooldownId = a + ".cooldown";
			TreeListNode cooldownNode = AppendNode(GetRankableData("Cooldown", DataManager.Inst.GetRunnerAbilityCooldown(id, a)), abilityNode, cooldownId);
			_nodes.Add(cooldownId, cooldownNode);
			_rankables.Add(cooldownId);

			int effectsCount = DataManager.Inst.GetBaseRunnerAbilityEffectCount(id, a);
			string effectsId = GetId(a, -1, true);
			TreeListNode effectsNode = AppendNode(new object[] { "Effects", effectsCount }, abilityNode, effectsId);
			_nodes.Add(effectsId, effectsNode);

			for (int e = 0; e < effectsCount; e++)
			{
				string effectId = GetId(a, e, true);
				_effects.Add(effectId);
				TreeListNode effectNode = AppendNode(GetEffectData(id,a,e), effectsNode, effectId);
				_nodes.Add(effectId, effectNode);
				
			}



			//string effectId = abilityId + ".effect";
			//TreeListNode effectNode = AppendNode(new object[] { "Effect" }, abilityNode, effectId);
			//_nodes.Add(effectId, effectNode);
			//_effects.Add(effectId);

			int conditionsCount = DataManager.Inst.GetRunnerAbilityConditionCount(id, a);
			string conditionsId = GetId(a, -1, false);
			TreeListNode conditionsNode = AppendNode(new object[] { "Conditions" , conditionsCount}, abilityNode, conditionsId);
			_nodes.Add(conditionsId, conditionsNode);

			for(int c = 0; c < conditionsCount; c++)
            {
				string conditionId = GetId(a, c, false);
				_conditions.Add(conditionId);
				TreeListNode conditionNode = AppendNode(GetAbilityConditionData(id, a, c), conditionsNode, conditionId);
				_nodes.Add(conditionId, conditionNode);
				_rankables.Add(conditionId);
            }
        }

		TreeListNode defaultsNode = AppendNode(new object[] { DEFAULTS }, GetRoot(), DEFAULTS);
		_nodes.Add(DEFAULTS, defaultsNode);

		RunnerDefaults defaults = DataManager.Inst.GetRunnerDefaults(id);
		AddDefaultValue(nameof(RunnerDefaults.DefaultColor), defaults.DefaultColor);
		AddDefaultValue(nameof(RunnerDefaults.DefaultScale), defaults.DefaultScale);
		AddDefaultValue(nameof(RunnerDefaults.DefaultString), defaults.DefaultString);

		ExpandAll();
		if (!string.IsNullOrEmpty(_selectedId) && _nodes.ContainsKey(_selectedId))
		{
			Selection.Add(_nodes[_selectedId]);
			Selection.Remove(GetRoot());
		}
		
    }
    #endregion populate

    #region suntree overrides
    protected override bool SpeedRender(int column, TreeListNode node)
    {
		return column != COL_BASE;
    }

	protected override string GetTreeName() { return "RunnerDetailList"; }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
        if (string.IsNullOrEmpty(id)) { return menu; }
		menu.Add(MENU_ADD_ABILITY);
        if (IsAbility(id))
        {
            menu.Add(MENU_REMOVE_ABILITY);
			menu.Add(MENU_ADD_CONDITION);
			menu.Add(MENU_ADD_EFFECT);
        }
        if (_conditions.Contains(id))
        {
			menu.Add(MENU_REMOVE_CONDITION);
        }
		if(_effects.Contains(id))
        {
			menu.Add(MENU_REMOVE_EFFECT);
        }
		return menu;
	}

	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{ 
		switch(operation)
        {
			case MENU_ADD_ABILITY:
				DataManager.Inst.AddRunnerAbility(_currentRunner);
				break;
			case MENU_REMOVE_ABILITY:
				{
					int abilityIndex = GetAbilityIndex(id);
					DataManager.Inst.RemoveRunnerAbility(_currentRunner, abilityIndex);
				}
                    break;
			case MENU_ADD_CONDITION:
				{
					int abilityIndex = GetAbilityIndex(id);
					DataManager.Inst.AddRunnerAbilityCondition(_currentRunner, abilityIndex);
				}
				break;
			case MENU_REMOVE_CONDITION:
				{
					if (TryParseId(id, out int abilityIndex, out int conditionIndex, out NodeType nodeType) && nodeType == NodeType.ConditionNode)
					{
						DataManager.Inst.RemoveRunnerAbilityCondition(_currentRunner,abilityIndex, conditionIndex);
					}
				}
                break;
			case MENU_ADD_EFFECT:
				{
					if (TryParseId(id, out int abilityIndex, out int effectIndex, out NodeType nodeType))
					{
						DataManager.Inst.AddBaseRunnerAbilityEffect(_currentRunner, abilityIndex);
					}
				}
				break;
			case MENU_REMOVE_EFFECT:
				{
					if (TryParseId(id, out int abilityIndex, out int effectIndex, out NodeType nodeType) && nodeType == NodeType.EffectNode)
					{
						DataManager.Inst.RemoveBaseRunnerAbilityEffect(_currentRunner, abilityIndex,effectIndex);
					}
				}
				break;
		}
	}

	protected override List<string> GetErrors(string id, bool exitEarly)
    {
		List<string> errors = new List<string>();
		if (_rankables.Contains(id))
		{
			RankableFloat rf = GetRankableFloat(id, out string name);
			if (rf.BaseValue != rf.FinalValue && rf.Curve == CurveType.None)
			{
				errors.Add("Rankable float appears to rank up but curve type is None!");
				if (exitEarly) { return errors; }
			}
			if ((name == "Speed" || name == "Health" || name == "HitStrength") && rf.BaseValue == 0)
            {
				errors.Add($"{name} Base value cannot be zero!");
				if(exitEarly) { return errors; }
            }
        }

		return errors;
    }

	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() { return new List<string>(); }
	protected override void InitialPopulateAction(string id)
	{
		// This space intentionally left blank.
	}
    #endregion suntree overrides

	public enum NodeType
    {
		None,
		EffectNode,
		ConditionNode
    }
}