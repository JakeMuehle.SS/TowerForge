﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;
using System.Linq;
public class DailyDealsList : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private HashSet<string> _arenas = new HashSet<string>();
	private HashSet<string> _dealSets = new HashSet<string>();
	private HashSet<string> _deals = new HashSet<string>();
	private HashSet<string> _cards = new HashSet<string>();
	private HashSet<string> _chests = new HashSet<string>();
	private HashSet<string> _cardHeaders = new HashSet<string>();
	private HashSet<string> _chestHeaders = new HashSet<string>();

	private const string MENU_ADD_SET = "Add deal set";
	private const string MENU_REMOVE_SET = "Remove deal set";
	private const string MENU_ADD_DEAL = "Add deal";
	private const string MENU_REMOVE_DEAL = "Remove deal";
	private const string MENU_ADD_CHEST = "Add Chest";
	private const string MENU_REMOVE_CHEST = "Remove Chest";
	private const string MENU_ADD_CARD = "Add card";
	private const string MENU_REMOVE_CARD = "Remove card";

	private const string CARDS = "Deal Cards";
	private const string CHESTS = "Deal Chests";

	private int COL_NAME;
	private int COL_COMMON;
	private int COL_PREMIUM;
	private int COL_GEMPACK;
	private int COL_PRICE;
	private int COL_VALUE;


	public DailyDealsList() : base()
	{
		//return;
		COL_NAME = AddColumn("Name", 100, 100,Editor_NameColumn,Validator_NameColumn).ColumnIndex;
		COL_VALUE = AddColumn("Value", 80, 80,Editor_Value,Validator_Value).ColumnIndex;
		COL_PRICE = AddColumn("Price", 80, 80,Editor_Currency,Validator_Price).ColumnIndex;
		COL_GEMPACK = AddColumn("IsGemPack", 80, 80,Editor_GemPack,Validator_GemPack).ColumnIndex;
        COL_COMMON = AddColumn("Common Awarded", 80, 80,Editor_Currency,Validator_Common).ColumnIndex;
		COL_PREMIUM = AddColumn("Premium Awarded", 80, 80,Editor_Currency,Validator_Premium).ColumnIndex;
		AddColumn("id");
		//DEV_InitialPopulate();

        Signals.RefreshArenaDailyDeals.Register(RefreshDailyDeals);
	}

	private void DEV_InitialPopulate()
    {
		for(int a = 0; a < FirebaseManager.Inst.GetArenaCount();a++)
        {
			for(int s = 0; s < 10; s++)
            {
				FirebaseManager.Inst.AddDailyDealSet(a);
				for(int d = 0; d < 3; d++)
                {
					FirebaseManager.Inst.AddDailyDeal(a, s);
					string key = $"deal_{a}_{s}_{d}";
					FirebaseManager.Inst.ChangeDailyDealKey(a, s, "<New 0>", key);
					FirebaseManager.Inst.AddDailyDealCard(a, s, key);
					RewardedCard card = new RewardedCard();
					card.name = ActionCardType.Frost.ToString();
					card.id = 1;
					card.amount = 1;
					FirebaseManager.Inst.SetDailyDealCard(a, s, key,0, card);
					ServerData_StorePack pack = new ServerData_StorePack();
					pack.price = 100;
					pack.isGemPack = false;
					FirebaseManager.Inst.SetDailyDealNonArrayProperties(a, s, key, pack);
                }
            }
        }
    }

	private object[] GetDealData(int arenaIndex, int setIndex, string dealKey, string id)
	{
		ServerData_StorePack pack =  FirebaseManager.Inst.GetDailyDeal(arenaIndex, setIndex, dealKey);
		return new object[]
		{
			dealKey,
			"",
			pack.price,
			pack.isGemPack,
			pack.CommonCurrency,
			pack.PremiumCurrency,
			id
		};
	}

	private object[] GetChestData(int arenaIndex, int setIndex, string dealKey, int chestIndex, string id)
    {

		return new object[]
		{
			FirebaseManager.Inst.GetDailyDealChest(arenaIndex,setIndex,dealKey,chestIndex),
			"",
			"",
			"",
			"",
			"",
			id
		};
	}

	private object[] GetCardData(int arenaIndex, int setIndex, string dealKey, int chestIndex, string id)
	{
		RewardedCard card = FirebaseManager.Inst.GetDailyDealCard(arenaIndex, setIndex, dealKey, chestIndex);
		if(card == null)
        {
			return new object[] {
				"",
			"",
			"",
			"",
			"",
			"",
			id};
        }
		return new object[] 
		{ 
			card.name,
			card.amount,
			"",
			"",
			"",
			"",
			id
		};
	}

	private object[] GetArenaData(int arenaIndex,string id)
	{
		return new object[] 
		{ 
			"Arena " + arenaIndex,
			"",
			"",
			"",
			"",
			"",
			id
		};
	}

	private object[] GetSetData(int arenaIndex, int setIndex,string id)
	{
		return new object[] 
		{ 
			"Set " + setIndex,
			"",
			"",
			"",
			"",
			"",
			id
		};
	}

    #region columnfunctions

	private RepositoryItem Editor_NameColumn(string id)
    {
		if(_deals.Contains(id))
        {
			return CreateEditorTextEdit();
        }
		if(_cards.Contains(id))
        {
			return CreateEditorDropDown(() =>
			{
				return DataManager.Inst.GetAllCards(true).Select(x => x.ToString()).ToList();
			});
        }
		if(_chests.Contains(id))
        {
			return CreateEditorDropDown<ChestType>();
        }
		return null;
    }

	private bool Validator_NameColumn(string id, object value)
    {
		if(!ParseId(id,out int arenaIndex, out int dealSetIndex, out string deal, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }
		if (_deals.Contains(id))
		{ return FirebaseManager.Inst.ChangeDailyDealKey(arenaIndex, dealSetIndex, deal, (string)value); }
		if(_cards.Contains(id))
        {
			RewardedCard card = GetRewardedCard(id);
			card.name = (string)value;
			card.id = EnumInt(card.name);
			
			return FirebaseManager.Inst.SetDailyDealCard(arenaIndex, dealSetIndex,deal, subTypeIndex, card);
        }
		if(_chests.Contains(id))
        {
			return FirebaseManager.Inst.SetDailyDealChest(arenaIndex, dealSetIndex, deal, subTypeIndex, GetEnum<ChestType>(value));
		}
		return false;
    }

	private int EnumInt(string enumName)
    {
        if (string.IsNullOrEmpty(enumName)) { return 0; }
		if (Enum.IsDefined(typeof(RunnerCardType), enumName))
		{
			return (int)Enum.Parse(typeof(RunnerCardType), enumName);
		}
		if (Enum.IsDefined(typeof(ActionCardType), enumName))
		{
			return (int)Enum.Parse(typeof(ActionCardType), enumName);
		}
		return 0;
	}

	private RepositoryItem Editor_Value(string id)
    {
        if (_cards.Contains(id))
        { return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive); }
		return null;
    }

	private bool Validator_Value(string id, object value)
    {
        if (!_cards.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }
		RewardedCard card = GetRewardedCard(id);
		card.amount = (int)value;
		return FirebaseManager.Inst.SetDailyDealCard(arenaIndex, dealSetIndex, deal, subTypeIndex, card);
	}


	private bool Validator_Price(string id, object value)
    {
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }

		ServerData_StorePack deal = GetDeal(id);
		deal.price = (int)value;
		return FirebaseManager.Inst.SetDailyDealNonArrayProperties(arenaIndex, dealSetIndex, dealkey, deal);
	}

	private RepositoryItem Editor_GemPack(string id)
    {
        if (!_deals.Contains(id)) { return null; }
		return CreateEditorCheckEdit();
    }

	private bool Validator_GemPack(string id, object value)
    {
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }

		ServerData_StorePack deal = GetDeal(id);
		deal.isGemPack = (bool)value;
		return FirebaseManager.Inst.SetDailyDealNonArrayProperties(arenaIndex, dealSetIndex, dealkey, deal);
    }

	private RepositoryItem Editor_Currency(string id)
    {
		if (!_deals.Contains(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
	}

	private bool Validator_Common(string id, object value)
    {
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }

		ServerData_StorePack deal = GetDeal(id);
		deal.CommonCurrency = (int)value;
		return FirebaseManager.Inst.SetDailyDealNonArrayProperties(arenaIndex, dealSetIndex, dealkey, deal);
	}

	private bool Validator_Premium(string id, object value)
    {
		if (!_deals.Contains(id)) { return false; }
		if (!ParseId(id, out int arenaIndex, out int dealSetIndex, out string dealkey, out RewardSubType subtype, out int subTypeIndex))
		{ return false; }

		ServerData_StorePack deal = GetDeal(id);
		deal.PremiumCurrency = (int)value;
		return FirebaseManager.Inst.SetDailyDealNonArrayProperties(arenaIndex, dealSetIndex, dealkey, deal);
	}

    private ServerData_StorePack GetDeal(string id)
    {
        ServerData_StorePack storePack = new ServerData_StorePack();
        if (!_deals.Contains(id) || !_nodes.ContainsKey(id)) { return default; }
        TreeListNode node = _nodes[id];
        storePack.name = (string)node.GetValue(COL_NAME);
		storePack.CommonCurrency = (int)node.GetValue(COL_COMMON);
		storePack.PremiumCurrency = (int)node.GetValue(COL_PREMIUM);
		storePack.isGemPack = (bool)node.GetValue(COL_GEMPACK);
		storePack.price = (int)node.GetValue(COL_PRICE);

		return storePack;
    }

	private RewardedCard GetRewardedCard(string id)
    {
		if(!_cards.Contains(id) || !_nodes.ContainsKey(id))
        {
			return default;
        }

		TreeListNode node = _nodes[id];
		RewardedCard card = new RewardedCard();
		card.amount = (int)node.GetValue(COL_VALUE);
		card.name = Convert.ToString(node.GetValue(COL_NAME));
		card.id = EnumInt(card.name);
		return card;
	}

    #endregion columnFunctions

    protected override string GetTreeName() { return nameof(DailyDealsList); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		menu.Add(MENU_ADD_SET);
		if(_dealSets.Contains(id))
        {
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_ADD_DEAL);
        }
		if(_deals.Contains(id) || _chestHeaders.Contains(id) || _cardHeaders.Contains(id))
        {
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_REMOVE_DEAL);
			menu.Add(MENU_ADD_DEAL);
			menu.Add(MENU_ADD_CARD);
			menu.Add(MENU_ADD_CHEST);
		}
		if(_chests.Contains(id))
        {
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_REMOVE_DEAL);
			menu.Add(MENU_REMOVE_CHEST);
			menu.Add(MENU_ADD_CARD);
			menu.Add(MENU_ADD_CHEST);
			menu.Add(MENU_ADD_DEAL);
		}
		if(_cards.Contains(id))
        {
			menu.Add(MENU_REMOVE_SET);
			menu.Add(MENU_REMOVE_DEAL);
			menu.Add(MENU_REMOVE_CARD);
			menu.Add(MENU_ADD_CARD);
			menu.Add(MENU_ADD_CHEST);
			menu.Add(MENU_ADD_DEAL);
		}
        return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{ 
		if(!ParseId(id, out int arenaIndex, out int dealSetIndex, out string deal, out RewardSubType subtype, out int subTypeIndex))
		{ return; }
		switch(operation)
        {
            case MENU_ADD_DEAL:
				FirebaseManager.Inst.AddDailyDeal(arenaIndex, dealSetIndex);
                break;
			case MENU_REMOVE_DEAL:
				FirebaseManager.Inst.RemoveDailyDeal(arenaIndex, dealSetIndex, deal);
				break;
			case MENU_ADD_SET:
				FirebaseManager.Inst.AddDailyDealSet(arenaIndex);
				break;
			case MENU_REMOVE_SET:
				FirebaseManager.Inst.RemoveDailyDealSet(arenaIndex, dealSetIndex);
				break;
			case MENU_ADD_CHEST:
				FirebaseManager.Inst.AddDailyDealChest(arenaIndex, dealSetIndex, deal);
				break;
			case MENU_REMOVE_CHEST:
				FirebaseManager.Inst.RemoveDailyDealChest(arenaIndex, dealSetIndex, deal, subTypeIndex);
				break;
			case MENU_ADD_CARD:
				FirebaseManager.Inst.AddDailyDealCard(arenaIndex, dealSetIndex, deal);
				break;
			case MENU_REMOVE_CARD:
				FirebaseManager.Inst.RemoveDailyDealCard(arenaIndex, dealSetIndex, deal, subTypeIndex);
				break;
        }
	}
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		List<string> ids = new List<string>();
		for(int i = 0; i < FirebaseManager.Inst.GetArenaCount(); i++)
        {
			ids.Add(GetId(i));
        }
		return ids;
	}

	private void RefreshDailyDeals(int i)
    {
		PopulateArena(GetId(i));
		MakeDirty(_nodes[GetId(i)]);
    }

	private string GetId(int arenaIndex, int dealSetIndex = -1, string deal = "", RewardSubType subtype = RewardSubType.None, int subTypeIndex = -1)
    {
		string id = arenaIndex.ToString();
		if (dealSetIndex >= 0)
		{
			id += $".{dealSetIndex}";
		}
		if(!string.IsNullOrEmpty(deal))
        {
			id += $".{deal}";
        }
		if(subtype != RewardSubType.None)
        {
			string header = "";
			switch(subtype)
            {
				case RewardSubType.Card:
					header = CARDS; break;
				case RewardSubType.Chest:
					header = CHESTS; break;
            }
			id += $".{header}";
		}
		if (subTypeIndex >= 0)
        {
			id += $".{subTypeIndex}";

		}
		return id;
    }

	private bool ParseId(string id, out int arenaIndex, out int dealSetIndex, out string deal, out RewardSubType stype, out int subTypeIndex)
    {
		dealSetIndex = -1;
		stype = RewardSubType.None;
		subTypeIndex = -1;
		deal = "";
		string[] segs = id.Split('.');
        if (!int.TryParse(segs[0], out arenaIndex))
        {
			return false;
        }
		if(segs.Length > 1)
        {
			if (!int.TryParse(segs[1], out dealSetIndex))
			{ return false; }
        }
		if(segs.Length > 2)
        {
			deal = segs[2];
        }
		if(segs.Length > 3)
        {
            switch(segs[3])
            {
				case CARDS:
					stype = RewardSubType.Card; break;
				case CHESTS:
					stype = RewardSubType.Chest; break;
				default:
					Debug.LogError($"Tried to parse header string {segs[3]} which is not handled!");
					return false;
            }
        }
		if(segs.Length > 4)
        {
			if(!int.TryParse(segs[4], out subTypeIndex))
			{ return false; }
        }
		return true;
    }

	private void PopulateArena(string arenaId)
    {
		if(!ParseId(arenaId, out int arenaIndex,out _,out _,out _, out _))
		{ return; }
		ClearArena(arenaId);
        TreeListNode arena = AppendNode(GetArenaData(arenaIndex, arenaId), GetRoot(), arenaId);
		_nodes.Add(arenaId,arena);
		_arenas.Add(arenaId);
        for (int s = 0; s < FirebaseManager.Inst.GetDailyDealSetCount(arenaIndex); s++)
        {
			string setId = GetId(arenaIndex, s);
			TreeListNode set = AppendNode(GetSetData(arenaIndex, s,setId), arena, setId);
			_nodes.Add(setId, set);
			_dealSets.Add(setId);
			foreach(string ddkey in FirebaseManager.Inst.GetDailyDealKeys(arenaIndex,s))
            {
				string dealId = GetId(arenaIndex, s, ddkey);
				TreeListNode deal = AppendNode(GetDealData(arenaIndex, s, ddkey,dealId), set, dealId);
				_nodes.Add(dealId, deal);
				_deals.Add(dealId);
				string chestsId = GetId(arenaIndex, s, ddkey, RewardSubType.Chest);
				TreeListNode chests = AppendNode(new object[] { CHESTS,"", "", "", "", "", chestsId }, deal, chestsId);
				_nodes.Add(chestsId, chests);
				_chestHeaders.Add(chestsId);
				for(int c = 0; c < FirebaseManager.Inst.GetDailyDealChestCount(arenaIndex,s,ddkey); c++)
                {
					string chestKey = GetId(arenaIndex,s,ddkey, RewardSubType.Chest,c);
					TreeListNode chest = AppendNode(GetChestData(arenaIndex,s,ddkey,c,chestKey), chests, chestKey);
					_chests.Add(chestKey);
					_nodes.Add(chestKey, chest);
                }

				string cardsId = GetId(arenaIndex, s, ddkey, RewardSubType.Card);
				TreeListNode cards = AppendNode(new object[] { CARDS, "", "", "", "", "", cardsId}, deal, cardsId);
				_nodes.Add(cardsId, cards);
				_cardHeaders.Add(cardsId);
				for(int c = 0; c < FirebaseManager.Inst.GetDailyDealCardCount(arenaIndex,s,ddkey); c++)
                {
					string cardId = GetId(arenaIndex, s, ddkey, RewardSubType.Card,c);
					TreeListNode card = AppendNode(GetCardData(arenaIndex, s, ddkey,c,cardId), cards, cardId);
					_cards.Add(cardId);
					_nodes.Add(cardId, card);
				}
            }
        }
		arena.ExpandAll();
    }

	private void ClearArena(string arenaId)
	{
		if (!_nodes.ContainsKey(arenaId))
		{
			return;
		}
		HashSet<TreeListNode> children = new HashSet<TreeListNode>(_nodes[arenaId].Nodes);
		foreach (TreeListNode child in children)
		{
			ClearArena(child.Tag.ToString());
		}
		_arenas.Remove(arenaId);
		_dealSets.Remove(arenaId);
        _nodes[arenaId].Remove();
		_deals.Remove(arenaId);
		_cards.Remove(arenaId);
		_chests.Remove(arenaId);
		_cardHeaders.Remove(arenaId);
		_chestHeaders.Remove(arenaId);	
		_nodes.Remove(arenaId);
	}
	protected override void InitialPopulateAction(string id)
	{
		PopulateArena(id);
	}
	
	private enum RewardSubType
    {
		None,
		Card,
		Chest
    }
}

