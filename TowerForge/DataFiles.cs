﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class SoftThreatData : DataFile
{
    public Dictionary<RunnerUnitType, SoftUnitThreat> UnitsThreat = new Dictionary<RunnerUnitType, SoftUnitThreat>();
}

public class SoftUnitThreat
{
    public string Reasoning;
    public float SoftThreat;
}

public class TowerForgeData : DataFile
{
    public string Stream;
}