﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class UserDecks : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
    private Dictionary<string,int> _decks =   new Dictionary<string, int>();
    private Dictionary<string, int> _actionNodes = new Dictionary<string, int>();
	private Dictionary<string, int> _runnerNodes = new Dictionary<string, int>();
	private Dictionary<string, int> _bossNodes = new Dictionary<string, int>();
	public UserDecks() : base()
	{
		AddColumn("ID", 100, 100);
		for (int i = 0; i < FirebaseManager.USERDECK_ACTION_COUNT; i++) // only because Actions are the largest. 
        {
			CreateColumn(i);
        }
	}

	private void CreateColumn(int cardIndex)
    {
		AddColumn("Card " + cardIndex, 100, 100, Editor_Card, (id, value) =>
		{
			if(_actionNodes.ContainsKey(id))
            {
				return FirebaseManager.Inst.SetUserDeckActionCard(_actionNodes[id], cardIndex, GetEnum<ActionCardType>(value));
            }
			else if(_runnerNodes.ContainsKey(id))
            {
				return FirebaseManager.Inst.SetUserDeckRunnerCard(_runnerNodes[id], cardIndex, GetEnum<RunnerCardType>(value));
            }
			else if(_bossNodes.ContainsKey(id))
            {
				return FirebaseManager.Inst.SetUserDeckBossCard(_bossNodes[id], cardIndex, GetEnum<RunnerCardType>(value));
            }
			return false;
		});
    }

	protected override bool IsHeader(string id)
    {
		return _decks.ContainsKey(id);
    }

	private RepositoryItem Editor_Card(string id)
    {
		if(_runnerNodes.ContainsKey(id) || _bossNodes.ContainsKey(id))
        {
			return CreateEditorDropDown<RunnerCardType>();
        }
		if(_actionNodes.ContainsKey(id))
        {
			return CreateEditorDropDown<ActionCardType>();
        }
		return null;
    }

	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id
		};
		return data;
	}

	private object[] GetRunnersData(int deckIndex)
    {
		object[] data = new object[1 + FirebaseManager.USERDECK_RUNNER_COUNT];
		data[0] = "Runners";
		for(int i = 0; i < FirebaseManager.USERDECK_RUNNER_COUNT; i++)
        {
			data[1 + i] = FirebaseManager.Inst.GetUserDeckRunnerCard(deckIndex, i);
        }
		return data;
    }

	private object[] GetActionsData(int deckIndex)
    {
		object[] data = new object[1 + FirebaseManager.USERDECK_ACTION_COUNT];
		data[0] = "Actions";
		for (int i = 0; i < FirebaseManager.USERDECK_ACTION_COUNT; i++)
		{
			data[1 + i] = FirebaseManager.Inst.GetUserDeckActionCard(deckIndex, i);
		}
		return data;
	}

	private object[] GetBossData(int deckIndex)
    {
		object[] data = new object[1 + FirebaseManager.USERDECK_BOSS_COUNT];
		data[0] = "Boss";
		for (int i = 0; i < FirebaseManager.USERDECK_BOSS_COUNT; i++)
		{
			data[1 + i] = FirebaseManager.Inst.GetUserDeckBossCard(deckIndex, i);
		}
		return data;
	}

	protected override string GetTreeName() { return nameof(UserDecks); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{ 
		for(int i = 0; i < FirebaseManager.USERDECK_COUNT; i++)
        {
			_decks.Add(GetDeckID(i), i);
        }
		return new List<string>(_decks.Keys);
	}
	protected override void InitialPopulateAction(string id)
	{
		PopulateDeck(id);
	}


	private void PopulateDeck(string deckId)
    {
		if(!_decks.ContainsKey(deckId))
        {
			return;
        }
		int deckIndex = _decks[deckId];

        TreeListNode deckNode = AppendNode(GetData(deckId), GetRoot(), deckId);
		_nodes.Add(deckId, deckNode);

		string runnerId = RunnerId(deckIndex);
		TreeListNode runnersNode = AppendNode(GetRunnersData(deckIndex), deckNode, runnerId);
		_nodes.Add(runnerId, runnersNode);
		_runnerNodes.Add(runnerId, deckIndex);

		string actionId = ActionsId(deckIndex);
		TreeListNode actionsNode = AppendNode(GetActionsData(deckIndex), deckNode, actionId);
		_nodes.Add(actionId, actionsNode);
		_actionNodes.Add(actionId, deckIndex);

		string bossId = BossId(deckIndex);
		TreeListNode bossNode = AppendNode(GetBossData(deckIndex), deckNode, bossId);
		_nodes.Add(bossId, bossNode);
		_bossNodes.Add(bossId, deckIndex);
    }

	private string GetDeckID(int deckIndex)
    {
		return $"Deck {deckIndex}";
    }

	private string RunnerId(int deckIndex)
    {
		return $"{deckIndex}.Runners";

	}

	private string ActionsId(int deckIndex)
    {
		return $"{deckIndex}.Actions";
	}

	private string BossId(int deckIndex)
    {
		return $"{deckIndex}.Boss";
	}
}