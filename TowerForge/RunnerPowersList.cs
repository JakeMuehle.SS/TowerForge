﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Linq;

public class RunnerPowersList : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	public RunnerPowersList() : base()
	{
		AddColumn("ID", 100, 100);
		Signals.ActionCardChangedLists.Register(ActionCardChangedLists);
	}
	private object[] GetData(string id)
	{
		List<ObjectType> actionCardObjectTypes = DataManager.Inst.GetRunnerPowerObjectTypes().ToList();
		object[] data = new object[1 + actionCardObjectTypes.Count];
        data[0] = DataManager.Inst.DisplayName(id);
		for (int i = 0; i < actionCardObjectTypes.Count; i++)
		{
			data[i + 1] = DataManager.Inst.GetActionCardAsset(id, actionCardObjectTypes[i]);
		}
		return data;
	}
	protected override string GetTreeName() { return nameof(RunnerPowersList); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}

	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }

	protected override void PrePopulate()
	{
		foreach (ObjectType otype in DataManager.Inst.GetRunnerPowerObjectTypes())
		{
			AddColumn(otype.ToString(), 120, 120, (id) => { return CreateEditorTextEdit(); }, (id, value) =>
			{
				return DataManager.Inst.SetActionCardAsset(id, otype, (string)value);
			},
			cellPipFunc: (id, value) =>
			{
				if (DataManager.Inst.CheckActionCardAssetDefault(id, otype))
				{
					return new CellPip(3, System.Drawing.Color.DarkRed);
				}
				return new CellPip(false);
			});
		}
	}
	protected override List<string> GetIDsToPopulate() 
	{
		List<string> ids = new List<string>();
		foreach(string id in DataManager.Inst.GetIds<ActionCardType>())
        {
			if(DataManager.Inst.ActionCardIsRunnerPower(id))
            {
				ids.Add(id);
            }
        }
		return ids;
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

	public void ActionCardChangedLists(string id)
    {
		if(DataManager.Inst.ActionCardIsRunnerPower(id))
        {
			if(!_nodes.ContainsKey(id))
            {
				InitialPopulateAction(id);
            }
        }
        else
        {
			if(_nodes.ContainsKey(id))
            {
				_nodes[id].Remove();
				_nodes.Remove(id);
            }
        }
    }

	public void DropAction(string id, List<string> droppingIds)
    {
		foreach(string did in droppingIds)
        {
			DataManager.Inst.MoveActionCard(did, true);
        }
    }
}
