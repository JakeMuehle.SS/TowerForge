﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;

public class DataManager
{
    private static DataManager _instance;
    private const float BASE_THREAT = 10;
    public const int MAX_RANK_INDEX = 3;

    private static Dictionary<Enum, string> _enumToAlias = new Dictionary<Enum, string>();
    private static Dictionary<string, Enum> _idToEnum = new Dictionary<string, Enum>();
    private static Dictionary<string, Type> _enumNameToType = new Dictionary<string, Type>();
    private Dictionary<AtomicActionEffectType, Type> _compatibility = new Dictionary<AtomicActionEffectType, Type>();
    private HashSet<Type> _localTypes = new HashSet<Type>();
    private HashSet<Type> _uploadableTypes = new HashSet<Type>();
    private Dictionary<Type, DataFile> _data = new Dictionary<Type, DataFile>();

    #region management stuff
    public static DataManager Inst
    {
        get
        {
            if (_instance == null)
            {
                _instance = new DataManager();
            }
            return _instance;
        }
    }


    public async Task Initialize()
    {
        try
        {
            

            //_localTypes.Add(typeof(AliasData));
            _localTypes.Add(typeof(SoftThreatData));
            _localTypes.Add(typeof(TowerForgeData));
            _localTypes.Add(typeof(AliasData));
            _uploadableTypes.Add(typeof(ActionCardsData));
            _uploadableTypes.Add(typeof(RunnersData));
            _uploadableTypes.Add(typeof(RunnerCardsData));
            _uploadableTypes.Add(typeof(ObjectsData));
            _uploadableTypes.Add(typeof(ArenasData));
            _uploadableTypes.Add(typeof(AIData));
            //_uploadableTypes.Add(typeof(LevelsData));

            LoadData<ActionCardsData>();
            LoadData<RunnersData>();
            LoadData<RunnerCardsData>();
            LoadData<ObjectsData>();
            LoadData<ArenasData>();
            LoadData<AIData>();
            LoadData<RunnerPowersData>();


            LoadData<TowerForgeData>();
            LoadData<AliasData>();
            LoadData<SoftThreatData>();
            //await DownloadData();

            PopulateAtomicActionEffectCompatibility();

            LoadAliases();
            Test();
        }
        catch(Exception e)
        {
            Debug.LogError("Error while initializing Datamanager: " + e.Message);
        }
    }

    public async Task DownloadData()
    {
        var acd = FirebaseManager.Inst.DownloadData<ActionCardsData>();
        var rd = FirebaseManager.Inst.DownloadData<RunnersData>();
        var rcd = FirebaseManager.Inst.DownloadData<RunnerCardsData>();
        var od = FirebaseManager.Inst.DownloadData<ObjectsData>();
        var ad = FirebaseManager.Inst.DownloadData<ArenasData>();
        var aid = FirebaseManager.Inst.DownloadData<AIData>();
        var rpd = FirebaseManager.Inst.DownloadData<RunnerPowersData>();
        //var ld = FirebaseManager.Inst.DownloadData<LevelsData>();

        _data[typeof(ActionCardsData)] = await acd;
        _data[typeof(RunnersData)] = await rd;
        //TEMP_TransferRunnerPowers();
        _data[typeof(RunnerCardsData)] = await rcd;
        _data[typeof(ObjectsData)] = await od;
        _data[typeof(ArenasData)] = await ad;
        _data[typeof(AIData)] = await aid;
        _data[typeof(RunnerPowersData)] = await rpd;
        //_data[typeof(LevelsData)] = await ld;
    }

    //private void TEMP_TransferRunnerPowers()
    //{
    //    RunnersData data = GetData<RunnersData>();
    //    HashSet<RunnerUnitType> types = data.Runners.Keys.ToHashSet();
    //    foreach (RunnerUnitType type in types)
    //    {
    //        RunnerUnitData runner = data.Runners[type];
    //        for(int i = 0; i < runner.Abilities.Count; i++)
    //        {
    //            RunnerAbilityData ability = runner.Abilities[i];
    //            ability.Effects = new List<BaseRunnerEffectData>() { ability.EffectData };
    //        }
    //    }
    //}

    public List<string> GetUploadableDataNames()
    {
        return _uploadableTypes.Select(x=>x.Name).ToList();
    }

    public List<DataFile> GetUploadableData()
    {
        List<DataFile> datas = new List<DataFile>();
        foreach(Type t in _uploadableTypes)
        {
            if (!_data.ContainsKey(t))
            {
                Debug.LogError($"Error! Data type {t.Name} is not properly set as an uploadable data type!");
                continue;
            }
            datas.Add(_data[t]);
        }
        return datas;
    }

    private void LoadData<T>() where T : DataFile, new()
    {
        string originalPath = TRDataUtils.Path;
        //if (typeof(T) == typeof(AliasData))
        //{
        //    TRDataUtils.Path = System.IO.Path.Combine(originalPath, "AliasData");
        //}
        if (!_data.ContainsKey(typeof(T)))
        {
            _data.Add(typeof(T), TRDataUtils.Load<T>(local: _localTypes.Contains(typeof(T))));
        }

        TRDataUtils.Path = originalPath;
    }

    //public async Task<string> GetManifestName()
    //{
    //    if(!_data.ContainsKey(typeof(TowerForgeData)))
    //    {
    //        // good gravy. So, we need to get the TowerForgeData which is a file that DataManager handles.
    //        // But we need this for the server data that FirebaseManager loads, which needs to initialize first because
    //        // it needs to be ready to handle the call to DownloadData. However, the Firebase Manager needs this particular
    //        // data file to know where to download stuff from, so we end up having to hotwire everything just so this 
    //        // specific file can get loaded lazily before anything else is actually started up.
    //        //
    //        // This functionality might belong better in some kind of tool save manager than the data manager, since it's
    //        // likely that both the DataManager and the FirebaseManager depend on it.
    //        _data.Add(typeof(TowerForgeData), TRDataUtils.Load<TowerForgeData>(local: true));
    //    }
    //    TowerForgeData data = GetData<TowerForgeData>();
    //    if(data == null || string.IsNullOrEmpty(data.Stream))
    //    {
    //        // prompt the user for the stream name.
    //        TowerForge.ChooseManifestForm cmf = new TowerForge.ChooseManifestForm();
    //        cmf.ShowDialog();
    //
    //        SaveActiveManifest(cmf.Manifest);
    //        
    //    }
    //    return data.Stream;
    //}

    //public void SaveActiveManifest(string stream)
    //{
    //    TowerForgeData data = GetData<TowerForgeData>();
    //    data.Stream = stream;
    //    TRDataUtils.Save(data, local: true);
    //}

    public void Save()
    {
        PrepSave();
        foreach(Type t in _data.Keys)
        {
            string originalPath = TRDataUtils.Path;
            if (t == typeof(AliasData))
            {
                TRDataUtils.Path = System.IO.Path.Combine(originalPath, "AliasData");
            }
            TRDataUtils.Save(_data[t],local:_localTypes.Contains(t));
            TRDataUtils.Path = originalPath;
        }
    }

    private void PrepSave()
    {
        ObjectsData data = GetData<ObjectsData>();
        foreach(RunnerUnitType runner in GetData<RunnersData>().Runners.Keys)
        {
            data.ValidateRunner(runner);
        }
        RunnerCardsData rcards = GetData<RunnerCardsData>();
        foreach(RunnerCardType rcard in rcards.RunnerCards.Keys)
        {
            data.ValidateRunnerCard(rcard);
        }
        foreach(RunnerCardType rcard in rcards.BossCards.Keys)
        {
            data.ValidateRunnerCard(rcard);
        }
        foreach (RunnerUnitType unit in GetData<RunnersData>().Runners.Keys)
        {
            data.ValidateRunner(unit);
        }
    }

    private void PopulateAtomicActionEffectCompatibility()
    {
        _compatibility.Add(AtomicActionEffectType.None, typeof(BaseActionEffectData));
        _compatibility.Add(AtomicActionEffectType.Damage, typeof(BaseActionEffectData));
        _compatibility.Add(AtomicActionEffectType.Teleport, typeof(BaseActionEffectData));
        _compatibility.Add(AtomicActionEffectType.Heal, typeof(BaseActionEffectData));
        _compatibility.Add(AtomicActionEffectType.HealthOverTime, typeof(ActionEffectInflictStatus));
        _compatibility.Add(AtomicActionEffectType.ModifySpeed, typeof(ActionEffectInflictStatus));
        _compatibility.Add(AtomicActionEffectType.InflictStatus, typeof(ActionEffectInflictStatus));
        _compatibility.Add(AtomicActionEffectType.InflictRattle, typeof(ActionEffectInflictRattle));
        _compatibility.Add(AtomicActionEffectType.GrantShields, typeof(BaseActionEffectData));

        // ADD THESE IN AS THEIR CODE GETS IMPLEMENTED
        //compatibility.Add(AtomicActionEffectType.Shred, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.ModifyAttackDamage, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.ModifyDuration, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.ModifyCritChance, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.ModifyDamageApplied, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.ModifyDuration, typeof(BaseActionEffectData));
        //compatibility.Add(AtomicActionEffectType.WindUpDamageApplied, typeof(BaseActionEffectData));
    }

    
    #endregion management stuff
    #region aliases
    private void LoadAliases()
    {
        _enumNameToType.Add(typeof(ActionCardType).Name,typeof(ActionCardType));
        _enumNameToType.Add(typeof(RunnerUnitType).Name,typeof(RunnerUnitType));
        _enumNameToType.Add(typeof(RunnerCardType).Name, typeof(RunnerCardType));
        //_enumNameToType.Add(typeof(BossCardType).Name,typeof(BossCardType));


        HashSet<string> processedAliases = new HashSet<string>();
        // Go through each enum list. If there's already an alias for that enum, use that.
        // If there's no alias but the enum exists in the primary data file it belongs to,
        // use the name of the enum as the alias.
        AliasData aliases = GetData<AliasData>();
        foreach(ActionCardType acard in GetEnumList<ActionCardType>())
        {
            if(acard == ActionCardType.None) { continue; }
            string s = acard.ToString();
            if (aliases.Aliases.ContainsKey(s))
            {
                processedAliases.Add(s);
                _enumToAlias.Add(acard, GetData<AliasData>().Aliases[s]);
                _idToEnum.Add(GetStringId<ActionCardType>(acard), acard);
                continue;
            }
            ActionCardsData data = GetData<ActionCardsData>();
            //if (data.ActionCards.ContainsKey(acard) || data.ActiveLibraryCards.ContainsKey(acard))
            {
                NewActionCard(acard);
                _enumToAlias.Add(acard, acard.ToString());
                _idToEnum.Add(GetStringId<ActionCardType>(acard), acard);
            }
        }
        foreach (RunnerUnitType unit in GetEnumList<RunnerUnitType>())
        {
            if(unit == RunnerUnitType.None) { continue; }
            string s = unit.ToString();
            if (aliases.Aliases.ContainsKey(s))
            {
                processedAliases.Add(s);
                _enumToAlias.Add(unit, GetData<AliasData>().Aliases[s]);
                _idToEnum.Add(GetStringId<RunnerUnitType>(unit), unit);
                continue;
            }
            // for now, always add Runners.
            if (!GetData<RunnersData>().Runners.ContainsKey(unit))
            {
                RunnerUnitData runner = new RunnerUnitData();
                GetData<RunnersData>().Runners.Add(unit, runner);
                // this will happen automatically when we validate before saving.
                //GetData<ObjectsData>().CreateNewRunnerUnit(unit);
            }
            {
                NewRunnerUnit(unit);
                _enumToAlias.Add(unit, unit.ToString());
                _idToEnum.Add(GetStringId<RunnerUnitType>(unit), unit);
            }
        }
        foreach (RunnerCardType acard in GetEnumList<RunnerCardType>())
        {
            if(acard == RunnerCardType.None) { continue; }
            string s = acard.ToString();
            if (aliases.Aliases.ContainsKey(s))
            {
                processedAliases.Add(s);
                _enumToAlias.Add(acard, GetData<AliasData>().Aliases[s]);
                _idToEnum.Add(GetStringId<RunnerCardType>(acard), acard);
                continue;
            }
            RunnerCardsData data = GetData<RunnerCardsData>();
            NewRunnerCard(acard);
            _enumToAlias.Add(acard, acard.ToString());
            _idToEnum.Add(GetStringId<RunnerCardType>(acard), acard);
            
        }

        HashSet<string> missingAliases = aliases.Aliases.Keys.Except(processedAliases).ToHashSet();
        foreach(string s in missingAliases)
        {
            aliases.Aliases.Remove(s);
        }

    }

    private bool TryGetNewEnum<T>(string newName, out T newEnum) where T : Enum
    {
        newEnum = default;
        List<T> list = GetEnumList<T>();
        foreach(T e in list)
        {
            if(e.ToString() == newName)
            {
                Debug.LogError($"The name {newName} is already taken for enum type {typeof(T).Name}!");
                return false;
            }
            if (_enumToAlias.ContainsKey(e) && _enumToAlias[e] == newName)
            {
                Debug.LogError($"The name {newName} is already used as an alias for enum type {typeof(T).Name}!");
                return false;
            }
        }
        foreach (T e in list)
        {
            if (_enumToAlias.ContainsKey(e)) { continue; }
            if(!Guid.TryParseExact(e.ToString(),"N", out Guid id)) { continue; }
            _enumToAlias.Add(e, newName);
            _idToEnum.Add(GetStringId<T>(e), e);
            GetData<AliasData>().Aliases.Add(e.ToString(), newName);
            //TRDataUtils.Save(GetData<AliasData>(), local: true);
            newEnum = e;
            return true;
        }
        Debug.LogError($"No remaining enums exist for enum type {typeof(T).Name}!");
        return false;
    }

    public void NewRunnerCard(RunnerCardType type)
    {
        //Debug.LogError("Adding new Runner Cards is disabled!");
        //return;
        //if(!TryGetNewEnum<RunnerCardType>(name, out RunnerCardType type)) { return; }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if (!data.RunnerCards.ContainsKey(type) && !data.BossCards.ContainsKey(type) && !data.DefaultArenaCards.ContainsKey(type))
        { data.RunnerCards.Add(type, new RunnerCardData()); }
        if (!data.ActiveLibraryCards.ContainsKey(type))
        { data.ActiveLibraryCards.Add(type, false); }
        GetData<ObjectsData>().CreateNewRunnerCard(type);
        //Signals.RunnerCardUpdated.Send(GetStringId<RunnerCardType>(type));
    }

    public void NewRunnerUnit(RunnerUnitType type)
    {
        //Debug.LogError("Adding new Runners is disabled!");
        //return;
        //if (!TryGetNewEnum<RunnerUnitType>(name, out RunnerUnitType type)) { return; }
        RunnerUnitData runner = new RunnerUnitData();
        if (!GetData<RunnersData>().Runners.ContainsKey(type))
        { GetData<RunnersData>().Runners.Add(type, runner); }
        GetData<ObjectsData>().CreateNewRunnerUnit(type);
        //Signals.RunnerUnitUpdated.Send(GetStringId<RunnerUnitType>(type));
    }

    public void NewActionCard(ActionCardType type)
    {
        //Debug.LogError("Adding new Actions is disabled!");
        //return;
        //if (!TryGetNewEnum<ActionCardType>(name, out ActionCardType type)) { return; }
        ActionCardData newCard = new ActionCardData(1, type, 0);
        ActionCardsData data = GetData<ActionCardsData>();
        if (!data.ActionCards.ContainsKey(type))
        { data.ActionCards.Add(type, newCard); }
        if (!data.ActiveLibraryCards.ContainsKey(type))
        { data.ActiveLibraryCards.Add(type, false); }
        GetData<ObjectsData>().CreateNewActionCard(type);
        //Signals.ActionCardUpdated.Send(GetStringId<ActionCardType>(type));
    }

    public string GetStringId<T>(T id) where T : Enum
    {
        return typeof(T).Name + ":" + id.ToString();
    }

    public string DisplayName(string id)
    {
        if(!_idToEnum.TryGetValue(id, out Enum e))
        {
            return "NOT FOUND";
        }
        _enumToAlias.TryGetValue(e, out string name);
        return name;
    }

    public bool CanBeRenamed(string id)
    {
        return GetData<AliasData>().Aliases.ContainsKey(id);
    }

    // Renaming is actually kind of difficult. We'll need to extract the type of enum it is from the name alone, and then send out the 
    // corresponding signal to update the UI. Even if we do this, we have to provide a mechanism to receive a new name. Finally, we have
    // to validate the name itself, which ironically isn't as hard in this case as accepting the user's initial intent.
    // For now, you'll have to rename stuff by modifying the alias file and restarting the tool. 
    //public bool Rename(string id, string newName)
    //{
    //
    //}

    public bool IsIdThisType<T>(string id) where T : Enum
    {
        if(!TryGetEnumFromId<T>(id, out T result))
        {
            return false;
        }
        if (((int)(object)result) == 0)
        { return false; }
        return true;
    }

    public bool TryGetEnumFromId<T>(string id, out T result) where T : Enum
    {
        result = default(T);
        if(!_idToEnum.TryGetValue(id, out Enum e))
        {
            return false;
        }
        if(!Enum.IsDefined(typeof(T), (int)(object)e))
        { return false; }
        result = (T)e;
        return true;
    }

    public bool TryGetEnumIntValue(string id, out int result)
    {
        result = -1;
        if (!_idToEnum.TryGetValue(id, out Enum e))
        {
            return false;
        }
        result = Convert.ToInt32(e);
        return true;
    }
    #endregion aliases

    
    #region test
    private void Test()
    {
        //RunnerUnitData runner = GetData<RunnersData>().Runners[RunnerUnitType.Skeleton];
        //
        //float speed = runner.Speed.Value;
        //
        //runner.SetLevel(1);
        //runner.SetRank(1);
        //
        //float speed2 = runner.Speed.Value;
    }

    private T GetData<T>() where T : DataFile
    {
        if (!_data.ContainsKey(typeof(T)))
        {
            Debug.LogError($"Error! {typeof(T).Name} is not handled by DataManager!");
            return null;
        }
        return (T)_data[typeof(T)];
    }
    #endregion
    #region Ids
    public List<T> GetEnumList<T>() where T : Enum
    {
        List<T> enums = Enum.GetValues(typeof(T)).Cast<T>().ToList();

        return enums;
    }

    private List<string> GetIdsInternal<T>() where T : Enum
    {
        List<string> names = Enum.GetNames(typeof(T)).ToList();
        names.Remove("None");
        return names;
    }

    public List<string> GetIds<T>() where T : Enum
    {
        List<T> values = GetEnumList<T>();
        for(int i = values.Count - 1; i >= 0; i--)
        {
            if (!_enumToAlias.ContainsKey(values[i]))
            {
                values.RemoveAt(i);
            }
        }
        List<string> ids = new List<string>();
        foreach(T value in values)
        {
            ids.Add(GetStringId<T>(value));
        }
        return ids;
    }

    //public List<string> GetActionCardIds()
    //{
    //    return GetIdsInternal<ActionCardType>();
    //}
    //
    //public List<string> GetRunnerCardIds()
    //{
    //    return GetIdsInternal<RunnerCardType>();
    //}
    //
    //public List<string> GetRunnerIds()
    //{
    //    return GetIdsInternal<RunnerUnitType>();
    //}

    public List<Enum> GetAllCards(bool validOnly = false)
    {
        List<Enum> cards = new List<Enum>();
        if (validOnly)
        {
            ActionCardsData cdata = GetData<ActionCardsData>();
            RunnerCardsData rdata = GetData<RunnerCardsData>();
            foreach (ActionCardType atype in cdata.ActiveLibraryCards.Keys)
            {
                if (cdata.ActiveLibraryCards[atype])
                { cards.Add(atype); }
            }
            foreach (RunnerCardType rtype in rdata.ActiveLibraryCards.Keys)
            {
                if (rdata.ActiveLibraryCards[rtype])
                { cards.Add(rtype); }
            }
            return cards;
        }

        foreach (ActionCardType atype in Utils.GetEnumList<ActionCardType>())
        {
            if (atype == ActionCardType.None) { continue; }
            cards.Add(atype);
        }
        foreach (RunnerCardType rtype in Utils.GetEnumList<RunnerCardType>())
        {
            if (rtype == RunnerCardType.None) { continue; }
            cards.Add(rtype);
        }
        return cards;
    }

    #endregion
    #region actionCards
    public int GetTowerUpgradeCost(string actionCardName)
    {
        if (!TryGetActionCard(actionCardName, out ActionCardData card))
        {
            return 0;
        }

        return card.UpgradeCost;
    }

    public bool SetTowerUpgradeCost(string cardID, int upgradeCost)
    {
        if (!TryGetActionCard(cardID, out ActionCardData card))
        {
            return false;
        }

        card.UpgradeCost = upgradeCost;
        Signals.ActionCardUpdated.Send(cardID);
        return true;
    }

    public int GetActionCardCost(string actionCardName)
    {
        if(!TryGetActionCard(actionCardName, out ActionCardData card))
        {
            return 0;
        }

        return card.Cost;
    }

    public bool SetActionCardCost(string actionCardName, int cost)
    {
        if (!TryGetActionCard(actionCardName, out ActionCardData card))
        {
            return false;
        }

        card.Cost = cost;
        Signals.ActionCardUpdated.Send(actionCardName);
        return true;

    }

    private bool TryGetActionCard(string actionCardName, out ActionCardData actionCard)
    {
        actionCard = null;
        if (!Enum.TryParse<ActionCardType>(actionCardName.Split(':')[1], out ActionCardType type))
        {
            Debug.LogError($"Error! {actionCardName} is not a ActionCardType!");
            return false;
        }

        if (!GetData<ActionCardsData>().ActionCards.TryGetValue(type, out actionCard))
        {
            Debug.LogError($"Error! {type.ToString()} does not exist in data!");
            return false;
        }
        return true;
    }

    private bool TryGetRunnerPower(string actionCardName, out PowerData pdata)
    {
        pdata = null;
        if (!Enum.TryParse<ActionCardType>(actionCardName.Split(':')[1], out ActionCardType type))
        {
            Debug.LogError($"Error! {actionCardName} is not a ActionCardType!");
            return false;
        }

        if (!GetData<RunnerPowersData>().ActionCards.TryGetValue(type, out ActionCardData adata))
        {
            Debug.LogError($"Error! {type.ToString()} does not exist in runner powers data!");
            return false;
        }
        pdata = adata.Power;
        return true;
    }

    public bool ActionCardIsRunnerPower(string actionCardName)
    {
        if (!Enum.TryParse<ActionCardType>(actionCardName.Split(':')[1], out ActionCardType type))
        {
            Debug.LogError($"Error! {actionCardName} is not a ActionCardType!");
            return false;
        }

        return GetData<RunnerPowersData>().ActionCards.ContainsKey(type);
    }

    public bool MoveActionCard(string actionCardName, bool toRunnerPower)
    {
        if (!Enum.TryParse<ActionCardType>(actionCardName.Split(':')[1], out ActionCardType type))
        {
            Debug.LogError($"Error! {actionCardName} is not a ActionCardType!");
            return false;
        }
        ActionCardsData adatas = GetData<ActionCardsData>();
        RunnerPowersData rpdatas = GetData<RunnerPowersData>();
        if(toRunnerPower)
        {
            if (!MoveActionCardInternal(type, adatas, rpdatas))
            {
                return false;
            }
        }
        else
        {
            if(!MoveActionCardInternal(type, rpdatas, adatas))
            {
                return false;
            }
        }
        Signals.ActionCardChangedLists.Send(actionCardName);
        return true;
    }

    private bool MoveActionCardInternal(ActionCardType type, ActionCardsData from, ActionCardsData to)
    {
        if (!from.ActionCards.ContainsKey(type))
        {
            Debug.LogError($"Tried to move ActionCard {type} but it was not found in the source ActionCardsData!");
            return false;
        }
        if (!to.ActionCards.ContainsKey(type))
        {
            to.ActionCards.Add(type, new ActionCardData());
        }
        to.ActionCards[type] = from.ActionCards[type];
        from.ActionCards.Remove(type);
        return true;
    }

    #region xowers
    private bool TryGetXower(string actionCardName, XowerType xowerType, int level, out XowerData xower)
    {
        xower = null;
        switch (xowerType)
        {
            case XowerType.Tower:
                if (!TryGetActionCard(actionCardName, out ActionCardData card))
                {
                    return false;
                }
                xower = card.Tower;
                break;
            case XowerType.Power:
                if (!TryGetActionCard(actionCardName, out ActionCardData card2))
                {
                    return false;
                }
                xower = card2.Power;
                break;
            case XowerType.RunnerPower:
                if(!TryGetRunnerPower(actionCardName, out PowerData power))
                {
                    return false;
                }
                xower = power;
                break;
            default:
                Debug.LogError($"Not set up to handle XowerType {xowerType}!");
                break;
        }
        xower.SetLevel(level);
        return true;
    }

    //public PowerData GetCopyOfPower(string cardId)
    //{
    //    if(!TryGetXower(cardId, true,0,out XowerData xower))
    //    {
    //        Debug.LogError("Could not find power for action card " + cardId);
    //        return null;
    //    }
    //    return TRDataUtils.DeepCopy<PowerData>((PowerData)xower);
    //}


    public float GetXowerAttackRate(string cardId, XowerType xowerType, int level)
    {
        if (!TryGetXower(cardId, xowerType, level, out XowerData xower))
        {
            return 0f;
        }
        return xower.Rate.Value;
    }

    public ReconcileType GetStatusReconcileType(UnitStatus status)
    {
        if(!GetData<ActionCardsData>().ReconcileTypes.TryGetValue(status, out ReconcileType rtype))
        {
            return ReconcileType.None;
        }
        return rtype;
    }

    public bool SetStatusReconcileType(UnitStatus status, ReconcileType rtype)
    {
        ActionCardsData data = GetData<ActionCardsData>();
        if(!data.ReconcileTypes.ContainsKey(status))
        {
            data.ReconcileTypes.Add(status, ReconcileType.None);
        }
        data.ReconcileTypes[status] = rtype;
        return true;
    }

    public string GetActionCardAsset(string cardId, ObjectType type)
    {
        if (!TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType cardType))
        {
            Debug.LogError($"CardID {cardId} is not a valid name for an action card!");
            return "";
        }
        if(!GetData<ObjectsData>().GetObjectTypesForData(nameof(ActionCardType)).Contains(type))
        {
            Debug.LogError($"Object type {type} is not one of the types that ActionCards use!");
            return "";
        }
        if (!GetData<ObjectsData>().ObjectMap[type].TryGetValue(cardType.ToString(), out string value))
        {
            return "";
        }
        return value;
    }

    public bool SetActionCardAsset(string cardId, ObjectType type, string asset)
    {
        if (!TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType cardType))
        {
            Debug.LogError($"CardID {cardId} is not a valid name for an action card!");
            return false;
        }
        ObjectsData data = GetData<ObjectsData>();
        if (!data.GetObjectTypesForData(nameof(ActionCardType)).Contains(type))
        {
            Debug.LogError($"Object type {type} is not one of the types that ActionCards use!");
            return false;
        }
        if (!data.ObjectMap[type].ContainsKey(cardType.ToString()))
        {
            data.ObjectMap[type].Add(cardType.ToString(), "");
        }
        data.ObjectMap[type][cardType.ToString()] = asset;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public bool CheckActionCardAssetDefault(string cardId, ObjectType type)
    {
        if (!TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType cardType))
        {
            return false;
        }
        if (!GetData<ObjectsData>().ObjectMap[type].TryGetValue(cardType.ToString(), out string value))
        {
            return false;
        }
        return value == DefaultObjectString(type);
    }

    public HashSet<ObjectType> GetActionCardObjectTypes()
    {
        return GetData<ObjectsData>().GetObjectTypesForData(nameof(ActionCardType));
    }

    public HashSet<ObjectType> GetRunnerPowerObjectTypes()
    {
        return GetData<ObjectsData>().GetObjectTypesForData(nameof(RunnerPowersData));
    }

    public float GetXowerBuildTime(string cardId, XowerType xowerType, int level)
    {
        if (!TryGetXower(cardId, xowerType, level, out XowerData xower))
        {
            return 0;
        }
        return xower.BuildTime.Value;
    }

    public RankableFloat GetXowerRankableFloat(string cardId, XowerType xowerType, bool inAimData, string propertyName)
    {
        if(!TryGetXower(cardId, xowerType, 0,out XowerData xower))
        {
            return null;
        }

        RankableFloat rfloat;
        if (inAimData)
        {
            if (!TryGetRankableField(xower.Effect.AimData, propertyName, out rfloat))
            {
                return null;
            }
        }
        else
        {
            if (!TryGetRankableField(xower, propertyName, out rfloat))
            {
                return null;
            }
        }
        return rfloat;
    }

    public bool SetXowerRankableFloat(string cardId, XowerType xowerType, string propertyName, bool inAimData, RankableFloat incRfloat)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        RankableFloat rfloat;

        if (inAimData)
        {
            if (!TryGetRankableField(xower.Effect.AimData, propertyName, out rfloat))
            {
                return false;
            }
        }
        else
        {
            if (!TryGetRankableField(xower, propertyName, out rfloat))
            {
                return false;
            }
        }

        rfloat.SetTo(incRfloat);
        Signals.ActionCardUpdated.Send(cardId);

        return true;
    }
    #endregion xowers


    #region Aiming

    public AimingType GetXowerAimType(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return AimingType.None;
        }
        return xower.Effect.AimData.AimType;
    }

    public bool SetXowerAimType(string cardId, XowerType xowerType, AimingType aimType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Effect.AimData.AimType = aimType;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public MarkType GetXowerMarkType(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return MarkType.None;
        }
        return xower.MarkType;
    }

    public bool SetXowerMarkType(string cardId, XowerType xowerType, MarkType markType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.MarkType = markType;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public SplashType GetXowerSplashType(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return SplashType.None;
        }
        return xower.Effect.AimData.SplashType;
    }

    public bool SetXowerSplashType(string cardId, XowerType xowerType, SplashType splashType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Effect.AimData.SplashType = splashType;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public AimPreference GetXowerAimPreference(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return AimPreference.None;
        }
        return xower.Effect.AimData.Preference;
    }

    public bool SetXowerAimPreference(string cardId, XowerType xowerType, AimPreference pref)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Effect.AimData.Preference = pref;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public ProjectileData GetXowerProjectileData(string cardId, XowerType xowerType)
    {
        if(!TryGetXower(cardId, xowerType, 0,out XowerData xower))
        {
            return new ProjectileData();
        }
        return xower.Projectile;
    }

    public bool SetXowerProjectileData(string cardId, XowerType xowerType, ProjectileData pdata)
    {
        if(!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Projectile = pdata;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public TowerFootprint GetTowerFootprint(string cardId)
    {
        if(!TryGetXower(cardId, XowerType.Tower ,0,out XowerData xower))
        {
            return TowerFootprint.None;
        }
        return ((TowerData)xower).FootprintSize;
    }

    public bool SetTowerFootprint(string cardId, TowerFootprint footprint)
    {
        if (!TryGetXower(cardId, XowerType.Tower, 0, out XowerData xower))
        {
            return false;
        }
        ((TowerData)xower).FootprintSize = footprint;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public HashSet<UnitStatus> GetXowerMarkStati(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return new HashSet<UnitStatus>();
        }
        return new HashSet<UnitStatus>(xower.Effect.AimData.MarkStati);
    }

    public bool SetXowerMarkStati(string cardId, XowerType xowerType, HashSet<UnitStatus> stati)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Effect.AimData.MarkStati = new HashSet<UnitStatus>(stati);
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public StatiInclusionType GetXowerMarkStatiInclusionType(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return StatiInclusionType.None;
        }
        return xower.Effect.AimData.StatiInclusionType;
    }

    public bool SetXowerMarkStatiInclusionType(string cardId, XowerType xowerType, StatiInclusionType StatiInclusionType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.Effect.AimData.StatiInclusionType = StatiInclusionType;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    //public int GetXowerTargetingDepth(string cardId, bool asPower)
    //{
    //    if (!TryGetXower(cardId, asPower, 0, out XowerData xower))
    //    {
    //        return -1;
    //    }
    //    return xower.Effect.AimData.Depth;
    //}
    //
    //public bool SetXowerTargetingDepth(string cardId, bool asPower, int depth)
    //{
    //    if (!TryGetXower(cardId, asPower, 0, out XowerData xower))
    //    {
    //        return false;
    //    }
    //    xower.Effect.AimData.Depth = depth;
    //    Signals.ActionCardUpdated.Send(cardId);
    //    return true;
    //
    //}

    public int GetXowerBurstCount(string cardId, XowerType xowerType)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return -1;
        }
        return xower.BurstCount;
    }

    public bool SetXowerBurstCount(string cardId, XowerType xowerType, int burstCount)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }
        xower.BurstCount = burstCount;
        Signals.ActionCardUpdated.Send(cardId);
        return true;

    }
    #endregion Aiming

    #region atomic effects

    #region effects add/delete
    public bool MoveXowerAtomicEffectToPrimaryFromSecondary(string cardId, XowerType xowerType, bool reverse, int index)
    {
        if(!TryGetXower(cardId, xowerType,0,out XowerData xower))
        {
            return false;
        }

        AtomicActionEffectData data;
        if(reverse)
        {
            if(index >= xower.Effect.PrimaryEffects.Count)
            { return false; }
            data = xower.Effect.PrimaryEffects[index];
        }
        else
        {
            if(index >= xower.Effect.SecondaryEffects.Count)
            { return false; }
            data = xower.Effect.SecondaryEffects[index];
        }

        AtomicActionEffectData copy = TRDataUtils.DeepCopy<AtomicActionEffectData>(data);

        if(!AddXowerAtomicEffectInternal(cardId, xowerType, !reverse, copy, false)) { return false; }
        if (!RemoveXowerAtomicEffectInternal(cardId, xowerType, reverse, index, false)) { return false; }
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public bool CopyXowerAtomicEffect(string cardId, XowerType xowerType, bool primaryList, int index)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        AtomicActionEffectData data;
        if (primaryList)
        {
            if (index >= xower.Effect.PrimaryEffects.Count)
            { return false; }
            data = xower.Effect.PrimaryEffects[index];
        }
        else
        {
            if (index >= xower.Effect.SecondaryEffects.Count)
            { return false; }
            data = xower.Effect.SecondaryEffects[index];
        }

        AtomicActionEffectData copy = TRDataUtils.DeepCopy<AtomicActionEffectData>(data);

        if (!AddXowerAtomicEffectInternal(cardId, xowerType, primaryList, copy, false)) { return false; }
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }
    public bool AddXowerAtomicEffect(string cardId, XowerType xowerType, bool primaryList)
    {
        AtomicActionEffectData data = new AtomicActionEffectData();
        return AddXowerAtomicEffectInternal(cardId, xowerType, primaryList, data, true);
    }

    private bool AddXowerAtomicEffectInternal(string cardId, XowerType xowerType, bool primaryList, AtomicActionEffectData atomicEffect, bool sendSignal)
    {
        if(!TryGetXower(cardId,xowerType,0, out XowerData xower))
        {
            return false;
        }

        if(primaryList)
        {
            xower.Effect.PrimaryEffects.Add(atomicEffect);
        }
        else
        {
            xower.Effect.SecondaryEffects.Add(atomicEffect);
        }
        if (sendSignal)
        { Signals.ActionCardUpdated.Send(cardId); }
        return true;
    }

    public bool RemoveXowerAtomicEffect(string cardId, XowerType xowerType, bool primaryList, int index)
    {
        return RemoveXowerAtomicEffectInternal(cardId, xowerType, primaryList, index, true);
    }

    private bool RemoveXowerAtomicEffectInternal(string cardId, XowerType xowerType, bool primaryList, int index, bool sendSignal)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        if (primaryList)
        {
            if (index >= xower.Effect.PrimaryEffects.Count) { return false; }
            xower.Effect.PrimaryEffects.RemoveAt(index);
        }
        else
        {
            if(index >= xower.Effect.SecondaryEffects.Count) { return false; }
            xower.Effect.SecondaryEffects.RemoveAt(index);
        }
        if (sendSignal)
        { Signals.ActionCardUpdated.Send(cardId); }
        return true;
    }
    #endregion effect add/delete
    #region effect properties

    private bool TryGetXowerAtomicEffect(string cardId, XowerType xowerType, int level, bool primaryList, int aeffectIndex, out AtomicActionEffectData aeData)
    {
        aeData = null;
        if (!TryGetXower(cardId, xowerType, level, out XowerData xower))
        {
            return false;
        }

        int count = primaryList ? xower.Effect.PrimaryEffects.Count : xower.Effect.SecondaryEffects.Count;

        if (aeffectIndex < 0 || aeffectIndex >= count)
        {
            Debug.LogError($"Atomic Effect index {aeffectIndex} was out of range for battleCard {cardId} (as {(xowerType == XowerType.Power ? "power" : "tower")})");
            return false;
        }

        if(primaryList)
        {
            aeData = xower.Effect.PrimaryEffects[aeffectIndex];
        }
        else
        {
            aeData = xower.Effect.SecondaryEffects[aeffectIndex];
        }
        return true;
    }

    public int GetXowerAtomicEffectCount(string cardId, XowerType xowerType, int level, bool primaryList)
    {
        if (!TryGetXower(cardId, xowerType, level, out XowerData xower))
        {
            return 0;
        }
        if (primaryList)
        {
            return xower.Effect.PrimaryEffects.Count;
        }
        return xower.Effect.SecondaryEffects.Count;
    }

    public float GetAtomicEffectDuration(string cardId, XowerType xowerType, int level, bool primaryList, int aeffectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, level, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        {
            return -1;
        }
        ActionEffectInflictStatus aeis = aedata.ActionEffect as ActionEffectInflictStatus;
        if (aeis == null)
        {
            return -1;
        }
        return aeis.Duration.Value;
    }

    public float GetAtomicEffectAmount(string cardId, XowerType xowerType, int level, bool primaryList, int aeffectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, level, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        {
            return -1;
        }
        return aedata.ActionEffect.Amount.Value;
    }

    public AtomicActionEffectType GetAtomicEffectType(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 1, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        {
            return AtomicActionEffectType.None;
        }
        return aedata.ActionEffect.Type;
    }

    public bool SetAtomicEffectType(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex, AtomicActionEffectType type)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        if (primaryList)
        {
            if(!SetEffectType(aeffectIndex, type, ref xower.Effect.PrimaryEffects)) { return false; }
        }
        else
        {
            if(!SetEffectType(aeffectIndex, type, ref xower.Effect.SecondaryEffects)) { return false; }
        }
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    private bool SetEffectType(int index, AtomicActionEffectType type, ref RankList<AtomicActionEffectData> list)
    {
        if(index >= list.Count)
        {
            Debug.LogError($"Atomic Effect index was out of range while trying to change an atomic effect type.");

            return false;
        }
        BaseActionEffectData data = list[index].ActionEffect;
        if(!_compatibility.ContainsKey(type))
        {
            Debug.LogError($"Not currently set up to handle events of type {type}!");
            return false;
        }
        if (_compatibility[data.Type] == _compatibility[type])
        {
            list[index].ActionEffect.Type = type;
            return true;
        }
        list[index].ActionEffect = (BaseActionEffectData)System.Activator.CreateInstance(_compatibility[type]);
        list[index].ActionEffect.Amount = data.Amount;
        list[index].ActionEffect.Type = type;

        return true;
    }


    public UnitStatus GetAtomicEffectStatusInflicted(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType,1, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        {
            return UnitStatus.None;
        }
        ActionEffectInflictStatus aeis = aedata.ActionEffect as ActionEffectInflictStatus;
        if (aeis == null)
        {
            return UnitStatus.None;
        }

        return aeis.StatusInflicted;
    }

    public bool SetAtomicEffectStatusInflicted(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex, UnitStatus status)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 1, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        {
            return false;
        }
        ActionEffectInflictStatus aeis = aedata.ActionEffect as ActionEffectInflictStatus;
        if (aeis == null)
        {
            return false;
        }

        aeis.StatusInflicted = status;
        return true;
    }
    #endregion effect properties

    #region effect conditions
    #region condition add/remove
    public bool AddXowerAtomicEffectCondition(string cardId, XowerType xowerType, bool primaryList, int atomicEffectIndex)
    {
        AtomicActionConditionData condition = new AtomicActionConditionData();
        return AddXowerAtomicEffectConditionInternal(cardId, xowerType, primaryList, atomicEffectIndex, condition, true);
    }
    private bool AddXowerAtomicEffectConditionInternal(string cardId, XowerType xowerType, bool primaryList, int atomicEffect, AtomicActionConditionData condition, bool sendSignal)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        if (primaryList)
        {
            if(atomicEffect >= xower.Effect.PrimaryEffects.Count)
            {
                return false;
            }
            xower.Effect.PrimaryEffects[atomicEffect].Condition.AtomicConditions.Add(condition);
        }
        else
        {
            if (atomicEffect >= xower.Effect.SecondaryEffects.Count)
            {
                return false;
            }
            xower.Effect.SecondaryEffects[atomicEffect].Condition.AtomicConditions.Add(condition);
        }
        if (sendSignal)
        { Signals.ActionCardUpdated.Send(cardId); }
        return true;
    }

    public bool CloneXowerAtomicEffectCondition(string cardId, XowerType xowerType, bool primaryList, int atomicEffectIndex, int conditionIndex)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        AtomicActionConditionData data;
        if (primaryList)
        {
            if (atomicEffectIndex >= xower.Effect.PrimaryEffects.Count || conditionIndex >= xower.Effect.PrimaryEffects[atomicEffectIndex].Condition.AtomicConditions.Count)
            { return false; }
            data = xower.Effect.PrimaryEffects[atomicEffectIndex].Condition.AtomicConditions[conditionIndex];
        }
        else
        {
            if (atomicEffectIndex >= xower.Effect.SecondaryEffects.Count || conditionIndex >= xower.Effect.SecondaryEffects[atomicEffectIndex].Condition.AtomicConditions.Count)
            { return false; }
            data = xower.Effect.SecondaryEffects[atomicEffectIndex].Condition.AtomicConditions[conditionIndex];
        }

        AtomicActionConditionData copy = TRDataUtils.DeepCopy<AtomicActionConditionData>(data);

        if (!AddXowerAtomicEffectConditionInternal(cardId, xowerType, primaryList, atomicEffectIndex, data, false)) { return false; }
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public bool RemoveXowerAtomicEffectCondition(string cardId, XowerType xowerType, bool primaryList, int atomicEffect, int conditionId)
    {
        return RemoveXowerAtomicEffectConditionInternal(cardId, xowerType,primaryList,atomicEffect,conditionId,true);
    }

    private bool RemoveXowerAtomicEffectConditionInternal(string cardId, XowerType xowerType, bool primaryList, int atomicEffect, int conditionId, bool sendSignal)
    {
        if (!TryGetXower(cardId, xowerType, 0, out XowerData xower))
        {
            return false;
        }

        if (primaryList)
        {
            if (atomicEffect >= xower.Effect.PrimaryEffects.Count || conditionId >= xower.Effect.PrimaryEffects[atomicEffect].Condition.AtomicConditions.Count)
            {
                return false;
            }
            xower.Effect.PrimaryEffects[atomicEffect].Condition.AtomicConditions.RemoveAt(conditionId);
        }
        else
        {
            if (atomicEffect >= xower.Effect.SecondaryEffects.Count || conditionId >= xower.Effect.SecondaryEffects[atomicEffect].Condition.AtomicConditions.Count)
            {
                return false;
            }
            xower.Effect.SecondaryEffects[atomicEffect].Condition.AtomicConditions.RemoveAt(conditionId);
        }
        if (sendSignal)
        { Signals.ActionCardUpdated.Send(cardId); }
        return true;
    }
    #endregion condition add/remove
    #region condition properties
    public HashSet<UnitStatus> GetAtomicEffectConditionalStati(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex)
    {
        if(!TryGetXowerAtomicEffect(cardId,xowerType,0,primaryList, aeffectIndex,out AtomicActionEffectData aedata))
        { return new HashSet<UnitStatus>();}
        return new HashSet<UnitStatus>(aedata.Condition.MarkStati);
    }

    public bool SetAtomicEffectConditionalStati(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex, HashSet<UnitStatus> stati)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 0, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        { return false; }
        aedata.Condition.MarkStati = stati;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public StatiInclusionType GetAtomicEffectConditionalStatiInclusionType(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 0, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        { return StatiInclusionType.None; }
        return aedata.Condition.StatiInclusionType;
    }

    public bool SetAtomicEffectConditionalStatiInclusionType(string cardId, XowerType xowerType, bool primaryList, int aeffectIndex, StatiInclusionType type)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 0, primaryList, aeffectIndex, out AtomicActionEffectData aedata))
        { return false; }
        aedata.Condition.StatiInclusionType = type;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public int GetXowerAtomicEffectAtomicConditionsCount(string cardId, XowerType xowerType, bool primaryList, int effectIndex)
    {
        if (!TryGetXowerAtomicEffect(cardId, xowerType, 1, primaryList, effectIndex, out AtomicActionEffectData aedata))
        {
            return 0;
        }

        return aedata.Condition.AtomicConditions.Count;
    }

    private bool TryGetXowerAtomicEffectAtomicCondition(string cardId, XowerType xowerType, int level, bool primaryList, int effectIndex, int conditionIndex, out AtomicActionConditionData acdata)
    {
        acdata = null;
        if (!TryGetXowerAtomicEffect(cardId, xowerType, level, primaryList, effectIndex, out AtomicActionEffectData ceData))
        {
            return false;
        }

        if (conditionIndex < 0 || conditionIndex >= ceData.Condition.AtomicConditions.Count)
        {
            Debug.LogError($"Condition #{conditionIndex} is out of range for ConditionalEffect #{effectIndex} for battleCard {cardId} (as {(xowerType == XowerType.Power ? "power" : "tower")})");
            return false;
        }
        acdata = ceData.Condition.AtomicConditions[conditionIndex];
        return true;
    }

    public AtomicActionConditionType GetXowerAtomicEffectAtomicConditionType(string cardId, XowerType xowerType, int level, bool primaryList, int effectIndex, int conditionIndex)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(cardId, xowerType, level, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return AtomicActionConditionType.None;
        }
        return acdata.ActionConditionType;
    }

    public bool SetXowerAtomicEffectAtomicConditionType(string cardId, XowerType xowerType, bool primaryList, int effectIndex, int conditionIndex, AtomicActionConditionType type)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(cardId, xowerType, 0, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return false;
        }
        acdata.ActionConditionType = type;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public float GetXowerAtomicEffectAtomicConditionValue(string cardId, XowerType xowerType, int level, bool primaryList, int effectIndex, int conditionIndex)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(cardId, xowerType, level, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return 0;
        }
        return acdata.CompareTo.Value;
    }

    public ConditionComparator GetXowerAtomicEffectAtomicConditionComparator(string cardId, XowerType xowerType, int level, bool primaryList, int effectIndex, int conditionIndex)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(cardId, xowerType, level, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return ConditionComparator.None;
        }
        return acdata.Comparator;
    }

    public bool SetXowerAtomicEffectAtomicConditionComparator(string cardId, XowerType xowerType,  bool primaryList, int effectIndex, int conditionIndex, ConditionComparator compare)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(cardId, xowerType, 0, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return false;
        }
        acdata.Comparator = compare;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }
    #endregion condition properties
    #endregion effect conditions

    #endregion atomic effects

    #endregion actionCards

    #region runnerCards

    public RunnerCardCategory GetRunnerCardCategory(string runnerCardId)
    {
        RunnerCardCategory category = RunnerCardCategory.None;
        if(!TryGetEnumFromId<RunnerCardType>(runnerCardId, out RunnerCardType type))
        {
            return category;
        }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if(data.RunnerCards.ContainsKey(type))
        {
            category = RunnerCardCategory.Runner;
        }
        if(data.DefaultArenaCards.ContainsKey(type))
        {
            if (category != RunnerCardCategory.None)
            { Debug.LogError($"RunnerCardType {type} is in more than one RunnerCardsList!"); }
            category = RunnerCardCategory.DefaultRunner;
        }
        if(data.BossCards.ContainsKey(type))
        {
            if (category != RunnerCardCategory.None)
            { Debug.LogError($"RunnerCardType {type} is in more than one RunnerCardsList!"); }
            category = RunnerCardCategory.Boss;
        }

        return category;
    }

    public bool SetRunnerCardCategory(string runnerCardId, RunnerCardCategory newCategory)
    {
        RunnerCardCategory oldCategory = GetRunnerCardCategory(runnerCardId);
        if(oldCategory == newCategory || newCategory == RunnerCardCategory.None)
        {
            return false;
        }
        if(!TryGetRunnerCard(runnerCardId, out RunnerCardData card))
        {
            return false;
        }
        if(!TryGetEnumFromId(runnerCardId, out RunnerCardType type))
        {
            return false;
        }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if(!data.ActiveLibraryCards.ContainsKey(type))
        {
            data.ActiveLibraryCards.Add(type, false);
        }
        data.RunnerCards.Remove(type);
        data.BossCards.Remove(type);
        data.DefaultArenaCards.Remove(type);
        switch(newCategory)
        {
            case RunnerCardCategory.Runner:
                data.ActiveLibraryCards[type] = true;
                data.RunnerCards.Add(type, card);
                break;
            case RunnerCardCategory.Boss:
                data.ActiveLibraryCards[type] = true;
                data.BossCards.Add(type, card);
                break;
            case RunnerCardCategory.DefaultRunner:
                data.ActiveLibraryCards[type] = false;
                data.DefaultArenaCards.Add(type, card);
                break;
            case RunnerCardCategory.Inactive:
                data.ActiveLibraryCards[type] = false;
                break;
            default:
                Debug.LogError($"Case {newCategory} not handled when setting RunnerCardCategory!");
                return false;
        }

        return true;
    }

    public List<string> GetRunnerCardsForCategory(string catString)
    {
        List<string> toReturn = new List<string>();
        if(!Enum.TryParse(catString, out RunnerCardCategory category))
        {
            Debug.LogError($"The string {catString} does not parse to a RunnerCardCategory!");
            return toReturn; 
        }

        RunnerCardsData data = GetData<RunnerCardsData>();
        switch(category)
        {
            case RunnerCardCategory.Runner:
                foreach(RunnerCardType type in data.RunnerCards.Keys)
                {
                    if(!data.ActiveLibraryCards.ContainsKey(type) || !data.ActiveLibraryCards[type])
                    { continue; }
                    toReturn.Add(GetStringId(type));
                }
                break;
            case RunnerCardCategory.DefaultRunner:
                foreach(RunnerCardType type in data.DefaultArenaCards.Keys)
                {
                    toReturn.Add(GetStringId(type));
                }
                break;
            case RunnerCardCategory.Boss:
                foreach(RunnerCardType type in data.BossCards.Keys)
                {
                    if (!data.ActiveLibraryCards.ContainsKey(type) || !data.ActiveLibraryCards[type])
                    { continue; }
                    toReturn.Add(GetStringId(type));
                }
                break;
            case RunnerCardCategory.Inactive:
                foreach(RunnerCardType type in data.ActiveLibraryCards.Keys)
                {
                    if (data.ActiveLibraryCards[type] || data.DefaultArenaCards.ContainsKey(type))
                    { continue; }
                    toReturn.Add(GetStringId(type));
                }
                break;
            default:
                Debug.LogError($"Category {category} is not a list of RunnerCardType that is handled!");
                break;
        }

        return toReturn;
    }

    public bool GetIsCardActive(string cardId)
    {
        bool active = false;
        if(TryGetEnumFromId<RunnerCardType>(cardId, out RunnerCardType runnerType))
        {
            RunnerCardsData rdata = GetData<RunnerCardsData>();
            if(rdata.ActiveLibraryCards.TryGetValue(runnerType, out active))
            {
                return active;
            }
        }
        else if(TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType actionType))
        {
            ActionCardsData adata = GetData<ActionCardsData>();
            if(adata.ActiveLibraryCards.TryGetValue(actionType, out active))
            {
                return active;
            }
        }
        return false;
    }

    public bool SetActionCardActive(string cardId, bool active)
    {
        if(!TryGetEnumFromId<ActionCardType>(cardId, out ActionCardType type))
        {
            return false;
        }
        ActionCardsData data = GetData<ActionCardsData>();
        if (!data.ActiveLibraryCards.ContainsKey(type))
        {
            data.ActiveLibraryCards.Add(type, active);
        }
        data.ActiveLibraryCards[type] = active;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
    }

    public bool GetIsRunnerCardActive(string runnerCardId)
    {
        if (!TryGetEnumFromId<RunnerCardType>(runnerCardId, out RunnerCardType type))
        {
            return false;
        }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if(!data.ActiveLibraryCards.ContainsKey(type))
        {
            return false;
        }
        return data.ActiveLibraryCards[type];
    }

    public bool SetIsRunnerCardActive(string runnerCardId, bool active)
    {
        if (!TryGetEnumFromId<RunnerCardType>(runnerCardId, out RunnerCardType type))
        {
            return false;
        }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if(!data.ActiveLibraryCards.ContainsKey(type))
        {
            data.ActiveLibraryCards.Add(type, active);
        }
        data.ActiveLibraryCards[type] = active;
        Signals.RunnerCardUpdated.Send(runnerCardId);
        return true;
    }


    private bool TryGetRunnerCard(string id, out RunnerCardData card)
    {
        card = null;
        if (!Enum.TryParse<RunnerCardType>(id.Split(':')[1], out RunnerCardType type))
        {
            Debug.LogError($"Error! {id} is not a RunnerCardType!");
            return false;
        }
        RunnerCardsData data = GetData<RunnerCardsData>();
        if (!data.RunnerCards.TryGetValue(type, out card))
        {
            if (!data.BossCards.TryGetValue(type, out card))
            {
                if (!data.DefaultArenaCards.TryGetValue(type, out card))
                {
                    Debug.LogError($"Error! {type.ToString()} does not exist in data!");
                    return false;

                }
            }
        }
        return true;
    }

    public int GetRunnerCardCost(string id)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return 0;
        }
        return card.Cost;
    }

    public bool SetRunnerCardCost(string id, int cost)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return false;
        }
        card.Cost = cost;
        Signals.RunnerCardUpdated.Send(id);
        return true;
    }

    public RunnerDistribution GetRunnerCardDistribution(string id)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return RunnerDistribution.Standard;
        }
        return card.Distribution;
    }

    public bool SetRunnerCardDistribution(string id, RunnerDistribution dist)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return false;
        }
        card.Distribution = dist;
        Signals.RunnerCardUpdated.Send(id);
        return true;
    }

    public float GetRunnerCardInterval(string id)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return 0;
        }
        return card.Interval;
    }

    public bool SetRunnerCardInterval(string id, float interval)
    {
        if(!TryGetRunnerCard(id,out RunnerCardData card))
        {
            return false;
        }
        card.Interval = interval;
        Signals.RunnerCardUpdated.Send(id);
        return true;
    }

    /// <summary>
    /// returns the indices of different compositions, based on rank. Most often, "1".
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public List<int> GetRunnerCardUniqueCompositionRanks(string id)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return new List<int>(); 
        }

        return card.Units.Keys.ToList();
    }

    public bool SetRunnerCardRoster(string id, Dictionary<int,List<RunnerUnitType>> rosters)
    {
        if(!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return false;
        }
        card.Units = rosters;
        Signals.RunnerCardUpdated.Send(id);
        return true;
    }

    public string GetRunnerCardArt(string cardId)
    {
        if (!TryGetEnumFromId<RunnerCardType>(cardId, out RunnerCardType cardType))
        {
            Debug.LogError($"CardID {cardId} is not a valid name for a Runner Card!");
            return "";
        }
        ObjectsData data = GetData<ObjectsData>();
        ObjectType type = ObjectType.RunnerCardArt_Library;
        if (!data.GetObjectTypesForData(nameof(RunnerCardType)).Contains(type))
        {
            Debug.LogError($"Object type {type} is not one of the types that RunnerCards use!");
            return "";
        }
        data.ObjectMap[type].TryGetValue(cardType.ToString(), out string assetName);
        return assetName;
    }

    public bool IsRunnerCardArtDefault(string cardId)
    {
        if (!TryGetEnumFromId<RunnerCardType>(cardId, out RunnerCardType cardType))
        {
            Debug.LogError($"CardID {cardId} is not a valid name for a runner card!");
            return false;
        }
        ObjectsData data = GetData<ObjectsData>();
        ObjectType type = ObjectType.RunnerCardArt_Library;
        if (!data.GetObjectTypesForData(nameof(RunnerCardType)).Contains(type))
        {
            Debug.LogError($"Object type {type} is not one of the types that RunnerCards use!");
            return false;
        }
        if (!data.ObjectMap[type].ContainsKey(cardType.ToString()))
        {
            return false;
        }
        if(!data.DefaultValues.ContainsKey(type))
        {
            Debug.LogError($"ObjectType {type} does not have a default value!");
            return false;
        }
        return data.ObjectMap[type][cardType.ToString()] == data.DefaultValues[type];
    }    

    public bool SetRunnerCardArt(string cardId, string assetName)
    {
        if (!TryGetEnumFromId<RunnerCardType>(cardId, out RunnerCardType cardType))
        {
            Debug.LogError($"CardID {cardId} is not a valid name for a runner card!");
            return false;
        }
        ObjectsData data = GetData<ObjectsData>();
        ObjectType type = ObjectType.RunnerCardArt_Library;
        if (!data.GetObjectTypesForData(nameof(RunnerCardType)).Contains(type))
        {
            Debug.LogError($"Object type {type} is not one of the types that RunnerCards use!");
            return false;
        }
        if (!data.ObjectMap[type].ContainsKey(cardType.ToString()))
        {
            data.ObjectMap[type].Add(cardType.ToString(), "");
        }
        data.ObjectMap[type][cardType.ToString()] = assetName;
        Signals.ActionCardUpdated.Send(cardId);
        return true;
        
    }

    private List<string> GetRunnerCardRankComposition(string id, int rank)
    {
        List<string> result = new List<string>();
        if (!TryGetRunnerCard(id, out RunnerCardData card))
        {
            return result;
        }
        int practicalRank = rank;
        while (!card.Units.ContainsKey(practicalRank))
        {
            practicalRank--;
            if (practicalRank < 0)
            {
                Debug.LogError($"Error! No troop composition data found for runnerCard {id}!");
                return result;
            }
        }
        foreach(RunnerUnitType unit in card.Units[practicalRank])
        {
            result.Add(GetStringId<RunnerUnitType>(unit));
        }
        return result;
    }

    public int GetRunnerCardUnitCountForRank(string id, int rank)
    {
        return GetRunnerCardRankComposition(id, rank).Count;
    }

    public string GetRunnerCardUnitInRankComposition(string id, int rank, int index)
    {
        return GetRunnerCardRankComposition(id, rank)[index];
    }

    #endregion runnerCards

    #region runners

    #region ability-specific
    public RankableFloat GetRunnerAbilityRankableFloat(string runner, int abilityIndex, int effectIndex, string propertyName)
    {
        if(!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return null;
        }

        //https://stackoverflow.com/questions/972636/casting-a-variable-using-a-type-variable
        dynamic deffect = Convert.ChangeType(effect, BaseRunnerEffectData.GetClassType(effect.Type));

        if(!TryGetRankableField(deffect,propertyName,out RankableFloat rfloat))
        {
            return null;
        }
        return rfloat;
    }

    public bool SetRunnerAbilityRankableFloat(string runner, int abilityIndex, int effectIndex, string propertyName, RankableFloat incRFloat)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }

        if (!TryGetRankableField(effect, propertyName, out RankableFloat rfloat))
        {
            return false;
        }

        rfloat.SetTo(incRFloat);
        Signals.RunnerAbilityEffectSelected.Send(runner, abilityIndex, effectIndex);
        Signals.RunnerUnitUpdated.Send(runner);
        return true;
    }

    private bool TryGetRunnerAbilityField(string runner, int abilityIndex, int effectIndex, string fieldName, out object value)
    {
        value = null;
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }

        FieldInfo finfo = effect.GetType().GetField(fieldName);
        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field!");
            return false;
        }
        value = finfo.GetValue(effect);
        return true;
    }

    public ActionCardType GetRunnerAbilityPower(string runner, int abilityIndex, int effectIndex)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return ActionCardType.None;
        }

        if (!(effect is RunnerEffectSpawnPower))
        {
            return ActionCardType.None;
        }
        return ((RunnerEffectSpawnPower)effect).PowerId;
    }

    public bool SetRunnerAbilityPower(string runner, int abilityIndex, int effectIndex, ActionCardType power)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }

        if(!(effect is RunnerEffectSpawnPower))
        {
            Debug.LogError("This ability is not a SpawnPower ability!");
            return false;
        }

        if(!GetData<RunnerPowersData>().ActionCards.ContainsKey(power))
        {
            Debug.LogError($"Tried to assign {power} as a runner spawn power but it is not in the RunnerPowersData file!");
            return false; 
        }
        ((RunnerEffectSpawnPower)effect).PowerId = power;
        Signals.RunnerUnitUpdated.Send(runner);
        return true;
    }

    private bool TrySetRunnerAbilityField<T>(string runner, int abilityIndex, int effectIndex, string fieldName, T value)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }

        FieldInfo finfo = effect.GetType().GetField(fieldName);
        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field!");
            return false;
        }
        finfo.SetValue(effect, value);
        return true;
    }

    public T GetRunnerAbilityFieldValue<T>(string runner, int abilityIndex, int effectIndex, string fieldName)
    {
        if(!TryGetRunnerAbilityField(runner,abilityIndex, effectIndex, fieldName, out object value))
        {
            return default(T);
        }
        return (T)value;
    }

    public bool SetRunnerAbilityFieldValue<T>(string runner, int abilityIndex, int effectIndex, string fieldName, T value)
    {
        if(!TrySetRunnerAbilityField<T>(runner,abilityIndex, effectIndex, fieldName, value))
        {
            return false;
        }
        Signals.RunnerAbilityEffectSelected.Send(runner, abilityIndex, effectIndex);
        return true;
    }

    public MinionSpawnData GetRunnerAbilitySpawnData(string runner, int abilityIndex, int effectIndex, int spawnIndex)
    {
        if(!TryGetBaseRunnerAbilityEffect(runner,abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return null;
        }
        RunnerEffectSpawnUnits seffect = (RunnerEffectSpawnUnits)effect;
        if(seffect == null)
        {
            Debug.LogError("Found the effect but it was not a spawn units effect!");
            return null;
        }
        if (spawnIndex >= seffect.Minions.Count)
        {
            Debug.LogError($"Spawn units effect only had {seffect.Minions.Count} spawns but YOU tried to access index {spawnIndex}, ya doofus.");
            return null;
        }
        return seffect.Minions[spawnIndex];
    }

    public RunnerDefaults GetRunnerDefaults(string runnerId)
    {
        if (!TryGetRunner(runnerId, out RunnerUnitData runner))
        {
            return new RunnerDefaults();
        }
        return runner.Defaults;
    }

    public bool SetRunnerDefaults(string runnerId, RunnerDefaults defaults)
    {
        if (!TryGetRunner(runnerId, out RunnerUnitData runner))
        {
            return false;
        }
        runner.Defaults = defaults;
        Signals.RunnerUnitUpdated.Send(runnerId);
        return true;
    }

    public bool SetRunnerAbilitySpawnData(string runner, int abilityIndex, int effectIndex, int spawnIndex, MinionSpawnData spawn)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }
        RunnerEffectSpawnUnits seffect = (RunnerEffectSpawnUnits)effect;
        if (seffect == null)
        {
            Debug.LogError("Found the effect but it was not a spawn units effect!");
            return false;
        }
        if (spawnIndex >= seffect.Minions.Count)
        {
            Debug.LogError($"Spawn units effect only had {seffect.Minions.Count} spawns but YOU tried to access index {spawnIndex}, ya doofus.");
            return false;
        }
        seffect.Minions[spawnIndex] = spawn;
        Signals.RunnerUnitUpdated.Send(runner);
        Signals.RunnerAbilityEffectSelected.Send(runner, abilityIndex, effectIndex);
        return true;
    }

    public int GetRunnerAbilitySpawnCount(string runner, int abilityIndex, int effectIndex)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return 0;
        }
        RunnerEffectSpawnUnits seffect = (RunnerEffectSpawnUnits)effect;
        if (seffect == null)
        {
            Debug.LogError("Found the effect but it was not a spawn units effect!");
            return 0;
        }
        return seffect.Minions.Count;
    }

    public bool AddRunnerAbilitySpawnData(string runner, int abilityIndex, int effectIndex)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }
        RunnerEffectSpawnUnits seffect = (RunnerEffectSpawnUnits)effect;
        if (seffect == null)
        {
            Debug.LogError("Found the effect but it was not a spawn units effect!");
            return false;
        }
        seffect.Minions.Add(new MinionSpawnData());
        Signals.RunnerUnitUpdated.Send(runner);
        Signals.RunnerAbilityEffectSelected.Send(runner, abilityIndex, effectIndex);
        return true;
    }

    public bool RemoveRunnerAbilitySpawnData(string runner, int abilityIndex, int effectIndex, int spawnIndex)
    {
        if (!TryGetBaseRunnerAbilityEffect(runner, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return false;
        }
        RunnerEffectSpawnUnits seffect = (RunnerEffectSpawnUnits)effect;
        if (seffect == null)
        {
            Debug.LogError("Found the effect but it was not a spawn units effect!");
            return false;
        }
        if(spawnIndex < 0 || spawnIndex >= seffect.Minions.Count)
        {
            Debug.LogError("Spawn index was outside the bounds of the array!");
            return false;
        }
        seffect.Minions.RemoveAt(spawnIndex);
        Signals.RunnerUnitUpdated.Send(runner);
        Signals.RunnerAbilityEffectSelected.Send(runner, abilityIndex, effectIndex);
        return true;
    }

    #endregion ability-specific
    private bool TryGetRunner(string runnerName, out RunnerUnitData runner)
    {
        runner = null;
        if (!Enum.TryParse<RunnerUnitType>(runnerName.Split(':')[1], out RunnerUnitType type))
        {
            Debug.LogError($"Error! {runnerName} is not a RunnerUnitType!");
            return false;
        }

        if(!GetData<RunnersData>().Runners.TryGetValue(type,out runner))
        {
            Debug.LogError($"Error! {type.ToString()} does not exist in data!");
            return false;
        }

        return true;
    }

    private bool TryGetRunner(string runnerName, int level, int rank, out RunnerUnitData runner)
    {
        if (!TryGetRunner(runnerName, out runner))
        {
            return false;
        }

        float rankRatio = (float)rank / (float)MAX_RANK_INDEX;

        runner.SetLevel(level);
        runner.SetRank(rankRatio);
        return true;
    }
    
    private bool TryGetRunnerAbility(string runnerName, int abilityIndex, out RunnerAbilityData ability)
    {
        ability = null;
        if(!TryGetRunner(runnerName, out RunnerUnitData runner))
        {
            return false;
        }

        if(abilityIndex >= runner.Abilities.Count)
        {
            Debug.LogError($"Runner has {runner.Abilities.Count} abilities, but tried to access ability index {abilityIndex}!");
            return false;
        }

        ability = runner.Abilities[abilityIndex];
        return true;
    }

    public int GetRunnerAbilityCount(string runnerName)
    {
        if(!TryGetRunner(runnerName, out RunnerUnitData runner))
        {
            return 0;
        }
        return runner.Abilities.Count;
    }

    public RankableFloat GetRunnerAbilityCooldown(string runnerName, int abilityIndex)
    {
        if(!TryGetRunnerAbility(runnerName,abilityIndex,out RunnerAbilityData ability))
        {
            return null;
        }
        return ability.Cooldown;
    }

    public bool SetRunnerAbilityCooldown(string runnerName, int abilityIndex, RankableFloat rf)
    {
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }

        ability.Cooldown.SetTo(rf);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    private bool TryGetBaseRunnerAbilityEffect(string runnerName, int abilityIndex, int effectIndex, out BaseRunnerEffectData effect)
    {
        effect = default;
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }

        if (effectIndex >= ability.Effects.Count)
        {
            Debug.LogError($"Effect index {effectIndex} was out of range for ability index {abilityIndex} on {runnerName}!");
            return false;
        }
        effect = ability.Effects[effectIndex];
        return true;
    }

    public int GetBaseRunnerAbilityEffectCount(string runnerName, int abilityIndex)
    {
        if(!TryGetRunnerAbility(runnerName,abilityIndex, out RunnerAbilityData data))
        {
            return 0;
        }
        return data.Effects.Count;
    }

    public bool RemoveBaseRunnerAbilityEffect(string runnerName, int abilityIndex, int effectIndex)
    {
        if (!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }

        if (effectIndex >= ability.Effects.Count)
        {
            Debug.LogError($"Effect index {effectIndex} was out of range for ability index {abilityIndex} on {runnerName}!");
            return false;
        }
        ability.Effects.RemoveAt(effectIndex);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public bool AddBaseRunnerAbilityEffect(string runnerName, int abilityIndex)
    {
        if (!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }
        ability.Effects.Add(new RunnerEffectGainStatus());
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public RunnerAbilityType GetRunnerAbilityEffectType(string runnerName, int abilityIndex, int effectIndex)
    {
        if(!TryGetBaseRunnerAbilityEffect(runnerName, abilityIndex, effectIndex, out BaseRunnerEffectData effect))
        {
            return RunnerAbilityType.None;
        }
        return effect.Type;
    }

    private bool TryGetRunnerAbilityCondition(string runnerName, int abilityIndex, int conditionIndex, out AtomicRunnerAbilityConditionData condition)
    {
        condition = null;
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }
        if(conditionIndex >= ability.Conditions.Count )
        {
            Debug.LogError($"Target ability has {ability.Conditions.Count} conditions, but tried to access index {conditionIndex}!");
            return false;
        }
        condition = ability.Conditions[conditionIndex];
        return true;
    }

    public int GetRunnerAbilityConditionCount(string runnerName, int abilityIndex)
    {
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return 0;
        }
        return ability.Conditions.Count;
    }

    public RunnerAbilityTrigger GetRunnerAbilityConditionTrigger(string runnerName, int abilityIndex, int conditionindex)
    {
        if(!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionindex, out AtomicRunnerAbilityConditionData condition))
        {
            return RunnerAbilityTrigger.None;
        }
        return condition.TriggerType;
    }

    public bool SetRunnerAbilityConditionTrigger(string runnerName, int abilityIndex, int conditionIndex, RunnerAbilityTrigger trigger)
    {
        if(!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return false;
        }
        condition.TriggerType = trigger;
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public ConditionComparator GetRunnerAbilityConditionComparator(string runnerName, int abilityIndex, int conditionIndex)
    {
        if (!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return ConditionComparator.None;
        }
        return condition.PublicComparator;
    }

    public bool SetRunnerAbilityConditionComparator(string runnerName, int abilityIndex, int conditionIndex, ConditionComparator comparator)
    {
        if (!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return false;
        }
        Signals.RunnerUnitUpdated.Send(runnerName);
        condition.PublicComparator = comparator;
        return true;
    }

    public RunnerAbilityConditionValueType GetRunnerAbilityConditionValueType(string runnerName, int abilityIndex, int conditionIndex)
    {
        if (!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return RunnerAbilityConditionValueType.None;
        }
        return condition.PublicValueType;
    }

    public bool SetRunnerAbilityConditionValueType(string runnerName, int abilityIndex, int conditionIndex, RunnerAbilityConditionValueType valueType)
    {
        if (!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return false;
        }
        condition.PublicValueType = valueType;
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public RankableFloat GetRunnerAbilityConditionCompareTo(string runnerName, int abilityIndex, int conditionIndex)
    {
        if(!TryGetRunnerAbilityCondition(runnerName,abilityIndex,conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return null;
        }
        return condition.PublicCompareTo;
    }

    public bool SetRunnerAbilityConditionCompareTo(string runnerName, int abilityIndex, int conditionIndex, RankableFloat incRfloat)
    {
        if (!TryGetRunnerAbilityCondition(runnerName, abilityIndex, conditionIndex, out AtomicRunnerAbilityConditionData condition))
        {
            return false;
        }
        condition.PublicCompareTo.SetTo(incRfloat);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }



    public bool AddRunnerAbilityCondition(string runnerName, int abilityIndex)
    {
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }
        // adding the base class is an error, and should be detected as such.
        ability.Conditions.Add(new AtomicRunnerAbilityConditionData());
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public bool AddRunnerAbility(string runnerName)
    {
        if(!TryGetRunner(runnerName, out RunnerUnitData runner))
        {
            return false;
        }

        RunnerAbilityData ability = new RunnerAbilityData();
        //ability.Effects = new RunnerEffectGainStatus();
        runner.Abilities.Add(ability);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public bool RemoveRunnerAbility(string runnerName, int abilityIndex)
    {
        if(!TryGetRunner(runnerName, out RunnerUnitData runner))
        {
            return false;
        }
        if(abilityIndex >= runner.Abilities.Count)
        {
            return false;
        }
        runner.Abilities.RemoveAt(abilityIndex);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public bool SetRunnerAbilityEffect(string runnerName, int abilityIndex, int effectIndex, RunnerAbilityType type)
    {
        if(!TryGetRunnerAbility(runnerName, abilityIndex, out RunnerAbilityData ability))
        {
            return false;
        }

        if(effectIndex >= ability.Effects.Count || effectIndex < 0)
        {
            Debug.LogError($"Ability index {effectIndex} is out of range!");
            return false;
        }

        if(ability.Effects[effectIndex].Type == type)
        {
            return true;
        }

        switch(type)
        {
            case RunnerAbilityType.SpawnUnits:
                ability.Effects[effectIndex] = new RunnerEffectSpawnUnits();
                break;
            //case RunnerAbilityType.StunAttacker:
            case RunnerAbilityType.GainStatus:
                ability.Effects[effectIndex] = new RunnerEffectGainStatus();
                break;
            case RunnerAbilityType.SpawnPower:
                ability.Effects[effectIndex] = new RunnerEffectSpawnPower();
                break;
            case RunnerAbilityType.MovementSpeed:
                ability.Effects[effectIndex] = new RunnerEffectMoveSpeed();
                break;
            case RunnerAbilityType.Teleport:
                ability.Effects[effectIndex] = new RunnerEffectTeleport();
                break;
            case RunnerAbilityType.Heal:
                ability.Effects[effectIndex] = new RunnerEffectHeal();
                break;
            case RunnerAbilityType.RemoveStatus:
                ability.Effects[effectIndex] = new RunnerEffectRemoveStatus();
                break;
            case RunnerAbilityType.ModifyIncomingDurations:
                ability.Effects[effectIndex] = new RunnerEffectModifyIncomingDuration();
                break;
            case RunnerAbilityType.ModifyIncomingValue:
                ability.Effects[effectIndex] = new RunnerEffectModifyIncomingValue();
                break;
            case RunnerAbilityType.ModifyIncomingModification:
                ability.Effects[effectIndex] = new RunnerEffectModifyIncomingModification();
                break;
            case RunnerAbilityType.GainShields:
                ability.Effects[effectIndex] = new RunnerEffectGainShields();
                break;
            default:
                Debug.LogError($"Error! Not set up to switch ability effect to type {type}!");
                return false;
        }
        Signals.RunnerAbilityEffectSelected.Send(runnerName, abilityIndex, effectIndex);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }

    public bool RemoveRunnerAbilityCondition(string runnerName, int abilityIndex, int conditionIndex)
    {
        if(!TryGetRunnerAbility(runnerName,abilityIndex,out RunnerAbilityData ability))
        {
            return false;
        }
        if(conditionIndex >= ability.Conditions.Count)
        {
            return false;
        }

        ability.Conditions.RemoveAt(conditionIndex);
        Signals.RunnerUnitUpdated.Send(runnerName);
        return true;
    }


    #region silly threat stuff

    public float GetRunnerThreat(string id, int level, int rank)
    {
        // Base threat represents the amount of threat a unit brings to the table simply by being a
        // separate entity that needs to be targeted individually. This has a greater effect on small
        // units, which makes sense since smaller units are the most likely to experience overkill
        // and waste the opponents fight power.
        // This should be balanced against the idea that small units can be taken down by AOE more 
        // quickly.
        return BASE_THREAT + GetRunnerSpeed(id, level, rank) * GetRunnerHealth(id, level, rank);
    }

    public float GetRunnerSpeed(string id, int level, int rank)
    {
        if(!TryGetRunner(id, level, rank, out RunnerUnitData runner))
        {
            Debug.LogError($"Error! Could not find RunnerData for {id}");
            return 0f;
        }

        return runner.Speed.Value;
    }

    public float GetRunnerHealth(string id, int level, int rank)
    {
        if (!TryGetRunner(id, level, rank, out RunnerUnitData runner))
        {
            Debug.LogError($"Error! Could not find RunnerData for {id}");
            return 0f;
        }

        return runner.Health.Value;
    }

    public string GetRunnerPrefab(string id)
    {
        if(!TryGetEnumFromId<RunnerUnitType>(id, out RunnerUnitType type))
        {
            return "";
        }
        if (!GetData<ObjectsData>().ObjectMap[ObjectType.Runner].TryGetValue(type.ToString(), out string value))
        {
            return "";
        }
        return value;
    }

    public bool SetRunnerPrefab(string id, string prefab)
    {
        if (!TryGetEnumFromId<RunnerUnitType>(id, out RunnerUnitType type))
        {
            return false;
        }
        ObjectsData data = GetData<ObjectsData>();
        if (!data.ObjectMap[ObjectType.Runner].ContainsKey(type.ToString()))
        {
            data.ObjectMap[ObjectType.Runner].Add(type.ToString(), prefab);
        }
        data.ObjectMap[ObjectType.Runner][type.ToString()] = prefab;
        Signals.RunnerUnitUpdated.Send(id);
        return true;
    }

    public bool IsRunnerDefaultPrefab(string id)
    {
        if (!TryGetEnumFromId<RunnerUnitType>(id, out RunnerUnitType type))
        {
            return false;
        }
        if (!GetData<ObjectsData>().ObjectMap[ObjectType.Runner].TryGetValue(type.ToString(), out string value))
        {
            return false;
        }
        return value == DefaultObjectString(ObjectType.Runner);
    }

    #endregion
    #region ObjectsData
    private string DefaultObjectString(ObjectType type)
    {
        return GetData<ObjectsData>().DefaultValues[type];
    }
    #endregion
    #region rankableProperties
    private bool TryGetRankableField(RankClass rankable, string fieldName, out RankableFloat rfloat)
    {
        rfloat = null;
        if (rankable == null)
        {
            Debug.LogError("Passed Rankable was null!");
            return false;
        }
        FieldInfo finfo = rankable.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field!");
            return false;
        }

        rfloat = finfo.GetValue(rankable) as RankableFloat;
        if (rfloat == null)
        {
            Debug.LogWarning($"Field {fieldName} was found but it was not a Rankable field!");
            return false;
        }
        return true;
    }

    #region runner rankable properties

    private bool TryGetRunnerRankableField(string unitId, string fieldName, out RankableFloat prop)
    {
        prop = null;
        if (!TryGetRunner(unitId, 0, 0, out RunnerUnitData runner))
        {
            return false;
        }

        if(!TryGetRankableField(runner,fieldName,out prop))
        {
            return false;
        }

        
        return true;
    }
    #endregion runner rankable properties

    public RankableFloat GetRunnerRankableFloat(string unitId, string propertyName)
    {
        if (!TryGetRunnerRankableField(unitId, propertyName, out RankableFloat prop))
        {
            return null;
        }
        return prop;
    }

    public bool SetRunnerRankableFloat(string unitId, string propertyName, RankableFloat incRfloat)
    {
        if(!TryGetRunnerRankableField(unitId,propertyName,out RankableFloat prop))
        {
            return false;
        }
        prop.SetTo(incRfloat);
        Signals.RunnerUnitUpdated.Send(unitId);
        return true;
    }

    public RankableFloat GetAtomicEffectRankableFloat(string actionCard, XowerType xowerType, bool primaryList, int index, string fieldName)
    {
        if(!TryGetXowerAtomicEffect(actionCard, xowerType,0, primaryList, index, out AtomicActionEffectData aeffect))
        {
            return null;
        }

        if(!TryGetRankableField(aeffect.ActionEffect,fieldName,out RankableFloat rfloat))
        {
            return null;
        }
        return rfloat;
    }

    public bool SetAtomicEffectRankableFloat(string actionCard, XowerType xowerType, bool primaryList, int index, string fieldName, RankableFloat incRfloat)
    {
        if(!TryGetXowerAtomicEffect(actionCard,xowerType,0,primaryList,index, out AtomicActionEffectData aeData))
        {
            return false;
        }

        if (!TryGetRankableField(aeData.ActionEffect, fieldName, out RankableFloat rfloat))
        {
            return false;
        }

        rfloat.SetTo(incRfloat);
        Signals.ActionCardUpdated.Send(actionCard);

        return true;
    }

    public RankableFloat GetAtomicConditionRankableFloat(string actionCard, XowerType xowerType, bool primaryList, int effectIndex, int conditionIndex, string fieldName)
    {
        if(!TryGetXowerAtomicEffectAtomicCondition(actionCard,xowerType,0,primaryList,effectIndex,conditionIndex, out AtomicActionConditionData acdata))
        {
            return null;
        }

        if(!TryGetRankableField(acdata,fieldName, out RankableFloat rfloat))
        {
            return null;
        }
        return rfloat;
    }

    public bool SetAtomicConditionRankableFloat(string actionCard, XowerType xowerType, bool primaryList, int effectIndex, int conditionIndex, string fieldName, RankableFloat incRfloat)
    {
        if (!TryGetXowerAtomicEffectAtomicCondition(actionCard, xowerType, 0, primaryList, effectIndex, conditionIndex, out AtomicActionConditionData acdata))
        {
            return false;
        }

        if (!TryGetRankableField(acdata, fieldName, out RankableFloat rfloat))
        {
            return false;
        }

        rfloat.SetTo(incRfloat);
        Signals.ActionCardUpdated.Send(actionCard);
        return true;
    }

    #endregion
    #endregion runners
    #region AI
    public List<AIPropertyType> GetAIValues()
    {
        List<AIPropertyType> values = GetEnumList<AIPropertyType>();
        values.Remove(AIPropertyType.None);
        return values;
    }

    public float GetAIValueForCard(ActionCardType type, AIPropertyType property)
    {
        AIData data = GetData<AIData>();
        if(!data.Towers.ContainsKey(type))
        {
            data.Towers.Add(type, new Dictionary<AIPropertyType, float>());
        }
        if (!data.Towers[type].ContainsKey(property))
        {
            data.Towers[type].Add(property, 0);
        }
        return data.Towers[type][property];
    }

    public bool SetAIValueForCard(ActionCardType type, AIPropertyType property, float value)
    {
        AIData data = GetData<AIData>();
        if (!data.Towers.ContainsKey(type))
        {
            data.Towers.Add(type, new Dictionary<AIPropertyType, float>());
        }
        if (!data.Towers[type].ContainsKey(property))
        {
            data.Towers[type].Add(property, 0);
        }
        data.Towers[type][property] = value;
        Signals.AIValuesUpdated.Send(GetStringId(type));
        return true;
    }

    public float GetAIValueForCard(RunnerCardType type, AIPropertyType property)
    {
        AIData data = GetData<AIData>();
        if (!data.Runners.ContainsKey(type))
        {
            data.Runners.Add(type, new Dictionary<AIPropertyType, float>());
        }
        if (!data.Runners[type].ContainsKey(property))
        {
            data.Runners[type].Add(property, 0);
        }
        return data.Runners[type][property];
    }

    public bool SetAIValueForCard(RunnerCardType type, AIPropertyType property, float value)
    {
        AIData data = GetData<AIData>();
        if (!data.Runners.ContainsKey(type))
        {
            data.Runners.Add(type, new Dictionary<AIPropertyType, float>());
        }
        if (!data.Runners[type].ContainsKey(property))
        {
            data.Runners[type].Add(property, 0);
        }
        data.Runners[type][property] = value;
        Signals.AIValuesUpdated.Send(GetStringId(type));
        return true;
    }
    #endregion AI
    #region rounds
    private bool TryGetRoundsData(string key, out RoundsData round)
    {
        return GetData<ArenasData>().RoundsDatas.TryGetValue(key, out round);
    }

    public bool SetRoundsDataKey(string oldKey, string newKey)
    {
        ArenasData data = GetData<ArenasData>();
        if(!data.RoundsDatas.ContainsKey(oldKey) || data.RoundsDatas.ContainsKey(newKey))
        { return false; }
        data.RoundsDatas.Add(newKey, data.RoundsDatas[oldKey]);
        data.RoundsDatas.Remove(oldKey);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public bool DuplicateRoundsKey(string roundsKey)
    {
        ArenasData data = GetData<ArenasData>();
        if(!data.RoundsDatas.TryGetValue(roundsKey,out RoundsData rounds))
        {
            return false;
        }
        RoundsData newRounds = TRDataUtils.DeepCopy<RoundsData>(rounds);
        int dupeIndex = 2;
        while(data.RoundsDatas.ContainsKey(DupeKey(roundsKey,dupeIndex)))
        {
            dupeIndex++;
        }
        data.RoundsDatas.Add(DupeKey(roundsKey, dupeIndex), newRounds);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public bool AddRounds()
    {
        ArenasData data = GetData<ArenasData>();
        string roundsKey = "newRounds";
        if (!data.RoundsDatas.TryGetValue(roundsKey, out RoundsData rounds))
        {
            return false;
        }
        RoundsData newRounds = new RoundsData();
        int dupeIndex = 2;
        while (data.RoundsDatas.ContainsKey(DupeKey(roundsKey, dupeIndex)))
        {
            dupeIndex++;
        }
        data.RoundsDatas.Add(DupeKey(roundsKey, dupeIndex), newRounds);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public bool RemoveRounds(string roundsKey)
    {
        ArenasData data = GetData<ArenasData>();
        if (!data.RoundsDatas.TryGetValue(roundsKey, out RoundsData rounds))
        {
            return false;
        }
        data.RoundsDatas.Remove(roundsKey);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    private string DupeKey(string name, int index)
    {
        return $"{name}({index})";
    }

    private bool TryGetRoundData(string roundsKey, int roundIndex, out RoundData round)
    {
        round = null;
        if(!TryGetRoundsData(roundsKey,out RoundsData rounds))
        {
            Debug.LogError($"Could not find roundsData with name {roundsKey}");
            return false;
        }
        if (roundIndex < 0 || roundIndex >= rounds.Rounds.Count)
        {
            Debug.LogError($"Tried to get round index {roundIndex} when there are only {rounds.Rounds.Count} rounds!");
            return false;
        }
        round = rounds.Rounds[roundIndex];
        return true;
    }

    public object GetRoundValue(string roundsKey, int index, string fieldName)
    {

        if(!TryGetRoundData(roundsKey,index,out RoundData round))
        { return default; }
        
        FieldInfo finfo = round.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field!");
            return default;
        }

        return finfo.GetValue(round);
    }

    public bool SetRoundValue(string roundsKey, int index, string fieldName, object value)
    {
        if (!TryGetRoundData(roundsKey, index, out RoundData round))
        { return false; }

        FieldInfo finfo = round.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field!");
            return false;
        }

        finfo.SetValue(round,value);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public object GetFinalRoundValue(string roundsKey, string fieldName)
    {
        if (!TryGetRoundsData(roundsKey, out RoundsData rounds))
        { return default; }

        FieldInfo finfo = rounds.FinalRound.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field in the FinalRound class!");
            return default;
        }

        return finfo.GetValue(rounds.FinalRound);
    }

    public bool SetFinalRoundValue(string roundsKey, string fieldName, object value)
    {
        if (!TryGetRoundsData(roundsKey, out RoundsData rounds))
        { return false; }

        FieldInfo finfo = rounds.FinalRound.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field in the FinalRound class!");
            return false;
        }

        finfo.SetValue(rounds.FinalRound, value);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public object GetRoundsValue(string key, string fieldName)
    {
        if (!TryGetRoundsData(key, out RoundsData rounds))
        { return default; }

        FieldInfo finfo = rounds.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field in the RoundsData class!");
            return default;
        }

        return finfo.GetValue(rounds);
    }

    public bool SetRoundsValue<T>(string roundsKey, string fieldName, T value)
    {
        if (!TryGetRoundsData(roundsKey, out RoundsData rounds))
        { return false; }

        FieldInfo finfo = rounds.GetType().GetField(fieldName);

        if (finfo == null)
        {
            Debug.LogWarning($"The name {fieldName} is not the name of a field in the RoundsData class!");
            return false;
        }

        finfo.SetValue(rounds, value);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public int GetRoundsCount(string roundsKey)
    {
        // really, this should always be 3. This function is just so we can detect errors to fix manually.
        if (!TryGetRoundsData(roundsKey, out RoundsData rounds))
        {
            return 0;
        }
        return rounds.Rounds.Count;
    }

    public List<string> GetRoundKeys()
    {
        ArenasData data = GetData<ArenasData>();
        return data.RoundsDatas.Keys.ToList();
    }

    #region supply changes/ boss spawns
    public int GetSupplyChangesCount(string roundsKey, int roundIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return 0; }
        return round.SupplyChanges.Count;
    }

    public bool RemoveSupplyChange(string roundsKey, int roundIndex, int supplyIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        if (supplyIndex < 0 || supplyIndex >= round.SupplyChanges.Count)
        { return false; }
        round.SupplyChanges.RemoveAt(supplyIndex);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public bool AddSupplyChange(string roundsKey, int roundIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        round.SupplyChanges.Add(new WaveSupplyChange());
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public WaveSupplyChange GetSupplyChange(string roundsKey, int roundIndex, int supplyIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return new WaveSupplyChange(); }
        if (supplyIndex < 0 || supplyIndex >= round.SupplyChanges.Count)
        { return new WaveSupplyChange(); }
        return round.SupplyChanges[supplyIndex];
    }

    public bool SetSupplyChange(string roundsKey, int roundIndex, int supplyIndex, WaveSupplyChange supplyChange)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        if (supplyIndex < 0 || supplyIndex >= round.SupplyChanges.Count)
        { return false; }
        round.SupplyChanges[supplyIndex] = supplyChange;
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public int GetBossTimesCount(string roundsKey, int roundIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return 0; }
        return round.BossSpawnTimes.Count;
    }

    public bool RemoveBossTime(string roundsKey, int roundIndex, int bossTimeIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        if (bossTimeIndex < 0 || bossTimeIndex >= round.BossSpawnTimes.Count)
        { return false; }
        round.BossSpawnTimes.RemoveAt(bossTimeIndex);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public bool AddBossTime(string roundsKey, int roundIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        round.BossSpawnTimes.Add(0);
        Signals.RoundsDataChanged.Send();
        return true;
    }

    public float GetBossTime(string roundsKey, int roundIndex, int bossTimeIndex)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return 0; }
        if (bossTimeIndex < 0 || bossTimeIndex >= round.BossSpawnTimes.Count)
        { return 0; }
        return round.BossSpawnTimes[bossTimeIndex];
    }

    public bool SetBossTime(string roundsKey, int roundIndex, int bossTimeIndex, float bossTime)
    {
        if (!TryGetRoundData(roundsKey, roundIndex, out RoundData round))
        { return false; }
        if (bossTimeIndex < 0 || bossTimeIndex >= round.BossSpawnTimes.Count)
        { return false; }
        round.BossSpawnTimes[bossTimeIndex] = bossTime;
        Signals.RoundsDataChanged.Send();
        return true;
    }
    #endregion supply changes/boss spawns
    #endregion
}

public enum RunnerCardCategory
{
    None,
    Runner,
    DefaultRunner,
    Boss,
    Inactive
}

