using System.ComponentModel;

namespace TowerForge
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabFormControl1 = new DevExpress.XtraBars.TabFormControl();
            this.tabFormPage1 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer1 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.tl_stati = new StatusTree();
            this.tl_runnerPowers = new RunnerPowersList();
            this.tl_powerEffects = new PowerEffectsTree();
            this.tl_powerDetails = new PowerDetailTree();
            this.tl_actionCards = new ActionCardsTree();
            this.tb_actionCardLevel = new DevExpress.XtraEditors.TrackBarControl();
            this.xowerSwitch = new DevExpress.XtraEditors.ToggleSwitch();
            this.tb_newActionType = new DevExpress.XtraEditors.TextEdit();
            this.b_createNewAction = new DevExpress.XtraEditors.SimpleButton();
            this.tl_xowerEffects = new TowerEffectsTree();
            this.tl_xowerDetail = new TowerDetailTree();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem4 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem16 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem18 = new DevExpress.XtraLayout.SplitterItem();
            this.tabFormPage2 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer2 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.tl_runnerPowerEffects = new RunnerPowerEffectsTree();
            this.tl_runnerPowerDetails = new RunnerPowerDetailTree();
            this.b_createNewRunnerUnit = new DevExpress.XtraEditors.SimpleButton();
            this.tb_newRunnerUnit = new DevExpress.XtraEditors.TextEdit();
            this.tl_abilityDetail = new RunnerAbilityTree();
            this.b_createNewRunnerCard = new DevExpress.XtraEditors.SimpleButton();
            this.tb_newRunnerCard = new DevExpress.XtraEditors.TextEdit();
            this.tb_runnerRank = new DevExpress.XtraEditors.TrackBarControl();
            this.tb_runnerLevel = new DevExpress.XtraEditors.TrackBarControl();
            this.tl_runnerUnits = new UnitsTree();
            this.tl_runnerUnitDetails = new RunnerDetailList();
            this.tl_runnerCardRoster = new RunnerCardRosterTree();
            this.tl_runnerCards = new RunnerCardTree();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem6 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem8 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem7 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem5 = new DevExpress.XtraLayout.SplitterItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem17 = new DevExpress.XtraLayout.SplitterItem();
            this.tabFormPage5 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer5 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.tl_roundsTree = new RoundsTree();
            this.tl_arenaCards = new ArenaCardsTree();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem11 = new DevExpress.XtraLayout.SplitterItem();
            this.tabFormPage3 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer3 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.b_changeStream = new DevExpress.XtraEditors.SimpleButton();
            this.l_currentStream = new DevExpress.XtraEditors.LabelControl();
            this.tl_userDecks = new UserDecks();
            this.tl_userStats = new UserStatsTree();
            this.tl_rarityTree = new RarityTree();
            this.tl_chestList = new ChestListTree();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem9 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem10 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem12 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem13 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabFormPage6 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer6 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.tl_simpleDeals = new SimpleDailyDealsList();
            this.tl_dailyDeals = new DailyDealsList();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem19 = new DevExpress.XtraLayout.SplitterItem();
            this.tabFormPage7 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer7 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl7 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabFormPage4 = new DevExpress.XtraBars.TabFormPage();
            this.tabFormContentContainer4 = new DevExpress.XtraBars.TabFormContentContainer();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.tl_xowerUtility = new XowerUtilityTree();
            this.tl_xowerAttack = new XowerAttackTree();
            this.tl_xowerTargets = new XowerTargetingTree();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem14 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem15 = new DevExpress.XtraLayout.SplitterItem();
            this.tl_aiValues = new AIValueTree();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).BeginInit();
            this.tabFormContentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_stati)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_powerEffects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_powerDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_actionCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_actionCardLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_actionCardLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xowerSwitch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newActionType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerEffects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem18)).BeginInit();
            this.tabFormContentContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowerEffects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowerDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newRunnerUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_abilityDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newRunnerCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerRank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerRank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerUnitDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerCardRoster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem17)).BeginInit();
            this.tabFormContentContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_roundsTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_arenaCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem11)).BeginInit();
            this.tabFormContentContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_userDecks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_userStats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_rarityTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_chestList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            this.tabFormContentContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_simpleDeals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_dailyDeals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem19)).BeginInit();
            this.tabFormContentContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).BeginInit();
            this.layoutControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            this.tabFormContentContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerUtility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerAttack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerTargets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_aiValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            this.SuspendLayout();
            // 
            // tabFormControl1
            // 
            this.tabFormControl1.Location = new System.Drawing.Point(0, 0);
            this.tabFormControl1.Name = "tabFormControl1";
            this.tabFormControl1.Pages.Add(this.tabFormPage1);
            this.tabFormControl1.Pages.Add(this.tabFormPage2);
            this.tabFormControl1.Pages.Add(this.tabFormPage5);
            this.tabFormControl1.Pages.Add(this.tabFormPage3);
            this.tabFormControl1.Pages.Add(this.tabFormPage6);
            this.tabFormControl1.Pages.Add(this.tabFormPage7);
            this.tabFormControl1.Pages.Add(this.tabFormPage4);
            this.tabFormControl1.SelectedPage = this.tabFormPage7;
            this.tabFormControl1.ShowAddPageButton = false;
            this.tabFormControl1.ShowTabCloseButtons = false;
            this.tabFormControl1.Size = new System.Drawing.Size(2534, 64);
            this.tabFormControl1.TabForm = this;
            this.tabFormControl1.TabIndex = 0;
            this.tabFormControl1.TabStop = false;
            this.tabFormControl1.OuterFormCreated += new DevExpress.XtraBars.OuterFormCreatedEventHandler(this.tabFormControl1_OuterFormCreated);
            // 
            // tabFormPage1
            // 
            this.tabFormPage1.ContentContainer = this.tabFormContentContainer1;
            this.tabFormPage1.Name = "tabFormPage1";
            this.tabFormPage1.Text = "ActionCards";
            // 
            // tabFormContentContainer1
            // 
            this.tabFormContentContainer1.Controls.Add(this.layoutControl1);
            this.tabFormContentContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer1.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer1.Name = "tabFormContentContainer1";
            this.tabFormContentContainer1.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer1.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.tl_stati);
            this.layoutControl1.Controls.Add(this.tl_runnerPowers);
            this.layoutControl1.Controls.Add(this.tl_powerEffects);
            this.layoutControl1.Controls.Add(this.tl_powerDetails);
            this.layoutControl1.Controls.Add(this.tl_actionCards);
            this.layoutControl1.Controls.Add(this.tb_actionCardLevel);
            this.layoutControl1.Controls.Add(this.xowerSwitch);
            this.layoutControl1.Controls.Add(this.tb_newActionType);
            this.layoutControl1.Controls.Add(this.b_createNewAction);
            this.layoutControl1.Controls.Add(this.tl_xowerEffects);
            this.layoutControl1.Controls.Add(this.tl_xowerDetail);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(5746, 1241, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // tl_stati
            // 
            this.tl_stati.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_stati.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_stati.Location = new System.Drawing.Point(12, 12);
            this.tl_stati.Name = "tl_stati";
            this.tl_stati.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_stati.OptionsBehavior.Editable = false;
            this.tl_stati.OptionsBehavior.ShowToolTips = false;
            this.tl_stati.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_stati.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_stati.OptionsMenu.EnableNodeMenu = false;
            this.tl_stati.OptionsSelection.MultiSelect = true;
            this.tl_stati.OptionsView.AutoWidth = false;
            this.tl_stati.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_stati.RootName = "Root";
            this.tl_stati.Size = new System.Drawing.Size(227, 1110);
            this.tl_stati.TabIndex = 17;
            this.tl_stati.TreeLevelWidth = 12;
            // 
            // tl_runnerPowers
            // 
            this.tl_runnerPowers.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerPowers.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerPowers.Caption = "RunnerPowers";
            this.tl_runnerPowers.Location = new System.Drawing.Point(255, 618);
            this.tl_runnerPowers.Name = "tl_runnerPowers";
            this.tl_runnerPowers.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerPowers.OptionsBehavior.Editable = false;
            this.tl_runnerPowers.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerPowers.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerPowers.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerPowers.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerPowers.OptionsSelection.MultiSelect = true;
            this.tl_runnerPowers.OptionsView.AutoWidth = false;
            this.tl_runnerPowers.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerPowers.OptionsView.ShowCaption = true;
            this.tl_runnerPowers.RootName = "Root";
            this.tl_runnerPowers.Size = new System.Drawing.Size(689, 504);
            this.tl_runnerPowers.TabIndex = 16;
            this.tl_runnerPowers.TreeLevelWidth = 12;
            // 
            // tl_powerEffects
            // 
            this.tl_powerEffects.Appearance.Caption.Font = new System.Drawing.Font("Tahoma", 16F);
            this.tl_powerEffects.Appearance.Caption.Options.UseFont = true;
            this.tl_powerEffects.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_powerEffects.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_powerEffects.Caption = "Power Effects";
            this.tl_powerEffects.Location = new System.Drawing.Point(1713, 526);
            this.tl_powerEffects.Name = "tl_powerEffects";
            this.tl_powerEffects.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_powerEffects.OptionsBehavior.Editable = false;
            this.tl_powerEffects.OptionsBehavior.ShowToolTips = false;
            this.tl_powerEffects.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_powerEffects.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_powerEffects.OptionsMenu.EnableNodeMenu = false;
            this.tl_powerEffects.OptionsSelection.MultiSelect = true;
            this.tl_powerEffects.OptionsView.AutoWidth = false;
            this.tl_powerEffects.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_powerEffects.OptionsView.ShowCaption = true;
            this.tl_powerEffects.RootName = "Root";
            this.tl_powerEffects.Size = new System.Drawing.Size(809, 596);
            this.tl_powerEffects.TabIndex = 15;
            this.tl_powerEffects.TreeLevelWidth = 12;
            // 
            // tl_powerDetails
            // 
            this.tl_powerDetails.Appearance.Caption.Font = new System.Drawing.Font("Tahoma", 16F);
            this.tl_powerDetails.Appearance.Caption.Options.UseFont = true;
            this.tl_powerDetails.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_powerDetails.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_powerDetails.Caption = "Power Properties";
            this.tl_powerDetails.Location = new System.Drawing.Point(1713, 12);
            this.tl_powerDetails.Name = "tl_powerDetails";
            this.tl_powerDetails.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_powerDetails.OptionsBehavior.Editable = false;
            this.tl_powerDetails.OptionsBehavior.ShowToolTips = false;
            this.tl_powerDetails.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_powerDetails.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_powerDetails.OptionsMenu.EnableNodeMenu = false;
            this.tl_powerDetails.OptionsSelection.MultiSelect = true;
            this.tl_powerDetails.OptionsView.AutoWidth = false;
            this.tl_powerDetails.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_powerDetails.OptionsView.ShowCaption = true;
            this.tl_powerDetails.RootName = "Root";
            this.tl_powerDetails.Size = new System.Drawing.Size(809, 498);
            this.tl_powerDetails.TabIndex = 14;
            this.tl_powerDetails.TreeLevelWidth = 12;
            // 
            // tl_actionCards
            // 
            this.tl_actionCards.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_actionCards.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_actionCards.Caption = "Action Cards";
            this.tl_actionCards.Location = new System.Drawing.Point(255, 89);
            this.tl_actionCards.Name = "tl_actionCards";
            this.tl_actionCards.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_actionCards.OptionsBehavior.Editable = false;
            this.tl_actionCards.OptionsBehavior.ShowToolTips = false;
            this.tl_actionCards.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_actionCards.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_actionCards.OptionsMenu.EnableNodeMenu = false;
            this.tl_actionCards.OptionsSelection.MultiSelect = true;
            this.tl_actionCards.OptionsView.AutoWidth = false;
            this.tl_actionCards.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_actionCards.OptionsView.ShowCaption = true;
            this.tl_actionCards.RootName = "Root";
            this.tl_actionCards.Size = new System.Drawing.Size(689, 513);
            this.tl_actionCards.TabIndex = 13;
            this.tl_actionCards.TreeLevelWidth = 12;
            // 
            // tb_actionCardLevel
            // 
            this.tb_actionCardLevel.EditValue = null;
            this.tb_actionCardLevel.Location = new System.Drawing.Point(343, 40);
            this.tb_actionCardLevel.Name = "tb_actionCardLevel";
            this.tb_actionCardLevel.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tb_actionCardLevel.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tb_actionCardLevel.Size = new System.Drawing.Size(601, 45);
            this.tb_actionCardLevel.StyleController = this.layoutControl1;
            this.tb_actionCardLevel.TabIndex = 12;
            this.tb_actionCardLevel.EditValueChanged += new System.EventHandler(this.tb_actionCardLevel_EditValueChanged);
            // 
            // xowerSwitch
            // 
            this.xowerSwitch.Location = new System.Drawing.Point(730, 12);
            this.xowerSwitch.Name = "xowerSwitch";
            this.xowerSwitch.Properties.OffText = "Towers";
            this.xowerSwitch.Properties.OnText = "Powers";
            this.xowerSwitch.Size = new System.Drawing.Size(214, 24);
            this.xowerSwitch.StyleController = this.layoutControl1;
            this.xowerSwitch.TabIndex = 11;
            this.xowerSwitch.Toggled += new System.EventHandler(this.xowerSwitch_Toggled);
            // 
            // tb_newActionType
            // 
            this.tb_newActionType.Location = new System.Drawing.Point(343, 12);
            this.tb_newActionType.Name = "tb_newActionType";
            this.tb_newActionType.Size = new System.Drawing.Size(185, 20);
            this.tb_newActionType.StyleController = this.layoutControl1;
            this.tb_newActionType.TabIndex = 10;
            // 
            // b_createNewAction
            // 
            this.b_createNewAction.Location = new System.Drawing.Point(532, 12);
            this.b_createNewAction.Name = "b_createNewAction";
            this.b_createNewAction.Size = new System.Drawing.Size(194, 22);
            this.b_createNewAction.StyleController = this.layoutControl1;
            this.b_createNewAction.TabIndex = 9;
            this.b_createNewAction.Text = "Create New Action";
            this.b_createNewAction.Click += new System.EventHandler(this.b_createNewAction_Click);
            // 
            // tl_xowerEffects
            // 
            this.tl_xowerEffects.Appearance.Caption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_xowerEffects.Appearance.Caption.Options.UseFont = true;
            this.tl_xowerEffects.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_xowerEffects.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_xowerEffects.Caption = "Tower Effects";
            this.tl_xowerEffects.FixedLineWidth = 8;
            this.tl_xowerEffects.Location = new System.Drawing.Point(960, 667);
            this.tl_xowerEffects.Name = "tl_xowerEffects";
            this.tl_xowerEffects.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_xowerEffects.OptionsBehavior.Editable = false;
            this.tl_xowerEffects.OptionsBehavior.ShowToolTips = false;
            this.tl_xowerEffects.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_xowerEffects.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_xowerEffects.OptionsMenu.EnableNodeMenu = false;
            this.tl_xowerEffects.OptionsSelection.MultiSelect = true;
            this.tl_xowerEffects.OptionsView.AutoWidth = false;
            this.tl_xowerEffects.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_xowerEffects.OptionsView.ShowCaption = true;
            this.tl_xowerEffects.RootName = "Xowers";
            this.tl_xowerEffects.Size = new System.Drawing.Size(737, 455);
            this.tl_xowerEffects.TabIndex = 8;
            this.tl_xowerEffects.TreeLevelWidth = 12;
            // 
            // tl_xowerDetail
            // 
            this.tl_xowerDetail.Appearance.Caption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_xowerDetail.Appearance.Caption.Options.UseFont = true;
            this.tl_xowerDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_xowerDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_xowerDetail.AppearancePrint.Caption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tl_xowerDetail.AppearancePrint.Caption.Options.UseFont = true;
            this.tl_xowerDetail.Caption = "Tower Properties";
            this.tl_xowerDetail.Location = new System.Drawing.Point(960, 12);
            this.tl_xowerDetail.Name = "tl_xowerDetail";
            this.tl_xowerDetail.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_xowerDetail.OptionsBehavior.Editable = false;
            this.tl_xowerDetail.OptionsBehavior.ShowToolTips = false;
            this.tl_xowerDetail.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_xowerDetail.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_xowerDetail.OptionsMenu.EnableNodeMenu = false;
            this.tl_xowerDetail.OptionsSelection.MultiSelect = true;
            this.tl_xowerDetail.OptionsView.AutoWidth = false;
            this.tl_xowerDetail.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_xowerDetail.OptionsView.ShowCaption = true;
            this.tl_xowerDetail.RootName = "Root";
            this.tl_xowerDetail.Size = new System.Drawing.Size(737, 639);
            this.tl_xowerDetail.TabIndex = 7;
            this.tl_xowerDetail.TreeLevelWidth = 12;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.splitterItem2,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.LayoutControlItem13,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.splitterItem1,
            this.splitterItem4,
            this.splitterItem16,
            this.layoutControlItem4,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.splitterItem3,
            this.splitterItem18});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(2534, 1134);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.tl_xowerEffects;
            this.layoutControlItem5.Location = new System.Drawing.Point(948, 655);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(741, 459);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.Location = new System.Drawing.Point(936, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(12, 1114);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.b_createNewAction;
            this.layoutControlItem6.Location = new System.Drawing.Point(520, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(198, 28);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.tb_newActionType;
            this.layoutControlItem7.Location = new System.Drawing.Point(243, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(277, 28);
            this.layoutControlItem7.Text = "New Action";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.xowerSwitch;
            this.layoutControlItem12.Location = new System.Drawing.Point(718, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(218, 28);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.tb_actionCardLevel;
            this.LayoutControlItem13.Location = new System.Drawing.Point(243, 28);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(693, 49);
            this.LayoutControlItem13.Text = "Action Card Level";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.tl_actionCards;
            this.layoutControlItem26.Location = new System.Drawing.Point(243, 77);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(693, 517);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.tl_powerDetails;
            this.layoutControlItem27.Location = new System.Drawing.Point(1701, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(813, 502);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.tl_powerEffects;
            this.layoutControlItem28.Location = new System.Drawing.Point(1701, 514);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(813, 600);
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(1689, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(12, 1114);
            // 
            // splitterItem4
            // 
            this.splitterItem4.AllowHotTrack = true;
            this.splitterItem4.Location = new System.Drawing.Point(948, 643);
            this.splitterItem4.Name = "splitterItem4";
            this.splitterItem4.Size = new System.Drawing.Size(741, 12);
            // 
            // splitterItem16
            // 
            this.splitterItem16.AllowHotTrack = true;
            this.splitterItem16.Location = new System.Drawing.Point(1701, 502);
            this.splitterItem16.Name = "splitterItem16";
            this.splitterItem16.Size = new System.Drawing.Size(813, 12);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.tl_xowerDetail;
            this.layoutControlItem4.Location = new System.Drawing.Point(948, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(741, 643);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.tl_runnerPowers;
            this.layoutControlItem35.Location = new System.Drawing.Point(243, 606);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(693, 508);
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.tl_stati;
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(231, 1114);
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextVisible = false;
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.Location = new System.Drawing.Point(243, 594);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(693, 12);
            // 
            // splitterItem18
            // 
            this.splitterItem18.AllowHotTrack = true;
            this.splitterItem18.Location = new System.Drawing.Point(231, 0);
            this.splitterItem18.Name = "splitterItem18";
            this.splitterItem18.Size = new System.Drawing.Size(12, 1114);
            // 
            // tabFormPage2
            // 
            this.tabFormPage2.ContentContainer = this.tabFormContentContainer2;
            this.tabFormPage2.Name = "tabFormPage2";
            this.tabFormPage2.Text = "Runners";
            // 
            // tabFormContentContainer2
            // 
            this.tabFormContentContainer2.Controls.Add(this.layoutControl2);
            this.tabFormContentContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer2.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer2.Name = "tabFormContentContainer2";
            this.tabFormContentContainer2.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer2.TabIndex = 2;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.tl_runnerPowerEffects);
            this.layoutControl2.Controls.Add(this.tl_runnerPowerDetails);
            this.layoutControl2.Controls.Add(this.b_createNewRunnerUnit);
            this.layoutControl2.Controls.Add(this.tb_newRunnerUnit);
            this.layoutControl2.Controls.Add(this.tl_abilityDetail);
            this.layoutControl2.Controls.Add(this.b_createNewRunnerCard);
            this.layoutControl2.Controls.Add(this.tb_newRunnerCard);
            this.layoutControl2.Controls.Add(this.tb_runnerRank);
            this.layoutControl2.Controls.Add(this.tb_runnerLevel);
            this.layoutControl2.Controls.Add(this.tl_runnerUnits);
            this.layoutControl2.Controls.Add(this.tl_runnerUnitDetails);
            this.layoutControl2.Controls.Add(this.tl_runnerCardRoster);
            this.layoutControl2.Controls.Add(this.tl_runnerCards);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // tl_runnerPowerEffects
            // 
            this.tl_runnerPowerEffects.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerPowerEffects.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerPowerEffects.Location = new System.Drawing.Point(1669, 792);
            this.tl_runnerPowerEffects.Name = "tl_runnerPowerEffects";
            this.tl_runnerPowerEffects.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerPowerEffects.OptionsBehavior.Editable = false;
            this.tl_runnerPowerEffects.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerPowerEffects.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerPowerEffects.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerPowerEffects.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerPowerEffects.OptionsSelection.MultiSelect = true;
            this.tl_runnerPowerEffects.OptionsView.AutoWidth = false;
            this.tl_runnerPowerEffects.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerPowerEffects.RootName = "Root";
            this.tl_runnerPowerEffects.Size = new System.Drawing.Size(841, 318);
            this.tl_runnerPowerEffects.TabIndex = 16;
            this.tl_runnerPowerEffects.TreeLevelWidth = 12;
            // 
            // tl_runnerPowerDetails
            // 
            this.tl_runnerPowerDetails.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerPowerDetails.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerPowerDetails.Location = new System.Drawing.Point(1669, 480);
            this.tl_runnerPowerDetails.Name = "tl_runnerPowerDetails";
            this.tl_runnerPowerDetails.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerPowerDetails.OptionsBehavior.Editable = false;
            this.tl_runnerPowerDetails.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerPowerDetails.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerPowerDetails.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerPowerDetails.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerPowerDetails.OptionsSelection.MultiSelect = true;
            this.tl_runnerPowerDetails.OptionsView.AutoWidth = false;
            this.tl_runnerPowerDetails.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerPowerDetails.RootName = "Root";
            this.tl_runnerPowerDetails.Size = new System.Drawing.Size(841, 296);
            this.tl_runnerPowerDetails.TabIndex = 15;
            this.tl_runnerPowerDetails.TreeLevelWidth = 12;
            // 
            // b_createNewRunnerUnit
            // 
            this.b_createNewRunnerUnit.Location = new System.Drawing.Point(1237, 12);
            this.b_createNewRunnerUnit.Name = "b_createNewRunnerUnit";
            this.b_createNewRunnerUnit.Size = new System.Drawing.Size(404, 22);
            this.b_createNewRunnerUnit.StyleController = this.layoutControl2;
            this.b_createNewRunnerUnit.TabIndex = 14;
            this.b_createNewRunnerUnit.Text = "Create New Runner Unit";
            this.b_createNewRunnerUnit.Click += new System.EventHandler(this.b_createNewRunnerUnit_Click);
            // 
            // tb_newRunnerUnit
            // 
            this.tb_newRunnerUnit.Location = new System.Drawing.Point(893, 12);
            this.tb_newRunnerUnit.Name = "tb_newRunnerUnit";
            this.tb_newRunnerUnit.Size = new System.Drawing.Size(340, 20);
            this.tb_newRunnerUnit.StyleController = this.layoutControl2;
            this.tb_newRunnerUnit.TabIndex = 13;
            // 
            // tl_abilityDetail
            // 
            this.tl_abilityDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_abilityDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_abilityDetail.Caption = "Unit Ability Detail";
            this.tl_abilityDetail.Location = new System.Drawing.Point(1669, 480);
            this.tl_abilityDetail.Name = "tl_abilityDetail";
            this.tl_abilityDetail.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_abilityDetail.OptionsBehavior.Editable = false;
            this.tl_abilityDetail.OptionsBehavior.ShowToolTips = false;
            this.tl_abilityDetail.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_abilityDetail.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_abilityDetail.OptionsMenu.EnableNodeMenu = false;
            this.tl_abilityDetail.OptionsSelection.MultiSelect = true;
            this.tl_abilityDetail.OptionsView.AutoWidth = false;
            this.tl_abilityDetail.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_abilityDetail.OptionsView.ShowCaption = true;
            this.tl_abilityDetail.RootName = "Root";
            this.tl_abilityDetail.Size = new System.Drawing.Size(841, 630);
            this.tl_abilityDetail.TabIndex = 12;
            this.tl_abilityDetail.TreeLevelWidth = 12;
            // 
            // b_createNewRunnerCard
            // 
            this.b_createNewRunnerCard.Location = new System.Drawing.Point(591, 110);
            this.b_createNewRunnerCard.Name = "b_createNewRunnerCard";
            this.b_createNewRunnerCard.Size = new System.Drawing.Size(209, 22);
            this.b_createNewRunnerCard.StyleController = this.layoutControl2;
            this.b_createNewRunnerCard.TabIndex = 11;
            this.b_createNewRunnerCard.Text = "Create New Runner Card";
            this.b_createNewRunnerCard.Click += new System.EventHandler(this.b_createNewRunnerCard_Click);
            // 
            // tb_newRunnerCard
            // 
            this.tb_newRunnerCard.Location = new System.Drawing.Point(101, 110);
            this.tb_newRunnerCard.Name = "tb_newRunnerCard";
            this.tb_newRunnerCard.Size = new System.Drawing.Size(486, 20);
            this.tb_newRunnerCard.StyleController = this.layoutControl2;
            this.tb_newRunnerCard.TabIndex = 10;
            // 
            // tb_runnerRank
            // 
            this.tb_runnerRank.EditValue = null;
            this.tb_runnerRank.Location = new System.Drawing.Point(101, 61);
            this.tb_runnerRank.Name = "tb_runnerRank";
            this.tb_runnerRank.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tb_runnerRank.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tb_runnerRank.Size = new System.Drawing.Size(699, 45);
            this.tb_runnerRank.StyleController = this.layoutControl2;
            this.tb_runnerRank.TabIndex = 9;
            this.tb_runnerRank.EditValueChanged += new System.EventHandler(this.tb_runnerRank_EditValueChanged);
            // 
            // tb_runnerLevel
            // 
            this.tb_runnerLevel.EditValue = null;
            this.tb_runnerLevel.Location = new System.Drawing.Point(101, 12);
            this.tb_runnerLevel.Name = "tb_runnerLevel";
            this.tb_runnerLevel.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tb_runnerLevel.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tb_runnerLevel.Size = new System.Drawing.Size(699, 45);
            this.tb_runnerLevel.StyleController = this.layoutControl2;
            this.tb_runnerLevel.TabIndex = 8;
            this.tb_runnerLevel.EditValueChanged += new System.EventHandler(this.tb_runnerLevel_EditValueChanged);
            // 
            // tl_runnerUnits
            // 
            this.tl_runnerUnits.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerUnits.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerUnits.Caption = "Units Overview";
            this.tl_runnerUnits.Location = new System.Drawing.Point(816, 38);
            this.tl_runnerUnits.Name = "tl_runnerUnits";
            this.tl_runnerUnits.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerUnits.OptionsBehavior.Editable = false;
            this.tl_runnerUnits.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerUnits.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerUnits.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerUnits.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerUnits.OptionsSelection.MultiSelect = true;
            this.tl_runnerUnits.OptionsView.AutoWidth = false;
            this.tl_runnerUnits.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerUnits.OptionsView.ShowCaption = true;
            this.tl_runnerUnits.RootName = "Root";
            this.tl_runnerUnits.Size = new System.Drawing.Size(825, 1084);
            this.tl_runnerUnits.TabIndex = 7;
            this.tl_runnerUnits.TreeLevelWidth = 12;
            // 
            // tl_runnerUnitDetails
            // 
            this.tl_runnerUnitDetails.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerUnitDetails.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerUnitDetails.Caption = "Unit Details";
            this.tl_runnerUnitDetails.Location = new System.Drawing.Point(1657, 12);
            this.tl_runnerUnitDetails.Name = "tl_runnerUnitDetails";
            this.tl_runnerUnitDetails.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerUnitDetails.OptionsBehavior.Editable = false;
            this.tl_runnerUnitDetails.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerUnitDetails.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerUnitDetails.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerUnitDetails.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerUnitDetails.OptionsSelection.MultiSelect = true;
            this.tl_runnerUnitDetails.OptionsView.AutoWidth = false;
            this.tl_runnerUnitDetails.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerUnitDetails.OptionsView.ShowCaption = true;
            this.tl_runnerUnitDetails.RootName = "Root";
            this.tl_runnerUnitDetails.Size = new System.Drawing.Size(865, 415);
            this.tl_runnerUnitDetails.TabIndex = 6;
            this.tl_runnerUnitDetails.TreeLevelWidth = 12;
            // 
            // tl_runnerCardRoster
            // 
            this.tl_runnerCardRoster.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerCardRoster.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerCardRoster.Caption = "Card Rank Rosters";
            this.tl_runnerCardRoster.Location = new System.Drawing.Point(12, 806);
            this.tl_runnerCardRoster.Name = "tl_runnerCardRoster";
            this.tl_runnerCardRoster.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerCardRoster.OptionsBehavior.Editable = false;
            this.tl_runnerCardRoster.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerCardRoster.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerCardRoster.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerCardRoster.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerCardRoster.OptionsSelection.MultiSelect = true;
            this.tl_runnerCardRoster.OptionsView.AutoWidth = false;
            this.tl_runnerCardRoster.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerCardRoster.OptionsView.ShowCaption = true;
            this.tl_runnerCardRoster.RootName = "Root";
            this.tl_runnerCardRoster.Size = new System.Drawing.Size(788, 316);
            this.tl_runnerCardRoster.TabIndex = 5;
            this.tl_runnerCardRoster.TreeLevelWidth = 12;
            // 
            // tl_runnerCards
            // 
            this.tl_runnerCards.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_runnerCards.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_runnerCards.Caption = "Card Overview";
            this.tl_runnerCards.Location = new System.Drawing.Point(12, 136);
            this.tl_runnerCards.Name = "tl_runnerCards";
            this.tl_runnerCards.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_runnerCards.OptionsBehavior.Editable = false;
            this.tl_runnerCards.OptionsBehavior.ShowToolTips = false;
            this.tl_runnerCards.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_runnerCards.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_runnerCards.OptionsMenu.EnableNodeMenu = false;
            this.tl_runnerCards.OptionsSelection.MultiSelect = true;
            this.tl_runnerCards.OptionsView.AutoWidth = false;
            this.tl_runnerCards.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_runnerCards.OptionsView.ShowCaption = true;
            this.tl_runnerCards.RootName = "Root";
            this.tl_runnerCards.Size = new System.Drawing.Size(788, 654);
            this.tl_runnerCards.TabIndex = 4;
            this.tl_runnerCards.TreeLevelWidth = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.splitterItem6,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.splitterItem8,
            this.layoutControlItem9,
            this.splitterItem7,
            this.layoutControlItem11,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.splitterItem5,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.tl_runnerCards;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(792, 658);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.tl_runnerUnitDetails;
            this.layoutControlItem10.Location = new System.Drawing.Point(1645, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(869, 419);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // splitterItem6
            // 
            this.splitterItem6.AllowHotTrack = true;
            this.splitterItem6.Location = new System.Drawing.Point(792, 26);
            this.splitterItem6.Name = "splitterItem6";
            this.splitterItem6.Size = new System.Drawing.Size(12, 1088);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.tb_runnerLevel;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(792, 49);
            this.layoutControlItem14.Text = "Runner Level";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.tb_runnerRank;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(792, 49);
            this.layoutControlItem15.Text = "Runner Rank";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.tb_newRunnerCard;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(579, 26);
            this.layoutControlItem16.Text = "New Runner Card";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.b_createNewRunnerCard;
            this.layoutControlItem17.Location = new System.Drawing.Point(579, 98);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(213, 26);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // splitterItem8
            // 
            this.splitterItem8.AllowHotTrack = true;
            this.splitterItem8.Location = new System.Drawing.Point(1645, 419);
            this.splitterItem8.Name = "splitterItem8";
            this.splitterItem8.Size = new System.Drawing.Size(869, 12);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.tl_runnerCardRoster;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 794);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(792, 320);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // splitterItem7
            // 
            this.splitterItem7.AllowHotTrack = true;
            this.splitterItem7.Location = new System.Drawing.Point(0, 782);
            this.splitterItem7.Name = "splitterItem7";
            this.splitterItem7.Size = new System.Drawing.Size(792, 12);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.tl_runnerUnits;
            this.layoutControlItem11.Location = new System.Drawing.Point(804, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(829, 1088);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.tb_newRunnerUnit;
            this.layoutControlItem24.Location = new System.Drawing.Point(792, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(433, 26);
            this.layoutControlItem24.Text = "New Runner";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.b_createNewRunnerUnit;
            this.layoutControlItem25.Location = new System.Drawing.Point(1225, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(408, 26);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // splitterItem5
            // 
            this.splitterItem5.AllowHotTrack = true;
            this.splitterItem5.Location = new System.Drawing.Point(1633, 0);
            this.splitterItem5.Name = "splitterItem5";
            this.splitterItem5.Size = new System.Drawing.Size(12, 1114);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(1645, 431);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(869, 683);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(845, 634);
            this.layoutControlGroup5.Text = "Ability Detail";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.tl_abilityDetail;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(845, 634);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.splitterItem17});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(845, 634);
            this.layoutControlGroup6.Text = "Runner Power Detail";
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.tl_runnerPowerDetails;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(845, 300);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.tl_runnerPowerEffects;
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 312);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(845, 322);
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextVisible = false;
            // 
            // splitterItem17
            // 
            this.splitterItem17.AllowHotTrack = true;
            this.splitterItem17.Location = new System.Drawing.Point(0, 300);
            this.splitterItem17.Name = "splitterItem17";
            this.splitterItem17.Size = new System.Drawing.Size(845, 12);
            // 
            // tabFormPage5
            // 
            this.tabFormPage5.ContentContainer = this.tabFormContentContainer5;
            this.tabFormPage5.Name = "tabFormPage5";
            this.tabFormPage5.Text = "Arenas";
            // 
            // tabFormContentContainer5
            // 
            this.tabFormContentContainer5.Controls.Add(this.layoutControl5);
            this.tabFormContentContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer5.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer5.Name = "tabFormContentContainer5";
            this.tabFormContentContainer5.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer5.TabIndex = 2;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.tl_roundsTree);
            this.layoutControl5.Controls.Add(this.tl_arenaCards);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup4;
            this.layoutControl5.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // tl_roundsTree
            // 
            this.tl_roundsTree.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_roundsTree.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_roundsTree.Caption = "Rounds Data";
            this.tl_roundsTree.Location = new System.Drawing.Point(1281, 12);
            this.tl_roundsTree.Name = "tl_roundsTree";
            this.tl_roundsTree.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_roundsTree.OptionsBehavior.Editable = false;
            this.tl_roundsTree.OptionsBehavior.ShowToolTips = false;
            this.tl_roundsTree.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_roundsTree.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_roundsTree.OptionsMenu.EnableNodeMenu = false;
            this.tl_roundsTree.OptionsSelection.MultiSelect = true;
            this.tl_roundsTree.OptionsView.AutoWidth = false;
            this.tl_roundsTree.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_roundsTree.OptionsView.ShowCaption = true;
            this.tl_roundsTree.RootName = "Root";
            this.tl_roundsTree.Size = new System.Drawing.Size(1241, 1110);
            this.tl_roundsTree.TabIndex = 7;
            this.tl_roundsTree.TreeLevelWidth = 12;
            // 
            // tl_arenaCards
            // 
            this.tl_arenaCards.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_arenaCards.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_arenaCards.Caption = "Arena Cards";
            this.tl_arenaCards.Location = new System.Drawing.Point(12, 12);
            this.tl_arenaCards.Name = "tl_arenaCards";
            this.tl_arenaCards.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_arenaCards.OptionsBehavior.Editable = false;
            this.tl_arenaCards.OptionsBehavior.ShowToolTips = false;
            this.tl_arenaCards.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_arenaCards.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_arenaCards.OptionsMenu.EnableNodeMenu = false;
            this.tl_arenaCards.OptionsSelection.MultiSelect = true;
            this.tl_arenaCards.OptionsView.AutoWidth = false;
            this.tl_arenaCards.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_arenaCards.OptionsView.ShowCaption = true;
            this.tl_arenaCards.RootName = "Root";
            this.tl_arenaCards.Size = new System.Drawing.Size(1253, 1110);
            this.tl_arenaCards.TabIndex = 6;
            this.tl_arenaCards.TreeLevelWidth = 12;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem29,
            this.splitterItem11});
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.tl_arenaCards;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(1257, 1114);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.tl_roundsTree;
            this.layoutControlItem29.Location = new System.Drawing.Point(1269, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(1245, 1114);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // splitterItem11
            // 
            this.splitterItem11.AllowHotTrack = true;
            this.splitterItem11.Location = new System.Drawing.Point(1257, 0);
            this.splitterItem11.Name = "splitterItem11";
            this.splitterItem11.Size = new System.Drawing.Size(12, 1114);
            // 
            // tabFormPage3
            // 
            this.tabFormPage3.ContentContainer = this.tabFormContentContainer3;
            this.tabFormPage3.Name = "tabFormPage3";
            this.tabFormPage3.Text = "Server";
            // 
            // tabFormContentContainer3
            // 
            this.tabFormContentContainer3.Controls.Add(this.layoutControl3);
            this.tabFormContentContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer3.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer3.Name = "tabFormContentContainer3";
            this.tabFormContentContainer3.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer3.TabIndex = 3;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.labelControl1);
            this.layoutControl3.Controls.Add(this.b_changeStream);
            this.layoutControl3.Controls.Add(this.l_currentStream);
            this.layoutControl3.Controls.Add(this.tl_userDecks);
            this.layoutControl3.Controls.Add(this.tl_userStats);
            this.layoutControl3.Controls.Add(this.tl_rarityTree);
            this.layoutControl3.Controls.Add(this.tl_chestList);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(114, 19);
            this.labelControl1.StyleController = this.layoutControl3;
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Current Stream:";
            // 
            // b_changeStream
            // 
            this.b_changeStream.Location = new System.Drawing.Point(261, 12);
            this.b_changeStream.Name = "b_changeStream";
            this.b_changeStream.Size = new System.Drawing.Size(99, 22);
            this.b_changeStream.StyleController = this.layoutControl3;
            this.b_changeStream.TabIndex = 10;
            this.b_changeStream.Text = "Change";
            this.b_changeStream.Click += new System.EventHandler(this.b_changeStream_Click);
            // 
            // l_currentStream
            // 
            this.l_currentStream.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_currentStream.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.l_currentStream.Appearance.Options.UseFont = true;
            this.l_currentStream.Appearance.Options.UseForeColor = true;
            this.l_currentStream.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.l_currentStream.Location = new System.Drawing.Point(130, 12);
            this.l_currentStream.Name = "l_currentStream";
            this.l_currentStream.Size = new System.Drawing.Size(127, 19);
            this.l_currentStream.StyleController = this.layoutControl3;
            this.l_currentStream.TabIndex = 9;
            this.l_currentStream.Text = "development";
            // 
            // tl_userDecks
            // 
            this.tl_userDecks.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_userDecks.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_userDecks.Caption = "New User Decks";
            this.tl_userDecks.Location = new System.Drawing.Point(376, 307);
            this.tl_userDecks.Name = "tl_userDecks";
            this.tl_userDecks.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_userDecks.OptionsBehavior.Editable = false;
            this.tl_userDecks.OptionsBehavior.ShowToolTips = false;
            this.tl_userDecks.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_userDecks.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_userDecks.OptionsMenu.EnableNodeMenu = false;
            this.tl_userDecks.OptionsSelection.MultiSelect = true;
            this.tl_userDecks.OptionsView.AutoWidth = false;
            this.tl_userDecks.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_userDecks.OptionsView.ShowCaption = true;
            this.tl_userDecks.RootName = "Root";
            this.tl_userDecks.Size = new System.Drawing.Size(2134, 399);
            this.tl_userDecks.TabIndex = 8;
            this.tl_userDecks.TreeLevelWidth = 12;
            // 
            // tl_userStats
            // 
            this.tl_userStats.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_userStats.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_userStats.Caption = "New Player Stats";
            this.tl_userStats.Location = new System.Drawing.Point(376, 12);
            this.tl_userStats.Name = "tl_userStats";
            this.tl_userStats.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_userStats.OptionsBehavior.Editable = false;
            this.tl_userStats.OptionsBehavior.ShowToolTips = false;
            this.tl_userStats.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_userStats.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_userStats.OptionsMenu.EnableNodeMenu = false;
            this.tl_userStats.OptionsSelection.MultiSelect = true;
            this.tl_userStats.OptionsView.AutoWidth = false;
            this.tl_userStats.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_userStats.OptionsView.ShowCaption = true;
            this.tl_userStats.RootName = "Root";
            this.tl_userStats.Size = new System.Drawing.Size(2134, 279);
            this.tl_userStats.TabIndex = 7;
            this.tl_userStats.TreeLevelWidth = 12;
            // 
            // tl_rarityTree
            // 
            this.tl_rarityTree.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_rarityTree.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_rarityTree.Caption = "Card Rarity Definitions";
            this.tl_rarityTree.Location = new System.Drawing.Point(376, 722);
            this.tl_rarityTree.Name = "tl_rarityTree";
            this.tl_rarityTree.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_rarityTree.OptionsBehavior.Editable = false;
            this.tl_rarityTree.OptionsBehavior.ShowToolTips = false;
            this.tl_rarityTree.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_rarityTree.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_rarityTree.OptionsMenu.EnableNodeMenu = false;
            this.tl_rarityTree.OptionsSelection.MultiSelect = true;
            this.tl_rarityTree.OptionsView.AutoWidth = false;
            this.tl_rarityTree.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_rarityTree.OptionsView.ShowCaption = true;
            this.tl_rarityTree.RootName = "Root";
            this.tl_rarityTree.Size = new System.Drawing.Size(2134, 400);
            this.tl_rarityTree.TabIndex = 5;
            this.tl_rarityTree.TreeLevelWidth = 12;
            // 
            // tl_chestList
            // 
            this.tl_chestList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_chestList.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_chestList.Caption = "Chest Reward Sequence";
            this.tl_chestList.Cursor = System.Windows.Forms.Cursors.Default;
            this.tl_chestList.Location = new System.Drawing.Point(12, 38);
            this.tl_chestList.Name = "tl_chestList";
            this.tl_chestList.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_chestList.OptionsBehavior.Editable = false;
            this.tl_chestList.OptionsBehavior.ShowToolTips = false;
            this.tl_chestList.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_chestList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_chestList.OptionsMenu.EnableNodeMenu = false;
            this.tl_chestList.OptionsSelection.MultiSelect = true;
            this.tl_chestList.OptionsView.AutoWidth = false;
            this.tl_chestList.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_chestList.OptionsView.ShowCaption = true;
            this.tl_chestList.RootName = "Root";
            this.tl_chestList.Size = new System.Drawing.Size(348, 1084);
            this.tl_chestList.TabIndex = 4;
            this.tl_chestList.TreeLevelWidth = 12;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.splitterItem9,
            this.layoutControlItem20,
            this.splitterItem10,
            this.layoutControlItem22,
            this.splitterItem12,
            this.layoutControlItem23,
            this.splitterItem13,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.tl_chestList;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(352, 1088);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // splitterItem9
            // 
            this.splitterItem9.AllowHotTrack = true;
            this.splitterItem9.Location = new System.Drawing.Point(2502, 0);
            this.splitterItem9.Name = "splitterItem9";
            this.splitterItem9.Size = new System.Drawing.Size(12, 1114);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.tl_rarityTree;
            this.layoutControlItem20.Location = new System.Drawing.Point(364, 710);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(2138, 404);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // splitterItem10
            // 
            this.splitterItem10.AllowHotTrack = true;
            this.splitterItem10.Location = new System.Drawing.Point(352, 0);
            this.splitterItem10.Name = "splitterItem10";
            this.splitterItem10.Size = new System.Drawing.Size(12, 1114);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.tl_userStats;
            this.layoutControlItem22.Location = new System.Drawing.Point(364, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(2138, 283);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // splitterItem12
            // 
            this.splitterItem12.AllowHotTrack = true;
            this.splitterItem12.Location = new System.Drawing.Point(364, 283);
            this.splitterItem12.Name = "splitterItem12";
            this.splitterItem12.Size = new System.Drawing.Size(2138, 12);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.tl_userDecks;
            this.layoutControlItem23.Location = new System.Drawing.Point(364, 295);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(2138, 403);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // splitterItem13
            // 
            this.splitterItem13.AllowHotTrack = true;
            this.splitterItem13.Location = new System.Drawing.Point(364, 698);
            this.splitterItem13.Name = "splitterItem13";
            this.splitterItem13.Size = new System.Drawing.Size(2138, 12);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.l_currentStream;
            this.layoutControlItem30.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(131, 26);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.b_changeStream;
            this.layoutControlItem31.Location = new System.Drawing.Point(249, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.labelControl1;
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(118, 26);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // tabFormPage6
            // 
            this.tabFormPage6.ContentContainer = this.tabFormContentContainer6;
            this.tabFormPage6.Name = "tabFormPage6";
            this.tabFormPage6.Text = "Store";
            // 
            // tabFormContentContainer6
            // 
            this.tabFormContentContainer6.Controls.Add(this.layoutControl6);
            this.tabFormContentContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer6.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer6.Name = "tabFormContentContainer6";
            this.tabFormContentContainer6.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer6.TabIndex = 4;
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.tl_simpleDeals);
            this.layoutControl6.Controls.Add(this.tl_dailyDeals);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(0, 0);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup7;
            this.layoutControl6.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // tl_simpleDeals
            // 
            this.tl_simpleDeals.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_simpleDeals.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_simpleDeals.Location = new System.Drawing.Point(1160, 12);
            this.tl_simpleDeals.Name = "tl_simpleDeals";
            this.tl_simpleDeals.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_simpleDeals.OptionsBehavior.Editable = false;
            this.tl_simpleDeals.OptionsBehavior.ShowToolTips = false;
            this.tl_simpleDeals.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_simpleDeals.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_simpleDeals.OptionsMenu.EnableNodeMenu = false;
            this.tl_simpleDeals.OptionsSelection.MultiSelect = true;
            this.tl_simpleDeals.OptionsView.AutoWidth = false;
            this.tl_simpleDeals.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_simpleDeals.RootName = "Root";
            this.tl_simpleDeals.Size = new System.Drawing.Size(1362, 1110);
            this.tl_simpleDeals.TabIndex = 5;
            this.tl_simpleDeals.TreeLevelWidth = 12;
            // 
            // tl_dailyDeals
            // 
            this.tl_dailyDeals.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_dailyDeals.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_dailyDeals.Location = new System.Drawing.Point(12, 12);
            this.tl_dailyDeals.Name = "tl_dailyDeals";
            this.tl_dailyDeals.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_dailyDeals.OptionsBehavior.Editable = false;
            this.tl_dailyDeals.OptionsBehavior.ShowToolTips = false;
            this.tl_dailyDeals.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_dailyDeals.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_dailyDeals.OptionsMenu.EnableNodeMenu = false;
            this.tl_dailyDeals.OptionsSelection.MultiSelect = true;
            this.tl_dailyDeals.OptionsView.AutoWidth = false;
            this.tl_dailyDeals.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_dailyDeals.RootName = "Root";
            this.tl_dailyDeals.Size = new System.Drawing.Size(1132, 1110);
            this.tl_dailyDeals.TabIndex = 4;
            this.tl_dailyDeals.TreeLevelWidth = 12;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.splitterItem19});
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.tl_dailyDeals;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(1136, 1114);
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.tl_simpleDeals;
            this.layoutControlItem38.Location = new System.Drawing.Point(1148, 0);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(1366, 1114);
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextVisible = false;
            // 
            // splitterItem19
            // 
            this.splitterItem19.AllowHotTrack = true;
            this.splitterItem19.Location = new System.Drawing.Point(1136, 0);
            this.splitterItem19.Name = "splitterItem19";
            this.splitterItem19.Size = new System.Drawing.Size(12, 1114);
            // 
            // tabFormPage7
            // 
            this.tabFormPage7.ContentContainer = this.tabFormContentContainer7;
            this.tabFormPage7.Name = "tabFormPage7";
            this.tabFormPage7.Text = "AI";
            // 
            // tabFormContentContainer7
            // 
            this.tabFormContentContainer7.Controls.Add(this.layoutControl7);
            this.tabFormContentContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer7.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer7.Name = "tabFormContentContainer7";
            this.tabFormContentContainer7.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer7.TabIndex = 5;
            // 
            // layoutControl7
            // 
            this.layoutControl7.Controls.Add(this.tl_aiValues);
            this.layoutControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl7.Location = new System.Drawing.Point(0, 0);
            this.layoutControl7.Name = "layoutControl7";
            this.layoutControl7.Root = this.layoutControlGroup8;
            this.layoutControl7.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl7.TabIndex = 0;
            this.layoutControl7.Text = "layoutControl7";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem39});
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup8.TextVisible = false;
            // 
            // tabFormPage4
            // 
            this.tabFormPage4.ContentContainer = this.tabFormContentContainer4;
            this.tabFormPage4.Name = "tabFormPage4";
            this.tabFormPage4.Text = "<Old>";
            // 
            // tabFormContentContainer4
            // 
            this.tabFormContentContainer4.Controls.Add(this.layoutControl4);
            this.tabFormContentContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer4.Location = new System.Drawing.Point(0, 64);
            this.tabFormContentContainer4.Name = "tabFormContentContainer4";
            this.tabFormContentContainer4.Size = new System.Drawing.Size(2534, 1134);
            this.tabFormContentContainer4.TabIndex = 2;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.tl_xowerUtility);
            this.layoutControl4.Controls.Add(this.tl_xowerAttack);
            this.layoutControl4.Controls.Add(this.tl_xowerTargets);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // tl_xowerUtility
            // 
            this.tl_xowerUtility.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_xowerUtility.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_xowerUtility.Caption = "Xower Utility";
            this.tl_xowerUtility.Location = new System.Drawing.Point(12, 765);
            this.tl_xowerUtility.Name = "tl_xowerUtility";
            this.tl_xowerUtility.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_xowerUtility.OptionsBehavior.Editable = false;
            this.tl_xowerUtility.OptionsBehavior.ShowToolTips = false;
            this.tl_xowerUtility.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_xowerUtility.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_xowerUtility.OptionsMenu.EnableNodeMenu = false;
            this.tl_xowerUtility.OptionsSelection.MultiSelect = true;
            this.tl_xowerUtility.OptionsView.AutoWidth = false;
            this.tl_xowerUtility.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_xowerUtility.OptionsView.ShowCaption = true;
            this.tl_xowerUtility.RootName = "Root";
            this.tl_xowerUtility.Size = new System.Drawing.Size(2510, 357);
            this.tl_xowerUtility.TabIndex = 6;
            this.tl_xowerUtility.TreeLevelWidth = 12;
            // 
            // tl_xowerAttack
            // 
            this.tl_xowerAttack.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_xowerAttack.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_xowerAttack.Caption = "Xower Damage";
            this.tl_xowerAttack.Location = new System.Drawing.Point(12, 398);
            this.tl_xowerAttack.Name = "tl_xowerAttack";
            this.tl_xowerAttack.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_xowerAttack.OptionsBehavior.Editable = false;
            this.tl_xowerAttack.OptionsBehavior.ShowToolTips = false;
            this.tl_xowerAttack.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_xowerAttack.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_xowerAttack.OptionsMenu.EnableNodeMenu = false;
            this.tl_xowerAttack.OptionsSelection.MultiSelect = true;
            this.tl_xowerAttack.OptionsView.AutoWidth = false;
            this.tl_xowerAttack.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_xowerAttack.OptionsView.ShowCaption = true;
            this.tl_xowerAttack.RootName = "Root";
            this.tl_xowerAttack.Size = new System.Drawing.Size(2510, 351);
            this.tl_xowerAttack.TabIndex = 5;
            this.tl_xowerAttack.TreeLevelWidth = 12;
            // 
            // tl_xowerTargets
            // 
            this.tl_xowerTargets.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_xowerTargets.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_xowerTargets.Caption = "Xower Targets";
            this.tl_xowerTargets.Location = new System.Drawing.Point(12, 12);
            this.tl_xowerTargets.Name = "tl_xowerTargets";
            this.tl_xowerTargets.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_xowerTargets.OptionsBehavior.Editable = false;
            this.tl_xowerTargets.OptionsBehavior.ShowToolTips = false;
            this.tl_xowerTargets.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_xowerTargets.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_xowerTargets.OptionsMenu.EnableNodeMenu = false;
            this.tl_xowerTargets.OptionsSelection.MultiSelect = true;
            this.tl_xowerTargets.OptionsView.AutoWidth = false;
            this.tl_xowerTargets.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_xowerTargets.OptionsView.ShowCaption = true;
            this.tl_xowerTargets.RootName = "Root";
            this.tl_xowerTargets.Size = new System.Drawing.Size(2510, 370);
            this.tl_xowerTargets.TabIndex = 4;
            this.tl_xowerTargets.TreeLevelWidth = 12;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.splitterItem14,
            this.splitterItem15});
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(2534, 1134);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tl_xowerTargets;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(2514, 374);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.tl_xowerAttack;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 386);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(2514, 355);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tl_xowerUtility;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 753);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(2514, 361);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem14
            // 
            this.splitterItem14.AllowHotTrack = true;
            this.splitterItem14.Location = new System.Drawing.Point(0, 374);
            this.splitterItem14.Name = "splitterItem14";
            this.splitterItem14.Size = new System.Drawing.Size(2514, 12);
            // 
            // splitterItem15
            // 
            this.splitterItem15.AllowHotTrack = true;
            this.splitterItem15.Location = new System.Drawing.Point(0, 741);
            this.splitterItem15.Name = "splitterItem15";
            this.splitterItem15.Size = new System.Drawing.Size(2514, 12);
            // 
            // tl_aiValues
            // 
            this.tl_aiValues.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tl_aiValues.Appearance.FocusedRow.Options.UseFont = true;
            this.tl_aiValues.Location = new System.Drawing.Point(12, 12);
            this.tl_aiValues.Name = "tl_aiValues";
            this.tl_aiValues.OptionsBehavior.AllowExpandOnDblClick = false;
            this.tl_aiValues.OptionsBehavior.Editable = false;
            this.tl_aiValues.OptionsBehavior.ShowToolTips = false;
            this.tl_aiValues.OptionsFilter.ExpandNodesOnFiltering = true;
            this.tl_aiValues.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.ParentBranch;
            this.tl_aiValues.OptionsMenu.EnableNodeMenu = false;
            this.tl_aiValues.OptionsSelection.MultiSelect = true;
            this.tl_aiValues.OptionsView.AutoWidth = false;
            this.tl_aiValues.OptionsView.RowImagesShowMode = DevExpress.XtraTreeList.RowImagesShowMode.InCell;
            this.tl_aiValues.RootName = "Root";
            this.tl_aiValues.Size = new System.Drawing.Size(2510, 1110);
            this.tl_aiValues.TabIndex = 4;
            this.tl_aiValues.TreeLevelWidth = 12;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.tl_aiValues;
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(2514, 1114);
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextVisible = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2534, 1198);
            this.Controls.Add(this.tabFormContentContainer7);
            this.Controls.Add(this.tabFormControl1);
            this.KeyPreview = true;
            this.Name = "MainWindow";
            this.TabFormControl = this.tabFormControl1;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).EndInit();
            this.tabFormContentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_stati)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_powerEffects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_powerDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_actionCards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_actionCardLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_actionCardLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xowerSwitch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newActionType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerEffects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem18)).EndInit();
            this.tabFormContentContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowerEffects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerPowerDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newRunnerUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_abilityDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_newRunnerCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerRank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerRank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_runnerLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerUnitDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerCardRoster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_runnerCards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem17)).EndInit();
            this.tabFormContentContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_roundsTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_arenaCards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem11)).EndInit();
            this.tabFormContentContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_userDecks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_userStats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_rarityTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_chestList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            this.tabFormContentContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_simpleDeals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_dailyDeals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem19)).EndInit();
            this.tabFormContentContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).EndInit();
            this.layoutControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            this.tabFormContentContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerUtility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerAttack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_xowerTargets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tl_aiValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private DevExpress.XtraBars.TabFormControl tabFormControl1;
        private DevExpress.XtraBars.TabFormPage tabFormPage1;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TrackBarControl tb_actionCardLevel;
        private DevExpress.XtraEditors.ToggleSwitch xowerSwitch;
        private DevExpress.XtraEditors.TextEdit tb_newActionType;
        private DevExpress.XtraEditors.SimpleButton b_createNewAction;
        private TowerEffectsTree tl_xowerEffects;
        private TowerDetailTree tl_xowerDetail;
        private XowerUtilityTree tl_xowerUtility;
        private XowerAttackTree tl_xowerAttack;
        private XowerTargetingTree tl_xowerTargets;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
        private DevExpress.XtraBars.TabFormPage tabFormPage2;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private UnitsTree tl_runnerUnits;
        private RunnerDetailList tl_runnerUnitDetails;
        private RunnerCardRosterTree tl_runnerCardRoster;
        private RunnerCardTree tl_runnerCards;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.SplitterItem splitterItem5;
        private DevExpress.XtraLayout.SplitterItem splitterItem7;
        private DevExpress.XtraLayout.SplitterItem splitterItem6;
        private DevExpress.XtraEditors.TrackBarControl tb_runnerRank;
        private DevExpress.XtraEditors.TrackBarControl tb_runnerLevel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.SimpleButton b_createNewRunnerCard;
        private DevExpress.XtraEditors.TextEdit tb_newRunnerCard;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private RunnerAbilityTree tl_abilityDetail;
        private DevExpress.XtraLayout.SplitterItem splitterItem8;
        private DevExpress.XtraBars.TabFormPage tabFormPage3;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private ChestListTree tl_chestList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.SplitterItem splitterItem9;
        private RarityTree tl_rarityTree;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.SplitterItem splitterItem10;
        private ArenaCardsTree tl_arenaCards;
        private UserStatsTree tl_userStats;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.SplitterItem splitterItem12;
        private UserDecks tl_userDecks;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SplitterItem splitterItem13;
        private DevExpress.XtraEditors.SimpleButton b_createNewRunnerUnit;
        private DevExpress.XtraEditors.TextEdit tb_newRunnerUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraBars.TabFormPage tabFormPage4;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer4;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private ActionCardsTree tl_actionCards;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem14;
        private DevExpress.XtraLayout.SplitterItem splitterItem15;
        private PowerEffectsTree tl_powerEffects;
        private PowerDetailTree tl_powerDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.SplitterItem splitterItem4;
        private DevExpress.XtraLayout.SplitterItem splitterItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.TabFormPage tabFormPage5;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer5;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private RoundsTree tl_roundsTree;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.SplitterItem splitterItem11;
        private DevExpress.XtraEditors.SimpleButton b_changeStream;
        private DevExpress.XtraEditors.LabelControl l_currentStream;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private RunnerPowerEffectsTree tl_runnerPowerEffects;
        private RunnerPowerDetailTree tl_runnerPowerDetails;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.SplitterItem splitterItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private RunnerPowersList tl_runnerPowers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.SplitterItem splitterItem18;
        private StatusTree tl_stati;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraBars.TabFormPage tabFormPage6;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer6;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DailyDealsList tl_dailyDeals;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private SimpleDailyDealsList tl_simpleDeals;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.SplitterItem splitterItem19;
        private DevExpress.XtraBars.TabFormPage tabFormPage7;
        private DevExpress.XtraBars.TabFormContentContainer tabFormContentContainer7;
        private DevExpress.XtraLayout.LayoutControl layoutControl7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private AIValueTree tl_aiValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
    }
}