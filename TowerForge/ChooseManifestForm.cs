﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerForge
{
    /*
    public partial class ChooseManifestForm : DevExpress.XtraEditors.XtraForm
    {
        public string Manifest = null;
        private Dictionary<string, DataStreamManifest> _manifests = new Dictionary<string, DataStreamManifest>();
        private string _originalMessage;

        public ChooseManifestForm()
        {
            InitializeComponent();
            cb_source.Enabled = false;
            cb_target.Enabled = false;  
            _originalMessage = l_message.Text;
            l_message.Text = "Hold on a second...";
            GetManifests();

        }

        private async Task GetManifests()
        {
            _manifests = await FirebaseManager.Inst.GetAllManifests();
            foreach(var cb in new List<DevExpress.XtraEditors.ComboBoxEdit>() { cb_source, cb_target})
            {
                cb.Enabled = true;
                cb.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.SingleClick;
                cb.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                cb.Properties.Items.Clear();
                foreach (string key in _manifests.Keys)
                {
                    cb.Properties.Items.Add(key);
                }
            }
            l_message.Text = _originalMessage;

        }

        private void b_setStream_Click(object sender, EventArgs e)
        {
            string streamManifest = (string)cb_target.EditValue;
            l_message.Text = $"Great!\nI'll point to the {streamManifest} manifest for all future changes.";
            Manifest = streamManifest;
        }


        private void b_copyManifest_Click(object sender, EventArgs e)
        {
            string mName = t_targetManifest.Text;
            if(string.IsNullOrEmpty(mName))
            {
                l_message.Text = "Try again but this time enter a valid target name, ya doofus.";
            }
            //else if (_manifests.ContainsKey(mName))
            //{
            //    l_message.Text = "I'm not going to copy over something that exists, even though I could...";
            //}
            else
            {
                l_message.Text = $"Great! I created {mName} but you still need to let me know what to point at.";
                string source = (string)cb_source.EditValue;
                DataStreamManifest newManifest = new DataStreamManifest();
                foreach(string key in _manifests[source].Versions.Keys)
                {
                    newManifest.Versions.Add(key, _manifests[source].Versions[key]);
                }
                newManifest.UpdatedAt = DateTime.UtcNow;
                if (!_manifests.ContainsKey(mName))
                {
                    _manifests.Add(mName, newManifest);
                    cb_source.Properties.Items.Add(mName);
                    cb_target.Properties.Items.Add(mName);
                }
                else
                {
                    _manifests[mName] = newManifest;
                }
                FirebaseManager.Inst.PushManifest(mName, newManifest);
            }
        }
    }//*/
}
