﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class XowerAttackTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private XowerType _xowerType = XowerType.Tower;
	private int _level = 1;
	public XowerAttackTree() : base()
    {
        AddColumn("ID", 100, 100);
		AddColumn("Cost",50,50);
        AddColumn("Damage",70,70);
		AddColumn("AttackSpeed");
		AddColumn("ConditionalDamage");
        AddColumn("MinThreat",80,80);
		AddColumn("MaxThreat",80,80);

		Signals.ShowPowersNotTowers.Register(HandleToggle);
		Signals.XowerLevelChanged.Register(HandleLevelChange);
		Signals.ActionCardSelected.Register(SelectActionCard);
		Signals.ActionCardUpdated.Register(RefreshActionCard);

		OnNodeSelect += (id) =>
		{
			Signals.ActionCardSelected.Send(id, this);
		};
	}
	private object[] GetData(string id)
    {
        float attackrate = DataManager.Inst.GetXowerAttackRate(id, _xowerType, _level);
		GetDamage(id, true, out float damage, out float conditionalDamage);

        object[] data = new object[]
		{
			DataManager.Inst.DisplayName(id),
			DataManager.Inst.GetActionCardCost(id),
			damage,
			attackrate,
			conditionalDamage,
			damage / attackrate,
			(damage + conditionalDamage) / attackrate
		};
		return data;
	}

	private void GetDamage(string id, bool primaryList, out float damage, out float conditionalDamage)
    {
		damage = 0;
		conditionalDamage = 0;
		float damageMod = 1;
		for (int i = 0; i < DataManager.Inst.GetXowerAtomicEffectCount(id, _xowerType, _level, primaryList); i++)
		{
			AtomicActionEffectType type = DataManager.Inst.GetAtomicEffectType(id, _xowerType, primaryList, i);
			float amount = DataManager.Inst.GetAtomicEffectAmount(id, _xowerType, _level, primaryList, i);
			bool hasConditions = DataManager.Inst.GetXowerAtomicEffectAtomicConditionsCount(id, _xowerType, primaryList, i) > 0;
			float toAdd = 0;
			if (type == AtomicActionEffectType.Damage)
			{
				toAdd += amount;
			}
			if (type == AtomicActionEffectType.HealthOverTime)
			{
				// This won't account for conditionally increased duration for DOT effects. Whatever.
				float duration = DataManager.Inst.GetAtomicEffectDuration(id, _xowerType, _level, primaryList, i);
				toAdd += amount * duration;
			}
			if(type == AtomicActionEffectType.Heal)
            {
				toAdd -= amount;
            }
			if(type == AtomicActionEffectType.ModifyDamageApplied)
            {
				damageMod *= amount;
            }
			// TODO JAKE: Handle windup damage.
			// TODO JAKE: Handle shred.

			if (hasConditions)
			{
				conditionalDamage += toAdd;
			}
			else
			{
				damage += toAdd;
            }
        }
		if(damageMod < 1 && conditionalDamage > 0)
        {
			Debug.LogError("Hey! You added conditional damage and then took it away with a mod! " +
                "That makes for poorly represented conditional damage!");
        }

        float extra = (damage + conditionalDamage) * (1 - damageMod);
        conditionalDamage -= extra;
    }

    private void HandleToggle(bool showPowers)
	{
		_xowerType = showPowers? XowerType.Power : XowerType.Tower;
		RebuildActionCards();
	}

	private void HandleLevelChange(int level)
	{
		_level = level;
		RebuildActionCards();
	}

	private void RebuildActionCards()
	{
		foreach (string id in _nodes.Keys)
		{
			RefreshActionCard(id);
		}
	}

	private void RefreshActionCard(string id)
	{
		object[] data = GetData(id);
		if (!_nodes.ContainsKey(id))
		{
			TreeListNode node = AppendNode(data, null,id);
			_nodes.Add(id, node);
		}
		else
		{
			for (int i = 0; i < data.Length; i++)
			{
				_nodes[id].SetValue(i, data[i]);
			}
		}
	}

	private void SelectActionCard(string id, SunTree sendingTree)
	{
		if (sendingTree == this) { return; }
		if (!_nodes.ContainsKey(id)) { return; }
		ClearSelection();
		SelectNode(_nodes[id]);
	}

	protected override string GetTreeName() { return nameof(XowerAttackTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return DataManager.Inst.GetIds<ActionCardType>();
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}