﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Linq;

public class RoundsTree : SunTree
{
	private const string PROPERTIES = "Properties";
	private const string SUPPLY_CHANGES = "SupplyChanges";
	private const string BOSS_TIMES = "BossTimes";
	private const string ROUNDS = "Rounds";

	private const string MENU_COPY_ROUNDS = "Duplicate this Rounds";
	private const string MENU_ADD_ROUNDS = "Add new Rounds";
	private const string MENU_REMOVE_ROUNDS = "Delete this Rounds";
	private const string MENU_ADD_SUPPLY = "Add supply change";
	private const string MENU_REMOVE_SUPPLY = "Remove supply change";
	private const string MENU_ADD_BOSS = "Add Boss spawn";
	private const string MENU_REMOVE_BOSS = "Remove Boss spawn";


	private int COL_NAME;
	private int COL_VALUE;
	private int COL_START;
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private HashSet<string> _rounds = new HashSet<string>();
	private HashSet<string> _headers = new HashSet<string>();
	private HashSet<string> _bossSpawns = new HashSet<string>();
	private HashSet<string> _supplyChanges = new HashSet<string>();
	private Dictionary<string, RoundFieldInfo> _roundFields = new Dictionary<string, RoundFieldInfo>();
	public RoundsTree() : base()
	{
		AddColumn("Property", 150, 150,Editor_Property,Validator_Property);
		COL_VALUE = AddColumn("Value",60,60,Editor_Value,Validator_Value).ColumnIndex;
		COL_START =  AddColumn("StartTime",60,60,Editor_Time,Validator_Time).ColumnIndex;
		COL_NAME = AddColumn("PropertyName").ColumnIndex;
		AddColumn("ID");

		_roundFields.Add("InitialSupplyRate", new RoundFieldInfo("InitialSupplyRate", RoundFieldSource.Final, typeof(float), "Final_InitialSupplyRate"));
		_roundFields.Add("TimeInterval", new RoundFieldInfo("TimeInterval", RoundFieldSource.Final, typeof(float), "Final_TimeInterval"));
		_roundFields.Add("SupplyIncreasePerInterval", new RoundFieldInfo("SupplyIncreasePerInterval", RoundFieldSource.Final, typeof(float), "Final_SupplyIncreasePerInterval"));
		_roundFields.Add("BossInterval", new RoundFieldInfo("BossInterval", RoundFieldSource.Final, typeof(float), "Final_BossInterval"));
		_roundFields.Add("RankIncreasePerInterval", new RoundFieldInfo("RankIncreasePerInterval", RoundFieldSource.Final, typeof(int), "Final_RankIncreasePerInterval"));
		_roundFields.Add("ElixirModifierPerInterval", new RoundFieldInfo("ElixirModifierPerInterval", RoundFieldSource.Final, typeof(float), "Final_ElixerModifierPerInterval"));
		_roundFields.Add("ElixirRate", new RoundFieldInfo("ElixirRate", RoundFieldSource.Rounds, typeof(float), "Round Elixir Rate"));
		_roundFields.Add("Index", new RoundFieldInfo("Index", RoundFieldSource.Round, typeof(int)));
		_roundFields.Add("CooldownTime", new RoundFieldInfo("CooldownTime", RoundFieldSource.Round, typeof(float)));
		_roundFields.Add("CountdownTime", new RoundFieldInfo("CountdownTime", RoundFieldSource.Round, typeof(float)));
		_roundFields.Add("Duration", new RoundFieldInfo("Duration", RoundFieldSource.Round, typeof(float)));
		_roundFields.Add("ElixirMultiplier", new RoundFieldInfo("ElixirMultiplier", RoundFieldSource.Round, typeof(float)));

		Signals.RoundsDataChanged.Register(RefreshRounds);
	}
	private object[] GetRoundsData(string id)
	{
		object[] data = new object[]
		{
			id
		};
		return data;
	}

    #region Editors/validators
	private RepositoryItem Editor_Time(string id)
    {
		if(!_supplyChanges.Contains(id))
        {
			return null;
        }
		return CreateEditorTextEdit(DataEditorType.Float);
    }

	private bool Validator_Time(string id, object value)
    {
        if (!_supplyChanges.Contains(id))
        { return false; }
		if(!TryParseId(id, out string roundsKey, out string propertyName, out int roundIndex, out int supplyIndex, out int bossIndex))
        { return false; }
		WaveSupplyChange supply = GetSupplyChange(id);
		supply.StartTime = (float)value;
		return DataManager.Inst.SetSupplyChange(roundsKey, roundIndex, supplyIndex, supply);
    }

    private RepositoryItem Editor_Property(string id)
    {
        if (!_rounds.Contains(id)) { return null; }
		return CreateEditorTextEdit();
    }
    private bool Validator_Property(string id, object value)
    {
		if(!_rounds.Contains(id) || ((string)value).Contains(".")) // '.' is a separator character and must not be included in a new RoundId.
        {
			return false;
        }

		return DataManager.Inst.SetRoundsDataKey(id, (string)value);
    }

	private RepositoryItem Editor_Value(string id)
    {
		if (!_nodes.ContainsKey(id) || _rounds.Contains(id) || _headers.Contains(id))
		{ return null; }
		if (_supplyChanges.Contains(id) || _bossSpawns.Contains(id))
		{
			return CreateEditorTextEdit(DataEditorType.Float);
		}
		if(!TryParseId(id, out string roundsKey, out string name, out int roundIndex, out int supplyIndex, out int bossIndex))
		{ return null; }
		if (!string.IsNullOrEmpty(name))
		{
			if (_roundFields[name].Type == typeof(int))
			{
				return CreateEditorTextEdit(DataEditorType.Integer);
			}
			else if (_roundFields[name].Type == typeof(float))
			{
				return CreateEditorTextEdit(DataEditorType.Float);
			}
		}
		if (supplyIndex >= 0 || bossIndex >= 0)
        {
			return CreateEditorTextEdit(DataEditorType.Float);
        }
		return null;
	}

	private bool Validator_Value(string id, object value)
    {
        if (!_nodes.ContainsKey(id) || _rounds.Contains(id) || _headers.Contains(id))
        { return false; }
		if(!TryParseId(id,out string roundsKey, out string property, out int roundIndex, out int supplyIndex, out int bossIndex))
        {
			return false;
        }
		if(_supplyChanges.Contains(id))
        {
			WaveSupplyChange supply = GetSupplyChange(id);
			supply.SupplyRate = (float)value;
			return DataManager.Inst.SetSupplyChange(roundsKey, roundIndex, supplyIndex, supply);
        }
		if(_bossSpawns.Contains(id))
        {
			return DataManager.Inst.SetBossTime(roundsKey, roundIndex, supplyIndex, (float)value);
        }
        //string name = (string)_nodes[id].GetValue(COL_NAME);
		if (!string.IsNullOrEmpty(property) && _roundFields.ContainsKey(property))
		{
			switch (_roundFields[property].Source)
            {
                case RoundFieldSource.Final:
					return DataManager.Inst.SetFinalRoundValue(roundsKey, property, value);
                case RoundFieldSource.Rounds:
					return DataManager.Inst.SetRoundsValue(roundsKey, property, value);
                case RoundFieldSource.Round:
					return DataManager.Inst.SetRoundValue(roundsKey, roundIndex, property, value);
				default:
					Debug.LogError($"Source {_roundFields[property].Source} is not handled!");
					break;
            }
		}
		return false;
	}

	private WaveSupplyChange GetSupplyChange(string id)
    {
        WaveSupplyChange supply = new WaveSupplyChange();
		supply.SupplyRate = (float)_nodes[id].GetValue(COL_VALUE);
        supply.StartTime = (float)_nodes[id].GetValue(COL_START);
		return supply;
    }
    #endregion Editors/validators

	private bool TryParseId(string id, out string roundsKey, out string propertyName, out int roundIndex, out int supplyIndex, out int bossIndex)
    {
		// examples:
		// default.Rounds.0.Supply.1
		// default.Properties.InitialSupplyRate
		// default.Rounds.2.BossSpawns
		// default.Rounds.1.Properties.Duration

		roundIndex = -1;
		supplyIndex = -1;
        bossIndex = -1;
		propertyName = null;
		roundsKey = null;
        if (string.IsNullOrEmpty(id)) { return false; }
        string[] segs = id.Split('.');
        roundsKey = segs[0];
		if(segs.Length > 2)
        {
            switch(segs[1])
            {
				case ROUNDS:
					if (!int.TryParse(segs[2], out roundIndex))
					{
						return false;
					}
					break;
				case PROPERTIES:
					propertyName = segs[2];
					return true;
            }
        }
		if(segs.Length > 4)
        {
            switch(segs[3])
            {
				case BOSS_TIMES:
					return int.TryParse(segs[4], out bossIndex);
				case SUPPLY_CHANGES:
                    return int.TryParse(segs[4],out supplyIndex);
				case PROPERTIES:
					propertyName = segs[4];
					return true;
            }
        }
		return true;
    }

    protected override string GetTreeName() { return nameof(RoundsTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		menu.Add(MENU_ADD_ROUNDS);
		if (string.IsNullOrEmpty(id)) { return menu; }
		if(!TryParseId(id,out string roundsKey, out string propertyName, out int roundIndex, out int supplyIndex, out int bossIndex))
		{ return menu; }

		if(!string.IsNullOrEmpty(roundsKey))
        {
			menu.Add(MENU_COPY_ROUNDS);
			menu.Add(MENU_REMOVE_ROUNDS);
        }
		if(roundIndex >= 0)
        {
			menu.Add(MENU_ADD_BOSS);
			menu.Add(MENU_ADD_SUPPLY);
        }
		if(supplyIndex >= 0)
        {
			menu.Add(MENU_REMOVE_SUPPLY);
        }
		if(bossIndex >= 0)
        {
			menu.Add(MENU_REMOVE_BOSS);
        }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{
		TryParseId(id, out string roundsKey, out string propertyName, out int roundIndex, out int supplyIndex, out int bossIndex);
		switch(operation)
        {
			case MENU_ADD_ROUNDS:
				DataManager.Inst.AddRounds();
                return;
            case MENU_COPY_ROUNDS:
				DataManager.Inst.DuplicateRoundsKey(roundsKey);
                return;
            case MENU_REMOVE_ROUNDS:
                DataManager.Inst.RemoveRounds(roundsKey);
				return;
            case MENU_ADD_SUPPLY:
				DataManager.Inst.AddSupplyChange(roundsKey, roundIndex);
                return;
            case MENU_REMOVE_SUPPLY:
                DataManager.Inst.RemoveSupplyChange(roundsKey, roundIndex, supplyIndex);
                return;
            case MENU_ADD_BOSS:
				DataManager.Inst.AddBossTime(roundsKey, roundIndex);
                return;
            case MENU_REMOVE_BOSS:
				DataManager.Inst.RemoveBossTime(roundsKey, roundIndex, bossIndex);
				return;
        }
    }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return DataManager.Inst.GetRoundKeys();
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetRoundsData(id), GetRoot(), id);
		_nodes.Add(id, node);
		_rounds.Add(id);

		//AddProperty(id, _roundFields["InitialSupplyRate"], GetHeader(id, PROPERTIES));
		//AddProperty<float>(id, _roundFields["TimeInterval"], GetHeader(id, PROPERTIES));
		//AddProperty<float>(id, _roundFields["SupplyIncreasePerInterval"], GetHeader(id, PROPERTIES));
		//AddProperty<float>(id, _roundFields["BossInterval"], GetHeader(id, PROPERTIES));
		//AddProperty<int>(id,   _roundFields["RankIncreasePerInterval"], GetHeader(id, PROPERTIES));
		//AddProperty<float>(id, _roundFields["ElixirModifierPerInterval"],  GetHeader(id, PROPERTIES));
		//AddProperty<float>(id, _roundFields["ElixirRate"],  GetHeader(id, PROPERTIES));

		foreach (string key in _roundFields.Keys)
		{
			if (_roundFields[key].Source == RoundFieldSource.Round) { continue; }
			AddProperty(id, _roundFields[key], GetHeader(id, PROPERTIES));
		}

		for (int i = 0; i < DataManager.Inst.GetRoundsCount(id); i++)
        {
			AddRound(id, i);
        }
	}

	private void RefreshRounds()
    {
		List<string> nodeIds = _nodes.Keys.ToList();
		foreach(string nodeId in nodeIds)
        {
			_nodes[nodeId].Remove();
			_nodes.Remove(nodeId);
        }
		_headers.Clear();
		_bossSpawns.Clear();
		_rounds.Clear();
		_supplyChanges.Clear();

		foreach(string id in DataManager.Inst.GetRoundKeys())
        {
			InitialPopulateAction(id);
        }
		ExpandAll();
    }

    protected override bool IsSubHeader(string id)
    {
		return _headers.Contains(id);
    }

    protected override bool IsHeader(string id)
    {
		return _rounds.Contains(id);
    }

    private void AddRound(string roundsKey, int roundIndex)
    {
        TreeListNode parent = GetHeader(roundsKey, ROUNDS);
		string id = parent.Tag.ToString() + "." + roundIndex;
		TreeListNode round = AppendNode(new object[] { GetRoundName(roundIndex), "", "", id }, parent, id);
		_nodes.Add(id, round);
		_rounds.Add(id);


		foreach(string key in _roundFields.Keys)
        {
            if (_roundFields[key].Source != RoundFieldSource.Round) { continue; }
			AddProperty(roundsKey, _roundFields[key], GetHeader(id, PROPERTIES), roundIndex);
        }

		for(int i = 0; i < DataManager.Inst.GetSupplyChangesCount(roundsKey,roundIndex); i++)
        {
			AddSupplyChange(id, roundsKey,roundIndex, i);
        }

		for(int i = 0; i < DataManager.Inst.GetBossTimesCount(roundsKey, roundIndex); i++)
        {
			AddBossTime(id,roundsKey, roundIndex, i);
        }
	}

	private void AddSupplyChange(string parentId, string roundsKey, int roundIndex, int supplyIndex)
    {
        TreeListNode parent = GetHeader(parentId, SUPPLY_CHANGES);
        string id = parent.Tag.ToString() + "." + supplyIndex.ToString();
		WaveSupplyChange change = DataManager.Inst.GetSupplyChange(roundsKey, roundIndex, supplyIndex);
		object[] data = new object[]
		{
			"Change " + supplyIndex.ToString(),
			change.SupplyRate,
			change.StartTime,
			supplyIndex,
			id
		};
		TreeListNode node = AppendNode(data, parent, id);
		_nodes.Add(id,node);
		_supplyChanges.Add(id);
    }

    private void AddBossTime(string parentId, string roundsKey, int roundIndex, int bossTimeIndex)
    {
		TreeListNode parent = GetHeader(parentId, BOSS_TIMES);
		string id = parent.Tag.ToString() + "." + bossTimeIndex.ToString();
		float change = DataManager.Inst.GetBossTime(roundsKey, roundIndex, bossTimeIndex);
		object[] data = new object[]
		{
			"Spawn " + bossTimeIndex.ToString(),
			change,
			"",
			bossTimeIndex,
			id
		};
		TreeListNode node = AppendNode(data, parent, id);
		_nodes.Add(id, node);
		_bossSpawns.Add(id);
	}

    private TreeListNode GetHeader(string parent, string headerName)
    {
		if(!_nodes.ContainsKey(parent))
        {
			Debug.LogError("Could not find parent node with id " + parent);
        }
		string id = parent + "." + headerName;
		if(_nodes.ContainsKey(id))
		{ return _nodes[id]; }
		TreeListNode node = AppendNode(new object[] { headerName,"","",id }, _nodes[parent], id);
		_nodes.Add(id, node);
		_headers.Add(id);
		return node;
    }

	private string GetRoundName(int index)
    {
		return "Round_" + index.ToString(); 
    }

	private void AddProperty(string roundsKey, RoundFieldInfo info,  TreeListNode parent,  int roundIndex = 0)
    {
		object value = default;
		switch(info.Source)
        {
			case RoundFieldSource.Final:
                value = DataManager.Inst.GetFinalRoundValue(roundsKey, info.Name);
				break;
			case RoundFieldSource.Rounds:
				value = DataManager.Inst.GetRoundsValue(roundsKey, info.Name);
				break;
			case RoundFieldSource.Round:
				value = DataManager.Inst.GetRoundValue(roundsKey, roundIndex, info.Name);
				break;
			default:
				Debug.LogError($"Error! Field Source {info.Source} is not handled!");
				break;

        }
		string id = parent.Tag.ToString() + "." + info.Name;
        object[] data = new object[]
		{
			info.DisplayName,
			value,
			"",
			info.Name,
			id
		};
		//int index = isTimeProperty ? 1 : 2;
		//data[index] = value;
		TreeListNode node = AppendNode(data, parent, id);
		_nodes.Add(id, node);
    }

	private enum RoundFieldSource
    {
		None,
		Final,
		Round,
		Rounds
    }

	private class RoundFieldInfo
    {
		public RoundFieldSource Source;
		public System.Type Type;
		public string Name;
		public string DisplayName;

		public RoundFieldInfo(string name, RoundFieldSource source, System.Type type)
        {
			Source = source;
			Type = type;
			Name = name;
			DisplayName = name;
        }

		public RoundFieldInfo(string name, RoundFieldSource source, System.Type type, string fieldName)
        {
			Source = source;
			Type = type;
			Name = name;
			DisplayName = fieldName;
		}
    }
}