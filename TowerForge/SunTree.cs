// ///////////////////////////////////////////////////////////////////////////////////////////////////
// SunTree
// Author: Jake Muehle
// Start Date: June 2023
// Description: SunTree inherits from work done in CosmosTreeList, started in April 2020.
//
//   CosmosTreeList is a multi-functional extension of the DevExpress TreeList, which handles
//   several common tasks needed in Star Citizen data manipulation out-of-the-box.
//   
//   A core philosophy of the CosmosTreeList is to store a string-based ID (Typically GUIDs) in the .Tag parameter 
//   of each node you add to your tree. This ID must be unique, and will be passed back through mandatory
//   abstract functions that CosmosTreeList will call. It is the responsibility of the implementing tree
//   to determine what to do with incoming string ids. 
//
//   A second philosophy of the CosmosTreeList is to leave data manipulation up to the managers. The string ID
//   stored in the .Tag parameter represents a reference to a piece of data. Best practice is for the tree
//   to report a data alteration to the data represented by the ID, and let the manager handle the change.
//   As a result, each CosmosTreeList only represents what the data reads as, and manipulating this data is
//   done in a predictable, central place.

//*
#define CACHING
#define STYLECACHING

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList.Columns;
using System.Windows.Forms;
using System.ComponentModel;
using DevExpress.XtraEditors.Controls;
using System.Security.Cryptography.X509Certificates;
//using ToolCore;
//using CosmoCore;
using DevExpress.Utils.DragDrop;
using System.Runtime.CompilerServices;
using DevExpress.Utils.Behaviors;
using DevExpress.DirectX.Common.Direct2D;
using DevExpress.Utils;
//using DevExpress.XtraCharts.Native;
using DevExpress.Data;
using DevExpress.XtraTreeList.Accessibility;
//using DevExpress.Office.History;
//using DevExpress.XtraRichEdit.Model;
//using CosmosUI.Properties;
using DevExpress.Utils.Helpers;
using DevExpress.Utils.Drawing;
using DevExpress.XtraBars;
using TowerForge.Properties;
//using CosmoCommon;


public abstract partial class SunTree : TreeList
{
	protected abstract List<string> ContextMenuItems(string id,bool multiselect);
	protected abstract void ContextMenuItemClicked(string operation, string id, List<string> otherSelectedIDs);
	protected virtual bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected abstract void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType);
	protected abstract void RuntimeInit();
	protected abstract void ClearEverything();
	protected virtual List<string> GetIDsToPopulate()
	{
		throw new NotImplementedException();
	}
	protected virtual string GetTreeName() { return "CosmoTreeList"; }
	protected virtual void InitialPopulateAction(string id) 
	{
		throw new NotImplementedException();
	}

// Here's what you need to copy for newly instantiated trees: - 6/16/2020 9:09 PM - JM
/*
using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class InheritedSunTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	public InheritedSunTree() : base()
	{
		AddColumn("ID",100,100);
	}
	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id
		};
		return data;
	}
	protected override string GetTreeName()	{ return nameof(InheritedSunTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect) 
	{
		List<string> menu = new List<string>();
		if(string.IsNullOrEmpty(id)) { return menu; }
		return menu; 
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() { }
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}
//*/

/// <summary>
/// BindAction prepares an individual item for binding and assumes that the inheriting tree stores it in a list. AssignBinding() must be implemented. 
/// </summary>
/// <param name="id"></param>
	protected virtual void BindAction(string id) 
	{
		throw new NotImplementedException();
	}
	protected virtual void AssignBinding() 
	{
		throw new NotImplementedException();
	}

	private List<string> m_largeOperations;
	//protected abstract void HandleInternalNode(string id);
	protected bool m_useBarGradient = false;
	protected bool m_disableEditorDrawOnDisabledRows = false;
	protected bool m_disableEditorDrawOnErrorRows = false;
	protected Dictionary<string, TreeListNode> c_directoryNodes;
	protected bool m_doActionOnNodeSelect = true;

	protected List<ColumnData> m_columnData;
	private Dictionary<Type, CellInteractType> m_defaultEditorInteractions;
	private HashSet<Type> c_popupTypes;
	private Color m_parentColor;
	private const string MENU_GOTO_ERROR = "Go to error";
	private const string MENU_GOTO_WARNING = "Go to warning";
	private const string MENU_EXPAND = "Expand all";
	private const string MENU_COLLAPSE = "Collapse all";
	private TreeListNode m_contextNode;
	private Dictionary<TreeListNode, StyleInfo> c_styleCache;
	private Dictionary<TreeListNode,Dictionary<int, RepositoryItem>> c_editorCache;
	private HashSet<TreeListNode> c_dirtyStyleNodes;
	//private List<object> m_showCancelIconWhileHoveringSources;
	//private List<object> m_showNormalIconWhileHoveringSources;
	private Dictionary<SunTree,ExternalTreeInfo> m_registeredDragDropControls;
	private List<SunTree> m_validDropTargets;
	public Action<string> OnNodeSelect;
	protected DragDropBehavior m_dragDrop;
	private bool m_supressCustomContent;
	private string m_emptyMessage;
	private bool m_showEmptyMessage;
	//private ToolTipController m_errorTooltip;

	private TreeListNode m_root;
	private string m_rootName;
	public string RootName 
	{ 
		get
		{
			return m_rootName;
		}
		set
		{
			m_rootName = value;
			if(m_root != null)
			{ m_root.SetValue(0, m_rootName); }
		}
	}

	// OK... let me think this through...
	// Generically, I want to be able to access a function that happens immediately after a repositoryeditor shows the active editor. 
	// I also want a function that happens when the editor is closed.
	// which function is called might be different, depending on the string ID of the thing we're using.

	#region init
	public SunTree()
	{
		RootName = "Root";
		m_largeOperations = new List<string>();
		OptionsBehavior.Editable = false; // this is handled by the mouse events in this class. - 7/14/2020 5:35 PM - JM
		OptionsSelection.MultiSelect = true; // this is too common a use case to leave false by default. - 8/4/2020 2:59 PM - JM
		OptionsBehavior.ShowToolTips = false; // Showed tooltip for truncated cells. Just annoying, really. - 7/31/2020 10:51 AM - JM
		m_validDropTargets = new List<SunTree>();
		m_validDropTargets.Add(this);
		m_registeredDragDropControls = new Dictionary<SunTree, ExternalTreeInfo>();
		m_parentColor = Color.Transparent;
		Appearance.FocusedRow.Font = new Font(Appearance.FocusedRow.Font.FontFamily, Appearance.FocusedRow.Font.Size, FontStyle.Bold);
		m_columnData = new List<ColumnData>();
		c_directoryNodes = new Dictionary<string, TreeListNode>();
		c_dirtyStyleNodes = new HashSet<TreeListNode>();
		c_styleCache = new Dictionary<TreeListNode, StyleInfo>();
		c_editorCache = new Dictionary<TreeListNode, Dictionary<int, RepositoryItem>>();
		InitInteractionTypes();

		//InitializeComponent();
		FormatRules.Clear();
		SetupDragDrop();
		InitToolTip();
		AddDragOverEvent(OnDragOver);
		AddDropEvent(OnDropNodes);
		AddBeginDragDropEvent(OnBeginDragDrop);
		AddEndDragDropEvent(OnEndDragDrop);
		AddDragLeaveEvent(OnLeaveDragDrop);
		OptionsBehavior.ExpandNodesOnFiltering = true;
		OptionsFilter.FilterMode = FilterMode.Extended;
		// grumble grumble dev express... if you don't disable their nodeMenu stuff then all click events get intercepted...
		OptionsMenu.EnableNodeMenu = false;
		OptionsBehavior.AllowExpandOnDblClick = false;
		OptionsView.AutoWidth = false;
		TreeLevelWidth = 12;
		MouseDoubleClick += new MouseEventHandler(OnMouseDoubleClick);
		MouseUp += new MouseEventHandler(OnMouseUp);
		MouseDown += new MouseEventHandler(OnMouseDown);
		//MouseHover += new EventHandler(OnMouseHover);
		NodeCellStyle += new GetCustomNodeCellStyleEventHandler(OnCellStyle);
		CustomNodeCellEdit += new GetCustomNodeCellEditEventHandler(OnAlwaysAssignCustomEditors);
		CustomNodeCellEditForEditing += new GetCustomNodeCellEditEventHandler(OnAssignCustomEditors);
		CustomDrawNodeIndicator += CosmosTreeList_CustomDrawNodeIndicator;
		CustomDrawNodeCell += OnDrawNodeCell;
		CustomColumnSort += OnSortNodes;
		FocusedNodeChanged += new FocusedNodeChangedEventHandler(OnFocusedNodeChanged);
		CellValueChanged += new CellValueChangedEventHandler(OnCellValueChanged);
		ToolTipController.GetActiveObjectInfo += ToolTipController_GetActiveObjectInfo;
		//ToolTipController.KeepWhileHovered = false;
		OptionsView.RowImagesShowMode = RowImagesShowMode.InCell;
		CustomDrawEmptyArea += CTL_CustomDrawEmptyArea;
		// http://dotnetfacts.blogspot.com/2009/01/identifying-run-time-and-design-mode.html - 5/18/2020 6:29 PM - JM
		if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
		{ AtRuntime(); }
	}

	#region empty area message
	private void CTL_CustomDrawEmptyArea(object sender, DevExpress.XtraTreeList.CustomDrawEmptyAreaEventArgs e)
	{
		// from https://docs.devexpress.com/WindowsForms/DevExpress.XtraTreeList.TreeList.CustomDrawEmptyArea - 8/4/2020 10:15 PM - JM
		if(!m_showEmptyMessage)
		{
			e.DefaultDraw();
			e.Handled = true;
			return;
		}
		e.Appearance.FontStyleDelta = FontStyle.Bold;
		e.Appearance.FontSizeDelta = 4;
		System.Drawing.StringFormat format = new StringFormat();
		format.Alignment = StringAlignment.Center;
		format.LineAlignment = StringAlignment.Center;

		e.Cache.DrawString(m_emptyMessage, e.Appearance.Font, e.Cache.GetSolidBrush(Color.Gray), e.Bounds, format);
		e.Handled = true;
	}

	protected void SetEmptyMessage(string message)
	{
		if(message == m_emptyMessage) { return; }
		m_emptyMessage = message;
		Refresh();
	}

	protected void SetShowEmptyMessage(bool show)
	{
		if(show == m_showEmptyMessage) { return; }
		m_showEmptyMessage = show;
		Refresh();
	}
	#endregion empty area message

	public void PopulateTree(Action<int, string> reportProgress, ListExpand expandType = ListExpand.ImmediateChildren)
	{
		PrePopulate();
		List<string> ids = GetIDsToPopulate();
		int seg = 100;
		int tic = ids.Count / seg;
		if(tic == 0) { tic = 1; }
		for(int i = 0; i < ids.Count; i++)
		{
			if(i % tic == 0) { reportProgress?.Invoke((seg * i) / ids.Count, $"Populating {GetTreeName()}..."); }
			InternalPopulateAction(ids[i]);
		}
		PostPopulate();
		InitExpand(expandType);
	}

	protected virtual void PrePopulate()
    {

    }

	protected virtual void PostPopulate()
    {

    }

	/// <summary>
	/// To use BindTree(), both BindAction() and AssignBinding() must be implemented.
	/// </summary>
	/// <param name="reportProgress"></param>
	/// <param name="expandType"></param>
	public void BindTree(Action<int,string> reportProgress)
	{
		//BeginOp("BindNodes");
		List<string> ids = GetIDsToPopulate();
		int seg = 100;
		int tic = ids.Count / seg;
		if(tic == 0) { tic = 1; }
		for(int i = 0; i < ids.Count; i++)
		{
			if(i % tic == 0) { reportProgress?.Invoke((seg * i) / ids.Count, $"Binding {GetTreeName()}..."); }
			InternalBindAction(ids[i]);
		}
		AssignBinding();
		InitExpand(ListExpand.All);
		//EndOp("BindNodes");
	}

	private void InternalBindAction(string id)
	{
		if(InvokeRequired)
		{
			Action<string> d = InternalBindAction;
			Invoke(d, new object[] { id });
			return;
		}
		BindAction(id);
	}

	private void InitExpand(ListExpand expandType)
	{
		if(InvokeRequired)
		{
			Action<ListExpand> d = new Action<ListExpand>(InitExpand);
			Invoke(d, new object[] { expandType });
			return;
		}

		switch(expandType)
		{
			case ListExpand.ImmediateChildren:
				ExpandRootSafe();
				break;
			case ListExpand.All:
				ExpandAll();
				break;
		}
	}

	private void InternalPopulateAction(string id)
	{
		if(InvokeRequired)
		{
			Action<string> d = InternalPopulateAction;
			Invoke(d, new object[] { id });
			return;
		}
		InitialPopulateAction(id);
	}

	#region hints
	// https://supportcenter.devexpress.com/ticket/details/t198860/show-image-in-node-indicator - 10/21/2020 4:48 PM - JM
	private void CosmosTreeList_CustomDrawNodeIndicator(object sender, CustomDrawNodeIndicatorEventArgs e)
	{
		if (!c_styleCache.ContainsKey(e.Node) || c_styleCache[e.Node].StatusImage == null) { return; }
		e.Cache.Graphics.DrawImage(c_styleCache[e.Node].StatusImage,
			new Point(e.Bounds.Left + (e.Bounds.Width - c_styleCache[e.Node].StatusImage.Width) / 2,
			e.Bounds.Top + (e.Bounds.Height - c_styleCache[e.Node].StatusImage.Height) / 2));
		e.Handled = true;
	}

	private void ToolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
	{
		TreeListHitInfo hit = CalcHitInfo(e.ControlMousePosition);
		if(hit.HitInfoType == HitInfoType.RowIndicator)
		{
			TreeListNode node = hit.Node;
			string id = (string)node.Tag;
			try
			{
				if (!CheckForCondition(node, HasWarning) && !CheckForCondition(node, HasError))
				{
					e.Info = null;
					return;
				}
				string message = "";
				List<string> errors = GetErrors(id.ToString(), false);
				List<string> warnings = GetWarnings(id.ToString(), false);
				if (errors.Count == 0 && warnings.Count == 0)
				{
					bool hasError = CheckForCondition(node, HasError);
					string singleproblemType = hasError ? "an error" : "a warning";
					string problemType = hasError ? "error" : "warning";
					message = $"A child of this node has {singleproblemType}. Right click and \"Go to {problemType}\" to jump to it.";
				}
				else
				{
					StringBuilder sb = new StringBuilder();
					if (errors.Count > 0)
					{
						sb.AppendLine("Errors:");
						foreach (string err in errors)
						{
							sb.AppendLine(err);
						}
					}
					if (warnings.Count > 0)
					{
						if (errors.Count > 0) { sb.AppendLine(""); }
						sb.AppendLine("Warnings:");
						foreach (string war in warnings)
						{
							sb.AppendLine(war);
						}
					}
					message = sb.ToString();
				}
				ToolTipControlInfo info = new ToolTipControlInfo(id, message);
				if (hit.HitInfoType == HitInfoType.Cell)
					info.HideHintOnMouseMove = true;
				e.Info = info;
			}
			catch (Exception ex)
			{
				Debug.LogError("Error while trying to display tooltip: " +ex.Message);
			}
		}
		else
		{
			e.Info = null;
		}
	}
	#endregion hints

	protected virtual bool SpeedRender(int column, TreeListNode node)
	{
		return true;
	}

	public override void ExpandAll()
	{
		if(InvokeRequired)
		{
			Action d = ExpandAll;
			Invoke(d);
			return;
		}
		base.ExpandAll();
	}

	public void ExpandRootSafe()
	{
		if(InvokeRequired)
		{
			Action d = ExpandRootSafe;
			Invoke(d);
			return;
		}
		GetRoot().Expand();
	}

	private void AtRuntime()
	{
		Signals.LargeOperationBegin.Register(LargeOperationBegin);
		Signals.LargeOperationEnd.Register( LargeOperationEnd);
		Signals.ClearEverything.Register(ClearThisList);
		Signals.DisableSunTreesExcept.Register(Disable);
		Signals.EnableAllSunTrees.Register(Enable);
		RuntimeInit();
	}

	public string CurrentHolds()
    {
		StringBuilder sb = new StringBuilder();
		foreach(string op in m_largeOperations)
        {
			sb.Append(op + ", ");
        }
		return sb.ToString().Trim(' ', ',');
    }

	private void Disable(List<SunTree> except)
	{
		string d1 = Name;
		foreach(var l in except)
		{
			string d2 = l.Name;
		}
		if (except.Contains(this)) { return; }
		Enabled = false;
	}

	private void Enable()
	{
		Enabled = true;
	}

	private void OnFocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
	{
		if(e.Node == null || string.IsNullOrEmpty((string)e.Node.Tag) || !m_doActionOnNodeSelect) { return; }
		OnNodeSelect?.Invoke(e.Node.Tag.ToString());
	}

	protected void ClearThisList()
	{
		c_directoryNodes.Clear();
		Nodes.Clear();
		m_root = null;
		ClearEverything();
	}


	private void LargeOperationBegin(string name)
	{
		if (InvokeRequired)
		{
			Action<string> d = new Action<string>(LargeOperationBegin);
			Invoke(d, new object[] { name });
			return;
		}
		if (m_largeOperations.Count() == 0)
		{
			SuspendLayout();
			BeginUpdate();
		}


		m_largeOperations.Add(name);
	}

	private void LargeOperationEnd(string name)
	{
		if(InvokeRequired)
		{
			Action<string> d = new Action<string>(LargeOperationEnd);
			Invoke(d, new object[] { name });
			return;
		}
		m_largeOperations.Remove(name);
		if (m_largeOperations.Count() == 0)
		{
			EndUpdate();
			ResumeLayout();
		}
	}

	private void InitInteractionTypes()
	{
		m_defaultEditorInteractions = new Dictionary<Type, CellInteractType>();
		m_defaultEditorInteractions.Add(typeof(TextEdit), CellInteractType.MouseDoubleClick);
		m_defaultEditorInteractions.Add(typeof(SpinEdit), CellInteractType.MouseDown);
		m_defaultEditorInteractions.Add(typeof(ComboBoxEdit), CellInteractType.MouseDown);
		m_defaultEditorInteractions.Add(typeof(CheckedComboBoxEdit), CellInteractType.MouseDown);

		c_popupTypes = new HashSet<Type>();
		c_popupTypes.Add(typeof(ComboBoxEdit));
		c_popupTypes.Add(typeof(ImageComboBoxEdit));
		c_popupTypes.Add(typeof(ColorPickEdit));
		c_popupTypes.Add(typeof(CheckedComboBoxEdit));
		// SUN JAKE - Disabled
		//c_popupTypes.Add(typeof(SearchLookUpEdit));
	}

	protected void ChangeDefaultInteraction(Type classType, CellInteractType interaction)
	{
		// if its not in the dictionary, it should be added to the dictionary in the InitInteractionTypes function. - 5/3/2020 1:35 AM - JM
		if (!m_defaultEditorInteractions.ContainsKey(classType)) { throw new NotImplementedException(); }
		m_defaultEditorInteractions[classType] = interaction;
	}

	public TreeListNode GetRoot()
	{
		if(m_root == null)
		{
			m_root = AppendNode(new object[] { RootName }, -1, "ROOT");
			c_directoryNodes.Add((string)m_root.Tag, m_root);
		}
		return m_root;
	}

	protected virtual object[] GetRootData()
	{
		object[] data = new object[]
		{
			RootName
		};
		return data;
	}

	public void RefreshRoot()
	{
		Signals.LargeOperationBegin.Send("CTL_RootRefresh");
		GetRoot(); // juuuuuust in case it wasn't initialized. - 12/9/2020 4:46 AM - JM
		object[] data = GetRootData();
		for(int i = 0; i < data.Length; i++)
		{
			m_root.SetValue(i, data[i]);
		}
		Signals.LargeOperationEnd.Send("CTL_RootRefresh");
	}

	public TreeListNode GetDirectoryNode(string dir)
	{
		string[] dirs = dir.Split('\\');
		TreeListNode current = GetRoot();
		for (int i = 0; i < dirs.Length - 1; i++) // adding the -1 is easy here because that will always be the file name. It's a hack. Sue me. - 4/7/2020 8:59 PM - JM
		{
			bool found = false;
			foreach (TreeListNode child in current.Nodes)
			{
				if ((string)child.GetValue(0) == dirs[i])
				{
					current = child;
					found = true;
					break;
				}
			}
			if (!found)
			{
				current = AppendNode(new object[] { dirs[i] }, current, (string)current.Tag + "\\" + dirs[i]);
				c_directoryNodes.Add((string)current.Tag, current);
			}

		}
		return current;
	}

	protected void ClearEmptyDirectoryNodes()
	{
		LargeOperationBegin("CTL_ClearDirectoryNodes"); // this doesn't need to be sent via signal because we're already in the class that handles it. - 8/3/2020 2:30 PM - JM
		ClearDirectoryIfEmpty(GetRoot());
		LargeOperationEnd("CTL_ClearDirectoryNodes");
	}

	private void ClearDirectoryIfEmpty(TreeListNode dNode)
	{
		string id = (string)dNode.Tag;
		if (!c_directoryNodes.ContainsKey(id)) { return; }
		for (int i = dNode.Nodes.Count - 1; i >= 0; i--)
		{
			ClearDirectoryIfEmpty(dNode.Nodes[i]);
		}
		if (dNode.Nodes.Count == 0 && id != "ROOT")
		{
			c_directoryNodes[id].Remove();
			c_directoryNodes.Remove(id);
		}
	}

	#endregion init
	#region input masking
	public const String TextEditorMaskGenericString = "[0-9a-zA-Z_\\-'!@#$%^&*()+={}<>,.?:;|/\\\\\" \\[\\]]*";      // Four backslashes required to allow the backslash in a RegEx expression.  (The language's string parser will remove two of them when de-escaping it for the string and then the RegEx
																													// required for the hyphen so that it's not treated as a range.  6/4/2017 @ 9:33 AM CST (ATX) - TZ
	public const String StandardTextEditorMask = "[a-zA-Z][0-9a-zA-Z_\\-()']*";
	public const String StandardTextEditorMaskWithSpace = "[a-zA-Z][0-9a-zA-Z_\\-()' ]*";
	public const String TextEditorMaskZeroAndPositiveFloats = @"\d{1,10}\.\d{0,4}";             // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  As opposed to the previous variant (@"\d+(\R.\d{0,4})?") this doesn't flip the decimal point for a comma with a German keyboard layout.  4/11/2018 @ 11:28 AM CST (ATX) - TZ
	public const String TextEditorMaskFloats = @"-?\d{1,10}\.\d{0,4}";                          // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  As opposed to the previous variant (@"-?\d+(\R.\d{0,4})?") this doesn't flip the decimal point for a comma with a German keyboard layout.  4/11/2018 @ 11:28 AM CST (ATX) - TZ
	public const String TextEditorMaskZeroAndNegativeFloats = @"-\d{1,10}\.\d{0,4}";                    // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  As opposed to the previous variant (@"-\d+(\R.\d{0,4})?") this doesn't flip the decimal point for a comma with a German keyboard layout.  4/11/2018 @ 11:28 AM CST (ATX) - TZ
	public const String TextEditorMaskIntegersOnly = @"-?[0-9]{0,10}";                          // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  6/4/2017 @ 9:09 AM CST (ATX) - TZ
	public const String TextEditorMaskZeroOrPositiveIntegers = @"[0-9]{0,10}";                  // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  6/4/2017 @ 9:09 AM CST (ATX) - TZ
	public const String TextEditorMaskZeroOrNegativeIntegers = @"-\d+";                         // Use with DevExpress.XtraEditors.Mask.MaskType.RegEx.  6/4/2017 @ 9:09 AM CST (ATX) - TZ

	private static void AssignTextEditorMask(DataEditorType type, RepositoryItemTextEdit te)
	{
		switch (type)
		{
			case DataEditorType.Integer:				te.Mask.EditMask = SunTree.TextEditorMaskIntegersOnly;			break;
			case DataEditorType.IntegerZeroOrPositive:	te.Mask.EditMask = SunTree.TextEditorMaskZeroOrPositiveIntegers;	break;
			case DataEditorType.IntegerZeroOrNegative:	te.Mask.EditMask = SunTree.TextEditorMaskZeroOrNegativeIntegers;	break;
			case DataEditorType.Float:					te.Mask.EditMask = SunTree.TextEditorMaskFloats;					break;
			case DataEditorType.FloatZeroOrPositive:	te.Mask.EditMask = SunTree.TextEditorMaskZeroAndPositiveFloats;	break;
			case DataEditorType.FloatZeroOrNegative:	te.Mask.EditMask = SunTree.TextEditorMaskZeroAndNegativeFloats;	break;
			case DataEditorType.String:					te.Mask.EditMask = SunTree.TextEditorMaskGenericString;			break;
			case DataEditorType.StringNoSpaces:			te.Mask.EditMask = SunTree.StandardTextEditorMaskWithSpace;		break;
			case DataEditorType.Default:				te.Mask.EditMask = SunTree.TextEditorMaskGenericString;			break;
		}

		if (te.Mask.EditMask != String.Empty)
			te.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
	}

	private static void AssignTextEditorMask(DataEditorType type, RepositoryItemMemoEdit te)
	{
		switch (type)
		{
			case DataEditorType.Integer:
			case DataEditorType.Long:
				te.Mask.EditMask = SunTree.TextEditorMaskIntegersOnly; break;
			case DataEditorType.IntegerZeroOrPositive:
			case DataEditorType.LongZeroOrPositive:
				te.Mask.EditMask = SunTree.TextEditorMaskZeroOrPositiveIntegers; break;
			case DataEditorType.IntegerZeroOrNegative:
			case DataEditorType.LongZeroOrNegative:
				te.Mask.EditMask = SunTree.TextEditorMaskZeroOrNegativeIntegers; break;
			case DataEditorType.Float:
			case DataEditorType.Double:
				te.Mask.EditMask = SunTree.TextEditorMaskFloats; break;
			case DataEditorType.FloatZeroOrPositive:
			case DataEditorType.DoubleZeroOrPositive:
				te.Mask.EditMask = SunTree.TextEditorMaskZeroAndPositiveFloats; break;
			case DataEditorType.FloatZeroOrNegative:
			case DataEditorType.DoubleZeroOrNegative:
				te.Mask.EditMask = SunTree.TextEditorMaskZeroAndNegativeFloats; break;
			case DataEditorType.StringNoSpaces: 
				te.Mask.EditMask = SunTree.StandardTextEditorMaskWithSpace; break;
			case DataEditorType.String:
			case DataEditorType.Default:
			default:
				te.Mask.EditMask = SunTree.TextEditorMaskGenericString; break;
		}

		if (te.Mask.EditMask != String.Empty)
			te.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
	}


#endregion input masking
#region column creation
	public delegate RepositoryItem ColumnEditorTypeCallBack(string id);
	protected delegate bool SetCellDataCallback(string id, object value);
	// SUN JAKE - Disabled DataCore Inheritance stuff.
	//protected delegate InheritState SetDataCoreImage(string id);
	protected delegate CellPip SetCellPipCallback(string id, object value);
	protected delegate int DataImportance(string id);



	protected ColumnData AddColumn(string caption, int minWidth = 100, int defaultWidth = 100, ColumnEditorTypeCallBack setEditors = null, 
		SetCellDataCallback setData = null, bool multiedit = false, string tooltip = null, bool visible = true, FormatConditionRuleBase formatRule = null, 
		SetCellPipCallback cellPipFunc = null, /*DataImportance importanceFunc = null,*/ SortOrder sortOrder = SortOrder.None, int separatorWidth = 0, bool fxed = false)
	{
		if (multiedit) 
		{
			OptionsSelection.MultiSelect = true; 
		}
		if(setEditors == null) { setEditors = Default_ReadOnlyTextEditor; }

		ColumnData data = CreateColumnData(caption, setEditors,setData,multiedit,cellPipFunc/*,importanceFunc*/);

		TreeListColumn c = new TreeListColumn();
		c.Caption = caption;
		c.FieldName = caption;
		c.MinWidth = minWidth;
		c.Width = defaultWidth;
		c.Visible = visible; // We can't assume visible is always true, because there might be supplemental information to store in a hidden column...?
		c.VisibleIndex = data.ColumnIndex; // By assigning the index it will remain constant even after changing position/order - 5/1/2020 3:42 PM - JM
		c.SeparatorWidth = separatorWidth;
		if(fxed)
        {
			c.Fixed = FixedStyle.Left;
        }

		if (c.VisibleIndex == 0) { c.Fixed = FixedStyle.Left; }
		if(data.ColumnIndex == 0)
		{
			c.SortOrder = SortOrder.Ascending;
		}
		if(sortOrder != SortOrder.None)
		{
			c.SortOrder = sortOrder;
			for(int i = 0; i < Columns.Count; i++)
			{
				Columns[i].SortOrder = SortOrder.None;
			}
		}
		if(data.DetermineImportance != null)
		{
			c.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
		}

		// somehow, rules are saved to properties in design-time if adjustments are made to the form this tree is in. 
		// Suppressing this rule addition in design time fixes that problem. - 11/13/2020 5:18 PM - JM
		if(LicenseManager.UsageMode != LicenseUsageMode.Designtime)
		{
			if(formatRule != null)
			{
				DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule newRule = new DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule();
				newRule.Name = Guid.NewGuid().ToString();
				newRule.Rule = formatRule;
				newRule.Column = c;
				FormatRules.Add(newRule);
			}
		}

		if(!string.IsNullOrEmpty(tooltip))
		{ c.ToolTip = tooltip; }

		Columns.Add(c);

		return data;
	}

	protected ColumnData CreateColumnData(string Caption, ColumnEditorTypeCallBack setEditors, SetCellDataCallback setData, bool multiedit, SetCellPipCallback setCellPip/*, DataImportance importanceFunc*/)
	{
		ColumnData cdata = new ColumnData(m_columnData.Count, Caption, setEditors, setData, multiedit,setCellPip/*, importanceFunc*/);

		m_columnData.Add(cdata);

		return cdata;
	}

	protected void RenameColumn(int index, string newName)
	{
		m_columnData[index].Rename(newName);
		Columns[index].Caption = newName;
	}

	protected void ClearColumns()
	{
		Columns.Clear();
		m_columnData.Clear();
	}

	protected class ColumnData
	{
			
		public delegate CellInteractType CellInteractTypeCallback(string id); // can we set the edit state for individual cells? 

		//RepositoryItem ColumnEditor = null;
		public string Caption { get; private set; }
		public int ColumnIndex { get; private set; }
		public ColumnEditorTypeCallBack OnAssignEditor { get; private set; }
		public SetCellDataCallback OnSetCellData { get; private set; }
		public Action<string> PostEditCallback { get; private set; }
		public CellInteractTypeCallback	OnCheckInteractType { get; private set; }
		public Action<string> OnCellExecution { get; private set; }
		public bool MultiEdit { get; private set; }
		public SetCellPipCallback OnSetCellPip { get; private set; }
		public DataImportance DetermineImportance { get; private set; }

		// Unlike Tony's code, all editors must be assigned via the custom cell editor event (ie, not via the column) - 5/2/2020 9:19 PM - JM
		public ColumnData(int index, string caption, ColumnEditorTypeCallBack setEditors, SetCellDataCallback setData, bool multiedit, SetCellPipCallback onSetCellPip/*, DataImportance importanceFunc*/)
		{
			ColumnIndex = index;
			Caption = caption;
			OnAssignEditor = setEditors;
			OnSetCellData = setData;
			MultiEdit = multiedit;
			OnSetCellPip = onSetCellPip;
			//DetermineImportance = importanceFunc;
		}

		public ColumnData SetInteractionType(CellInteractTypeCallback callback)
		{
			OnCheckInteractType = callback;
			return this;
		}

		public ColumnData SetCustomCellExecutionAction(Action<string> callback)
		{
			OnCellExecution = callback;
			return this;
		}

		public ColumnData SetPostEditCallback(Action<string> callback)
		{
			PostEditCallback = callback;
			return this;
		}

		public void Rename(string newName)
		{
			Caption = newName;
		}
	}




	// https://docs.devexpress.com/WindowsForms/17866/controls-and-libraries/tree-list/feature-center/appearances-and-look-and-feel/appearances/conditional-formatting - 11/12/2020 11:56 AM - JM
	protected FormatConditionRuleIconSet Rule_GreenRedArrows(decimal midpoint = 0)
	{
		FormatConditionRuleIconSet rule = new FormatConditionRuleIconSet();
		rule.IconSet = new FormatConditionIconSet();
		rule.IconSet.ValueType = FormatConditionValueType.Number;
		FormatConditionIconSetIcon icon1 = new FormatConditionIconSetIcon();
		FormatConditionIconSetIcon icon2 = new FormatConditionIconSetIcon();
		FormatConditionIconSetIcon icon3 = new FormatConditionIconSetIcon();

		// https://docs.devexpress.com/Xamarin/262304/grid/conditional-formatting/predefined-format-names
		icon1.PredefinedName = "Arrows3_1.png";
		icon2.PredefinedName = "Arrows3_2.png";
		icon3.PredefinedName = "Arrows3_3.png";

		icon1.Value = midpoint;
		icon1.ValueComparison = FormatConditionComparisonType.Greater;
		icon2.Value = midpoint;
		icon2.ValueComparison = FormatConditionComparisonType.GreaterOrEqual;
		icon3.Value = decimal.MinValue;
		icon3.ValueComparison = FormatConditionComparisonType.GreaterOrEqual;

		rule.IconSet.Icons.Add(icon1);
		rule.IconSet.Icons.Add(icon2);
		rule.IconSet.Icons.Add(icon3);

		return rule;
	}

	private FormatConditionRuleIconSet Rule_EmptyDefault()
	{
		FormatConditionRuleIconSet rule = new FormatConditionRuleIconSet();
		rule.IconSet = new FormatConditionIconSet();
		rule.IconSet.ValueType = FormatConditionValueType.Automatic;
		FormatConditionIconSetIcon icon = new FormatConditionIconSetIcon();
		icon.PredefinedName = "";
		icon.Value = 0;
		icon.ValueComparison = FormatConditionComparisonType.Greater;
		rule.IconSet.Icons.Add(new FormatConditionIconSetIcon());
		return rule;
	}

	//protected FormatConditionRuleIconSet Rule_DataCore(decimal midpoint = 0)
	//{
	//	FormatConditionRuleIconSet rule = new FormatConditionRuleIconSet();
	//	rule.IconSet = new FormatConditionIconSet();
	//	//rule.IconSet.ValueType = FormatConditionValueType.Number;
	//	rule.IconSet.ValueType = FormatConditionValueType.
	//	FormatConditionIconSetIcon icon1 = new FormatConditionIconSetIcon();
	//	FormatConditionIconSetIcon icon2 = new FormatConditionIconSetIcon();
	//	FormatConditionIconSetIcon icon3 = new FormatConditionIconSetIcon();
	//
	//	// https://docs.devexpress.com/Xamarin/262304/grid/conditional-formatting/predefined-format-names
	//	icon1.PredefinedName = "Arrows3_1.png";
	//	icon2.PredefinedName = "Arrows3_2.png";
	//	icon3.PredefinedName = "Arrows3_3.png";
	//
	//	icon1.Value = midpoint;
	//	icon1.ValueComparison = FormatConditionComparisonType.Greater;
	//	icon2.Value = midpoint;
	//	icon2.ValueComparison = FormatConditionComparisonType.GreaterOrEqual;
	//	icon3.Value = decimal.MinValue;
	//	icon3.ValueComparison = FormatConditionComparisonType.GreaterOrEqual;
	//
	//	rule.IconSet.Icons.Add(icon1);
	//	rule.IconSet.Icons.Add(icon2);
	//	rule.IconSet.Icons.Add(icon3);
	//
	//	return rule;
	//}

	//private void ctl_CustomDataUpdateFormatCondition(object sender, DevExpress.Custom)

	// https://docs.devexpress.com/WindowsForms/DevExpress.XtraEditors.FormatConditionRuleDataBar - 12/18/2020 4:47 PM - JM
	protected FormatConditionRuleDataBar Rule_PositiveNegativeBar(int min, int max)//decimal max)
	{
		FormatConditionRuleDataBar rule = new FormatConditionRuleDataBar();
		rule.AppearanceNegative.BackColor = UIColors.MIN_BAR_COLOR;
		rule.Appearance.BackColor = UIColors.MAX_BAR_COLOR;
		rule.Appearance.Options.UseBackColor = true;
		rule.AppearanceNegative.Options.UseBackColor = true;
		rule.DrawAxisAtMiddle = true;
		rule.AllowNegativeAxis = true;
		rule.AxisColor = Color.Black;
		rule.ShowBarOnly = true;
		rule.Minimum = min;
		rule.MinimumType = FormatConditionValueType.Number;
		rule.Maximum = max;
		rule.MaximumType = FormatConditionValueType.Number;

		return rule;
	}
		
	protected FormatConditionRuleDataBar Rule_PositiveNegativeBar(int max = 100)
	{
		return Rule_PositiveNegativeBar(-Math.Abs(max), Math.Abs(max));
	}

	// this function is now handled by the "importanceFunc" delegate when creating a column. - 6/28/2021 11:35 AM - JM
	//protected void SetColumnSortOrder(int columnIndex, SortOrder sort, bool reset)
	//{
	//	if(reset)
	//	{
	//		for(int i = 0; i < Columns.Count; i++)
	//		{
	//			Columns[i].SortOrder = SortOrder.None;
	//		}
	//	}
	//	Columns[columnIndex].SortOrder = sort;
	//}


	private void OnCellValueChanged(object sender, CellValueChangedEventArgs e)
	{
		MakeDirty(e.Node);
	}

	protected void MakeDirty(TreeListNode node, bool recursive = false) // this is only protected so that binding-style assignment can declare dirtiness. - 2/6/2021 6:36 PM - JM
	{
		if(node == null) { return; }
		c_editorCache.Remove(node);
		c_dirtyStyleNodes.Add(node);
		if(recursive)
		MakeDirty(node.ParentNode);
	}

	private void OnAlwaysAssignCustomEditors(object sender, GetCustomNodeCellEditEventArgs e)
	{
		if (!SpeedRender(e.Column.AbsoluteIndex, e.Node))
		{ OnAssignCustomEditors(sender, e); }
	}

	private void OnAssignCustomEditors(object sender, GetCustomNodeCellEditEventArgs e)
	{
		e.RepositoryItem = GetCachedEditor(e.Node, e.Column.AbsoluteIndex);
	}

	private RepositoryItem GetCachedEditor(TreeListNode node, int column)
	{
		if (c_editorCache.ContainsKey(node) && c_editorCache[node].ContainsKey(column))
		{
			return c_editorCache[node][column];
		}
		if (!c_editorCache.ContainsKey(node)) { c_editorCache.Add(node, new Dictionary<int, RepositoryItem>()); }
		//CosmosTreeList ctl = sender as CosmosTreeList;
		//int column = e.Column.AbsoluteIndex;
		string id = (string)node.Tag;
		if (m_disableEditorDrawOnDisabledRows && IsDisabled(id)) { return null; }
		if (m_disableEditorDrawOnErrorRows && HasError(id)) { return null; }
		RepositoryItem ri = m_columnData[column].OnAssignEditor?.Invoke(id);
		if(ri == null) { ri = Default_ReadOnlyTextEditor(); }
		//if (ri == null) { ri.ReadOnly = true; } // wow. Assigning null is not possible, apparently. That took a frustrating amount of time to track down... - 8/4/2020 2:41 PM - JM
		//if(e.RepositoryItem == null) { e.RepositoryItem = Default_ReadOnlyTextEditor(); }

		TreeListNode toDirty = node;
		if (m_columnData[column].OnSetCellData != null)
		{
			ri.Validating += new CancelEventHandler((sender2, e2) =>
			{
				Signals.LargeOperationBegin.Send("CosmoTreeCellEdited");
				//TreeListNode toDirty = // RIGHT HERE I need to know the cell that I'm editing, so I can dirty it and all its parents. - 10/29/2020 1:16 PM - JM
				object v = (sender2 as BaseEdit).EditValue;
				if(ri.Tag is DataEditorType)
				{

					// This was clever. Sorting didn't work properly with strings, so I converted everything back into their expected types post-edit. - 7/14/2023 2:29 PM - JM
					switch(GetDataType((DataEditorType)ri.Tag))
					{
						case DataEditorType.Long:
							v = Convert.ToInt64(v);
							(sender2 as BaseEdit).EditValue = (long)v;
							break;
						case DataEditorType.Integer:
							v = Convert.ToInt32(v);
							(sender2 as BaseEdit).EditValue = (int)v;
							break;
						case DataEditorType.Float:
							v = Convert.ToSingle(v);
							(sender2 as BaseEdit).EditValue = (float)v;
							break;
						case DataEditorType.Double:
							v = Convert.ToDouble(v);
							(sender2 as BaseEdit).EditValue = (double)v;
							break;
					}
				}
				e2.Cancel = !m_columnData[column].OnSetCellData(id, v);
				if (!e2.Cancel)
				{
					while (toDirty != null)
					{
						c_dirtyStyleNodes.Add(toDirty);
						toDirty = toDirty.ParentNode;
					}
				}
				PostValueEdit();
				Signals.LargeOperationEnd.Send("CosmoTreeCellEdited");
			});
		}
		c_editorCache[node].Add(column, ri);

		// Devexpress doesn't have a way to have the bar return as a solid color depending on the progress, and instead always uses the
		// gradient from StartColor to EndColor. This seems like the more difficult case, and they don't intend to change this.
		// Thus, I have to custom handle ProgressBars to set both the Start and the End color to a gradient that I find on my own.
		// Its a pain, but whatever. - 5/14/2020 5:42 PM - JM
		if (ri is RepositoryItemProgressBar && !m_useBarGradient)
		{
			object v = node.GetValue(column);
			if (v == null || string.IsNullOrWhiteSpace(v.ToString())) { return null; }
			if(!int.TryParse(v.ToString(),out int val)) { return null; }
			RepositoryItemProgressBar bar = ri as RepositoryItemProgressBar;
				
			// SUN JAKE - Disabled.
			//Color c = CosmoCore.Utils.GetGradient(UIColors.MIN_BAR_COLOR, UIColors.MAX_BAR_COLOR, (float)val / (float)bar.Maximum);
			Color c = UIColors.MAX_BAR_COLOR;
			bar.StartColor = c;
			bar.EndColor = c;
		}

		return ri;
	}

	protected virtual void PostValueEdit() { }

#endregion column creation
#region ToolTip
	private void InitToolTip()
	{
		ToolTipController = new ToolTipController();
		ToolTipController.InitialDelay = 0;
		ToolTipController.ToolTipAnchor = DevExpress.Utils.ToolTipAnchor.Cursor;
		ToolTipController.ToolTipLocation = ToolTipLocation.TopRight;
	}
#endregion
#region DragDrop
	private void SetupDragDrop()
	{
		BehaviorManager.Default.SetBehaviors(this, new Behavior[] {
			(Behavior)DragDropBehavior.Create(typeof(TreeListDragDropSource), true, true, true, true, new DragDropEvents())});
		m_dragDrop = BehaviorManager.Default.GetBehavior<DragDropBehavior>(this);

	}

	internal void RegisterDropTreeInternal(SunTree from, Action<string,List<string>> dropAction, bool dropAnywhere, string tooltip, string singularTooltip)
	{
		if (m_registeredDragDropControls.ContainsKey(from)) 
		{ 
			// TODO: Error: Passed tree has already been registered with this tree!
			throw new Exception(); 
		}
		m_registeredDragDropControls.Add(from, new ExternalTreeInfo(dropAction, tooltip, dropAnywhere,singularTooltip));
		from.AddValidDropTarget(this);
		string check1 = this.Name;
		string check2 = from.Name;
	}

	public delegate string DragOverTooltipDelegate(string id, bool plural);
	internal void RegisterDropTreeInternal(SunTree from, Action<string, List<string>> dropAction, bool dropAnywhere, DragOverTooltipDelegate getHoverText)
	{
		if (m_registeredDragDropControls.ContainsKey(from))
		{
			// TODO: Error: Passed tree has already been registered with this tree!
			throw new Exception();
		}
		m_registeredDragDropControls.Add(from, new ExternalTreeInfo(dropAction, getHoverText, dropAnywhere));
		from.AddValidDropTarget(this);

	}

	internal void AddValidDropTarget(SunTree target)
	{
		m_validDropTargets.Add(target);
	}

	public void RegisterValidDrop(SunTree target, Action<string, List<string>> dropAction, bool dropAnywhere, DragOverTooltipDelegate getHoverText)
	{
		target.RegisterDropTreeInternal(this, dropAction, dropAnywhere, getHoverText);
		return;

	}

	public void RegisterValidDrop(SunTree target, Action<string, List<string>> dropAction, bool dropAnywhere, string tooltip, string singularTooltip = null)
	{
		target.RegisterDropTreeInternal(this, dropAction, dropAnywhere, tooltip, singularTooltip);
		return;
	}

	public void RegisterValidDrop(SunTree target, Action<List<string>> dropAction, string tooltip, string singularTooltip = null)
	{
		Action<string, List<string>> action = (string id, List<string> ids) =>
		{
			dropAction(ids);
		};
		RegisterValidDrop(target, action, true, tooltip, singularTooltip);
	}

	public void AddDragOverEvent(DragOverEventHandler handler)
	{
		m_dragDrop.DragOver += handler;
	}

	public void AddDropEvent(DragDropEventHandler handler)
	{
		m_dragDrop.DragDrop += handler;
	}

	public void AddBeginDragDropEvent(BeginDragDropEventHandler handler)
	{
		m_dragDrop.BeginDragDrop += handler;
	}

	public void AddEndDragDropEvent(EndDragDropEventHandler handler)
	{
		m_dragDrop.EndDragDrop += handler;
	}

	public void AddDragLeaveEvent(DragLeaveEventHandler handler)
	{
		m_dragDrop.DragLeave += handler;
	}

	private void OnDragOver(object sender, DragOverEventArgs e)
	{
		Point ttPoint = new Point(e.Location.X, e.Location.Y - 10);
		List<TreeListNode> nodes = e.Data as List<TreeListNode>;
		//Signal.Send<string>(CCSig.DebugMessage, "Hovering over: " + ttPoint.ToString());
		if (e.Source != this) 
		{
			string current = Name;
			SunTree ctl = e.Source as SunTree;
			string from = ctl.Name;
			if(ctl == null) { return; } // let's only process CosmosTreeLists for now. - 8/3/2020 3:05 PM - JM
			if(m_registeredDragDropControls.ContainsKey(ctl))
			{
				TreeListHitInfo otherHit = CalcHitInfo(PointToClient(e.Location));
				string textString = "";
				if(m_registeredDragDropControls[ctl].CustomTextDelegate != null)
				{
					Signals.DebugMessage.Send(PointToClient(e.Location).ToString());
					string targetID = null;
					if(otherHit.Node != null) { targetID = otherHit.Node.Tag.ToString(); }
					textString = m_registeredDragDropControls[ctl].CustomTextDelegate(targetID, nodes.Count != 1);
				}
				else
				{
					textString = m_registeredDragDropControls[ctl].GetText(e);
				}
				ToolTipController.ShowHint(textString,ttPoint);
				if(m_registeredDragDropControls[ctl].LocationIrrelevant)
				{
					Rectangle rect = ((SunTree)e.Target).ViewInfo.ViewRects.Client;
					e.InsertIndicatorSize = rect.Size;
					e.InsertIndicatorLocation = PointToScreen(rect.Location);
					e.InsertType = InsertType.After;
					e.Handled = true;
				}
			}
			else
			{
				ToolTipController.HideHint();
				// SUN JAKE removed custom Cursor handling.
				//e.Cursor = DragAndDropCursors.NoneEffectCursor;
			}
			//e.Handled = true;
			return;
		}

		// At this point, we've determined to be hovering over the treelist this drag initiated from.
		// We now want to handle multi-edit behavior. - 8/3/2020 4:59 PM - JM

		TreeListHitInfo hit = CalcHitInfo(PointToClient(e.Location));
		if(nodes.Count == 0) { return; }
		// we should only proc multi-edit visuals if:
		// 1) We are hovering over a cell
		// 2) The ids of the dragging nodes and the target node are compatible (earlier we ensured that all of the nodes
		//    we are currently dragging must be compatible).
		if (hit.HitInfoType == HitInfoType.Cell &&  CheckMultiEditCompatability((string)nodes[0].Tag,(string)hit.Node.Tag,hit.Column.AbsoluteIndex))
		{
			// If this column is the first (name) column, assume that we're trying to multi-edit all of the possible multi-edit fields.
			object value = hit.Node.GetValue(hit.Column.AbsoluteIndex);
			if(hit.Column.AbsoluteIndex == 0)
			{
				ToolTipController.ShowHint($"Copy all applicable fields from \"{value}\" to dragged nodes", ttPoint);
				// SUN JAKE removed custom Cursor handling.
				//e.Cursor = DragAndDropCursors.CopyEffectCursor;
				//e.InsertIndicatorSize = hit.Node.Size;
				//e.InsertIndicatorLocation = PointToScreen(hit.Bounds.Location);
				e.Handled = true;
			}
			// further qualify multi-edit visuals: If this cell is flagged as multi-edit.
			else if(m_columnData[hit.Column.AbsoluteIndex].MultiEdit)
			{
				string name = Columns[hit.Column.AbsoluteIndex].FieldName;
				ToolTipController.ShowHint($"Use \"{value}\" for dragged nodes", ttPoint);
				e.InsertIndicatorSize = hit.Bounds.Size;
				e.InsertIndicatorLocation = PointToScreen(hit.Bounds.Location);
				// SUN JAKE removed custom Cursor handling.
				//e.Cursor = DragAndDropCursors.CopyEffectCursor;
				e.Handled = true;
			}

		}
		else if (hit.HitInfoType != HitInfoType.Row)
		{
			ToolTipController.HideHint();
		}
	}

	private void OnBeginDragDrop(object sender, EventArgs e)
	{
		// The registered drop lists are a lists of things that are allowed to drop onto this tree. When we start the
		// dragdrop, we want the opposite--trees that have this tree registered to allow drops onto them.
		// for this reason, the two lists are separate. - 8/4/2020 12:34 PM - JM
		Signals.DisableSunTreesExcept.Send( m_validDropTargets);
	}

	private void OnEndDragDrop(object sender, EventArgs e)
	{
		Signals.EnableAllSunTrees.Send();
	}

	private void OnLeaveDragDrop(object sender, EventArgs e)
	{
		ToolTipController.HideHint();
	}

	private void OnDropNodes(object sender, DragDropEventArgs e)
	{
		e.Action = DragDropActions.None;
		ToolTipController.HideHint();
		List<TreeListNode> nodes = e.Data as List<TreeListNode>;
		if(nodes.Count == 0) { return; }
		TreeListHitInfo hit = CalcHitInfo(PointToClient(e.Location));

		if (e.Source != this) 
		{
			SunTree ctl = e.Source as SunTree;
			if(ctl == null) { return; }
			if(m_registeredDragDropControls.ContainsKey(ctl))
			{
				List<string> ids = new List<string>();
				foreach(TreeListNode node in nodes)
				{
					ids.Add((string)node.Tag);
				}
				string targetID = ""; // null causes faults in TryGetValue for dictionaries. - 8/4/2020 2:13 PM - JM

				if (hit.HitInfoType == HitInfoType.Cell) 
				{
					TreeListNode targetNode = hit.Node;
					if(e.InsertType == InsertType.Before && targetNode.PrevNode != null)
					{
						targetNode = targetNode.PrevNode;
					}
					//else if(e.InsertType == InsertType.After)
					//{
					//	targetNode = targetNode.NextNode;
					//}
					targetID = (string)targetNode.Tag;
				}
				Signals.LargeOperationBegin.Send("CTL_NodeDrop");
#if !DEBUG
				try
				{
#endif
				m_registeredDragDropControls[ctl].OnDrop(targetID, ids);
				Signals.LargeOperationEnd.Send("CTL_NodeDrop");
#if !DEBUG
				}
				catch
				{
					Signal.Send<string>(CCSig.LargeOperationEnd, "CTL_NodeDrop");
				}
#endif

			}
			return; 
		}

		if (hit.HitInfoType == HitInfoType.Cell)
		{
			Signals.LargeOperationBegin.Send("CTLMultiEdit");
			string target = (string)hit.Node.Tag;
			// Let's talk about the CheckIDCompatibility call, for a second... *sits in chair backwards*
			// By this point, a few things should have happened:
			// 1) the nodes we are dragging should be homogenous in type. This is why we can pick the first one from the list
			//    to represent all of the nodes.
			// 2) This call prevents us from multi-editing between incompatible types of nodes. Because of this, the hover
			//    call should have already told us that this won't be a multi-edit, but a regular drop, instead.
			// 3) Since the only way to proc a multi-edit is to also be compatible, we only need to know the actual target
			//    of the drop (the insertType isn't necessary). This should corroborate with the hover type we got from the
			//    hover call.
			// - 6/15/2020 11:51 AM - JM
			if (AllowCopy(target, (string)nodes[0].Tag.ToString()))
			{
				if (hit.Column.AbsoluteIndex == 0)
				{
					for (int i = 1; i < m_columnData.Count; i++)
					{
						CopyFunction(i, nodes, hit);
						//if(m_columnData[i].MultiEdit && CheckMultiEditCompatability((string)nodes[0].Tag, target, i))
						//{
						//	object valObj = hit.Node.GetValue(hit.Column.AbsoluteIndex);
						//	foreach(TreeListNode copyTo in nodes)
						//	{
						//		if(m_columnData[hit.Column.AbsoluteIndex].OnSetCellData((string)copyTo.Tag, valObj))
						//			copyTo.SetValue(hit.Column.AbsoluteIndex, valObj);
						//	}
						//}
					}
				}
				else if (CopyFunction(hit.Column.AbsoluteIndex, nodes, hit))
				{
					// do nothing. Copyfunction will do its thing, or it won't and it will fail. - 4/18/2021 2:56 PM - JM
				}
				//else if (m_columnData[hit.Column.AbsoluteIndex].MultiEdit && CheckMultiEditCompatability((string)nodes[0].Tag, target,hit.Column.AbsoluteIndex)) 
				//{
				//	object valObj = hit.Node.GetValue(hit.Column.AbsoluteIndex);
				//	foreach (TreeListNode copyTo in nodes)
				//	{
				//		if (m_columnData[hit.Column.AbsoluteIndex].OnSetCellData((string)copyTo.Tag, valObj))
				//		copyTo.SetValue(hit.Column.AbsoluteIndex, valObj);
				//	}
				//}
			}
			else
			{
				foreach (TreeListNode dropping in nodes)
				{
					HandleSelfNodeDrop(hit.Node, (string)dropping.Tag, e.InsertType);
				}
			}
			Signals.LargeOperationEnd.Send("CTLMultiEdit");
		}

	}

	protected virtual bool AllowCopy(string targetId, string sourceId)
    {
		return true;
    }

	private bool CopyFunction(int columnIndex, List<TreeListNode> nodes,TreeListHitInfo hit)
	{
		string target = (string)hit.Node.Tag;
		if(m_columnData[columnIndex].MultiEdit && CheckMultiEditCompatability((string)nodes[0].Tag, target, columnIndex))
		{
			object valObj = hit.Node.GetValue(columnIndex);
			foreach(TreeListNode copyTo in nodes)
			{
				if(m_columnData[columnIndex].OnSetCellData((string)copyTo.Tag, valObj))
					copyTo.SetValue(columnIndex, valObj);
			}
			return true;
		}
		return false;
	}
#endregion DragDrop
#region makeEditors

	public delegate Dictionary<string,bool> OpenCheckedComboBoxCallback();
	//public delegate void CloseCheckedComboBoxCallback(Dictionary<string,bool> data);
	protected RepositoryItemCheckedComboBoxEdit CreateEditorCheckedComboBoxEdit(OpenCheckedComboBoxCallback openCallback)//, SetCellDataCallback closeCallback) // MULTI_EDIT_REFACTOR - 6/15/2020 2:34 PM - JM
	{
		RepositoryItemCheckedComboBoxEdit comboBox = new RepositoryItemCheckedComboBoxEdit();
		comboBox.TextEditStyle = TextEditStyles.DisableTextEditor;
		comboBox.AutoHeight = true;
		comboBox.DropDownRows = 25;
		comboBox.SeparatorChar = ',';
		comboBox.SynchronizeEditValueWithCheckedItems = false; // not sure what this one does...
			

		comboBox.Enter += new EventHandler((s,e) => 
		{ 
			if(openCallback == null) { return; }
			// populate the control with the dictionary handed to us by the callback.
			// convert to a list of KVPs so we can sort it. - 12/15/2020 4:19 PM - JM
			List<KeyValuePair<string, bool>> data = openCallback().ToList();
			data.Sort((p1,p2)=>p1.Key.CompareTo(p2.Key));

			comboBox.Items.Clear();
				
			foreach (KeyValuePair<string,bool> kvp in data)
			{
				comboBox.Items.Add(kvp.Key,kvp.Value);
			}
		});

		return comboBox;
	}

	public delegate List<string> ReturnStringListCallback();
		
	// SUN JAKE - Removed Search Look up.
	//protected RepositoryItemSearchLookUpEdit CreateEditorSearchDropDown(ReturnStringListCallback openCallback)
	//{
	//	RepositoryItemSearchLookUpEdit comboBox = new RepositoryItemSearchLookUpEdit();
	//
	//	comboBox.AutoHeight = true;
	//	comboBox.NullText = "---";
	//	comboBox.Enter += new EventHandler((s, e) =>
	//	{
	//		if(openCallback == null) { return; }
	//		comboBox.DataSource = openCallback();
	//		ShowEditor();
	//		((SearchLookUpEdit)ActiveEditor).ShowPopup();
	//	});
	//
	//	return comboBox;
	//}

	protected RepositoryItemComboBox CreateEditorDropDown(ReturnStringListCallback openCallback)//, TextValidationCallback closeCallback)
	{
			
		RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
		comboBox.AutoHeight = true;
		comboBox.DropDownRows = 25;
		comboBox.ShowDropDown = ShowDropDown.SingleClick;
		comboBox.TextEditStyle = TextEditStyles.DisableTextEditor;
			

		comboBox.Enter += new EventHandler((s, e) =>
		{
			if (openCallback == null) { return; }
			List<string> data = openCallback();
			comboBox.Items.Clear();

			foreach (string v in data)
			{
				if(v == null) { continue; }
				comboBox.Items.Add(v);
			}
			ShowEditor();
			((ComboBoxEdit)ActiveEditor).ShowPopup();
		});

		return comboBox;
	}

	//protected RepositoryItemButtonEdit CreateEditorButton(string text)
    //{
	//	RepositoryItemButtonEdit button = new RepositoryItemButtonEdit();
	//	button.AutoHeight = true;
	//	button.Buttons[0].Kind = ButtonPredefines.Glyph;
	//	button.Buttons[0].Caption = text;
	//
	//	return button;
    //}

	protected RepositoryItemComboBox CreateEditorDropDown<T>() where T : Enum
    {
		return CreateEditorDropDown(() =>
		{
			return Enum.GetNames(typeof(T)).ToList();
		});
    }

	protected RepositoryItemColorPickEdit CreateEditorColor()
    {
		return new RepositoryItemColorPickEdit();
    }

	protected RepositoryItem Default_ReadOnlyTextEditor()
	{
		return Default_ReadOnlyTextEditor("");
	}

	private RepositoryItem Default_ReadOnlyTextEditor(string id)
	{
		// the default editor is being instantiated and assigned while searching through treelist nodes.
		// instead of defaulting to assigning a text edit (which takes time), just use null, and then
		// check if the assigned editor is null when checking if something is read-only. - 8/3/2020 12:57 PM - JM

		RepositoryItemTextEdit editor = new RepositoryItemTextEdit();
		editor.ReadOnly = true;
		return editor;
		//return null;//
	}

	protected RepositoryItem Quick_CreateTextEditor(string id)
	{
		return CreateEditorTextEdit();
	}

	//protected RepositoryItem Quick_CreateLocStringEditor(string id)
	//{
	//	return Quick_CreateLocStringEditor();
	//}

	//protected RepositoryItem Quick_CreateLocStringEditor()
	//{
	//	return CreateEditorSearchDropDown(() =>
	//	{
	//		return Tool.Instance.Loc.GetIDs();
	//	});
	//}

	protected RepositoryItem Quick_CreateFloatEditor(string id)
    {
		return CreateEditorTextEdit(DataEditorType.Float);
    }

	protected T GetEnum<T>(object value)
	{
		try
		{
			return (T)Enum.Parse(typeof(T), value.ToString());
		}
        catch
        {
			Debug.LogError($"Could not parse string {value} to enum {typeof(T).Name}!");
			return	default(T);
        }
    }


    protected HashSet<T> GetEnumList<T>(object checkedListEditValue) where T : struct
    {
		string toParse = checkedListEditValue.ToString();
        string[] vals = toParse.Split(',');
		HashSet<T> enumList = new HashSet<T>();
        foreach (string val in vals)
        {
			if(Enum.TryParse<T>(val, out T result))
            {
				enumList.Add(result);
            }
        }
		return enumList;
    }

	public delegate bool TextValidationCallback(string id, string value); // Gives 'true' for greenlight.

	// Got rid of KeyHandlerCallback, because I don't know why it was here in the first place? - 5/3/2020 1:22 AM - JM
	protected RepositoryItemTextEdit CreateEditorTextEdit(DataEditorType type = DataEditorType.Default)//, TextValidationCallback validatingCallback = null)
	{
		RepositoryItemTextEdit editor = new RepositoryItemTextEdit();
		AssignTextEditorMask(type, editor);

		editor.AutoHeight = false;
		editor.Mask.BeepOnError = true;
		editor.Mask.ShowPlaceHolders = false;
		editor.Name = "Text Editor";
		editor.Validating += TextEditValidate_DefaultCallback;
		editor.ValidateOnEnterKey = true;
		editor.Tag = type; // I need a way to store the dataEditorType inside this editor so that I can convert the Edit value after a validation... - 12/1/2020 3:01 PM - JM
		return editor;
	}

	protected RepositoryItemMemoEdit CreateMultiLineTextEdit(DataEditorType type = DataEditorType.Default)
	{
		OptionsBehavior.AutoNodeHeight = true; // If we want multiline, we want this. - 7/2/2020 7:33 PM - JM
		RepositoryItemMemoEdit editor = new RepositoryItemMemoEdit();
		AssignTextEditorMask(type, editor);
		editor.Mask.BeepOnError = true;
		editor.Validating += TextEditValidate_DefaultCallback;
		editor.ValidateOnEnterKey = true;

		return editor;
	}

	protected RepositoryItem Quick_Multiline(string id)
	{
		return CreateMultiLineTextEdit();
	}

	protected RepositoryItem Quick_CreateBar(string id)
	{
		return CreateEditorProgressBar();
	}

	protected RepositoryItemProgressBar CreateEditorProgressBar()
	{
		RepositoryItemProgressBar bar = new RepositoryItemProgressBar();
		bar.LookAndFeel.UseDefaultLookAndFeel = false;
		bar.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
		bar.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
		bar.Step = 1;
		bar.Maximum = 100;
		bar.StartColor = UIColors.MIN_BAR_COLOR;
		bar.EndColor = UIColors.MAX_BAR_COLOR;
			
		// color creation has to be handled in the actual nodeeditor assignment. Thanks, DevExpress. - 5/14/2020 5:45 PM - JM

		return bar;
	}

	protected RepositoryItemTrackBar CreateEditorSlider(int segments)
	{
		OptionsBehavior.AutoNodeHeight = true;
		RepositoryItemTrackBar bar = new RepositoryItemTrackBar();
		bar.Minimum = 0;
		bar.Maximum = segments;
		//bar.TickFrequency = 10;
		//bar.LargeChange = 10;
		//bar.SmallChange = 10;
		bar.TickStyle = TickStyle.Both;
		bar.AutoSize = true;

		return bar;
	}

	protected RepositoryItemSpinEdit CreateEditorSpinEdit(Int32 Min, Int32 Max, CancelEventHandler validatingCallback,  String TextEditMask = null)
	{
		// SpinEdit controls get a custom callback function - rather than RepositoryItemSpinEdit.ValueChanged - because if a min/max value is specified and the validation logic clamps the value the ValueChanged callback will not be called.  DevExpress has stated
		// they don't intend to change that - which is idiotic - so instead a callback function can be specified that will specify both the SpinEdit control and the new value (of type Decimal.)  7/21/2018 @ 3:57 PM CST (ATX) - TZ

		RepositoryItemSpinEdit SpinEditEditor = new RepositoryItemSpinEdit();

		SpinEditEditor.AutoHeight = false;

		SpinEditEditor.Mask.EditMask = (TextEditMask == null) ? "N00" : TextEditMask;

		if (TextEditMask != null)
			SpinEditEditor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;

		SpinEditEditor.MaxValue = new decimal(new int[] { Max, 0, 0, 0 });
		SpinEditEditor.MinValue = new decimal(new int[] { Min, 0, 0, 0 });
		SpinEditEditor.Name = "SpinEditor";
		SpinEditEditor.Validating += validatingCallback;

		return SpinEditEditor;
	}

	public delegate bool BoolValidationCallback(string id, bool val);
	protected RepositoryItemCheckEdit CreateEditorCheckEdit()
	{
		RepositoryItemCheckEdit CheckEditEditor = new RepositoryItemCheckEdit();
		return CheckEditEditor;
	}
#endregion makeEditors
#region Mouse events
	private void ShowPopupIfApplicable()
	{
		if (c_popupTypes.Contains(ActiveEditor.GetType()))
		{ (ActiveEditor as PopupContainerEdit)?.ShowPopup(); }
	}

	protected override void OnMouseUp(MouseEventArgs e)
	{
#if !DEBUG
		try
		{
#endif
			base.OnMouseUp(e);
#if !DEBUG
	}
		catch
		{
			// The DevExpress TreeList Crashes when performing a filter on incompatible data types (ie, bools, strings and null) - 8/11/2020 3:20 PM - JM
		}
#endif
	}
	private void OnMouseUp(object sender, MouseEventArgs e)
	{
		OptionsBehavior.Editable = false; //lol.... - 7/14/2020 5:44 PM - JM
		TreeList tl = sender as TreeList;
		TreeListHitInfo hit = tl.CalcHitInfo(e.Location);
		if(e.Button == MouseButtons.Right)
		{
			// walking this through now because I'm tired.
			// For AddContextMenuItems we'll want to pass a list of IDs now, and each tree will handle the ids as they should
			// alternatively, we could create a second function just to handle multi-node right-clicks.
			// In either case, we should only invoke a multi-node right click when we're right clicking on one of the currently selected
			// nodes. Right clicking on a node outside the selection or on nothing should give us the menu similar to what each tree
			// is currently providing us. Many of these context menus' old functions will need to be updated for multi-node operations. - 10/28/2020 1:01 AM - JM
			ContextMenuStrip menu = new ContextMenuStrip();
			//ToolStripMenuItem item = new ToolStripMenuItem();
			bool rowHit = hit != null && (hit.HitInfoType == HitInfoType.Cell || hit.HitInfoType == HitInfoType.RowIndicator);
			string id = null;
			if(rowHit)
			{
				id = (string)hit.Node.Tag;
			}
			List<string> otherSelectedIDs = new List<string>();
			if(rowHit && Selection.Contains(hit.Node))
			{
				foreach(TreeListNode s in Selection)
				{
					if(s == hit.Node) { continue; }
					otherSelectedIDs.Add((String)s.Tag);
				}
			}
			if(rowHit && otherSelectedIDs.Count == 0) // only do this if we're targeting a single row.
			{ 
				FocusedNode = hit.Node;
				bool separator = true;
				if(hit.Node.Expanded)
				{
					menu.Items.Add(CreateMenuItem(MENU_COLLAPSE));
				}
				else if(hit.Node.HasChildren)
				{
					menu.Items.Add(CreateMenuItem(MENU_EXPAND));
				}
				else
				{ separator = false; }
				if(separator)
				{ menu.Items.Add(new ToolStripSeparator()); }
				separator = true;
				ToolStripMenuItem item = new ToolStripMenuItem();
				TreeListNode lowest = null;
				if (CheckForCondition(hit.Node,HasError)) 
				{
					m_contextNode = hit.Node;
					item = new ToolStripMenuItem();
					item.Text = MENU_GOTO_ERROR;
					item.Image = Resources.gotoError_16x16;
					
					menu.Items.Add(item); 
				}
				else if(CheckForCondition(hit.Node,HasWarning))
				{
					m_contextNode = hit.Node;
					item = new ToolStripMenuItem();
					item.Text = MENU_GOTO_WARNING;
					item.Image = Resources.gotoWarning_16x16;
					menu.Items.Add(item);
				}
				else
				{ separator = false; }
				if (separator)
				{ menu.Items.Add(new ToolStripSeparator()); }
			}
			AddContextMenuItems(menu, id, otherSelectedIDs.Count > 0);

			// It's ok to stuff a strange object inside the Tag instead of the traditional string ID, because this is entirely encapsulated
			// within the CosmosTreeList class, and nobody else has to handle it or predict what the Tag is. - 10/28/2020 11:50 AM - JM
			menu.Tag = new ContextMenuSelectedNodeInfo(id,otherSelectedIDs);
			menu.ItemClicked += new ToolStripItemClickedEventHandler(Callback_ContextMenu_ItemClicked);

			Point treeOrigin = tl.PointToScreen(new Point(0, 0));
			Point mousePos = new Point(e.Location.X, e.Location.Y);
			menu.Show(tl, mousePos);

			return;
		}

		// Now handle left-clicks. - 8/11/2020 1:20 PM - JM
		if (hit == null || hit.HitInfoType != HitInfoType.Cell) { return; }
		if (m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType != null)
		{
			CellInteractType type = m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType((string)hit.Node.Tag);
			if ((type & CellInteractType.MouseUpToExecute) != 0)
			{
				m_columnData[hit.Column.AbsoluteIndex].OnCellExecution?.Invoke((string)hit.Node.Tag);
				return;
			}
		}

		GetCachedEditor(hit.Node, hit.Column.AbsoluteIndex); 
		if (CheckForReadOnly(hit.Node, hit.Column.AbsoluteIndex)) { return; }

		OptionsBehavior.Editable = true;
		ShowEditor();

		if(ActiveEditor == null) { return; }
		if (ActiveEditor.ReadOnly) { HideEditor();  return; }
		if(ActiveEditor is CheckEdit)
		{
			((CheckEdit)ActiveEditor).Checked =  !((CheckEdit)ActiveEditor).Checked;
		}
		ShowPopupIfApplicable();
		return;

			

		// ...nuts. how do I get the editor for the cell I'm looking at to check if I should default to acting on it? - 5/3/2020 1:47 AM - JM
		// maaaaybe ask the column data to see what kind of repositoryeditor it returns for the function? mabye? - 5/5/2020 3:45 AM - JM
		// Took some time, but this ended up being solved by caching editor values per cell anyway. Now I can just check the repositoryEditor for
		// each cell and query the properties there. - 7/14/2020 4:59 PM - JM
		//if(m_defaultEditorInteractions[] == CellInteractType.MouseUp)
		//{
		//	ShowEditor();
		//	ShowPopupIfApplicable();
		//}
		// Find the editor for this cell
		// check if its a type that should interact with MouseUp
		// interact with the editor.
	}

		

	private bool CheckForReadOnly(TreeListNode node, int columnIndex)
	{
		if (!c_editorCache.ContainsKey(node)) { return false; }
		if (!c_editorCache[node].ContainsKey(columnIndex)) { return false; }
		if(c_editorCache[node][columnIndex] == null) { return true; }
		if (c_editorCache[node][columnIndex].ReadOnly) { return true; }
		return false;
	}

	private bool FindCondition(TreeListNode node, ref TreeListNode lowestNode, NodeCondition condition)
	{
		if (!CheckForCondition(node,condition)) { return false; }
		foreach(TreeListNode child in node.Nodes)
		{
			if(FindCondition(child,ref lowestNode,condition))
			{ return true; }
		}
		// if we've reached this point, then we are an error and we have no children which are errors. We are now the lowest node. - 7/8/2020 2:51 PM - JM
		lowestNode = node;
		return true;
	}

	private void OnMouseHover(object sender, EventArgs e)
	{
		ToolTipController.ShowHint("Success!");
	}

	private void OnMouseDown(object sender, MouseEventArgs e)
	{
		TreeList tl = sender as TreeList;
		TreeListHitInfo hit = tl.CalcHitInfo(e.Location);
		if(hit == null || hit.HitInfoType != HitInfoType.Cell) { return; }
		if (m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType != null)
		{
			CellInteractType type = m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType((string)hit.Node.Tag);
			// if the result is NOT default and the result IS mousedown, override.
			if ((type & CellInteractType.Default) == 0 && (type & CellInteractType.MouseDown) != 0)
			{
				// interact with the editor.
				ShowEditor();
				return;
			}
			if((type & CellInteractType.MouseDownToExecute) != 0)
			{
				m_columnData[hit.Column.AbsoluteIndex].OnCellExecution?.Invoke((string)hit.Node.Tag);
				return;
			}
		}
		// Find the editor for this cell
		// check if its a type that should interact with MouseDown
		// interact with the editor.
	}

	private void OnMouseDoubleClick(Object sender, MouseEventArgs e)
	{
		TreeList tl = sender as TreeList;
		TreeListHitInfo hit = tl.CalcHitInfo(e.Location);
		if (hit == null || hit.HitInfoType != HitInfoType.Cell) { return; }
		if (m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType != null)
		{
			CellInteractType type = m_columnData[hit.Column.AbsoluteIndex].OnCheckInteractType((string)hit.Node.Tag);
			// if the result is NOT default and the result IS mousddoubleclick, override.
			if ((type & CellInteractType.Default) == 0 && (type & CellInteractType.MouseDoubleClick) != 0)
			{
				// interact with the editor.
				ShowEditor();
				return;
			}
			if ((type & CellInteractType.MouseDoubleClickToExecute) != 0)
			{
				m_columnData[hit.Column.AbsoluteIndex].OnCellExecution?.Invoke((string)hit.Node.Tag);
				return;
			}
		}
		// Find the editor for this cell
		// check if its a type that should interact with MouseDoubleClick
		// interact with the editor.
	}

	private void AddContextMenuItems(ContextMenuStrip menu, string id, bool multiSelect)
	{
		HashSet<string> allowedMultiples = new HashSet<string>()
		{
			"-"
		};
		foreach(string item in ContextMenuItems(id,multiSelect))
		{
			foreach(ToolStripItem i in menu.Items)
			{
				if(allowedMultiples.Contains(i.Name)) { continue; }
				if(i.Name == item) { throw new ArgumentException("This item already exists in the context menu!"); }
			}
			menu.Items.Add(CreateMenuItem(item));
		}
	}

	private ToolStripItem CreateMenuItem(string text)
	{
		if(text == "-")
		{ return new ToolStripSeparator(); }
		ToolStripMenuItem item = new ToolStripMenuItem();
		Image i = GetImageForMenuItem(text);
		if(i != null)
		{
			item.Image = i;
		}
		item.Text = text;
		return item;
	}

	private Image GetImageForMenuItem(string text)
	{
		string firstWord = Utils.SplitFirst(text, ' ').ToLower();
		switch (firstWord)
		{
			case "add":
			case "create":
			case "new":
				return Resources.add_16x16;
			case "save":
				return Resources.save_16x16;
			case "remove":
			case "delete":
				return Resources.delete_16x16;
			case "show":
			case "find":
			case "see":
			case "display":
			case "look":
			case "locate":
			case "focus":
			case "view":
				if(Utils.SplitLast(text,' ').ToLower().Contains("explorer")) // there might be a period at the end, although that's not following style patterns... - 10/29/2020 2:45 PM - JM
				{ return Resources.findFolder_16x16; }
				return Resources.find_16x16;
			case "exclude":
			case "hide":
				return Resources.hide_16x16;
			case "include":
				return Resources.show_16x16;
			case "expand":
				return Resources.expand_16x16;
			case "collapse":
				return Resources.collapse_16x16;
			case "reset":
			case "refresh":
			case "reload":
				return Resources.refresh_16x16;
			case "goto":
				return Resources.goto_16x16;
			default:
				return null;
		}
		return null;
	}

	// also want to do something similar for a drag/drop.
	private void Callback_ContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
	{
		Signals.LargeOperationBegin.Send( "ContextMenuItemOperation");
#if !DEBUG
		try
		{
#endif
			string selectedText = e.ClickedItem.Text;
			// first, handle CosmoTree events. If nothing catches, move on to the inherited function.
			TreeListNode newTarget = null;
			switch(selectedText)
			{
				case MENU_GOTO_ERROR:
					FindCondition(m_contextNode, ref newTarget, HasError);
					FocusNode(newTarget);
					break;
				case MENU_GOTO_WARNING:
					FindCondition(m_contextNode, ref newTarget, HasWarning);
					FocusNode(newTarget);
					break;
				case MENU_EXPAND:
					FocusedNode.ExpandAll();
					break;
				case MENU_COLLAPSE:
					CollapseNode(FocusedNode);
					break;
				default: // this covers all cases not explicitly covered by the base class.
					ContextMenuSelectedNodeInfo info = (ContextMenuSelectedNodeInfo)((ContextMenuStrip)sender).Tag;
					ContextMenuItemClicked(selectedText, info.ID, info.OtherSelectedIDs);
					break;
			}
			Signals.LargeOperationEnd.Send( "ContextMenuItemOperation");
#if !DEBUG
		}
		catch
		{
			Signal.Send<string>(CCSig.LargeOperationEnd, "ContextMenuItemOperation");
		}
#endif
	}

	protected void FocusNode(TreeListNode node)
	{
		ClearSelection();
		Selection.SelectNode(node);
		SetFocusedNode(node);
	}

	protected void CollapseNode(TreeListNode node)
	{
		Signals.LargeOperationBegin.Send( "CTL_CollapseNodes");
		NodesIterator.DoLocalOperation(CollapseLocal, node.Nodes);
		node.Collapse();
		Signals.LargeOperationEnd.Send( "CTL_CollapseNodes");
	}

	private void CollapseLocal(TreeListNode node)
	{
		node.Collapse();
	}

	class CustomNodeOperation : DevExpress.XtraTreeList.Nodes.Operations.TreeListOperation
	{
		public CustomNodeOperation()
			: base()
		{
		}
		public override void Execute(TreeListNode node)
		{
			if (node.HasChildren)
				node.Expanded = false;
		}
		public override bool NeedsFullIteration
		{
			get
			{
				return true;
			}
		}
		public override bool NeedsVisitChildren(TreeListNode node)
		{
			return true;
		}
	}

	private void CollapseNodes(TreeListNode node)
	{
		foreach(TreeListNode child in node.Nodes)
		{
			if (child.HasChildren)
			CollapseNodes(child);
		}
		node.Collapse();
	}
		

#endregion callbacks
#region callbacks
	protected virtual void TextEditValidate_DefaultCallback(Object sender, CancelEventArgs e)
	{
		if (ActiveEditor.EditValue == null || ActiveEditor.EditValue.ToString() == String.Empty)
			e.Cancel = true;
	}
	#endregion
	#region style
	// https://docs.devexpress.com/WindowsForms/5603/controls-and-libraries/tree-list/feature-center/sorting/custom-sorting - 6/27/2021 3:53 PM - JM
	private void OnSortNodes(object sender, CustomColumnSortEventArgs e)
	{
		if(m_columnData[e.Column.AbsoluteIndex].DetermineImportance == null) { return; }
		int n1 = m_columnData[e.Column.AbsoluteIndex].DetermineImportance((string)e.Node1.Tag);
		int n2 = m_columnData[e.Column.AbsoluteIndex].DetermineImportance((string)e.Node2.Tag);

		if(n1 > n2)
		{ e.Result = e.SortOrder == SortOrder.Ascending ? -1 : 1; }
		if(n2 > n1)
		{ e.Result = e.SortOrder == SortOrder.Ascending ? 1 : -1; }
	}

	public class CellPip
    {
		public int HoleSize;
        public Color PenColor;
		public bool Use;

        public CellPip(int holeSize, Color c)
        {
			HoleSize = holeSize;
			PenColor = c;
			Use = true;
        }

		public CellPip(bool use)
        {
			Use = use;
        }
    }
	private void OnDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
	{
		// SUN JAKE - Disabled drawing DataCore stuff.
		if(m_columnData[e.Column.AbsoluteIndex].OnSetCellPip != null)
		{
//
			CellPip pip = m_columnData[e.Column.AbsoluteIndex].OnSetCellPip((string)e.Node.Tag, e.CellValue);
			if(pip == null || !pip.Use) { return; }
			e.CellText = "    " + e.CellText.Trim();
			e.DefaultDraw();
			if(pip.HoleSize > 5) { pip.HoleSize = 5; }
			int width = 6 - pip.HoleSize;
			Color c = pip.PenColor;

			//switch(pip)
			//{
			//	case InheritState.Source:
			//		width = 6;
			//		c = Color.DarkCyan;
			//		break;
			//	case InheritState.Altered:
			//		width = 4;
			//		c = Color.DarkCyan;
			//		break;
			//	case InheritState.Default:
			//		width = 2;
			//		c = Color.LightGreen;
			//		break;
			//	case InheritState.Inherited:
			//		width = 4;
			//		c = Color.IndianRed;
			//		break;
			//	default:
			//		return;
			//}
			int d = e.Bounds.Height - 7 - width;
			int x = e.Bounds.X + width / 2 + 2;
			int y = e.Bounds.Y + (e.Bounds.Height - d) / 2;
			e.Cache.DrawEllipse(x, y, d, d, c, width);
//
			e.Handled = true;
		}
	}

	protected virtual bool UseNodeImage(string id, out int index)
    {
		index = -1;
		return false;
    }

	protected void SetImages(List<Bitmap> images)
    {
		ImageList imageList = new ImageList();
		foreach(Bitmap bitmap in images)
        {
			imageList.Images.Add(bitmap);
        }
		StateImageList = imageList;
	}

	protected void Flush()
    {
		c_styleCache.Clear();
		c_editorCache.Clear();
		c_dirtyStyleNodes.Clear();
    }

	private void OnCellStyle(object sender, GetCustomNodeCellStyleEventArgs e)
	{
		if (!c_dirtyStyleNodes.Contains(e.Node) && c_styleCache.ContainsKey(e.Node)) 
		{
			StyleInfo sc = c_styleCache[e.Node]; // style cache - 7/9/2020 12:59 PM - JM
			if(sc.DirtyFont)
			{ e.Appearance.Font = new Font(Appearance.Row.Font.FontFamily, Appearance.Row.Font.Size, FontStyle.Italic); }
			if(Selection.Contains(e.Node))
			{ e.Appearance.BackColor = UIColors.SELECTED_COLOR; }
			else if(sc.UseColor)
			{ e.Appearance.BackColor = sc.BackgroundColor; }
			e.Node.StateImageIndex = sc.ImageIndex;
			return;
		}
		if (!c_styleCache.ContainsKey(e.Node)) { c_styleCache.Add(e.Node, new StyleInfo()); }
		StyleInfo s = c_styleCache[e.Node];
		s.UseColor = true;
		s.StatusImage = null;
		string id = (string)e.Node.Tag;
		if(CheckForCondition(e.Node,IsDirty)) // do font effects first, then do color effects.
		{
			e.Appearance.Font = new Font(Appearance.Row.Font.FontFamily, Appearance.Row.Font.Size, FontStyle.Italic);
			s.DirtyFont = true;
		}

		if(!string.IsNullOrEmpty(id) && UseNodeImage(id,out int index))
        {
			s.ImageIndex = index;
        }
        else
        {
            s.ImageIndex = -1;
			e.Node.StateImageIndex = -1;
        }

        if (string.IsNullOrEmpty(id))
		{
			s.UseColor = false;
		}
		else if (IsSelected(id))
		{
			e.Appearance.BackColor = UIColors.SELECTED_COLOR;
		}
		else if(CheckForCondition(e.Node,HasError))
		{
			e.Appearance.BackColor = UIColors.ERROR_COLOR;
			s.StatusImage = Resources.error_16x16;
				
		}
		else if(CheckForCondition(e.Node,HasWarning))
		{
			e.Appearance.BackColor = UIColors.WARNING_COLOR;
			s.StatusImage = Resources.warning_16x16;
		}
		else if(IsHeader(id))
		{
			e.Appearance.BackColor = UIColors.HEADER_COLOR;
		}
		else if(IsSubHeader(id))
		{
			e.Appearance.BackColor = UIColors.SUBHEADER_COLOR;
		}
		else if(IsReferenced(id))
		{
			e.Appearance.BackColor = UIColors.REFERENCED_COLOR;
		}
		else if(IsDisabled(id))
		{
			e.Appearance.BackColor = UIColors.DISABLED_COLOR;
		}
		else
		{
			s.UseColor = false;
		}

		if(s.UseColor)
		{ 
			s.BackgroundColor = e.Appearance.BackColor; 
		}
		c_dirtyStyleNodes.Remove(e.Node);
	}

	public override void RefreshNode(TreeListNode node)
	{
		MakeDirty(node);
		base.RefreshNode(node);
	}

	//public void RefreshNodeEditors(TreeListNode node)
	//{
	//	c_editorCache.Remove(node);
	//	RefreshNode(node);
	//}

	private bool CheckForCondition(TreeListNode node, NodeCondition condition)
	{
		if(node == null) { return false; }
		string id = (string)node.Tag;
		if (string.IsNullOrEmpty(id)) { return false; }
		if (condition(id)) { return true; }
		foreach(TreeListNode child in node.Nodes)
		{
			if (CheckForCondition(child, condition)) { return true; }
		}
		return false;
	}

	private delegate bool NodeCondition(string id);

	protected virtual bool IsHeader(string id)
	{
		return false;
	}

	//protected virtual bool IsError(string id)
	//{
	//	return false;
	//}

	protected virtual List<string> GetErrors(string id, bool exitEarly)
	{
		return new List<string>();
	}

	private bool HasError(string id)
	{
		try
		{
			return GetErrors(id, true).Count > 0;
		}
		catch
		{ 
			return false;
		}
	}

	protected virtual List<string> GetWarnings(string id, bool exitEarly)
	{
		return new List<string>();
	}

	private bool HasWarning(string id)
	{
		try
		{
			return GetWarnings(id, true).Count > 0;
		}
		catch
		{ return false; }
	}

	protected virtual bool IsSubHeader(string id)
	{
		return false;
	}

	protected virtual bool IsReferenced(string id)
	{ return false; }

	protected virtual bool IsDirty(string id)
	{ return false; }

	protected virtual bool IsDisabled(string id)
	{ return false; }

	protected virtual bool IsSelected(string id)
	{
		return false;
	}

	#endregion style
	#region helpers
	protected void BeginOp(string opName)
	{
		Signals.LargeOperationBegin.Send( opName);
	}

	protected void EndOp(string opName)
	{
		Signals.LargeOperationEnd.Send( opName);
	}
	#endregion

	private DataEditorType GetDataType(DataEditorType type)
	{
		switch(type)
		{
			case DataEditorType.Float:
			case DataEditorType.FloatZeroOrNegative:
			case DataEditorType.FloatZeroOrPositive:
				return DataEditorType.Float;
			case DataEditorType.Double:
			case DataEditorType.DoubleZeroOrNegative:
			case DataEditorType.DoubleZeroOrPositive:
				return DataEditorType.Double;
			case DataEditorType.Integer:
			case DataEditorType.IntegerZeroOrNegative:
			case DataEditorType.IntegerZeroOrPositive:
				return DataEditorType.Integer;
			case DataEditorType.Long:
			case DataEditorType.LongZeroOrNegative:
			case DataEditorType.LongZeroOrPositive:
				return DataEditorType.Long;
			case DataEditorType.String:
			case DataEditorType.StringNoSpaces:
			default:
				return DataEditorType.String;
		}
	}

	class StyleInfo
	{
		public bool DirtyFont;
		public bool UseColor;
		public Color BackgroundColor;
		public Bitmap StatusImage;
		public int ImageIndex = -1;

		public StyleInfo(bool dirty, bool colored, Bitmap statusImage, Color c = default(Color), int imageIndex = -1)
        {
            DirtyFont = dirty;
            UseColor = colored;
            BackgroundColor = c;
            StatusImage = statusImage;
            ImageIndex = imageIndex;
        }

        public StyleInfo() { }
	}
}

class ExternalTreeInfo
{
	public Action<string,List<string>> OnDrop { get; private set; }
	public bool LocationIrrelevant { get; private set; }
	private string ToolTipText;
	private string SingularTipText;
	public SunTree.DragOverTooltipDelegate CustomTextDelegate { get; private set; }
		
	public string GetText(DragOverEventArgs e)
	{
		List<TreeListNode> nodes = e.Data as List<TreeListNode>;
		bool plural = true;
		if(nodes != null && nodes.Count == 1) { plural = false; }
		//if(CustomTextDelegate != null)
		//{
		//	return CustomTextDelegate(e);
		//}
		if(!plural && !string.IsNullOrEmpty(SingularTipText))
		{ return SingularTipText; }
		return ToolTipText;
	}

	public ExternalTreeInfo(Action<string,List<string>> onDrop, string tooltipText, bool locationIrrelevant, string singularTipText)
	{
		ToolTipText = tooltipText;
		OnDrop = onDrop;
		LocationIrrelevant = locationIrrelevant;
		SingularTipText = singularTipText;
	}

	public ExternalTreeInfo(Action<string,List<string>> onDrop, SunTree.DragOverTooltipDelegate customTextDelegate, bool locationIrrelevant)
	{
		OnDrop = onDrop;
		CustomTextDelegate = customTextDelegate;
		ToolTipText = "ERROR: CustomText delegate was sent, but the function was null!";
		LocationIrrelevant = locationIrrelevant;
	}
}

class ContextMenuSelectedNodeInfo
{
	public string ID;
	public List<string> OtherSelectedIDs;

	public ContextMenuSelectedNodeInfo(string id, List<string> otherIDs)
	{
		ID = id;
		OtherSelectedIDs = otherIDs;
	}
}


#region enums
public enum CellInteractType
{
	Default = 0,
	None = 1,
	MouseUp = 2,
	MouseUpToExecute = 4,
	MouseDown = 8,
	MouseDownToExecute = 16,
	MouseClick = 32,
	MouseClickToExecute = 64,
	MouseDoubleClick = 128,
	MouseDoubleClickToExecute = 256
}

public enum DataEditorType
{
	Default,
	Integer,
	IntegerZeroOrPositive,
	IntegerZeroOrNegative,
	Long,
	LongZeroOrPositive,
	LongZeroOrNegative,
	Float,
	FloatZeroOrPositive,
	FloatZeroOrNegative,
	Double,
	DoubleZeroOrPositive,
	DoubleZeroOrNegative,
	String,
	StringNoSpaces
}

public enum ListExpand
{
	None,
	ImmediateChildren,
	All
}
	
//public delegate DFFieldData GetPropertyDelegate(string id);

#endregion enums


//*/
