using System.Drawing;

public class UIColors
{
    public static Color MIN_BAR_COLOR = Color.Aqua;
    public static Color MAX_BAR_COLOR = Color.Chocolate;
    public static Color SELECTED_COLOR = Color.FromArgb(60,100,60);
    public static Color ERROR_COLOR = Color.Firebrick;
    public static Color WARNING_COLOR = Color.Goldenrod;
    public static Color HEADER_COLOR = Color.FromArgb(40,40,100);
    public static Color SUBHEADER_COLOR = Color.FromArgb(40,40,60);
    public static Color REFERENCED_COLOR = Color.Fuchsia;
    public static Color DISABLED_COLOR = Color.FromArgb(30, 30, 30);

}