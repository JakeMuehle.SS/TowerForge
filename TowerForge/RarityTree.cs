﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;
public class RarityTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private int _maxLevel;
	private List<int> _levelColumns = new List<int>();
	private HashSet<string> _cards = new HashSet<string>();
	private HashSet<string> _currencies = new HashSet<string>();
	private HashSet<string> _commands = new HashSet<string>();

	private const string COMMAND = "command";
	private const string CURRENCY = "currency";
	private const string CARDS = "cards";

	public RarityTree() : base()
    {
		AddColumn("#", 30, 30);
        AddColumn("Rarity", 100, 100);
		AddColumn("StartingLevel",75,75,Editor_Int,Validator_StartingLevel);
	}

	protected override void PrePopulate()
    {
        //if (_serverInitialized) { return; }
		//_serverInitialized = true;
		_maxLevel = FirebaseManager.Inst.GetMaxLevel();

		for (int i = 0; i < _maxLevel; i++)
		{
			CreateLevelColumn(i);
		}
	}

    protected override void RuntimeInit()
    {

    }

	private bool ParseId(string id, out CardRarity rarity, out string property)
    {
		rarity = CardRarity.None;
		property = null;
		string[] segs = id.Split('.');
		if(segs.Length < 2)
        {
			return false;
        }
		if (!Enum.TryParse<CardRarity>(segs[0], out rarity))
		{ return false; }
		property = segs[1];
		return true;
    }

	private string PropertyId(string rarityIntString, string property)
    {
		return rarityIntString + "." + property;
    }

	private void CreateLevelColumn(int level)
    {
        _levelColumns.Add(AddColumn($"L{level}", 60, 60, Editor_Int, (id, value) =>
        {
			//if(!Enum.TryParse<CardRarity>(id, out CardRarity rarity))
			//{ return false; }
			//return FirebaseManager.Inst.SetUpgradeForRarityLevel(rarity, level, (int)value);
			if(!ParseId(id, out CardRarity rarity, out string property))
            {
				return false;
            }
			switch(property)
            {
				case CARDS: return FirebaseManager.Inst.SetUpgradeForRarityLevel(rarity, level, (int)value);
				case COMMAND: return FirebaseManager.Inst.SetCommandForRarityLevel(rarity, level, (int)value);
				case CURRENCY: return FirebaseManager.Inst.SetCurrencyForRarityLevel(rarity, level, (int)value);
				default:
					Debug.LogError("Not set up to handle property " + property);
					return false;
			}
        }).ColumnIndex);
    }

	private RepositoryItem Editor_Int(string id)
    {
		return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
    }

	private bool Validator_StartingLevel(string id, object value)
    {
		if (!Enum.TryParse<CardRarity>(id, out CardRarity rarity))
		{ return false; }
		return FirebaseManager.Inst.SetRarityStartingLevel(rarity, (int)value);
    }

	private object[] GetData(string id)
	{
		if(!Enum.TryParse<CardRarity>(id,out CardRarity rarity))
        {
			return new object[] { id };
        }
		object[] data = new object[3 + _levelColumns.Count];
		data[0] = (int)rarity;
		data[1] = id;
		data[2] = FirebaseManager.Inst.GetRarityStartingLevel(rarity);
		//for(int i = 0; i < _levelColumns.Count; i++)
        //{
		//	int count = FirebaseManager.Inst.GetUpgradeForRarityLevel(rarity, i);
		//	data[_levelColumns[i]] = count;
		//	
        //}


		return data;
	}

	private object[] GetCardsData(string id)
    {
		if (!Enum.TryParse<CardRarity>(id, out CardRarity rarity))
		{
			return new object[] { id };
		}
		object[] data = new object[3 + _levelColumns.Count];
		data[1] = CARDS;
		for (int i = 0; i < _levelColumns.Count; i++)
		{
			int count = FirebaseManager.Inst.GetUpgradeForRarityLevel(rarity, i);
			data[_levelColumns[i]] = count;
			if (count >= 0)
			{
				//FirebaseManager.Inst.SetUpgradeForRarityLevel(rarity, i, count);
			}
		}
		return data;
	}

	private object[] GetCurrencyData(string id)
    {
		if (!Enum.TryParse<CardRarity>(id, out CardRarity rarity))
		{
			return new object[] { id };
		}
		object[] data = new object[3 + _levelColumns.Count];
		data[1] = CURRENCY;
		for (int i = 0; i < _levelColumns.Count; i++)
		{
			int count = FirebaseManager.Inst.GetCurrencyForRarityLevel(rarity, i);
			data[_levelColumns[i]] = count;
			if (count >= 0)
			{
				//FirebaseManager.Inst.SetCurrencyForRarityLevel(rarity, i, count);
			}
		}
		return data;
	}

	private object[] GetCommandData(string id)
    {
		if (!Enum.TryParse<CardRarity>(id, out CardRarity rarity))
		{
			return new object[] { id };
		}
		object[] data = new object[3 + _levelColumns.Count];
        data[1] = COMMAND;
		for (int i = 0; i < _levelColumns.Count; i++)
		{
			int count = FirebaseManager.Inst.GetCommandForRarityLevel(rarity, i);
			data[_levelColumns[i]] = count;
			if (count >= 0)
			{
				//FirebaseManager.Inst.SetCommandForRarityLevel(rarity, i, count);
			}
		}
		return data;
	}

	protected override string GetTreeName() { return nameof(RarityTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return new List<string>()
		{
			CardRarity.Common.ToString(),
			CardRarity.Rare.ToString(),
			CardRarity.Epic.ToString(),
			CardRarity.Legendary.ToString()
        };
	}
	protected override void InitialPopulateAction(string id)
	{
		//PostServerInitialize();
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
		//return;

		string cardId = PropertyId(id, CARDS);
		TreeListNode cards = AppendNode(GetCardsData(id), node, cardId);
		_cards.Add(cardId);
		_nodes.Add(cardId, cards);

		string commandId = PropertyId(id, COMMAND);
		TreeListNode command = AppendNode(GetCommandData(id), node, commandId);
		_commands.Add(commandId);
		_nodes.Add(commandId, command);

		string currencyId = PropertyId(id, CURRENCY);
		TreeListNode currency = AppendNode(GetCurrencyData(id), node, currencyId);
		_currencies.Add(currencyId);
		_nodes.Add(currencyId,currency);
	}

}

