﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Text;
public class XowerUtilityTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private XowerType _xowerType = XowerType.Tower;
	private int _level = 1;
	public XowerUtilityTree() : base()
	{
		AddColumn("ID", 100, 100);
		AddColumn("Cost");
		AddColumn("AvgThreat");
		AddColumn("MoveModifier");
		AddColumn("ConditionalMoveMod");
		AddColumn("Status");
		AddColumn("Duration");

		Signals.ShowPowersNotTowers.Register(HandleToggle);
		Signals.XowerLevelChanged.Register(HandleLevelChange);
		Signals.ActionCardSelected.Register(SelectActionCard);
		Signals.ActionCardUpdated.Register(RefreshActionCard);

		OnNodeSelect += (id) =>
		{
			Signals.ActionCardSelected.Send(id, this);
		};
	}
	private object[] GetData(string id)
	{
		float attackrate = DataManager.Inst.GetXowerAttackRate(id, _xowerType, _level);
		GetUtility(id, true, out float speedMod, out float conditionalSpeed, out HashSet<UnitStatus> inflictedStatus, out float duration);

		StringBuilder sb = new StringBuilder();
		foreach (UnitStatus us in inflictedStatus)
		{
			if (sb.Length > 0)
			{ sb.Append(", "); }
			sb.Append(us.ToString());
		}

		object[] data = new object[]
		{
			DataManager.Inst.DisplayName(id),
			DataManager.Inst.GetActionCardCost(id),
			"?",
			speedMod,
			conditionalSpeed,
			sb.ToString(),
			duration
		};
		return data;
	}

	private void GetUtility(string id, bool primaryList, out float speedMod, out float conditionalSpeed, out HashSet<UnitStatus> inflictedStatus, out float duration)
	{
		speedMod = 0;
        conditionalSpeed = 0;
		duration = 0;
		float durationMod = 1;
		inflictedStatus = new HashSet<UnitStatus>();
        for (int i = 0; i < DataManager.Inst.GetXowerAtomicEffectCount(id, _xowerType, _level, primaryList); i++)
		{
			AtomicActionEffectType type = DataManager.Inst.GetAtomicEffectType(id, _xowerType, primaryList, i);
            float amount = DataManager.Inst.GetAtomicEffectAmount(id, _xowerType, _level, primaryList, i);
            float d = DataManager.Inst.GetAtomicEffectDuration(id, _xowerType, _level, primaryList, i);
			if(duration > 0 && d > 0)
            {
				if (System.Math.Abs(duration - d) > 0.00001)
				{ Debug.LogError($"More than one duration for action {id} as {(_xowerType == XowerType.Power ? "power" : "tower")}!"); }
            }
			
            bool hasConditions = DataManager.Inst.GetXowerAtomicEffectAtomicConditionsCount(id, _xowerType, primaryList, i) > 0;
			float toAdd = 0;
			if (type == AtomicActionEffectType.ModifySpeed)
			{
				toAdd += amount;
				duration = System.Math.Max(d, duration);
				inflictedStatus.Add(DataManager.Inst.GetAtomicEffectStatusInflicted(id, _xowerType, primaryList, i));
			}
			if(type == AtomicActionEffectType.InflictStatus)
            {
				duration = System.Math.Max(d, duration);
				inflictedStatus.Add(DataManager.Inst.GetAtomicEffectStatusInflicted(id, _xowerType, primaryList, i));
			}
			if(type == AtomicActionEffectType.ModifyDuration)
            {
				durationMod *= amount;
            }

			if (hasConditions)
			{
				conditionalSpeed += toAdd;
			}
			else
			{
				speedMod += toAdd;
			}
		}
		duration *= durationMod;
		inflictedStatus.Remove(UnitStatus.None);
	}

	private void HandleToggle(bool showPowers)
	{
		_xowerType = showPowers ? XowerType.Power : XowerType.Tower;
		RebuildActionCards();
	}

	private void HandleLevelChange(int level)
	{
		_level = level;
		RebuildActionCards();
	}

	private void RebuildActionCards()
	{
		foreach (string id in _nodes.Keys)
		{
			RefreshActionCard(id);
		}
	}

	private void RefreshActionCard(string id)
	{
		object[] data = GetData(id);
		if (!_nodes.ContainsKey(id))
		{
			TreeListNode node = AppendNode(data, null,id);
			_nodes.Add(id, node);
		}
		else
		{
			for (int i = 0; i < data.Length; i++)
			{
				_nodes[id].SetValue(i, data[i]);
			}
		}
	}

	private void SelectActionCard(string id, SunTree sendingTree)
	{
		if (sendingTree == this) { return; }
		if (!_nodes.ContainsKey(id)) { return; }
		ClearSelection();
		SelectNode(_nodes[id]);
	}

	protected override string GetTreeName() { return nameof(XowerUtilityTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate()
	{
		return DataManager.Inst.GetIds<ActionCardType>();
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}