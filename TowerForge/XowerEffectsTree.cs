﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;
using System.Text;
using System.Linq;
using TowerForge.Properties;


public class PowerEffectsTree : XowerEffectsTree
{
    public PowerEffectsTree() : base()
    {
        _xowerType = XowerType.Power;
    }
}

public class TowerEffectsTree : XowerEffectsTree
{
    public TowerEffectsTree() : base()
    {
        _xowerType = XowerType.Tower;
    }
}

public class RunnerPowerEffectsTree : XowerEffectsTree
{
    public RunnerPowerEffectsTree() : base()
    {
        _xowerType = XowerType.RunnerPower;
    }
}
public abstract class XowerEffectsTree : SunTree
{
    private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
    private HashSet<string> _atomicEffectListHeaders = new HashSet<string>(); // "Primary" or "Secondary"
    private HashSet<string> _atomicEffects =    new HashSet<string>();
    private HashSet<string> _atomicConditions = new HashSet<string>();
    private HashSet<string> _conditionHeaders = new HashSet<string>();
    private HashSet<string> _rankableFloats = new HashSet<string>();
    protected XowerType _xowerType = XowerType.None;
    private int _level = 1;
    private string _actionCard;

    //private int AMOUNT_BASE;
    //private int AMOUNT_SCALAR;
    //private int AMOUNT_FINAL;
    //private int AMOUNT_CURVE;
    //private int AMOUNT_RANDCAP;
    //private int DURATION_BASE;
    //private int DURATION_SCALAR;
    //private int DURATION_FINAL;
    //private int DURATION_CURVE;
    //private int DURATION_RANDCAP;

    private int COL_NAME;
    private int COL_BASE;
    private int COL_FINAL;
    private int COL_SCALAR;
    private int COL_RANDCAP;
    private int COL_CURVE;

    private const string PROP_DURATION = "Duration";
    private const string PROP_AMOUNT = "Amount";
    private const string PROP_COMPARETO = "CompareTo";

    private const string LIST_PRIMARY = "Primary";
    private const string LIST_SECONDARY = "Secondary";

    private const string LIST_CONDITIONS = "Conditions";

    private const string MENU_ADD_EFFECT = "Add Atomic Effect";
    private const string MENU_DELETE_EFFECT = "Delete Atomic Effect";
    private const string MENU_CLONE_EFFECT = "Duplicate this effect";
    private const string MENU_ADD_CONDITION = "Add Condition";
    private const string MENU_REMOVE_CONDITION = "Remove Condition";
    private const string MENU_CLONE_CONDITION = "Duplicate Condition";



    #region columns 
    public XowerEffectsTree() : base()
    {
        SetImages(new List<System.Drawing.Bitmap>() { Resources.collapse_16x16 });

        COL_NAME  = AddColumn("Effect", 100, 100, SetEditor_Type, Validator_ChangeEffectType,cellPipFunc: RankablePip).ColumnIndex;
        AddColumn("Stati",100,100,Editor_Stati,Validator_Stati);
        AddColumn("Logic",100,100,Editor_Logic,Validator_Logic);
        COL_BASE = AddColumn("Value", 75, 75, Quick_CreateFloatEditor, Validator_Base).ColumnIndex;
        COL_FINAL = AddColumn("Final", 75, 75, Quick_CreateFloatEditor, Validator_Final).ColumnIndex;
        COL_SCALAR = AddColumn("Scalar",75,75,Quick_CreateFloatEditor, Validator_Scalar).ColumnIndex;
        COL_RANDCAP = AddColumn("RandCap", 75, 75, Quick_CreateFloatEditor, Validator_RandCap).ColumnIndex;
        COL_CURVE = AddColumn("Curve", 75, 75, Editor_Curve, Validator_Curve).ColumnIndex;
        AddColumn("ID");

        //RootName = "Xowers";
        //AddColumn("Effect",75,75,SetEditor_Type,Validator_ChangeEffectType, fxed:true);
        //AMOUNT_BASE = AddColumn("Amount",75,75,Quick_CreateFloatEditor,Validator_AmountBase, fxed:true).ColumnIndex;
        //AddColumn("Status Infl.",75,75,SetEditor_InflictStatus,Validator_StatusInflicted,fxed: true);
        //AddColumn("Req. Stati", 100,100, SetEditor_ReqStati, Validator_ReqStati, fxed: true);
        //AddColumn("Inc. Type",75,75,SetEditor_IncType,Validator_IncType, fxed: true);
        //DURATION_BASE = AddColumn("Duration",75,75,Quick_CreateFloatEditor,Validator_DurationBase, fxed:true).ColumnIndex;
        //AMOUNT_SCALAR = AddColumn("AmountScalar",75,75,Quick_CreateFloatEditor, Validator_AmountScalar).ColumnIndex;
        //AMOUNT_FINAL = AddColumn("AmountFinal", 75, 75, Quick_CreateFloatEditor, Validator_AmountFinal).ColumnIndex;
        //AMOUNT_CURVE = AddColumn("AmountCurve", 100, 100, SetEditor_Curves, Validator_AmountCurve).ColumnIndex;
        //AMOUNT_RANDCAP = AddColumn("AmountRand", 75, 75, Quick_CreateFloatEditor, Validator_AmountRandCap).ColumnIndex;
        //DURATION_SCALAR = AddColumn("DurationScalar", 75, 75, Quick_CreateFloatEditor,Validator_DurationScalar).ColumnIndex;
        //DURATION_FINAL = AddColumn("DurationFinal", 75, 75, Quick_CreateFloatEditor,Validator_DurationFinal).ColumnIndex;
        //DURATION_CURVE = AddColumn("DurationCurve", 100, 100, SetEditor_Curves, Validator_DurationCurve).ColumnIndex;
        //DURATION_RANDCAP = AddColumn("DurationRand", 75, 75, Quick_CreateFloatEditor, Validator_DurationRandCap).ColumnIndex;
        //AddColumn("ID");
        //FixedLineWidth = 8;


        //Signals.ShowPowersNotTowers.Register(HandleToggle);
        Signals.XowerLevelChanged.Register(HandleLevelChange);
        Signals.ActionCardSelected.Register(HandleCardChange);
        Signals.ActionCardUpdated.Register(HandleCardUpdated);
    }

    #endregion
    #region getdata

    private CellPip RankablePip(string id, object _)
    {
        if(!_rankableFloats.Contains(id))
        {
            return new CellPip(0, System.Drawing.Color.Transparent);
        }
        RankableFloat rf = GetRankableFloat(id);
        return Utils.GetRankablePip(rf);
    }

    private object[] GetHeader(string id)
    {
        return new object[]
        {
            id,
            //"Amount",
            //"Status",
            //"",
            //"",
            //"Duration",
            //"AmountScalar",
            //"AmountFinal",
            //"AmountCurve",
            //"AmountRand",
            //"DurationScalar",
            //"DurationFinal",
            //"DurationCurve",
            //"DurationRand"
        };
    }

    private object[] GetAtomicEffectData(string id, XowerType xowerType, bool primaryList, int index)
    {
        AtomicActionEffectType type = DataManager.Inst.GetAtomicEffectType(_actionCard, xowerType, primaryList, index);
        string status = "";
        if (DurationTypeEffect(type))
        {
            status = DataManager.Inst.GetAtomicEffectStatusInflicted(_actionCard, xowerType, primaryList, index).ToString();
        }
        return new object[]
        {
            type,
            status, //DataManager.Inst.GetAtomicEffectStatusInflicted(_actionCard,asPower,primaryList,index),
            "",
            "",
            "",
            "",
            "",
            "",
            //DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Amount")?.BaseValue,
            //DataManager.Inst.GetAtomicEffectStatusInflicted(_actionCard,asPower,primaryList,index),
            //Utils.ListString<UnitStatus>(DataManager.Inst.GetAtomicEffectConditionalStati(_actionCard,asPower,primaryList,index)),
            //DataManager.Inst.GetAtomicEffectConditionalStatiInclusionType(_actionCard,asPower,primaryList,index),
            //!DurationTypeEffect(type)? null : DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Duration")?.BaseValue,
            //DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Amount")?.Scalar,
            //DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Amount")?.FinalValue,
            //DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Amount")?.Curve,
            //DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Amount")?.RandomCap,
            //!DurationTypeEffect(type)? null : DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Duration")?.Scalar,
            //!DurationTypeEffect(type)? null : DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Duration")?.FinalValue,
            //!DurationTypeEffect(type)? null : DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Duration")?.Curve,
            //!DurationTypeEffect(type)? null : DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,asPower,primaryList,index,"Duration")?.RandomCap,
            id
        };
    }

    private object[] GetAtomicEffectPropertyData(string id, XowerType xowerType, bool primaryList, int index, string property)
    {
        //string status = "";
        //if (property == PROP_DURATION)
        //{ status = DataManager.Inst.GetAtomicEffectStatusInflicted(_actionCard, asPower, primaryList, index).ToString(); }
        return new object[]
        {
            property,
            "",//status,
            "",
            DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,xowerType,primaryList,index,property)?.BaseValue,
            DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,xowerType,primaryList,index,property)?.FinalValue,
            DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,xowerType,primaryList,index,property)?.Scalar,
            DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,xowerType,primaryList,index,property)?.RandomCap,
            DataManager.Inst.GetAtomicEffectRankableFloat(_actionCard,xowerType,primaryList,index,property)?.Curve,
            id
        };
    }

    private object[] GetAtomicEffectConditionHeaderData(string id, XowerType xowerType, bool primaryList, int effectIndex)
    {
        string list = Utils.ListString<UnitStatus>(DataManager.Inst.GetAtomicEffectConditionalStati(_actionCard, xowerType, primaryList, effectIndex));
        return new object[]
        {
            LIST_CONDITIONS,
            Utils.ListString<UnitStatus>(DataManager.Inst.GetAtomicEffectConditionalStati(_actionCard,xowerType,primaryList,effectIndex)),
            DataManager.Inst.GetAtomicEffectConditionalStatiInclusionType(_actionCard,xowerType,primaryList,effectIndex),
            "",
            "",
            "",
            "",
            "",
            id
        };
    }

    protected override bool UseNodeImage(string id, out int index)
    {
        index = -1;
        //if(!_rankableFloats.Contains(id))
        //{
        //    return false;
        //}
        //if (UnequalFloatValues(id,COL_BASE,COL_FINAL))
        //{
        //    index = 0;
        //    return true;
        //}
        return false;
    }

    private bool UnequalFloatValues(string id, int col1, int col2)
    {
        object o1 = _nodes[id].GetValue(col1);
        object o2 = _nodes[id].GetValue(col2);

        if(o1 == null || o2 == null)
        {
            return false;
        }

        float f1 = (float)o1;
        float f2 = (float)o2;
        return f1 != f2;
    }

    private bool DurationTypeEffect(AtomicActionEffectType type)
    {
        return type == AtomicActionEffectType.HealthOverTime ||
            type == AtomicActionEffectType.ModifySpeed ||
            type == AtomicActionEffectType.InflictStatus;
    }

    private bool HasDuration(string id)
    {
        if (!_atomicEffects.Contains(id)) { return false; }
        if (!TryParseId(id,out bool primaryList,out int atomicId, out int conditionId, out string _))
        {
            return false;
        }
        return DurationTypeEffect(DataManager.Inst.GetAtomicEffectType(_actionCard, _xowerType, primaryList, atomicId));
    }

    private object[] GetAtomicEffectConditionsData(string id, XowerType xowerType, bool primaryList, int effectIndex, int conditionindex)
    {
        return new object[]
        {
            DataManager.Inst.GetXowerAtomicEffectAtomicConditionType(_actionCard,xowerType,_level,primaryList, effectIndex,conditionindex),
            "",
            DataManager.Inst.GetXowerAtomicEffectAtomicConditionComparator(_actionCard,xowerType,_level,primaryList, effectIndex,conditionindex),
            DataManager.Inst.GetAtomicConditionRankableFloat(_actionCard,xowerType,primaryList,effectIndex,conditionindex,PROP_COMPARETO).BaseValue,
            DataManager.Inst.GetAtomicConditionRankableFloat(_actionCard,xowerType,primaryList,effectIndex,conditionindex,PROP_COMPARETO).FinalValue,
            DataManager.Inst.GetAtomicConditionRankableFloat(_actionCard,xowerType,primaryList,effectIndex,conditionindex,PROP_COMPARETO).Scalar,
            DataManager.Inst.GetAtomicConditionRankableFloat(_actionCard,xowerType,primaryList,effectIndex,conditionindex,PROP_COMPARETO).Curve,
            DataManager.Inst.GetAtomicConditionRankableFloat(_actionCard,xowerType,primaryList,effectIndex,conditionindex,PROP_COMPARETO).RandomCap,
            id
        };
    }
    #endregion
    #region Editor Setters
    private RepositoryItem Editor_Logic(string id)
    {
        if(_conditionHeaders.Contains(id))
        {
            return CreateEditorDropDown<StatiInclusionType>();
        }
        if(_atomicConditions.Contains(id))
        {
            return CreateEditorDropDown<ConditionComparator>();
        }
        return null;
    }
    private RepositoryItem Editor_Stati(string id)
    {
        if(_conditionHeaders.Contains(id))
        {
            return CreateEditorCheckedComboBoxEdit(() =>
            {
                List<UnitStatus> statusList = new List<UnitStatus>((UnitStatus[])Enum.GetValues(typeof(UnitStatus)));
                Dictionary<string, bool> toReturn = new Dictionary<string, bool>();
                if(!TryParseId(id,out bool primaryList, out int atomicid, out _, out _))
                {
                    return toReturn;
                }
                HashSet<UnitStatus> stati = DataManager.Inst.GetAtomicEffectConditionalStati(_actionCard, _xowerType,primaryList,atomicid);
                foreach (UnitStatus status in statusList)
                {
                    toReturn.Add(status.ToString(), stati.Contains(status));
                }
                return toReturn;
            });
        }
        if(_atomicEffects.Contains(id)) // TODO JAKE: Make sure this is a type that supports an inflicted status.
        {
            return CreateEditorDropDown<UnitStatus>();
        }
        return null;
    }

    private RepositoryItem SetEditor_Type(string id)
    {
        if (_atomicEffects.Contains(id))
        {
            return CreateEditorDropDown<AtomicActionEffectType>();
        }
        if(_atomicConditions.Contains(id))
        {
            return CreateEditorDropDown<AtomicActionConditionType>();
        }
        return null;
    }

    private RepositoryItem SetEditor_InflictStatus(string id)
    {
        if (_atomicEffects.Contains(id))
        {
            return CreateEditorDropDown<UnitStatus>();
        }
        if (_atomicConditions.Contains(id))
        {
            return CreateEditorDropDown<ConditionComparator>();
        }
        return null;
    }

    private RepositoryItem SetEditor_IncType(string id)
    {
        if(_atomicEffects.Contains(id))
        {
            return CreateEditorDropDown<StatiInclusionType>();
        }
        return null;
    }


    private RepositoryItem SetEditor_ReqStati(string id)
    {
        if(!_atomicEffects.Contains(id))
        {
            return null;
        }
        return CreateEditorCheckedComboBoxEdit(() =>
        {
            List<UnitStatus> statusList = Utils.GetEnumList<UnitStatus>();
            Dictionary<string, bool> toReturn = new Dictionary<string, bool>();
            if(!TryParseId(id,out bool primaryList,out int atomicId, out int _, out string _))
            {
                return null;
            }
            HashSet <UnitStatus> stati = DataManager.Inst.GetAtomicEffectConditionalStati(_actionCard, _xowerType,primaryList,atomicId); 
            foreach (UnitStatus status in statusList)
            {
                toReturn.Add(status.ToString(), stati.Contains(status));
            }
            return toReturn;
        });
    }

    private RepositoryItem Editor_Curve(string id)
    {
        return CreateEditorDropDown<CurveType>();
    }
    #endregion
    #region validation
    private bool Validator_Logic(string id, object value)
    {
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string _))
        {
            return false;
        }
        if(_conditionHeaders.Contains(id))
        {
            return DataManager.Inst.SetAtomicEffectConditionalStatiInclusionType(_actionCard, _xowerType, primaryList, atomicId, GetEnum<StatiInclusionType>(value));
        }
        if(_atomicConditions.Contains(id))
        {
            ConditionComparator comparator = GetEnum<ConditionComparator>(value);
            return DataManager.Inst.SetXowerAtomicEffectAtomicConditionComparator(_actionCard, _xowerType, primaryList, atomicId, conditionId, comparator);
        }
        return false;
    }

    private bool Validator_Stati(string id, object value)
    {
        if (!TryParseId(id, out bool primaryList, out int effectIndex, out _, out _))
        { return false; }
        if (_conditionHeaders.Contains(id))
        {
            HashSet<UnitStatus> stati = GetEnumList<UnitStatus>(value);
            return DataManager.Inst.SetAtomicEffectConditionalStati(_actionCard, _xowerType, primaryList, effectIndex, stati);
        }
        if(_atomicEffects.Contains(id))
        {
            return DataManager.Inst.SetAtomicEffectStatusInflicted(_actionCard, _xowerType, primaryList,effectIndex, GetEnum<UnitStatus>(value));
        }
        return false;
    }

    private RankableFloat GetRankableFloat(string id)
    {
        RankableFloat rf = new RankableFloat();
        if(!_nodes.ContainsKey(id))
        {
            Debug.LogError("Yo bro why u gettin rankable floats when there r none?");
            return rf;
        }
        TreeListNode node = _nodes[id];
        rf.BaseValue = (float)node.GetValue(COL_BASE);
        rf.FinalValue = (float)node.GetValue(COL_FINAL);
        rf.Scalar = (float)node.GetValue(COL_SCALAR);
        rf.RandomCap = (float)node.GetValue(COL_RANDCAP);
        rf.Curve = GetEnum<CurveType>(node.GetValue(COL_CURVE));

        return rf;
    }
    private bool Validator_Base(string id, object value)
    {
        if(!_nodes.ContainsKey(id))
        {
            return false;
        }
        if(!TryParseId(id,out bool primaryList, out int atomicId, out int conditionId, out string propertyName))
        { return false; }
        RankableFloat rf = GetRankableFloat(id);
        rf.BaseValue = (float)value;
        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _xowerType, primaryList, atomicId, propertyName, rf);
    }

    private bool Validator_Final(string id, object value)
    {
        if (!_nodes.ContainsKey(id))
        {
            return false;
        }
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string propertyName))
        { return false; }
        RankableFloat rf = GetRankableFloat(id);
        rf.FinalValue = (float)value;
        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _xowerType, primaryList, atomicId, propertyName, rf);
    }

    private bool Validator_Scalar(string id, object value)
    {
        if (!_nodes.ContainsKey(id))
        {
            return false;
        }
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string propertyName))
        { return false; }
        RankableFloat rf = GetRankableFloat(id);
        rf.Scalar = (float)value;
        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _xowerType, primaryList, atomicId, propertyName, rf);
    }

    private bool Validator_RandCap(string id, object value)
    {
        if (!_nodes.ContainsKey(id))
        {
            return false;
        }
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string propertyName))
        { return false; }
        RankableFloat rf = GetRankableFloat(id);
        rf.RandomCap = (float)value;
        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _xowerType, primaryList, atomicId, propertyName, rf);
    }

    private bool Validator_Curve(string id, object value)
    {
        if (!_nodes.ContainsKey(id))
        {
            return false;
        }
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string propertyName))
        { return false; }
        RankableFloat rf = GetRankableFloat(id);
        rf.Curve = GetEnum<CurveType>(value);
        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _xowerType, primaryList, atomicId, propertyName, rf);
    }

    private bool Validator_ChangeEffectType(string id, object value)
    {
        if(!TryParseId(id,out bool primaryList,out int atomicId, out int conditionId, out string _))
        { return false; }
        if (_atomicEffects.Contains(id))
        {
            AtomicActionEffectType type = GetEnum<AtomicActionEffectType>(value);
            return DataManager.Inst.SetAtomicEffectType(_actionCard, _xowerType, primaryList, atomicId, type);
        }
        if (_atomicConditions.Contains(id))
        {
            AtomicActionConditionType type = GetEnum<AtomicActionConditionType>(value);
            return DataManager.Inst.SetXowerAtomicEffectAtomicConditionType(_actionCard,_xowerType,primaryList,atomicId,conditionId,type);
        }
        return false;
    }

    private bool Validator_StatusInflicted(string id, object value)
    {
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string _))
        { return false; }

        if (_atomicEffects.Contains(id))
        {
            UnitStatus status = GetEnum<UnitStatus>(value);
            return DataManager.Inst.SetAtomicEffectStatusInflicted(_actionCard, _xowerType, primaryList, atomicId, status);
        }
        if (_atomicConditions.Contains(id))
        {
            ConditionComparator comparator = GetEnum<ConditionComparator>(value);
            return DataManager.Inst.SetXowerAtomicEffectAtomicConditionComparator(_actionCard, _xowerType, primaryList, atomicId, conditionId, comparator);
        }
        return false;
    }

    //private bool Validator_AmountBase(string id, object value)
    //{
    //    if(!TryParseId(id,out bool primaryList,out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, true);
    //    rf.BaseValue = (float)value;
    //    if (_atomicEffects.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard,_asPower,primaryList,atomicId,"Amount",rf);
    //    }
    //    if(_atomicConditions.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicConditionRankableFloat(_actionCard, _asPower, primaryList, atomicId, conditionId, "CompareTo", rf);       
    //    }
    //    return false;
    //}

    private bool Validator_ReqStati(string id, object value)
    {
        if(!_atomicEffects.Contains(id)) { return false; }
        if (!TryParseId(id, out bool primaryList, out int atomicId, out int _, out string _))
        {
            return false;
        }
        HashSet<UnitStatus> toReturn = GetEnumList<UnitStatus>(value);

        return DataManager.Inst.SetAtomicEffectConditionalStati(_actionCard, _xowerType, primaryList, atomicId,toReturn);
    }

    private bool Validator_IncType(string id, object value)
    {
        if (!_atomicEffects.Contains(id)) { return false; }

        if (!TryParseId(id, out bool primaryList, out int atomicId, out int _, out string _))
        {
            return false;
        }

        return DataManager.Inst.SetAtomicEffectConditionalStatiInclusionType(_actionCard, _xowerType, primaryList, atomicId, GetEnum<StatiInclusionType>(value));
    }

    //private bool Validator_AmountScalar(string id, object value)
    //{
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, true);
    //    rf.Scalar = (float)value;
    //    if (_atomicEffects.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Amount", rf);
    //    }
    //    if (_atomicConditions.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicConditionRankableFloat(_actionCard, _asPower, primaryList, atomicId, conditionId, "CompareTo", rf);
    //    }
    //    return false;
    //}

    //private bool Validator_AmountRandCap(string id, object value)
    //{
    //     if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, true);
    //    rf.RandomCap = (float) value;
    //    if (_atomicEffects.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Amount", rf);
    //    }
    //    if (_atomicConditions.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicConditionRankableFloat(_actionCard, _asPower, primaryList, atomicId, conditionId, "CompareTo", rf);
    //    }
    //    return false;
    //}
    //
    //private bool Validator_AmountFinal(string id, object value)
    //{
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, true);
    //    rf.FinalValue = (float)value;
    //    if (_atomicEffects.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Amount", rf);
    //    }
    //    if (_atomicConditions.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicConditionRankableFloat(_actionCard, _asPower, primaryList, atomicId, conditionId, "CompareTo", rf);
    //    }
    //    return false;
    //}
    //
    //private bool Validator_AmountCurve(string id, object value)
    //{
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, true);
    //    rf.Curve = GetEnum<CurveType>(value);
    //    if (_atomicEffects.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Amount", rf);
    //    }
    //    if (_atomicConditions.Contains(id))
    //    {
    //        return DataManager.Inst.SetAtomicConditionRankableFloat(_actionCard, _asPower, primaryList, atomicId, conditionId, "CompareTo", rf);
    //    }
    //    return false;
    //}
    //
    //private bool Validator_DurationBase(string id, object value)
    //{
    //    if (!HasDuration(id)) { return false; }
    //
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, false);
    //    rf.BaseValue = (float)value;
    //    return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Duration", rf);
    //}

    //private bool Validator_DurationScalar(string id, object value)
    //{
    //    if (!HasDuration(id)) { return false; }
    //
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, false);
    //    rf.Scalar = (float)value;
    //    return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Duration", rf);
    //}
    //
    //private bool Validator_DurationFinal(string id, object value)
    //{
    //    if (!HasDuration(id)) { return false; }
    //
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, false);
    //    rf.FinalValue = (float)value;
    //    return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Duration", rf);
    //}
    //
    //private bool Validator_DurationCurve(string id, object value)
    //{
    //    if (!HasDuration(id)) { return false; }
    //
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, false);
    //    rf.Curve = GetEnum<CurveType>(value);
    //    return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Duration", rf);
    //}
    //
    //private bool Validator_DurationRandCap(string id, object value)
    //{
    //    if (!HasDuration(id)) { return false; }
    //
    //    if (!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId))
    //    {
    //        return false;
    //    }
    //
    //    RankableFloat rf = GetRankableFloat(id, false);
    //    rf.RandomCap = (float)value;
    //    return DataManager.Inst.SetAtomicEffectRankableFloat(_actionCard, _asPower, primaryList, atomicId, "Duration", rf);
    //}

    //private RankableFloat GetRankableFloat(string nodeId, bool isAmountNotDuration)
    //{
    //    TreeListNode node = _nodes[nodeId];
    //    RankableFloat rf = new RankableFloat();
    //    rf.BaseValue = (float)node.GetValue(isAmountNotDuration ? AMOUNT_BASE : DURATION_BASE);
    //    rf.Scalar = (float)node.GetValue(isAmountNotDuration ? AMOUNT_SCALAR : DURATION_SCALAR);
    //    rf.FinalValue = (float)node.GetValue(isAmountNotDuration ? AMOUNT_FINAL : DURATION_FINAL);
    //    rf.Curve = GetEnum<CurveType>(node.GetValue(isAmountNotDuration ? AMOUNT_CURVE : DURATION_CURVE));
    //    rf.RandomCap = (float)node.GetValue(isAmountNotDuration ? AMOUNT_RANDCAP : DURATION_RANDCAP);
    //
    //    return rf;
    //}


    #endregion
    #region ID handling

    private string GetAtomicEffectId(bool primaryList, int index)
    {
        return  HeaderKey(primaryList) + "." + index.ToString();
    }

    private string GetAtomicEffectPropertyId(bool primaryList,int index, string property)
    {
        return GetAtomicEffectId(primaryList,index) + "." + property;
    }

    private string GetConditionHeaderId(bool primaryList, int index)
    {
        return GetAtomicEffectId(primaryList, index) + "." + LIST_CONDITIONS;
    }

    private string GetAtomicEffectAtomicConditionId(bool primaryList, int effectIndex, int conditionIndex)
    {
        return GetConditionHeaderId(primaryList, effectIndex) + "." + conditionIndex.ToString();
    }

    private bool TryParseId(string id, out bool primaryList, out int atomicId, out int conditionId, out string propertyName)
    {
        // Examples:
        // Primary.0.Duration
        // Primary.0.Conditions.0.Duration
        // Secondary.1.Amount
        // Secondary.0.Conditions.0.CompareTo

        propertyName = "";
        atomicId = -1;
        conditionId = -1;
        primaryList = false;
        string[] segs = id.Split('.');
        if (segs.Length < 2) { return false; }
        if(!TryHeaderKeyIsPrimary(segs[0], out primaryList)) { return false; }
        atomicId = int.Parse(segs[1]);
        if (segs.Length > 2)
        {
            switch (segs[2])
            {
                case LIST_CONDITIONS:
                    if (segs.Length < 4)
                    { return true; }
                    conditionId = int.Parse(segs[3]);
                    propertyName = PROP_COMPARETO; // the entire conditions data only really has one RF.
                    break;
                default:
                    propertyName = segs[2];
                    break;
            }
        }
        return true;
    }

    string HeaderKey(bool primaryList)
    {
        return primaryList ? LIST_PRIMARY : LIST_SECONDARY;
    }

    bool TryHeaderKeyIsPrimary(string header, out bool primary)
    {
        primary = false;
        if(header == LIST_PRIMARY)
        {
            primary = true;
            return true;
        }
        if(header == LIST_SECONDARY)
        {
            primary = false;
            return true;
        }
        return false;
    }
    #endregion
    #region Signals

    //private void HandleToggle(bool showPowers)
    //{
    //    _xowerType = showPowers;
    //    RefreshActionCard();
    //}

    private void HandleLevelChange(int level)
    {
        _level = level;
        RefreshActionCard();
    }

    private void HandleCardChange(string actionCard, SunTree tree)
    {
        if(DataManager.Inst.ActionCardIsRunnerPower(actionCard) != (_xowerType == XowerType.RunnerPower))
        { return; }
        _actionCard = actionCard;
        RefreshActionCard();
    }

    private void HandleCardUpdated(string actionCard)
    {
        if(actionCard == _actionCard)
        {
            RefreshActionCard();
        }
    }
    #endregion

    private void RefreshActionCard()
    {
        string selectedId = "";
        if (Selection.Count > 0)
        {
            selectedId = Selection[0].Tag.ToString();
        }

        List<string> nids = _nodes.Keys.ToList();
        foreach(string nid in nids)
        {
            Nodes.Remove(_nodes[nid]);
            _nodes.Remove(nid);
        }
        _atomicEffectListHeaders.Clear();
        _atomicEffects.Clear();
        _atomicConditions.Clear();
        _conditionHeaders.Clear();
        _rankableFloats.Clear();


        InitialPopulateAction(_actionCard);
        //AddChildren(true);
        //AddChildren(false);

        if(!string.IsNullOrEmpty(selectedId) && _nodes.ContainsKey(selectedId))
        {
            SetFocusedNode(_nodes[selectedId]);
        }
    }

    private void ClearNode(TreeListNode node)
    {
        foreach(TreeListNode child in node.Nodes)
        {
            ClearNode(child);
        }
        Nodes.Remove(node);
        _nodes.Remove(node.Tag.ToString());
    }

    protected override bool SpeedRender(int column, TreeListNode node)
    {
        return false;
    }

    protected override string GetTreeName() { return nameof(XowerEffectsTree); }
    protected override List<string> ContextMenuItems(string id, bool multiSelect)
    {
        List<string> menu = new List<string>();
        if (string.IsNullOrEmpty(id)) { return menu; }
        if(_atomicEffectListHeaders.Contains(id))
        {
            menu.Add(MENU_ADD_EFFECT);
            return menu;
        }
        if(!TryParseId(id, out bool primaryList, out int atomicId, out int conditionId, out string _))
        {
            return menu;
        }
        menu.Add(MENU_ADD_CONDITION);
        menu.Add(MENU_DELETE_EFFECT);
        menu.Add(MENU_CLONE_EFFECT);
        menu.Add(MENU_ADD_CONDITION);
        if (_atomicConditions.Contains(id))
        {
            menu.Add(MENU_CLONE_CONDITION);
            menu.Add(MENU_REMOVE_CONDITION);
        }
        return menu;
    }
    protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
    {
        bool primaryList;
        int atomicId;
        int conditionId;
        string propertyName;
        switch (operation)
        {
            case MENU_ADD_EFFECT:
                if (TryIsPrimaryList(id, out primaryList))
                { 
                    DataManager.Inst.AddXowerAtomicEffect(_actionCard, _xowerType, primaryList); 
                }
                break;
            case MENU_CLONE_EFFECT:
                if(TryParseId(id,out primaryList,out atomicId, out conditionId, out propertyName))
                {
                    DataManager.Inst.CopyXowerAtomicEffect(_actionCard, _xowerType, primaryList, atomicId); 
                }
                break;
            case MENU_DELETE_EFFECT:
                if(TryParseId(id,out primaryList, out atomicId, out conditionId, out propertyName))
                {
                    DataManager.Inst.RemoveXowerAtomicEffect(_actionCard,_xowerType,primaryList,atomicId);
                }
                break;
            case MENU_ADD_CONDITION:
                if(TryParseId(id,out primaryList, out atomicId, out conditionId, out propertyName))
                {
                    DataManager.Inst.AddXowerAtomicEffectCondition(_actionCard, _xowerType, primaryList, atomicId);
                }
                break;
            case MENU_REMOVE_CONDITION:
                if (TryParseId(id, out primaryList, out atomicId, out conditionId, out propertyName))
                {
                    DataManager.Inst.RemoveXowerAtomicEffectCondition(_actionCard, _xowerType, primaryList, atomicId, conditionId);
                }
                break;
            case MENU_CLONE_CONDITION:
                if (TryParseId(id, out primaryList, out atomicId, out conditionId, out propertyName))
                {
                    DataManager.Inst.CloneXowerAtomicEffectCondition(_actionCard, _xowerType, primaryList, atomicId, conditionId);
                }
                break;
        }
    }

    private bool TryIsPrimaryList(string id, out bool primaryList)
    {
        if (TryParseId(id, out primaryList, out int _, out int _, out string _))
        {
            return true;
        }
        if(TryHeaderKeyIsPrimary(id, out primaryList))
        {
            return true;
        }
        return false;
    }
    protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
    protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType)
    {
        if (targetNode == GetRoot()) { return; }
        if(!TryParseId(droppingID,out bool primaryList, out int atomicId,out int conditionId, out string propertyName))
        { return; }

        string parentId = targetNode.Tag.ToString();
        // we have to do some additional checking in case we're trying to use "Before" insert type on the node 
        // right below the root.
        if(insertType == InsertType.Before)
        {
            parentId = targetNode.PrevVisibleNode.Tag.ToString();
            if (parentId == GetRoot().Tag.ToString()) { return; }
        }

        while(!_atomicEffectListHeaders.Contains(parentId))
        {
            if (_nodes[parentId].ParentNode == null) 
            {
                Debug.LogError("Something went horribly wrong, could not properly find the parent of this node.");
                return; 
            }
            parentId = _nodes[parentId].ParentNode.Tag.ToString();
        }

        if(!TryHeaderKeyIsPrimary(parentId, out bool primaryRoot))
        {
            Debug.LogError($"Error! Somehow we are assuming that the node with id {parentId} is a header!");
            return;
        }
        if(primaryRoot == primaryList)
        {
            Debug.Log("Dropped a node onto the list it was already in.");
            return;
        }

        if (!DataManager.Inst.MoveXowerAtomicEffectToPrimaryFromSecondary(_actionCard, _xowerType, primaryList, atomicId))
        {
            return;
        }
    }
    protected override void RuntimeInit() { }
    protected override void ClearEverything() { }
    protected override List<string> GetIDsToPopulate()
    {
        return new List<string>() { DataManager.Inst.GetStringId<ActionCardType>(ActionCardType.Fake_DeerPower) };
    }
    protected override void InitialPopulateAction(string cardId) 
    {
        //return;
        _actionCard = cardId;

        for (int i = 0; i < 2; i++)
        {
            bool primaryList = i == 0;
            string headerId = HeaderKey(primaryList);
            TreeListNode node = AppendNode(GetHeader(headerId), GetRoot(), headerId);
            _nodes.Add(headerId, node);
            _atomicEffectListHeaders.Add(headerId);
            AddChildren(primaryList);
        }
    }

    protected override bool IsHeader(string id)
    {
        return _atomicEffectListHeaders.Contains(id);
    }

    protected override bool IsSubHeader(string id)
    {
        return _atomicEffects.Contains(id);
    }

    private void AddChildren(bool primaryList)
    {
        TreeListNode node = _nodes[HeaderKey(primaryList)];
        for (int i = 0; i < DataManager.Inst.GetXowerAtomicEffectCount(_actionCard, _xowerType, _level, primaryList); i++)
        {
            string aid = GetAtomicEffectId(primaryList, i);
            TreeListNode atomicEffectNode = AppendNode(GetAtomicEffectData(aid, _xowerType, primaryList, i), node, aid);
            _nodes.Add(aid, atomicEffectNode);
            
            _atomicEffects.Add(aid);

            // Maybe add a "StatusInflicted" node, instead of jamming it into the AtomicEffect?

            AddProperty(primaryList, i, PROP_AMOUNT);
            
            //AtomicActionEffectType type = DataManager.Inst.GetAtomicEffectType(_actionCard, _asPower, primaryList, i);
            if(DurationTypeEffect(DataManager.Inst.GetAtomicEffectType(_actionCard, _xowerType, primaryList, i)))
            {
                AddProperty(primaryList,i,PROP_DURATION);
            }
            
            string conditionsHeaderId = GetConditionHeaderId(primaryList, i);
            TreeListNode conditionHeaderNode = AppendNode(GetAtomicEffectConditionHeaderData(conditionsHeaderId, _xowerType, primaryList, i),atomicEffectNode, conditionsHeaderId);
            _nodes.Add(conditionsHeaderId, conditionHeaderNode);
            _conditionHeaders.Add(conditionsHeaderId);
            
            
            for (int j = 0; j < DataManager.Inst.GetXowerAtomicEffectAtomicConditionsCount(_actionCard, _xowerType,primaryList, i); j++)
            {
                string cid = GetAtomicEffectAtomicConditionId(primaryList, i, j);
                TreeListNode condition = AppendNode(GetAtomicEffectConditionsData(cid, _xowerType, primaryList, i, j), conditionHeaderNode, cid);
                _nodes.Add(cid, condition);
                _atomicConditions.Add(cid);
                _rankableFloats.Add(cid);
            }
        }
        ExpandAll();
    }

    private void AddProperty(bool primaryList, int i, string propertyName)
    {
        string aid = GetAtomicEffectId(primaryList, i);
        TreeListNode atomicEffectNode = _nodes[aid];
        string propId = GetAtomicEffectPropertyId(primaryList, i, propertyName);
        TreeListNode propertyNode = AppendNode(GetAtomicEffectPropertyData(propId, _xowerType, primaryList, i, propertyName), atomicEffectNode, propId);
        _nodes.Add(propId, propertyNode);
        _rankableFloats.Add(propId);
    }

    
}