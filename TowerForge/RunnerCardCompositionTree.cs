﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class RunnerCardRosterTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private Dictionary<string, string> _compositions = new Dictionary<string, string>();
	private HashSet<string> _ranks = new HashSet<string>();

	private string _currentCardId;

	private const string ADD_RUNNER = "Add Runner";
	private const string REMOVE_RUNNER = "Remove Runner";

	public RunnerCardRosterTree() : base()
	{
		AddColumn("Name", 100, 100);//,ColumnEditor,ValidateColumn);
		AddColumn("ID",visible:false);

		Signals.RunnerCardSelected.Register(ChangeCardID);
		OnNodeSelect += (id) =>
		{
			string sendId = id;
			if(_compositions.ContainsKey(id))
            {
				sendId = _compositions[id];	
            }
			if (int.TryParse(sendId, out int index))
			{ Signals.RankSelected.Send(index); }
		};
	}
	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			$"Rank {id}"
		};
		return data;
	}

	public void HandleUnitDrops(string targetId, List<string> unitIds)
    {
		if(!_nodes.ContainsKey(targetId))
        {
			return;
        }
		TreeListNode parent = _nodes[targetId];
		if(_compositions.ContainsKey(targetId))
        {
			parent = parent.ParentNode;
        }

		foreach(string unitId in unitIds)
        {
			string cid = parent.Tag.ToString() + "." + parent.Nodes.Count.ToString();
            TreeListNode node = AppendNode(GetUnitData(unitId), parent, cid);
			_nodes.Add(cid, node);
			_compositions.Add(cid, parent.Tag.ToString());
        }
        SetComps();
    }

	//private RepositoryItem ColumnEditor(string id)
    //{
	//	if(_compositions.ContainsKey(id))
    //    {
	//		return CreateEditorDropDown<RunnerUnitType>();
    //    }
	//	return null;
    //}

	//private bool ValidateColumn(string id, object value)
    //{
	//	if((string)value == RunnerUnitType.None.ToString())
    //    {
	//		return false;
    //    }
	//	return true;
    //}

	protected override List<string> GetErrors(string id, bool exitEarly)
    {
		List<string> errors = new List<string>();
		if(_ranks.Contains(id) || !_nodes.ContainsKey(id))
        {
			return errors;
        }
		if(!System.Enum.TryParse<RunnerUnitType>((string)_nodes[id].GetValue(0), out RunnerUnitType type))
        {
            errors.Add("Can not parse Runner Unit Type!");
			if (exitEarly)
			{ return errors; }
        }
		if(type == RunnerUnitType.None)
        {
			errors.Add("Runner must not be 'None'!");
			if (exitEarly)
			{ return errors; }
        }
		return errors;
    }

	private object[] GetUnitData(string unitId)
    {
		return new object[]
		{
			DataManager.Inst.DisplayName(unitId),
			unitId
		};
    }

	private void ChangeCardID(string id)
    {
		_currentCardId = id;
		foreach(string comp in _compositions.Keys)
        {
			Nodes.Remove(_nodes[comp]);
			_nodes.Remove(comp);
        }
		_compositions.Clear();

		GetRoot();
		RootName = DataManager.Inst.DisplayName(id);

		List<int> ranks = DataManager.Inst.GetRunnerCardUniqueCompositionRanks(id);
		for(int i = 0; i < ranks.Count; i++)
        {
			int squadCount = DataManager.Inst.GetRunnerCardUnitCountForRank(id, ranks[i]);
			for(int j = 0; j < squadCount; j++)
            {
				string unitId = DataManager.Inst.GetRunnerCardUnitInRankComposition(id, ranks[i], j);
				string cid = ranks[i] + "." + j;
				TreeListNode node = AppendNode(GetUnitData(unitId), _nodes[ranks[i].ToString()], cid);
				_nodes.Add(cid, node);
                _compositions.Add(cid, ranks[i].ToString());
            }
        }
    }

    protected override void PostValueEdit()
    {
		SetComps();
    }

    private void SetComps()
    {
		Dictionary<int, List<RunnerUnitType>> rosters = new Dictionary<int, List<RunnerUnitType>>();
		foreach(string nodeId in _compositions.Keys)
        {
			string parentId = _nodes[nodeId].ParentNode.Tag.ToString();
			int parentIndex = int.Parse(parentId);
			if(!rosters.ContainsKey(parentIndex))
            {
				rosters.Add(parentIndex, new List<RunnerUnitType>());
            }
			string unitId = _nodes[nodeId].GetValue(1).ToString();
			if (DataManager.Inst.TryGetEnumFromId<RunnerUnitType>(unitId, out RunnerUnitType type))
			{
				rosters[parentIndex].Add(type);
			}
        }
        DataManager.Inst.SetRunnerCardRoster(_currentCardId, rosters);
    }
	protected override string GetTreeName() { return "RunnerCardDetailsTree"; }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		//if (string.IsNullOrEmpty(id)) { return menu; }
		//if(_ranks.Contains(id))
        //{
		//	menu.Add(ADD_RUNNER);
        //}
		if(_compositions.ContainsKey(id))
        {
			menu.Add(REMOVE_RUNNER);
        }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{ 
		switch(operation)
        {
			//case ADD_RUNNER:
			//	if(_ranks.Contains(id))
            //    {
			//		string cid = id + "." + _nodes[id].Nodes.Count.ToString();
			//		TreeListNode node = AppendNode(new object[] { RunnerUnitType.None.ToString() }, _nodes[id], cid);
			//		_nodes.Add(cid, node);
			//		_compositions.Add(cid,id);
            //    }
			//	break;
			case REMOVE_RUNNER:
				_nodes[id].Remove();
				_nodes.Remove(id);
				_compositions.Remove(id);
				SetComps();
				ChangeCardID(_currentCardId);
				break;
		}
	}
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return new List<string>()
		{
			"0",
			"1",
			"2",
			"3"
		};
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), GetRoot(), id);
		_nodes.Add(id, node);
		_ranks.Add(id);
	}

    protected override bool IsHeader(string id)
    {
		return _ranks.Contains(id);
    }

}