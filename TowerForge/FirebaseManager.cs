﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Firestore;

public class FirebaseManager
{
    #region variables
    private static FirebaseManager _instance;
    private FirestoreUtil _firestore;
    private ServerData_LibraryInfo _gameLibrary;
    private ServerData_Loot _lootData;
    private ServerData_UserCardHistory _userCardHistory;
    private ServerData_ReadOnlyProfile _readOnlyProfile;
    private ServerData_UserLibrary _userLibrary;
    private ServerData_ReadWriteProfile _readWriteProfile;
    private ServerData_PrivateStore _privateStore;
    //private DataStreamManifest _cachedManifest;

    //public string Manifest;

    

    private Dictionary<string, string> _contentVersions = new Dictionary<string, string>();

    //private Dictionary<string, DataInfoClass> _dataAddresses = new Dictionary<string, DataInfoClass>();

    #endregion variables
    #region init
    public static FirebaseManager Inst
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FirebaseManager();
            }
            return _instance;
        }
    }
    public void Init()
    {
        _firestore = new FirestoreUtil();
        _firestore.Init();
        //_dataAddresses = TRDataUtils.DataAddresses();
        //Signals.Save.Register(Save);
    }

    public void FetchData(Action onComplete)
    {
        FetchDataAsync(onComplete);
    }

    public async Task FetchDataAsync(Action onComplete = null)
    {
        //Manifest = await DataManager.Inst.GetManifestName();
        List<Task> tasks = new List<Task>();

        //await AdjustAccounts();

        //Task<DataStreamManifest> m = GetManifest(Manifest);
        tasks.Add(FetchGameLibrary());
        tasks.Add(FetchLoot());
        tasks.Add(FetchUserBackend());
        tasks.Add(FetchUserReadOnlyProfile());
        tasks.Add(FetchUserLibrary());
        tasks.Add(FetchReadWriteProfile());
        tasks.Add(FetchPrivateStore());

        await Task.WhenAll(tasks);
        //_cachedManifest = await m;
        onComplete?.Invoke();
    }

    private async Task FetchPrivateStore()
    {
        try
        {
            _privateStore = new ServerData_PrivateStore();
            //List<string> address = new List<string>() { TRDataUtils.SERVERDATA_LIBRARY_COLLECTION, TRDataUtils.SERVERDATA_LIBRARY_DOC };
            _privateStore = await _firestore.GetDocAsObject<ServerData_PrivateStore>(_privateStore.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing PrivateStore!");
        }
    }

    private async Task FetchGameLibrary()
    {
        try
        {
            _gameLibrary = new ServerData_LibraryInfo();
            //List<string> address = new List<string>() { TRDataUtils.SERVERDATA_LIBRARY_COLLECTION, TRDataUtils.SERVERDATA_LIBRARY_DOC };
            _gameLibrary = await _firestore.GetDocAsObject<ServerData_LibraryInfo>(_gameLibrary.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing GameLibrary!");
        }
    }

    private async Task FetchLoot()
    {
        try
        {
            _lootData = new ServerData_Loot();
            //List<string> address = new List<string>() { TRDataUtils.DATA_COLLECTION,TRDataUtils.SERVERDATA_DOC, TRDataUtils.SERVERDATA_LOOT_COLLECTION, TRDataUtils.SERVERDATA_LOOT_DOC };
            //_lootData = await _firestore.GetDocAsObject<ServerData_Loot>(address);
            _lootData = await _firestore.GetDocAsObject<ServerData_Loot>(_lootData.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing Loot!");
        }
    }

    private async Task FetchUserBackend()
    {
        try
        {
            _userCardHistory = new ServerData_UserCardHistory();
            _userCardHistory = await _firestore.GetDocAsObject<ServerData_UserCardHistory>(_userCardHistory.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing UserBackend!");
        }
    }

    private async Task FetchUserReadOnlyProfile()
    {
        try
        {
            _readOnlyProfile = new ServerData_ReadOnlyProfile();
            _readOnlyProfile = await _firestore.GetDocAsObject<ServerData_ReadOnlyProfile>(_readOnlyProfile.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing ReadOnlyProfile!");
        }
    }
    private async Task FetchUserLibrary()
    {
        try
        {
            _userLibrary = new ServerData_UserLibrary();
            _userLibrary = await _firestore.GetDocAsObject<ServerData_UserLibrary>(_userLibrary.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing UserLibrary!");
        }
    }

    private async Task FetchReadWriteProfile()
    {
        try
        {
            _readWriteProfile = new ServerData_ReadWriteProfile();
            _readWriteProfile = await _firestore.GetDocAsObject<ServerData_ReadWriteProfile>(_readWriteProfile.TemplateAddress());
        }
        catch
        {
            Debug.LogError("Error while parsing ReadWriteProfile!");
        }
    }
    #endregion init/ variables
    #region save
    public void Save()
    {
        PrepSave();
        List<ServerData_BaseData> datas = new List<ServerData_BaseData>()
        {
            _gameLibrary,
            _lootData,
            _userCardHistory,
            _readOnlyProfile,
            _userLibrary,
            _readWriteProfile,
            _privateStore
        };
        List<Task> tasks = new List<Task>();

        foreach (ServerData_BaseData data in datas)
        {
            List<string> address = data.TemplateAddress();
            tasks.Add(_firestore.WriteObjectToDoc(data.TemplateAddress(), data));
        }
        //SaveAllDatas();
    }

    #region blah
    public async Task<T> DownloadData<T>()
    {
        Type dataFileType = typeof(T);
        string dataFileName = dataFileType.Name;
        string data = await GetDataFile(dataFileName);
        if (data == null)
        {
            Debug.LogError($"Unable to load data file: {dataFileName} from server");
            return default;
        }

        return TRDataUtils.Deserialize<T>(data);
    }

    private async Task<LevelData> LoadLevelDataFromServer(string levelFileName, string folderName)
    {
        string data = await GetLevelDataFile(levelFileName, folderName);
        if (data == null)
        {
            Debug.LogError($"Unable to load level data file: {levelFileName} from server");
            return default;
        }
        return TRDataUtils.Deserialize<LevelData>(data);
    }

    private string GetDataFileURL(string fileName)
    {
        return $"{TRDataUtils.BaseCloudURL}/{TRDataUtils.DataBucket}/{TRDataUtils.GameDataBranch}/{fileName}.json";
    }
    private string GetDataFileURL(string fileName, string folderName)
    {
        return $"{TRDataUtils.BaseCloudURL}/{TRDataUtils.DataBucket}/{TRDataUtils.GameDataBranch}/{folderName}/{fileName}.json";
    }
    public async Task<string> GetDataFile(string fileName)
    {
        string url = GetDataFileURL(fileName);
        return await RequestFileFromServer(url);
    }

    public async Task<string> GetLevelDataFile(string levelFileName, string folderName)
    {

        string url = GetDataFileURL(levelFileName, folderName);
        return await RequestFileFromServer(url);
    }
    private async Task<string> RequestFileFromServer(string url)
    {
        using HttpClient client = new HttpClient();
        return client.GetStringAsync(url).Result;
    }

    #endregion blah


    //private async Task SaveAllDatas()
    //{
    //    List<Task> tasks = new List<Task>();
    //
    //    bool updateManifest = false;
    //    foreach (string dataFileType in DataManager.Inst.GetUploadableDataNames())
    //    {
    //        // TODO: Find a way to skip this file if it isn't dirty.
    //        if (!_contentVersions.ContainsKey(dataFileType))
    //        {
    //            updateManifest = true;
    //            tasks.Add(CacheNewFileName(dataFileType));
    //        }
    //    }
    //    await Task.WhenAll(tasks);
    //
    //    tasks.Clear();
    //    foreach (DataFile data in DataManager.Inst.GetUploadableData())
    //    {
    //        string dataFileType = data.GetType().Name;
    //        FirestoreGameData newDoc = new FirestoreGameData()
    //        {
    //            FormatVersion = _dataAddresses[dataFileType].CurrentVersion,
    //            Data = TRDataUtils.Serialize(data)
    //        };
    //
    //        _cachedManifest.Versions[dataFileType] = _contentVersions[dataFileType];
    //
    //        List<string> dataAddress = new List<string>()
    //        {
    //            TRDataUtils.DATA_COLLECTION,
    //            TRDataUtils.GAMEDATA_DOC,
    //            _dataAddresses[dataFileType].CollectionName,
    //            _contentVersions[dataFileType]
    //        };
    //        tasks.Add(_firestore.WriteObjectToDoc<FirestoreGameData>(dataAddress, newDoc));
    //    }
    //    await Task.WhenAll(tasks);
    //
    //    //if (updateManifest)
    //    {
    //        _cachedManifest.UpdatedAt = DateTime.UtcNow;
    //        await PushManifest(Manifest, _cachedManifest);
    //    }
    //}

    //private async Task CacheNewFileName(string dataFileType)
    //{
    //    FirestoreDataInfo info = await GetInfo(dataFileType);
    //    string fv = _dataAddresses[dataFileType].CurrentVersion.ToString("x2");
    //    string newDocName = _dataAddresses[dataFileType].DocName + "_" + fv + "_" + info.FormatVersionToNextContentVersion[fv].ToString("x2");
    //    _contentVersions.Add(dataFileType, newDocName);
    //    _cachedManifest.Versions[dataFileType] = newDocName;
    //    info.FormatVersionToNextContentVersion[fv]++;
    //    await PushInfo(dataFileType, info);
    //}

    //private async Task SaveToCloudAsync<T>(T data) where T : DataFile
    //{
    //    string dataTypeName = typeof(T).Name;  
    //    FirestoreDataInfo info = await GetInfo(dataTypeName);
    //    string fv = _dataAddresses[dataTypeName].CurrentVersion.ToString("x2");
    //    if (!info.FormatVersionToNextContentVersion.ContainsKey(fv))
    //    {
    //        info.FormatVersionToNextContentVersion.Add(fv, 1);
    //    }
    //
    //    string newDocName = _dataAddresses[dataTypeName].DocName + "_" + fv + "_" + info.FormatVersionToNextContentVersion[fv].ToString("x2");
    //    info.FormatVersionToNextContentVersion[fv]++;
    //
    //    List<string> dataAddress = new List<string>()
    //    {
    //        _dataAddresses[dataTypeName].CollectionName,
    //        newDocName
    //    };
    //
    //    // Since we've already fetched fresh data, might as well cache it!
    //    _dataAddresses[dataTypeName].Info = info;
    //
    //    string json = TRDataUtils.Serialize(data);
    //
    //
    //    FirestoreGameData newDoc = new FirestoreGameData()
    //    {
    //        FormatVersion = _dataAddresses[dataTypeName].CurrentVersion,
    //        Data = json
    //    };
    //
    //    // push the modified info file and the newly created document up to firestore.
    //    List<Task> pushTasks = new List<Task>();
    //    pushTasks.Add(_firestore.WriteObjectToDoc(dataAddress, newDoc));
    //    pushTasks.Add(PushInfo(dataTypeName, info));
    //    pushTasks.Add(UpdateManifest(dataTypeName, newDocName));
    //    await Task.WhenAll(pushTasks);
    //    //Debug.Log($"Created {newDocName} from {path}!");
    //}

    //private async Task UpdateManifest(string dataTypeName, string newVersion)
    //{
    //    DataStreamManifest data = await GetManifest(Manifest);
    //
    //    data.Versions[dataTypeName] = newVersion;
    //    data.UpdatedAt = DateTime.UtcNow;
    //
    //    //List<string> streamAddress = new List<string>() { TRDataUtils.MANIFEST_COLLECTION, streamManifest };
    //    await PushManifest(Manifest, data);
    //}

    //public async Task<bool> ManifestExists(string streamManifest)
    //{
    //    List<string> streamAddress = new List<string>() { TRDataUtils.MANIFEST_COLLECTION, streamManifest };
    //    DataStreamManifest data = await _firestore.GetDocAsObject<DataStreamManifest>(streamAddress);
    //    return data != null;
    //}

    //public async Task<Dictionary<string, DataStreamManifest>> GetAllManifests()
    //{
    //    List<string> address = new List<string>()
    //    {
    //        TRDataUtils.MANIFEST_COLLECTION
    //    };
    //
    //    Debug.Log("Found Manifests");
    //    Dictionary<string, DataStreamManifest> response = await _firestore.GetAllDocsInCollection<DataStreamManifest>(address, "UpdatedAt");
    //    foreach (string id in response.Keys)
    //    {
    //        Debug.Log("Manifest: " + id);
    //    }
    //
    //    // Debug - To Remove:
    //
    //    return response;
    //}


    //private async Task<DataStreamManifest> GetManifest(string manifestName)
    //{
    //    List<string> address = new List<string>() { TRDataUtils.MANIFEST_COLLECTION, manifestName };
    //    return await _firestore.GetDocAsObject<DataStreamManifest>(address);
    //}
    //
    //public async Task PushManifest(string manifestName, DataStreamManifest data)
    //{
    //    List<string> address = new List<string>() { TRDataUtils.MANIFEST_COLLECTION, manifestName };
    //    await _firestore.WriteObjectToDoc(address, data);
    //}
    //
    //private string ManifestName()
    //{
    //    return Manifest;
    //}
    //
    //private async Task PushInfo(string key, FirestoreDataInfo info)
    //{
    //    List<string> address = new List<string>()
    //    {
    //        TRDataUtils.DATA_COLLECTION,
    //        TRDataUtils.GAMEDATA_DOC,
    //        _dataAddresses[key].CollectionName,
    //        TRDataUtils.GAMEDATA_INFO_DOC
    //    };
    //    await _firestore.WriteObjectToDoc(address,info);
    //}
    //
    //private async Task<FirestoreDataInfo> GetInfo(string key)
    //{
    //    List<string> address = new List<string>()
    //    {
    //        TRDataUtils.DATA_COLLECTION,
    //        TRDataUtils.GAMEDATA_DOC,
    //        _dataAddresses[key].CollectionName,
    //        TRDataUtils.GAMEDATA_INFO_DOC
    //    };
    //    return await _firestore.GetDocAsObject<FirestoreDataInfo>(address);
    //}

    private async Task OBS_AdjustAccounts()
    {
        List<string> docs = new List<string>()
        {
            "2SUhyTth5jY7ZRFN1ehPK2zoLj82",
            "3VOyh5MNK8Y8bVJArNlu9u90KlR2",
            "4Eob39b9meRgv0LlYofj0bcQbj52",
            "4nBgG3TZnddN9P50up2ZcipDcR43",
            "9PcLA81Jd5MgEZGijBopqz4fFFq1",
            "Dk4hmP7U8tWlof4fjlAmxeE809k2",
            "DysWcm9mlVN82lCbkgBp7VKgCXf2",
            "EBBV0D1l9gPb8YrN3pzzcnIS8hG2",
            "FHLTZSBZckZ9oGkEHyKOCftxDwk2",
            "FfBF3Lhq9Pd1XcvOQzT1r2X0hKV2",
            "IR4H4uleX4e4ewPRiyPCVJiiNRt2",
            "IcIBGaT70AOeKX1IX6Bu3fvAIBw2",
            "OdJQhQokk4XmuufCiZ86SQQV2df1",
            "PXaWKOtBenSUilhAaNCRqjWDqAv1",
            "QOQ0jTgSZya2QFlGVs39QZ9jQ412",
            "TfsVmJ6bkabfBBDIYSYsbOWTIc73",
            "VJor7TFZNmhWAOOthudcoSxMbPF2",
            "ViWpMMJqoIcgGiMKI0IMj44hmYI3",
            "XKikuaRlR4MWN1SJQkLxiRGmPtt1",
            "alU4xIQzhOaOL5TaYJtqMDnn5tD3",
            "bTgW9crdW8UN2YMRm5Y27O2fRJv1",
            "fEAd3oZt5tfVSs6P4BiHsJYryks1",
            "gIPfWU1vyJQnpY0mdhzjWlKisnz1",
            "iXOH5iMhb2V4KdeMfkBnePZGqUl2",
            "jYuFxSk5cfbqP71eiaqORP1gmCx1",
            "jdKDH4ebIAVW3ZUcEeQje90ayFC3",
            "kstufK7owZX9reGttqUrTaGJZgl1",
            "m1AXxo6M90PBKnjpfC2nr5ZR1D93",
            "my6WimWPPfgyy4A27Hz2TjYcf9r1",
            "uqawfJWOLgYVdtncxCoHXLxDUoA3",
            "xHHrBZd8sJfnoCu1dovuVfC1PLl1"
        };

        foreach (string user in docs)
        {
            List<string> chAddress = new List<string>() { "users", user, "backendData", "cardHistory" };
            ServerData_UserCardHistory ch = await _firestore.GetDocAsObject<ServerData_UserCardHistory>(chAddress);
            if (ch != null && ch.cardHistories != null)
            {
                foreach (string rarity in ch.cardHistories.Keys.ToHashSet())
                {
                    ReplaceHistory("10014", "20001", ch.cardHistories[rarity]);
                    ReplaceHistory("10017", "20002", ch.cardHistories[rarity]);
                    ReplaceHistory("10011", "20003", ch.cardHistories[rarity]);
                    ReplaceHistory("10006", "20004", ch.cardHistories[rarity]);
                }
                await _firestore.WriteObjectToDoc<ServerData_UserCardHistory>(chAddress, ch);
            }

            List<string> lAddress = new List<string>() { "users", user, "userReadOnlyData", "library" };
            ServerData_UserLibrary lib = await _firestore.GetDocAsObject<ServerData_UserLibrary>(lAddress);
            if(lib != null)
            {
                ReplaceLibrary("10014", "20001", lib);
                ReplaceLibrary("10017", "20002", lib);
                ReplaceLibrary("10011", "20003", lib);
                ReplaceLibrary("10006", "20004", lib);
                await _firestore.WriteObjectToDoc<ServerData_UserLibrary>(lAddress, lib);
            }

            List<string> rwAddress = new List<string>() { "users", user, "userReadWriteData", "readWriteProfile" };
            ServerData_ReadWriteProfile rw = await _firestore.GetDocAsObject<ServerData_ReadWriteProfile>(rwAddress);
            if(rw != null)
            {
                ReplaceDeck(10014, 20001, rw);
                ReplaceDeck(10017, 20002, rw);
                ReplaceDeck(10011, 20003, rw);
                ReplaceDeck(10006, 20004, rw);
                await _firestore.WriteObjectToDoc<ServerData_ReadWriteProfile>(rwAddress, rw);
            }
        }
    }

    private void ReplaceDeck(int old, int newname, ServerData_ReadWriteProfile profile)
    {
        for(int d = 0; d < profile.decks.Count; d++)
        {
            for(int r = 0; r < profile.decks[d].runnerCards.Count; r++)
            {
                if ((int)profile.decks[d].runnerCards[r] == old)
                {
                    profile.decks[d].runnerCards[r] = (RunnerCardType)newname;
                }
            }
            if ((int)profile.decks[d].bossCard == old)
            {
                profile.decks[d].bossCard = (RunnerCardType)newname;
            }
        }
    }

    private void ReplaceLibrary(string old, string newname, ServerData_UserLibrary library)
    {
        if (library.cards.ContainsKey(old))
        {
            library.cards.Add(newname, library.cards[old]);
            library.cards[newname].id = int.Parse(newname);
            library.cards.Remove(old);
        }
    }

    private void ReplaceHistory(string old, string newname, ServerData_RarityHistory history)
    {
        if(history.history.ContainsKey(old))
        {
            history.history.Add(newname, history.history[old]);
            history.history[newname].id = int.Parse(newname);
            history.history.Remove(old);
        }
    }


    private void PrepSave()
    {
        // Set this to false if you want defaults to emulate the game.
        bool unlockAll = true;

        _userLibrary.Cards = new Dictionary<string,ServerData_CardStats>();
        _userCardHistory.cardHistories.Clear();
        _userCardHistory.cardHistories.Add(CardRarity.Common.ToString(), new ServerData_RarityHistory());
        _userCardHistory.cardHistories.Add(CardRarity.Rare.ToString(), new ServerData_RarityHistory());
        _userCardHistory.cardHistories.Add(CardRarity.Epic.ToString(), new ServerData_RarityHistory());
        _userCardHistory.cardHistories.Add(CardRarity.Legendary.ToString(), new ServerData_RarityHistory());

        List<string> cardIds = DataManager.Inst.GetIds<ActionCardType>();
        cardIds.AddRange(DataManager.Inst.GetIds<RunnerCardType>());
        foreach (string cardId in cardIds)
        {
            if (!DataManager.Inst.TryGetEnumIntValue(cardId, out int intId))
            {
                continue;
            }
            bool firstArea = _gameLibrary.arenasData[0].unlockedCardIds.Contains(intId) || unlockAll;

            ServerData_CardStats card = new ServerData_CardStats();
            card.id = intId;
            card.name = DataManager.Inst.DisplayName(cardId);
            card.discoverable = firstArea;
            card.level = firstArea ? 1 : 0;
            card.count = firstArea ? 1 : 0;
            _userLibrary.Cards.Add(intId.ToString(), card);

            if (firstArea && DataManager.Inst.GetIsCardActive(cardId))
            {
                CardRarity rarity = GetRarity(intId);

                ServerData_CardHistory cardHistory = new ServerData_CardHistory();
                cardHistory.draws = 0;
                cardHistory.id = intId;
                cardHistory.lowerBound = 0;
                cardHistory.name = DataManager.Inst.DisplayName(cardId);
                cardHistory.upperBound = 0;
                _userCardHistory.cardHistories[rarity.ToString()].history.Add(cardHistory.id.ToString(),cardHistory);
            }
        }

        foreach (string key in _userCardHistory.cardHistories.Keys)
        {
            _userCardHistory.cardHistories[key].totalCardDraws = 0;
            int count = _userCardHistory.cardHistories[key].history.Count;
            float separator = 0;
            float increment = 1f / (float)count;
            foreach (ServerData_CardHistory h in _userCardHistory.cardHistories[key].history.Values)
            {
                h.lowerBound = separator;
                separator += increment;
                h.upperBound = separator;
                h.draws = 1;
                _userCardHistory.cardHistories[key].totalCardDraws++;
            }
        }

        for(int i = 0; i < _readOnlyProfile.chests.Count; i++)
        {
            _readOnlyProfile.chests[i] = null;
        }
        //foreach(ServerData_UserChest chest in _readOnlyProfile.chests)
        //{
        //    //chest.unlockedDate = DateTime.SpecifyKind(chest.unlockedDate,DateTimeKind.Utc);
        //    chest.unlockedDate = null;
        //}
    }

    #endregion save
    #region public data modification
    #region store
    #region dailyDeals

    public bool ClearArenaDeals(int arenaIndex)
    {
        int arenaCount = _gameLibrary.arenasData.Count;
        if (arenaIndex >= arenaCount || arenaIndex < 0)
        {
            Debug.LogError("arenaIndex was out of bounds!");
            return false;
        }
        while (arenaIndex >= _privateStore.arenaDailyDeals.Count)
        {
            _privateStore.arenaDailyDeals.Add(new ServerData_ArenaDailyDeals());
        }
        _privateStore.arenaDailyDeals[arenaIndex] = new ServerData_ArenaDailyDeals();
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    private bool TryGetArenaDailyDeals(int arenaIndex, out ServerData_ArenaDailyDeals arenaDailyDeals)
    {
        arenaDailyDeals = new ServerData_ArenaDailyDeals();
        int arenaCount = _gameLibrary.arenasData.Count;
        if(arenaIndex >= arenaCount || arenaIndex < 0) 
        {
            Debug.LogError("arenaIndex was out of bounds!");
            return false; 
        }
        while(arenaIndex >= _privateStore.arenaDailyDeals.Count)
        {
            _privateStore.arenaDailyDeals.Add(new ServerData_ArenaDailyDeals());
        }
        arenaDailyDeals = _privateStore.arenaDailyDeals[arenaIndex];
        return true;
    }

    private bool TryGetDailyDealSet(int arenaIndex, int dealSetIndex, out ServerData_DailyDealsSet dealSet)
    {
        dealSet = new ServerData_DailyDealsSet();
        if(!TryGetArenaDailyDeals(arenaIndex, out ServerData_ArenaDailyDeals arenaDailyDeals))
        {
            return false;
        }
        if(dealSetIndex >= arenaDailyDeals.dailyDealSets.Count || dealSetIndex <0)
        {
            Debug.LogError("dealSetIndex was out of range!");
            return false;
        }
        dealSet = arenaDailyDeals.dailyDealSets[dealSetIndex];
        return true;

    }

    public int GetDailyDealSetCount(int arenaIndex)
    {
        if (!TryGetArenaDailyDeals(arenaIndex, out ServerData_ArenaDailyDeals arenaDailyDeals))
        {
            return 0;
        }
        
        return arenaDailyDeals.dailyDealSets.Count;
    }

    public bool AddDailyDealSet(int arenaIndex)
    {
        if (!TryGetArenaDailyDeals(arenaIndex, out ServerData_ArenaDailyDeals arenaDailyDeals))
        {
            return false;
        }
        arenaDailyDeals.dailyDealSets.Add(new ServerData_DailyDealsSet());
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool RemoveDailyDealSet(int arenaIndex,int dealSetIndex)
    {
        if (!TryGetArenaDailyDeals(arenaIndex, out ServerData_ArenaDailyDeals arenaDailyDeals))
        {
            return false;
        }
        if (dealSetIndex >= arenaDailyDeals.dailyDealSets.Count || dealSetIndex < 0)
        {
            Debug.LogError("dealSetIndex was out of range!");
            return false;
        }
        arenaDailyDeals.dailyDealSets.RemoveAt(dealSetIndex);
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public List<string> GetDailyDealKeys(int arena, int dealSetIndex)
    {
        if (!TryGetDailyDealSet(arena, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return default;
        }
        return dealSet.dailyDeals.Keys.ToList();
    }

    private bool TryGetDailyDeal(int arena, int dealSetIndex, string dealId, out ServerData_StorePack deal)
    {
        deal = default;
        if (!TryGetDailyDealSet(arena, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        if (!dealSet.dailyDeals.ContainsKey(dealId))
        {
            Debug.LogError($"Deal pack for arena {arena} set {dealSetIndex} does not contain a deal named {dealId}!");
            return false;
        }
        deal = dealSet.dailyDeals[dealId];
        return true;
    }

    public ServerData_StorePack GetDailyDeal(int arena,int dealSetIndex,string dealId)
    {
        if(!TryGetDailyDeal(arena,dealSetIndex,dealId, out ServerData_StorePack deal))
        { return default; }
        return deal;
    }

    public bool SetDailyDealNonArrayProperties(int arenaIndex, int dealSetIndex, string dealId, ServerData_StorePack deal)
    {
        if (!TryGetDailyDealSet(arenaIndex, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        if (!dealSet.dailyDeals.ContainsKey(dealId))
        {
            Debug.LogError($"Deal pack for arena {arenaIndex} set {dealSetIndex} does not contain a deal named {dealId}!");
            return false;
        }
        dealSet.dailyDeals[dealId].name = deal.name;
        dealSet.dailyDeals[dealId].CommonCurrency = deal.CommonCurrency;
        dealSet.dailyDeals[dealId].PremiumCurrency = deal.PremiumCurrency;
        dealSet.dailyDeals[dealId].isGemPack = deal.isGemPack;
        dealSet.dailyDeals[dealId].price = deal.price;
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool SetWholeDailyDeal(int arenaIndex, int dealSetIndex, string dealId, ServerData_StorePack deal)
    {
        if (!TryGetDailyDealSet(arenaIndex, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        if (!dealSet.dailyDeals.ContainsKey(dealId))
        {
            Debug.LogError($"Deal pack for arena {arenaIndex} set {dealSetIndex} does not contain a deal named {dealId}!");
            return false;
        }
        dealSet.dailyDeals[dealId] = deal;
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public int GetDailyDealCardCount(int arenaIndex, int dealSetIndex, string dealId)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return 0; }
        return deal.Cards.Count;
    }

    public int GetDailyDealChestCount(int arenaIndex, int dealSetIndex, string dealId)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return 0; }
        return deal.Chests.Count;
    }

    public RewardedCard GetDailyDealCard(int arenaIndex, int dealSetIndex, string dealId, int cardIndex)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return default; }
        if(cardIndex >= deal.Cards.Count || cardIndex < 0)
        {
            Debug.LogError("cardIndex is out of range!");
            return default;
        }
        return deal.Cards[cardIndex];
    }

    public bool SetDailyDealCard(int arenaIndex, int dealSetIndex, string dealId, int cardIndex, RewardedCard card)
    {
        if(card == null)
        {
            return false;
        }
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return false; }
        if (cardIndex >= deal.Cards.Count || cardIndex < 0)
        {
            Debug.LogError("cardIndex is out of range!");
            return false;
        }
        deal.Cards[cardIndex] = card;
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool AddDailyDealCard(int arenaIndex, int dealSetIndex, string dealId)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return false; }
        deal.Cards.Add(new RewardedCard());
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool RemoveDailyDealCard(int arenaIndex, int dealSetIndex, string dealId, int cardIndex)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return false; }
        if (cardIndex >= deal.Cards.Count || cardIndex < 0)
        {
            Debug.LogError("cardIndex is out of range!");
            return false;
        }
        deal.Cards.RemoveAt(cardIndex);
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public ChestType GetDailyDealChest(int arenaIndex, int dealSetIndex, string dealId, int chestIndex)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return ChestType.None; }
        if (chestIndex >= deal.Chests.Count || chestIndex < 0)
        {
            Debug.LogError("chestIndex is out of range!");
            return ChestType.None;
        }
        if (!Enum.TryParse<ChestType>(deal.Chests[chestIndex], out ChestType chestType))
        {
            Debug.LogError($"Chest in index {chestIndex} is not a ChestType");
            return ChestType.None;
        }
        return chestType;
    }

    public bool SetDailyDealChest(int arenaIndex, int dealSetIndex, string dealId, int chestIndex, ChestType chest)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return default; }
        if (chestIndex >= deal.Chests.Count || chestIndex < 0)
        {
            Debug.LogError("chestIndex is out of range!");
            return false;
        }
        deal.Chests[chestIndex] = chest.ToString();
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool AddDailyDealChest(int arenaIndex, int dealSetIndex, string dealId)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return false; }
        deal.Chests.Add(ChestType.Common.ToString());
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool RemoveDailyDealChest(int arenaIndex, int dealSetIndex, string dealId, int chestIndex)
    {
        if (!TryGetDailyDeal(arenaIndex, dealSetIndex, dealId, out ServerData_StorePack deal))
        { return false; }
        if (chestIndex >= deal.Chests.Count || chestIndex < 0)
        {
            Debug.LogError("chestIndex is out of range!");
            return false;
        }
        deal.Chests.RemoveAt(chestIndex);
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool AddDailyDeal(int arenaIndex, int dealSetIndex, out string dealId)
    {
        dealId = null;
        if (!TryGetDailyDealSet(arenaIndex, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        //string uid = GenericUniqueKey(dealSet.dailyDeals.Keys.ToHashSet());
        dealId = $"deal_{arenaIndex}_{dealSetIndex}_{dealSet.dailyDeals.Count}";
        dealSet.dailyDeals.Add(dealId, new ServerData_StorePack());
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool AddDailyDeal(int arenaIndex, int dealSetIndex)
    {
        return AddDailyDeal(arenaIndex, dealSetIndex, out _);
    }

    public bool RemoveDailyDeal(int arenaIndex, int dealSetIndex, string key)
    {
        if (!TryGetDailyDealSet(arenaIndex, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        if(!dealSet.dailyDeals.ContainsKey(key))
        {
            return false;
        }
        dealSet.dailyDeals.Remove(key);
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    public bool ChangeDailyDealKey(int arenaIndex, int dealSetIndex, string oldKey, string newKey)
    {
        if (oldKey == newKey) { return true; }
        if (!TryGetDailyDealSet(arenaIndex, dealSetIndex, out ServerData_DailyDealsSet dealSet))
        {
            return false;
        }
        if (!dealSet.dailyDeals.ContainsKey(oldKey) || dealSet.dailyDeals.ContainsKey(newKey))
        {
            Debug.LogError("Invalid rename!");
            return false;
        }
        dealSet.dailyDeals.Add(newKey, dealSet.dailyDeals[oldKey]);
        dealSet.dailyDeals.Remove(oldKey);
        Signals.RefreshArenaDailyDeals.Send(arenaIndex);
        return true;
    }

    #endregion dailydeals
    #endregion store
    #region chests
    public List<string> GetChestList()
    {
        return new List<string>(_lootData.ChestList);
    }

    public bool SetChestList(List<string> chestList)
    {
        HashSet<string> valid = new HashSet<string>();
        valid.Add(ChestType.Common.ToString());
        valid.Add(ChestType.Rare.ToString());
        valid.Add(ChestType.Epic.ToString());
        valid.Add(ChestType.Legendary.ToString());
        foreach (string item in chestList)
        {
            if (!valid.Contains(item))
            {
                return false;
            }
        }
        _lootData.ChestList = chestList;
        // TODO JAKE: Maybe push this up to the server?
        return true;
    }
    #endregion chests
    #region cards
    public int GetMaxLevel()
    {
        return _gameLibrary.maxCardLevel;
    }

    public bool SetMaxLevel(int level)
    {
        if (level < 0)
        {
            return false;
        }
        _gameLibrary.maxCardLevel = level;
        return true;
    }

    public int GetCurrencyForRarityLevel(CardRarity rarity, int level)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return -1;
        }
        if (level < 0 || level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            return -1;
        }
        return _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].requiredCurrency;
    }

    public bool SetCurrencyForRarityLevel(CardRarity rarity, int level, int count)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return false;
        }
        while (level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Add(new ServerData_CardAdvancementStats());
        }
        _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].requiredCurrency = count;
        Signals.LibraryUpdated.Send();
        return true;
    }

    public int GetCommandForRarityLevel(CardRarity rarity, int level)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return -1;
        }
        if (level < 0 || level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            return -1;
        }
        return _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].commandReward;
    }

    public bool SetCommandForRarityLevel(CardRarity rarity, int level, int count)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return false;
        }
        while (level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Add(new ServerData_CardAdvancementStats());
        }
        _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].commandReward = count;
        Signals.LibraryUpdated.Send();
        return true;
    }

    public int GetUpgradeForRarityLevel(CardRarity rarity, int level)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return -1;
        }
        if(level < 0 || level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            return -1;
        }    
        return _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].requiredCards;
    }

    public bool SetUpgradeForRarityLevel(CardRarity rarity, int level, int count)
    {
        if((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return false;
        }
        while (level >= _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Count)
        {
            _gameLibrary.rarityInfo[(int)rarity].cardAdvancement.Add(new ServerData_CardAdvancementStats());
        }
        _gameLibrary.rarityInfo[(int)rarity].cardAdvancement[level].requiredCards = count;
        Signals.LibraryUpdated.Send();
        return true;
    }

    public int GetRarityStartingLevel(CardRarity rarity)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return -1;
        }
        return _gameLibrary.rarityInfo[(int)rarity].startingLevel;
    }

    public bool SetRarityStartingLevel(CardRarity rarity, int startingLevel)
    {
        if ((int)rarity >= _gameLibrary.rarityInfo.Count || (int)rarity < 0)
        {
            return false;
        }
        _gameLibrary.rarityInfo[(int)rarity].startingLevel = startingLevel;
        Signals.LibraryUpdated.Send();
        return true;
    }

    public CardRarity GetRarity(ActionCardType card)
    {
        return GetRarity((int)card);
    }

    public CardRarity GetRarity(RunnerCardType card)
    {
        return GetRarity((int)card);
    }

    public CardRarity GetRarity(int cardId)
    {
        if(!_gameLibrary.cardRarities.ContainsKey(cardId.ToString()))
        {
            return CardRarity.None;
        }
        return _gameLibrary.cardRarities[cardId.ToString()];
    }

    public bool SetRarity(ActionCardType type, CardRarity rarity)
    {
        if(SetRarity((int)type, rarity))
        {
            Signals.ActionCardUpdated.Send(DataManager.Inst.GetStringId<ActionCardType>(type));
            return true;
        }
        return false;
    }

    public bool SetRarity(RunnerCardType type, CardRarity rarity)
    {
        if(SetRarity((int)type, rarity))
        {
            Signals.RunnerCardUpdated.Send(DataManager.Inst.GetStringId<RunnerCardType>(type));
            return true;
        }
        return false;
    }

    public bool SetRarity(int cardId, CardRarity rarity)
    {
        string cardString = cardId.ToString();
        if(!_gameLibrary.cardRarities.ContainsKey(cardString))
        {
            _gameLibrary.cardRarities.Add(cardString, CardRarity.None);
        }
        _gameLibrary.cardRarities[cardString] = rarity;
        Signals.LibraryUpdated.Send();
        return true;

    }
    #endregion Cards
    #region decks
    public const int USERDECK_RUNNER_COUNT = 4;
    public const int USERDECK_ACTION_COUNT = 6;
    public const int USERDECK_BOSS_COUNT = 1;
    public const int USERDECK_COUNT = 5;

    private void ValidateDecks()
    {
        while (USERDECK_COUNT > _readWriteProfile.decks.Count)
        {
            _readWriteProfile.decks.Add(new ServerData_ServerDeck());
        }
        foreach(ServerData_ServerDeck deck in _readWriteProfile.decks)
        {
            while(USERDECK_RUNNER_COUNT > deck.runnerCards.Count)
            {
                deck.runnerCards.Add(RunnerCardType.None);
            }
            while(USERDECK_ACTION_COUNT > deck.actionCards.Count)
            {
                deck.actionCards.Add(ActionCardType.None);
            }
            // don't do this for boss cards, because it isn't an array.
        }
    }

    public ActionCardType GetUserDeckActionCard(int deck, int index)
    {
        if(deck < 0 || deck >= USERDECK_COUNT || index < 0 || index >= USERDECK_ACTION_COUNT)
        {
            return ActionCardType.None;
        }
        ValidateDecks();
        return _readWriteProfile.decks[deck].actionCards[index];
    }

    public bool SetUserDeckActionCard(int deck, int index, ActionCardType type)
    {
        if (deck < 0 || deck >= USERDECK_COUNT || index < 0 || index >= USERDECK_ACTION_COUNT)
        {
            return false;
        }
        ValidateDecks();
        _readWriteProfile.decks[deck].actionCards[index] = type;
        return true;
    }

    public RunnerCardType GetUserDeckRunnerCard(int deck, int index)
    {
        if (deck < 0 || deck >= USERDECK_COUNT || index < 0 || index >= USERDECK_RUNNER_COUNT)
        {
            return RunnerCardType.None;
        }
        ValidateDecks();
        return _readWriteProfile.decks[deck].runnerCards[index];
    }

    public bool SetUserDeckRunnerCard(int deck, int index, RunnerCardType type)
    {
        if (deck < 0 || deck >= USERDECK_COUNT || index < 0 || index >= USERDECK_RUNNER_COUNT)
        {
            return false;
        }
        ValidateDecks();
        _readWriteProfile.decks[deck].runnerCards[index] = type;
        return true;
    }

    public RunnerCardType GetUserDeckBossCard(int deck, int index)
    {
        if (deck < 0 || deck >= USERDECK_COUNT || index != 0)
        {
            return RunnerCardType.None;
        }
        ValidateDecks();
        return _readWriteProfile.decks[deck].bossCard;
    }

    public bool SetUserDeckBossCard(int deck, int index, RunnerCardType type)
    {
        if (deck < 0 || deck >= USERDECK_COUNT || index != 0)
        {
            return false;
        }
        ValidateDecks();
        _readWriteProfile.decks[deck].bossCard = type;
        return true;
    }

    #endregion decks
    #region arena
    public int GetArenaForCardUnlock(int cardId)
    {
        int arena = -1;
        for (int a = 0; a < _gameLibrary.arenasData.Count; a++)
        {
            if (_gameLibrary.arenasData[a].unlockedCardIds.Contains(cardId))
            {
                if(arena >= 0)
                {
                    Debug.LogError($"Error!! The card {cardId} shows up in arena {a} AND arena {arena}! This must be fixed manually!");
                }
                arena = a;
            }
        }
        return arena;
    }

    public int GetArenaCount()
    {
        return _gameLibrary.arenasData.Count;
    }

    public bool AddArena(out int arenaIndex)
    {
        _gameLibrary.arenasData.Add(new ServerData_ArenaData());
        arenaIndex = _gameLibrary.arenasData.Count - 1;
        Signals.LibraryUpdated.Send();
        return true;
    }

    public bool SetArenaUnlockedCards(Dictionary<int, List<int>> cardIds)
    {
        foreach (int arenaIndex in cardIds.Keys)
        {
            while (arenaIndex >= _gameLibrary.arenasData.Count)
            {
                _gameLibrary.arenasData.Add(new ServerData_ArenaData());
            }
            _gameLibrary.arenasData[arenaIndex].unlockedCardIds = cardIds[arenaIndex];
        }
        Signals.LibraryUpdated.Send();
        return true;
    }

    public bool SetCardUnlockedArena(int cardId, int arena)
    {
        if(arena > _gameLibrary.arenasData.Count) // don't check for < 0 in this case, because that's what we send if we're removing it from all arenas.
        {
            return false;
        }
        for(int i = 0; i < _gameLibrary.arenasData.Count; i++)
        {
            if(i == arena && !_gameLibrary.arenasData[i].unlockedCardIds.Contains(cardId))
            {
                _gameLibrary.arenasData[i].unlockedCardIds.Add(cardId);
            }
            if(i != arena)
            {
                _gameLibrary.arenasData[i].unlockedCardIds.Remove(cardId);
            }
        }
        Signals.LibraryUpdated.Send();
        return true;
    }

    public Dictionary<CardRarity,List<int>> GetAllCardsUnlockedByArena(int arena)
    {
        Dictionary<CardRarity, List<int>> result = new Dictionary<CardRarity, List<int>>();
        if (arena >= _gameLibrary.arenasData.Count) // don't check for < 0 in this case, because that's what we send if we're removing it from all arenas.
        {
            return result;
        }
        for (int i = 0; i <= arena; i++)
        {
            foreach(int cardId in _gameLibrary.arenasData[i].unlockedCardIds)
            {
                CardRarity rarity = FirebaseManager.Inst.GetRarity(cardId);
                if(!result.ContainsKey(rarity))
                { result.Add(rarity, new List<int>()); }
                result[rarity].Add(cardId);
            }
        }
        return result;
    }

    public int GetELO(int arenaIndex)
    {
        if (arenaIndex < 0 || arenaIndex >= _gameLibrary.arenasData.Count)
        {
            return -1;
        }
        return _gameLibrary.arenasData[arenaIndex].eloRequirement;
    }

    public bool SetELO(int arenaIndex, int ELO)
    {
        if (arenaIndex < 0 || arenaIndex >= _gameLibrary.arenasData.Count)
        {
            return false;
        }
        _gameLibrary.arenasData[arenaIndex].eloRequirement = ELO;
        Signals.LibraryUpdated.Send();
        return true;
    }


    #endregion arena
    #region single player stats

    public int GetUserLootIndex()
    {
        return _userCardHistory.lootIndex;
    }

    public bool SetUserLootIndex(int lootIndex)
    {
        if(lootIndex < 0 || lootIndex >= _lootData.ChestList.Count)
        { return false; }
        _userCardHistory.lootIndex = lootIndex;
        return true;
    }

    public int GetUserStartingGems()
    {
        return _userLibrary.gems;
    }

    public bool SetUserStartingGems(int gems)
    {
        if(gems < 0) { return false; }
        _userLibrary.gems = gems;
        return true;
    }

    public int GetUserStartingGold()
    {
        return _userLibrary.gold;
    }

    public bool SetUserStartingGold(int gold)
    {
        if (gold < 0) { return false; }
        _userLibrary.gold = gold;
        return true;
    }

    public int GetUserStartingBaseHealth()
    {
        return _readOnlyProfile.baseHealth;
    }

    public bool SetUserStartingBaseHealth(int baseHealth)
    {
        if (baseHealth < 0) { return false; }
        _readOnlyProfile.baseHealth = baseHealth;
        return true;
    }

    public int GetUserStartingLevel()
    {
        return _readOnlyProfile.userLevel;
    }

    public bool SetUserStartingLevel(int startingLevel)
    {
        if (startingLevel < 0) { return false; }
        _readOnlyProfile.userLevel = startingLevel;
        return true;
    }

    public int GetUserStartingDeck()
    {
        return _readWriteProfile.activeDeck;
    }

    public bool SetUserStartingDeck(int deck)
    {
        if (deck < 0 || deck >= _readWriteProfile.decks.Count)
        {
            return false;
        }
        _readWriteProfile.activeDeck = deck;
        return true;
    }

    #endregion single player stats
    #endregion public data modification

    private string GenericUniqueKey(HashSet<string> currentKeys)
    {
        int keyInt = 0;
        string key = $"<New {keyInt}>";
        while (currentKeys.Contains(key))
        {
            keyInt++;
            key = $"<New {keyInt}>";
        }
        return key;
    }
    //private async Task 
}

#region firestore Util
public class FirestoreUtil
{ 

    private FirestoreDb _db;

    public void Init()
    {
        //https://www.youtube.com/watch?v=ptEtTNQ0dXg
        string path = AppDomain.CurrentDomain.BaseDirectory + @"credentials.json";
        Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

        _db = FirestoreDb.Create("tower-rush-836bf");
        Debug.Log("Connected to Firestore.");
    }


    #region FIRESTORE WRITES
    public async Task WriteObjectToDoc<T>(List<string> address, T data, Action callback = null)
    {

        DocumentReference docRef = GetDocRef(address);
        if (docRef == null) { return; }

        try
        {
            WriteResult result = await docRef.SetAsync(data);

        }
        catch(Exception e)
        {
            Debug.LogError("hey you suck here's the error: " + e.Message);
        }
        
        Debug.Log("Added data document to " + docRef.Path);
        callback?.Invoke();
    }

    public async Task UpdateDocument(List<string> address, Dictionary<string, object> data)
    {

        DocumentReference cityRef = GetDocRef(address);
        WriteResult result = await cityRef.UpdateAsync(data);
        
    }
    #endregion

    #region PUBLIC GETS
    public async Task<T> GetDocAsObject<T>(List<string> address) where T : class
    {
        //if (!_auth.FirebaseUserReady) { return null; }

        DocumentReference docRef = GetDocRef(address);
        if (docRef == null) { return null; }

        T thing = null;
        await docRef.GetSnapshotAsync().ContinueWith((task) =>
        {
            if (task.Status != TaskStatus.RanToCompletion)
            {
                Debug.LogError("CRITICAL ERROR: Firestore data did not succeed in fetching data. Perhaps your credentials have expired?");
            }
            var snapshot = task.Result;
            if (snapshot.Exists)
            {
                try
                {
                    thing = snapshot.ConvertTo<T>();
                }
                catch
                {
                    Debug.LogError($"Error while trying to parse document as {typeof(T).Name}.");
                }
            }
        });
        return thing;
    }

    public async Task<Dictionary<string, T>> GetAllDocsInCollection<T>(List<string> address, string orderBy)
    {
        Dictionary<string, T> things = new Dictionary<string, T>();
        CollectionReference collection = GetCollectionRef(address);
        Query allCitiesQuery = collection.OrderBy(orderBy);
        QuerySnapshot result = await allCitiesQuery.GetSnapshotAsync();
        if (result != null)
        {
            foreach (DocumentSnapshot documentSnapshot in result.Documents)
            {
                string id = documentSnapshot.Id;
                T thing = documentSnapshot.ConvertTo<T>();
                things.Add(id, thing);
            }
        }
        return things;
    }

    public async Task<List<string>> GetAllDocNamesInCollection(List<string> address)
    {
        List<string> toReturn = new List<string>();
        CollectionReference collection = GetCollectionRef(address);
        Query q = collection.OrderBy("UpdatedAt");
        QuerySnapshot result = await q.GetSnapshotAsync();
        if(result != null)
        {
            foreach(DocumentSnapshot docSnapshot in result.Documents)
            {
                toReturn.Add(docSnapshot.Id);
            }
        }
        return toReturn;
    }
    #endregion

    #region PRIVATE HELPERS
    private DocumentReference GetDocRef(List<string> address)
    {
        if (address.Count <= 1 || address.Count % 2 != 0)
        {
            return null;
        }

        DocumentReference docRef = _db.Collection(address[0]).Document(address[1]);
        if (address.Count > 2)
        {
            for (int i = 2; i < address.Count; i += 2)
            {
                docRef = docRef.Collection(address[i]).Document(address[i + 1]);
            }
        }

        return docRef;
    }

    private CollectionReference GetCollectionRef(List<string> address)
    {
        if (address.Count == 0 || address.Count % 2 != 1)
        {
            return null;
        }

        CollectionReference collectionRef = _db.Collection(address[0]);
        if (address.Count > 1)
        {
            for (int i = 2; i < address.Count; i += 2)
            {
                collectionRef = collectionRef.Document(address[i]).Collection(address[i + 1]);
            }
        }

        return collectionRef;
    }
    #endregion
}
#endregion