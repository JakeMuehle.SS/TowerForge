﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class TEST_TREE : TreeList
{
    public TEST_TREE()
    {
        OptionsMenu.EnableNodeMenu = false;
        MouseUp += (s, e) =>
        {
            int test = 0;
        };

        Columns.Add(new DevExpress.XtraTreeList.Columns.TreeListColumn() 
        { 
            Width = 100, 
            Caption = "Column",
            FieldName = "Column",
            Visible = true,
        });
        TreeListNode node = AppendNode(new object[] { "hello" }, null);
        AppendNode(new object[] { "hal" }, node);
    }
}

