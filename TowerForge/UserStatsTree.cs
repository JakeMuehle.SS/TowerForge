﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class UserStatsTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();

	private const string NODE_LOOT_INDEX = "Loot index";
	private const string NODE_GEMS = "Starting Gems";
	private const string NODE_GOLD = "Starting Gold";
	private const string NODE_BASE_HEALTH = "Starting Base Health";
	private const string NODE_LEVEL = "Starting User Level";
	private const string NODE_DECK = "Active Deck";
	private const string NODE_MAX = "Max Card Level";
	public UserStatsTree() : base()
	{
		AddColumn("Property", 100, 100);
		AddColumn("Value", 100, 100, Editor_Value,Validator_Value);
	}
	private object[] GetData(string id)
	{
		string value = "";
		switch(id)
        {
			case NODE_LOOT_INDEX:
				value = FirebaseManager.Inst.GetUserLootIndex().ToString();
				break;
			case NODE_GEMS:
				value = FirebaseManager.Inst.GetUserStartingGems().ToString();
				break;
			case NODE_GOLD:
				value = FirebaseManager.Inst.GetUserStartingGold().ToString();
				break;
			case NODE_BASE_HEALTH:
				value = FirebaseManager.Inst.GetUserStartingBaseHealth().ToString();
				break;
			case NODE_LEVEL:
				value = FirebaseManager.Inst.GetUserStartingLevel().ToString();
				break;
			case NODE_DECK:
				value = FirebaseManager.Inst.GetUserStartingDeck().ToString();
				break;
			case NODE_MAX:
				value = FirebaseManager.Inst.GetMaxLevel().ToString();
				break;
			default:
				Debug.LogError($"UserStatsTree is not set up to show value for {id}!");
				break;

        }
		object[] data = new object[]
		{
			id,
			value
		};
		return data;
	}

	private RepositoryItem Editor_Value(string id)
    {
		return CreateEditorTextEdit(DataEditorType.IntegerZeroOrPositive);
    }

	private bool Validator_Value(string id, object value)
    {
		switch(id)
        {
			case NODE_LOOT_INDEX:
				return FirebaseManager.Inst.SetUserLootIndex((int)value);
			case NODE_GEMS:
				return FirebaseManager.Inst.SetUserStartingGems((int)value);
			case NODE_GOLD:
				return FirebaseManager.Inst.SetUserStartingGold((int)value);
			case NODE_BASE_HEALTH:
				return FirebaseManager.Inst.SetUserStartingBaseHealth((int)value);
			case NODE_LEVEL:
				return FirebaseManager.Inst.SetUserStartingLevel((int)value);
			case NODE_DECK:
				return FirebaseManager.Inst.SetUserStartingDeck((int)value);
			case NODE_MAX:
				return FirebaseManager.Inst.SetMaxLevel((int)value);
            default:
				Debug.LogError($"Case {id} not handled in the user stats tree validation!");
				return false;
        }
    }


	protected override string GetTreeName() { return nameof(UserStatsTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return new List<string>() 
		{ 
			NODE_LOOT_INDEX,
			NODE_DECK,
			NODE_GEMS,
			NODE_GOLD,
			NODE_LEVEL,
			NODE_BASE_HEALTH,
			NODE_MAX
		};
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}