﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;

public class RunnerAbilityTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();

	private int BASE_COL;
	private int FINAL_COL;
	private int SCALAR_COL;
	private int CURVE_COL;
	private int RANDCAP_COL;
	private int NAME_COL;

	private const string FIELD_ACTION_EFFECT = "TargetActionEffect";
	private const string FIELD_INTEREST_VALUES = "InterestValues";

	private string _runnerName;
	private int _abilityIndex;
	private int _effectIndex;
	private HashSet<string> _rankableNodes = new HashSet<string>();
	private RunnerAbilityType _abilityType;

	private const string MENU_ADD_SPAWN_UNIT = "Add spawned unit";
	private const string MENU_REMOVE_SPAWN_UNIT = "Remove spawned unit";

	private delegate bool HandleRankableFloatDelegate(string name, RankableFloat rankableFloat);

	private HandleRankableFloatDelegate _handleRankableFloat;
	public RunnerAbilityTree() : base()
	{
		AddColumn("ID", 100, 100);

		//Signals.RunnerAbilitySelected.Register(HandleRunnerAbilitySelected);
		Signals.RunnerAbilityEffectSelected.Register(HandleRunnerAbilityEffectSelected);
		Signals.RunnerUnitSelected.Register(ClearTree);
	}

	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id
		};
		return data;
	}


	private void HandleRunnerAbilityEffectSelected(string runnerId, int abilityIndex, int effectIndex)
	{
		ClearTree("");
		if (abilityIndex < 0) { return; }
		_runnerName = runnerId;
		_abilityIndex = abilityIndex;
		_effectIndex = effectIndex;

		// this is the most common case. Overwrite it if you need something trickier.
		_handleRankableFloat = (name, rf) =>
		{
			return DataManager.Inst.SetRunnerAbilityRankableFloat(runnerId, abilityIndex, effectIndex, name, rf);
		};

		_abilityType = DataManager.Inst.GetRunnerAbilityEffectType(runnerId, abilityIndex, effectIndex);
		switch (_abilityType)
		{
			case RunnerAbilityType.SpawnUnits: Ability_SpawnUnits(runnerId, abilityIndex, effectIndex); break;
			//case RunnerAbilityType.StunAttacker:						Ability_StunAttacker(runnerId, abilityIndex); break;
			case RunnerAbilityType.GainStatus: Ability_GainStatus(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.RemoveStatus: Ability_RemoveStatus(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.MovementSpeed: Ability_MoveSpeed(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.SpawnPower:	Ability_SpawnPower(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.Teleport: Ability_Teleport(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.Heal: Ability_Heal(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.GainShields: Ability_Shields(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.ModifyIncomingDurations: Ability_ModifyIncomingDurations(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.ModifyIncomingValue: Ability_ModifyIncomingValue(runnerId, abilityIndex, effectIndex); break;
			case RunnerAbilityType.ModifyIncomingModification: Ability_ModifyIncomingModification(runnerId, abilityIndex, effectIndex); break;
			default:
				Debug.LogError($"RunnerAbilityTree not set up to edit abilities of type {_abilityType}!");
				return;
		}
	}

	private void ClearTree(string id)
	{

		foreach (string nodeId in _nodes.Keys)
		{
			_nodes[nodeId].Remove();
		}
		_nodes.Clear();
		_rankableNodes.Clear();
		ClearColumns();
		Flush();
	}

	private void Ability_GainStatus(string runnerId, int abilityIndex, int effectIndex)
	{

		AddColumn("Property");
		AddColumn("Status", 75, 75, Editor_Status, Validator_Status);
		AddRankableFloatColumns();

		TreeListNode statusNode = AppendNode(new object[] { "Status", DataManager.Inst.GetRunnerAbilityFieldValue<UnitStatus>(runnerId, abilityIndex, effectIndex, "Status") }, null, "Status");
		_nodes.Add("Status", statusNode);
		MakeDirty(statusNode);

		TreeListNode durationNode = AppendNode(new object[] { "Duration", "" }, null, "Duration");
		AssignRankableFloatValues(durationNode, runnerId, abilityIndex, effectIndex, "Duration");
		_nodes.Add("Duration", durationNode);

	}

	private RepositoryItem Editor_Status(string id)
	{
		if (_nodes.ContainsKey(id) && !_rankableNodes.Contains(id))
		{
			return CreateEditorDropDown<UnitStatus>();
		}
		return null;
	}

	private bool Validator_Status(string id, object value)
	{
		if (!_nodes.ContainsKey(id) || _rankableNodes.Contains(id))
		{
			return false;
		}

		return DataManager.Inst.SetRunnerAbilityFieldValue<UnitStatus>(_runnerName, _abilityIndex, _effectIndex, "Status", GetEnum<UnitStatus>(value));
	}

	#region custom spawn units stuff
	private void Ability_SpawnUnits(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Unit", 100, 100, Editor_RunnerUnitType, CustomValidator_SetSpawnUnit);
		AddColumn("PathPosition", 75, 75, Quick_CreateFloatEditor, CustomValidator_SetSpawnPathPos);
		AddColumn("H.Offset", 75, 75, Quick_CreateFloatEditor, CustomValidator_SetSpawnOffset);

		for (int i = 0; i < DataManager.Inst.GetRunnerAbilitySpawnCount(runnerId, abilityIndex, effectIndex); i++)
		{
			PopulateSpawnUnit(runnerId, abilityIndex, effectIndex, i);
		}
		ExpandAll();
	}

	private void PopulateSpawnUnit(string runnerId, int abilityIndex, int effectIndex, int spawnIndex)
	{
		MinionSpawnData spawn = DataManager.Inst.GetRunnerAbilitySpawnData(runnerId, abilityIndex, effectIndex, spawnIndex);
		TreeListNode snode = AppendNode(new object[]
		{
			spawn.Type,
			spawn.SplinePosition,
			spawn.SplineOffset
		}, GetRoot(), spawnIndex.ToString());
		_nodes.Add(spawnIndex.ToString(), snode);
	}

	private bool RemoveSpawnUnit(string nodeId)
	{
		if (!int.TryParse(nodeId, out int spawnIndex))
		{
			return false;
		}

		return DataManager.Inst.RemoveRunnerAbilitySpawnData(_runnerName, _abilityIndex, _effectIndex, spawnIndex);
	}

	private RepositoryItem Editor_RunnerUnitType(string id)
	{
		return CreateEditorDropDown<RunnerUnitType>();
	}

	private bool CustomValidator_SetSpawnUnit(string id, object value)
	{
		if (!int.TryParse(id, out int index))
		{ return false; }

		MinionSpawnData spawn = GetSpawnData(id);

		if (spawn == null)
		{ return false; }

		spawn.Type = GetEnum<RunnerUnitType>(value);
		return DataManager.Inst.SetRunnerAbilitySpawnData(_runnerName, _abilityIndex, _effectIndex, index, spawn);
	}

	private bool CustomValidator_SetSpawnPathPos(string id, object value)
	{
		if (!int.TryParse(id, out int index))
		{ return false; }

		MinionSpawnData spawn = GetSpawnData(id);

		if (spawn == null)
		{ return false; }

		spawn.SplinePosition = (float)value;
		return DataManager.Inst.SetRunnerAbilitySpawnData(_runnerName, _abilityIndex, _effectIndex, index, spawn);
	}

	private bool CustomValidator_SetSpawnOffset(string id, object value)
	{
		if (!int.TryParse(id, out int index))
		{ return false; }

		MinionSpawnData spawn = GetSpawnData(id);

		if (spawn == null)
		{ return false; }

		spawn.SplineOffset = (float)value;
		return DataManager.Inst.SetRunnerAbilitySpawnData(_runnerName, _abilityIndex, _effectIndex, index, spawn);
	}

	private MinionSpawnData GetSpawnData(string nodeId)
	{
		if (!_nodes.ContainsKey(nodeId)) { return null; }
		MinionSpawnData spawn = new MinionSpawnData();
		spawn.Type = GetEnum<RunnerUnitType>(_nodes[nodeId].GetValue(0));
		spawn.SplinePosition = (float)_nodes[nodeId].GetValue(1);
		spawn.SplineOffset = (float)_nodes[nodeId].GetValue(2);
		return spawn;
	}
	#endregion custom spawnunits stuff

	private void Ability_MoveSpeed(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddColumn("Status", 75, 75, Editor_Status, Validator_Status);
		AddRankableFloatColumns();

		TreeListNode statusNode = AppendNode(new object[] { "Status", DataManager.Inst.GetRunnerAbilityFieldValue<UnitStatus>(runnerId, abilityIndex, effectIndex, "Status") }, null, "Status");
		_nodes.Add("Status", statusNode);

		TreeListNode durationNode = AppendNode(new object[] { "Duration", "" }, null, "Duration");
		AssignRankableFloatValues(durationNode, runnerId, abilityIndex, effectIndex, "Duration");
		_nodes.Add("Duration", durationNode);

		TreeListNode speedNode = AppendNode(new object[] { "SpeedFactor", "" }, null, "SpeedFactor");
		AssignRankableFloatValues(speedNode, runnerId, abilityIndex, effectIndex, "SpeedFactor");
		_nodes.Add("SpeedFactor", speedNode);
	}

	private void Ability_Teleport(string runnerId, int abilityIndex, int effectIndex)
    {
		AddColumn("Property");
		AddRankableFloatColumns();

        TreeListNode amountNode = AppendNode(new object[] { "Amount" }, null, "Amount");
		AssignRankableFloatValues(amountNode, runnerId, abilityIndex, effectIndex, "Amount");
		_nodes.Add("Amount", amountNode);

    }

    private void Ability_SpawnPower(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Target Power",120,120,Editor_TargetPower,Validator_TargetPower);
		//AddColumn("Apply",50,50,Editor_ApplyPower,Validator_ApplyPower);

		TreeListNode powerNode = AppendNode(new object[] { ActionCardType.None, false }, null, "abilityPower");
		_nodes.Add("abilityPower", powerNode);
	}

	private void Ability_ModifyIncomingDurations(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddColumn("Value", 100, 100, Editor_ValueColumn, Validator_ValueColumn);
		AddRankableFloatColumns();

		TreeListNode targetEffectNode = AppendNode(new object[] { FIELD_ACTION_EFFECT, DataManager.Inst.GetRunnerAbilityFieldValue<AtomicActionEffectType>(runnerId, abilityIndex, effectIndex, FIELD_ACTION_EFFECT) }, null, FIELD_ACTION_EFFECT);
		_nodes.Add(FIELD_ACTION_EFFECT, targetEffectNode);

		TreeListNode scalarNode = AppendNode(new object[] { "Scalar", "" }, null, "Scalar");
		AssignRankableFloatValues(scalarNode, runnerId, abilityIndex, effectIndex, "Scalar");
		_nodes.Add("Scalar", scalarNode);
	}

	private RepositoryItem Editor_TargetPower(string id)
    {
		return CreateEditorDropDown<ActionCardType>();
    }

	private bool Validator_TargetPower(string id, object value)
    {
		return DataManager.Inst.SetRunnerAbilityPower(_runnerName, _abilityIndex, _effectIndex, GetEnum<ActionCardType>(value));
	}

	private RepositoryItem Editor_ApplyPower(string id)
    {
		return CreateEditorCheckEdit();
    }

	//private bool Validator_ApplyPower(string id, object value)
    //{
	//	ActionCardType type = GetEnum<ActionCardType>(_nodes[id].GetValue(0));
	//	PowerData powerData = DataManager.Inst.GetCopyOfPower(DataManager.Inst.GetStringId<ActionCardType>(type));
	//	return DataManager.Inst.SetRunnerAbilityPower(_runnerName, _abilityIndex, GetEnum<ActionCardType>(value));
    //}

	private RepositoryItem Editor_ValueColumn(string id)
	{
		switch (id)
		{
			case FIELD_ACTION_EFFECT:
				return CreateEditorDropDown<AtomicActionEffectType>();
			case FIELD_INTEREST_VALUES:
				return CreateEditorDropDown<ConditionComparator>();
		}
		return null;
	}

	private bool Validator_ValueColumn(string id, object value)
	{
		switch (id)
		{
			case FIELD_ACTION_EFFECT:
				return DataManager.Inst.SetRunnerAbilityFieldValue<AtomicActionEffectType>(_runnerName, _abilityIndex, _effectIndex, id, GetEnum<AtomicActionEffectType>(value));
			case FIELD_INTEREST_VALUES:
				return DataManager.Inst.SetRunnerAbilityFieldValue<ConditionComparator>(_runnerName, _abilityIndex, _effectIndex, id, GetEnum<ConditionComparator>(value));
		}
		return false;
	}
	private void Ability_RemoveStatus(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddColumn("Status", 75, 75, Editor_Status, Validator_Status);

		TreeListNode statusNode = AppendNode(new object[] { "Status", DataManager.Inst.GetRunnerAbilityFieldValue<UnitStatus>(runnerId, abilityIndex, effectIndex, "Status") }, null, "Status");
		_nodes.Add("Status", statusNode);
	}
	private void Ability_ModifyIncomingValue(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddColumn("Value", 100, 100, Editor_ValueColumn, Validator_ValueColumn);
		AddRankableFloatColumns();

		TreeListNode targetEffectNode = AppendNode(new object[] { FIELD_ACTION_EFFECT, DataManager.Inst.GetRunnerAbilityFieldValue<AtomicActionEffectType>(runnerId, abilityIndex, effectIndex, FIELD_ACTION_EFFECT) }, null, FIELD_ACTION_EFFECT);
		_nodes.Add(FIELD_ACTION_EFFECT, targetEffectNode);
		TreeListNode interestValueNode = AppendNode(new object[] { FIELD_INTEREST_VALUES, DataManager.Inst.GetRunnerAbilityFieldValue<ConditionComparator>(runnerId, abilityIndex, effectIndex, FIELD_INTEREST_VALUES) }, null, FIELD_INTEREST_VALUES);
		_nodes.Add(FIELD_INTEREST_VALUES, interestValueNode);

		TreeListNode scalarNode = AppendNode(new object[] { "Scalar", "" }, null, "Scalar");
		AssignRankableFloatValues(scalarNode, runnerId, abilityIndex, effectIndex, "Scalar");
		_nodes.Add("Scalar", scalarNode);
	}
	private void Ability_ModifyIncomingModification(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddColumn("Value", 100, 100, Editor_ValueColumn, Validator_ValueColumn);
		AddRankableFloatColumns();

		TreeListNode targetEffectNode = AppendNode(new object[] { FIELD_ACTION_EFFECT, DataManager.Inst.GetRunnerAbilityFieldValue<AtomicActionEffectType>(runnerId, abilityIndex, effectIndex, FIELD_ACTION_EFFECT) }, null, FIELD_ACTION_EFFECT);
		_nodes.Add(FIELD_ACTION_EFFECT, targetEffectNode);
		TreeListNode interestValueNode = AppendNode(new object[] { FIELD_INTEREST_VALUES, DataManager.Inst.GetRunnerAbilityFieldValue<ConditionComparator>(runnerId, abilityIndex, effectIndex, FIELD_INTEREST_VALUES) }, null, FIELD_INTEREST_VALUES);
		_nodes.Add(FIELD_INTEREST_VALUES, interestValueNode);

		TreeListNode scalarNode = AppendNode(new object[] { "Scalar", "" }, null, "Scalar");
		AssignRankableFloatValues(scalarNode, runnerId, abilityIndex, effectIndex, "Scalar");
		_nodes.Add("Scalar", scalarNode);

	}
	private void Ability_Heal(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddRankableFloatColumns();

		TreeListNode durationNode = AppendNode(new object[] { "Amount" }, null, "Amount");
		AssignRankableFloatValues(durationNode, runnerId, abilityIndex, effectIndex, "Amount");
		_nodes.Add("Amount", durationNode);
	}

	private void Ability_Shields(string runnerId, int abilityIndex, int effectIndex)
	{
		AddColumn("Property");
		AddRankableFloatColumns();

		TreeListNode durationNode = AppendNode(new object[] { "Amount" }, null, "Amount");
		AssignRankableFloatValues(durationNode, runnerId, abilityIndex, effectIndex, "Amount");
		_nodes.Add("Amount", durationNode);
	}

	private void Ability_StunAttacker(string runnerId, int abilityIndex)
	{

	}


    #region rankablefloats

	private void AssignRankableFloatValues(TreeListNode node, string runnerName, int abilityIndex, int effectIndex, string rfloatName)
    {
		RankableFloat rfloat = DataManager.Inst.GetRunnerAbilityRankableFloat(runnerName, abilityIndex, effectIndex, rfloatName);
		node.SetValue(BASE_COL, rfloat.BaseValue);
		node.SetValue(FINAL_COL, rfloat.FinalValue);
		node.SetValue(CURVE_COL, rfloat.Curve);
		node.SetValue(RANDCAP_COL, rfloat.RandomCap);
		node.SetValue(SCALAR_COL, rfloat.Scalar);
		node.SetValue(NAME_COL, rfloatName);
		_rankableNodes.Add(node.Tag.ToString());
    }
    private void AddRankableFloatColumns()
    {
		BASE_COL = AddColumn("BaseValue",75,75,Quick_CreateFloatEditor,Validator_Base).ColumnIndex;
		FINAL_COL = AddColumn("FinalValue", 75, 75, Quick_CreateFloatEditor, Validator_Final).ColumnIndex;
		CURVE_COL = AddColumn("Curve", 75, 75, Editor_CurveEnum, Validator_Curve).ColumnIndex;
		RANDCAP_COL = AddColumn("RandCap", 75, 75, Quick_CreateFloatEditor, Validator_RandCap).ColumnIndex;
		SCALAR_COL = AddColumn("Scalar", 75, 75, Quick_CreateFloatEditor, Validator_Scalar).ColumnIndex;
		NAME_COL = AddColumn("PropertyName").ColumnIndex;
    }

	private RepositoryItem Editor_CurveEnum(string id)
    {
		return CreateEditorDropDown<CurveType>();
    }

	private bool Validator_Base(string id, object value)
    {
		RankableFloat rf = GetRankableFloat(id);
		if(rf == null) { return false; }
		rf.BaseValue = (float)value;
		return _handleRankableFloat(id, rf);
    }

	private bool Validator_Final(string id, object value)
    {
		RankableFloat rf = GetRankableFloat(id);
		if (rf == null) { return false; }
		rf.FinalValue = (float)value;
		return _handleRankableFloat(id, rf);
	}

	private bool Validator_Curve(string id, object value)
    {
		RankableFloat rf = GetRankableFloat(id);
		if (rf == null) { return false; }
		rf.Curve = GetEnum<CurveType>(value);
		return _handleRankableFloat(id, rf);
	}

	private bool Validator_RandCap(string id, object value)
    {
		RankableFloat rf = GetRankableFloat(id);
		if (rf == null) { return false; }
		rf.RandomCap = (float)value;
		return _handleRankableFloat(id, rf);
	}

	private bool Validator_Scalar(string id, object value)
	{
		RankableFloat rf = GetRankableFloat(id);
		if (rf == null) { return false; }
		rf.Scalar = (float)value;
		return _handleRankableFloat(id, rf);
	}

	private RankableFloat GetRankableFloat(string name)
    {
		if (!_nodes.ContainsKey(name))
		{
			Debug.LogError($"Could not find rankable float with name {name}!");
			return null;
		}

		RankableFloat rf = new RankableFloat();
		rf.BaseValue = (float)_nodes[name].GetValue(BASE_COL);
		rf.FinalValue = (float)_nodes[name].GetValue(FINAL_COL);
		rf.RandomCap = (float)_nodes[name].GetValue(RANDCAP_COL);
		rf.Scalar = (float)_nodes[name].GetValue(SCALAR_COL);
		rf.Curve = GetEnum<CurveType>(_nodes[name].GetValue(CURVE_COL));

		return rf;
    }

    #endregion rankablefloats

    protected override string GetTreeName() { return nameof(RunnerAbilityTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		switch(_abilityType)
        {
			case RunnerAbilityType.SpawnUnits:
				menu.Add(MENU_ADD_SPAWN_UNIT);
				if(!string.IsNullOrEmpty(id) && _nodes.ContainsKey(id))
				{ menu.Add(MENU_REMOVE_SPAWN_UNIT); }
				break;
        }
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) 
	{ 
		switch(operation)
        {
			case MENU_ADD_SPAWN_UNIT:
				DataManager.Inst.AddRunnerAbilitySpawnData(_runnerName, _abilityIndex, _effectIndex);
				break;
			case MENU_REMOVE_SPAWN_UNIT:
				RemoveSpawnUnit(id);
				break;
        }
	}
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return new List<string>();
	}

	protected override bool SpeedRender(int col, TreeListNode node)
    {
		return false;
    }
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}
