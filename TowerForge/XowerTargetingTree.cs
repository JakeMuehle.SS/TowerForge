﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System.Text;
using System;
public class XowerTargetingTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	private XowerType _xowerType = XowerType.Tower; 
	private int _level = 1;
    public XowerTargetingTree() : base()
    {
        AddColumn("ID", 100, 100);
		AddColumn("Targets",100,100,SetEditor_Targets,Validator_MarkType);
		AddColumn("Pref",70,70,SetEditor_Preference,Validator_Preference);
		AddColumn("Splash", 60,60, SetEditor_SplashType,Validator_Splash);
		AddColumn("StatiInclusionType",100,100,SetEditor_IncType,Validator_InclusionType);
		AddColumn("Stati", 100, 100, SetEditor_Stati,Validator_Stati);
		AddColumn("Depth",60,60,SetEditor_Depth,Validator_TargetDepth);

		Signals.ShowPowersNotTowers.Register(HandleToggle);
        Signals.XowerLevelChanged.Register(HandleLevelChange);
		Signals.ActionCardSelected.Register(SelectActionCard);
		Signals.ActionCardUpdated.Register(RefreshActionCard);

		OnNodeSelect += (id) =>
		{
			Signals.ActionCardSelected.Send(id, this); 
		};
    }
    private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			DataManager.Inst.DisplayName(id),
			DataManager.Inst.GetXowerMarkType(id,_xowerType),
			DataManager.Inst.GetXowerAimPreference(id,_xowerType),
			DataManager.Inst.GetXowerSplashType(id,_xowerType),
			DataManager.Inst.GetXowerMarkStatiInclusionType(id, _xowerType),
			Utils.ListString<UnitStatus>(DataManager.Inst.GetXowerMarkStati(id,_xowerType)),
			DataManager.Inst.GetXowerRankableFloat(id,_xowerType,true,"Depth")
		};
		return data;
	}

    #region SetEditors

	private RepositoryItem SetEditor_IncType(string id)
    {
		return CreateEditorDropDown<StatiInclusionType>();
    }

	private RepositoryItem SetEditor_SplashType(string id)
    {
		return CreateEditorDropDown<SplashType>();
    }

	private RepositoryItem SetEditor_Preference(string id)
    {
		return CreateEditorDropDown<AimPreference>();
    }

	private RepositoryItem SetEditor_Targets(string id)
    {
		return CreateEditorDropDown<MarkType>();
    }

	private RepositoryItem SetEditor_Depth(string id)
    {
		return CreateEditorTextEdit(DataEditorType.Integer);
    }

    private RepositoryItem SetEditor_Stati(string id)
	{
		return CreateEditorCheckedComboBoxEdit(() =>
		{
			List<UnitStatus> statusList = new List<UnitStatus>((UnitStatus[])Enum.GetValues(typeof(UnitStatus)));
			Dictionary<string, bool> toReturn = new Dictionary<string, bool>();
			//if(!GetAtomicIndices(id, out string type, out int atomicId))
			//{
			//    return toReturn;
			//}
			HashSet<UnitStatus> stati = DataManager.Inst.GetXowerMarkStati(id, _xowerType);
			foreach (UnitStatus status in statusList)
			{
				toReturn.Add(status.ToString(), stati.Contains(status));
			}
			return toReturn;
		});
	}

	#endregion set editors
	#region validators

	private bool Validator_MarkType(string id, object value)
    {
		MarkType type = GetEnum<MarkType>(value);
		return DataManager.Inst.SetXowerMarkType(id, _xowerType, type);
    }

	private bool Validator_Preference(string id, object value)
    {
		AimPreference pref = GetEnum<AimPreference>(value);
		return DataManager.Inst.SetXowerAimPreference(id, _xowerType, pref);
    }

	private bool Validator_Splash(string id, object value)
    {
		SplashType splash = GetEnum<SplashType>(value);
		return DataManager.Inst.SetXowerSplashType(id, _xowerType, splash);
    }

	private bool Validator_InclusionType(string id, object value)
    {
		StatiInclusionType incType = GetEnum<StatiInclusionType>(value);
		return DataManager.Inst.SetXowerMarkStatiInclusionType(id, _xowerType, incType);
    }

	private bool Validator_TargetDepth(string id, object value)
    {
		int depth = (int)value;
		//nyeah. Don't deal with this for now.
		return false;// DataManager.Inst.SetXowerTargetingDepth(id, _asPower, depth);
    }

	private bool Validator_Stati(string id, object value)
    {
		HashSet<UnitStatus> stati = GetEnumList<UnitStatus>(value);
		return DataManager.Inst.SetXowerMarkStati(id, _xowerType, stati);
    }

	#endregion

	private void SelectActionCard(string id, SunTree sendingTree)
    {
		if(sendingTree == this) { return; }
		if(!_nodes.ContainsKey(id)) { return; }
		ClearSelection();
		SelectNode(_nodes[id]);
    }

	private void HandleToggle(bool showPowers)
    {
		_xowerType = showPowers? XowerType.Power: XowerType.Tower;
		RebuildActionCards();
    }

	private void HandleLevelChange(int level)
    {
		_level = level;
		RebuildActionCards();
    }

	private void RebuildActionCards()
    {
		foreach(string id in _nodes.Keys)
        {
			RefreshActionCard(id);
        }
    }

	private void RefreshActionCard(string id)
	{
		object[] data = GetData(id);
		if (!_nodes.ContainsKey(id))
		{
			TreeListNode node = AppendNode(data, null,id);
			_nodes.Add(id, node);
		}
		else
		{
			for (int i = 0; i < data.Length; i++)
			{
				_nodes[id].SetValue(i, data[i]);
			}
		}
	}

	protected override string GetTreeName() { return "XowerTargetingTree"; }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate() 
	{
		return DataManager.Inst.GetIds<ActionCardType>();
	}
	protected override void InitialPopulateAction(string id)
	{
		TreeListNode node = AppendNode(GetData(id), null, id);
		_nodes.Add(id, node);
	}

}