using System;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TowerForge
{
    public partial class MainWindow : DevExpress.XtraBars.TabForm
    {
        //private XowerDetailControl _xowerDetail;
        private bool _saved;

        public MainWindow()
        {
            AsyncInit(PostServerInit);
        }

        private async Task AsyncInit(Action onComplete)
        {

            TRDataUtils.Path = "C:\\Work\\Projects\\tower-rush-game-data";
            FirebaseManager.Inst.Init();
            await FirebaseManager.Inst.FetchDataAsync();
            await DataManager.Inst.Initialize();
            onComplete?.Invoke();
        }

        public void PostServerInit()
        {
            EnumTest();
            InitializeComponent();
            PopulateTrees();
            SetupTrackBars();
            l_currentStream.Text = "---";//FirebaseManager.Inst.Manifest;
            //SetupDetail();

            Signals.XowerLevelChanged.Send(1);
        }

        private void EnumTest()
        {

        }

        private void PopulateTrees()
        {
            //Signals.ActionCardSelected.Send(DataManager.Inst.GetStringId<ActionCardType>(ActionCardType.Frost),null);
            Signals.LargeOperationBegin.Send("Initialize");
            tl_xowerEffects.PopulateTree(null, ListExpand.All);
            tl_powerEffects.PopulateTree(null, ListExpand.All);
            tl_runnerPowerEffects.PopulateTree(null, ListExpand.All);
            tl_runnerUnitDetails.PopulateTree(null, ListExpand.All);
            tl_runnerUnits.PopulateTree(null, ListExpand.All);
            //tl_towersTree.PopulateTree(null, ListExpand.All);
            tl_runnerCardRoster.PopulateTree(null, ListExpand.All);
            tl_runnerCards.PopulateTree(null, ListExpand.All);
            //tl_xowerTargets.PopulateTree(null, ListExpand.All);
            //tl_xowerAttack.PopulateTree(null, ListExpand.All);
            //tl_xowerUtility.PopulateTree(null, ListExpand.All);
            tl_xowerDetail.PopulateTree(null, ListExpand.All);
            tl_powerDetails.PopulateTree(null, ListExpand.All);
            tl_runnerPowerDetails.PopulateTree(null, ListExpand.All);
            tl_chestList.PopulateTree(null, ListExpand.All); 
            tl_rarityTree.PopulateTree(null, ListExpand.All);
            tl_arenaCards.PopulateTree(null, ListExpand.All);
            tl_userStats.PopulateTree(null, ListExpand.All);
            tl_userDecks.PopulateTree(null, ListExpand.All);
            tl_actionCards.PopulateTree(null, ListExpand.All);
            tl_roundsTree.PopulateTree(null, ListExpand.All);
            tl_runnerPowers.PopulateTree(null, ListExpand.All);
            tl_stati.PopulateTree(null, ListExpand.All);
            tl_dailyDeals.PopulateTree(null, ListExpand.All);
            tl_simpleDeals.PopulateTree(null, ListExpand.All);
            tl_aiValues.PopulateTree(null, ListExpand.All);

            tl_runnerPowers.RegisterValidDrop(tl_actionCards, tl_actionCards.DropAction, true, "Change to Action Card");
            tl_actionCards.RegisterValidDrop(tl_runnerPowers, tl_runnerPowers.DropAction, true, "Change to Runner Power");
            tl_runnerUnits.RegisterValidDrop(tl_runnerCardRoster, tl_runnerCardRoster.HandleUnitDrops, false, "Add cards");

            Signals.LargeOperationEnd.Send("Initialize");
        }

        private void SetupTrackBars()
        {
            tb_runnerRank.Properties.Minimum = 0;
            tb_runnerRank.Properties.Maximum = 10;

            tb_runnerLevel.Properties.Minimum = 0;
            tb_runnerLevel.Properties.Maximum = FirebaseManager.Inst.GetMaxLevel();
        }

        private void xowerSwitch_Toggled(object sender, EventArgs e)
        {
            Signals.ShowPowersNotTowers.Send(xowerSwitch.IsOn);

        }

        private void tb_runnerLevel_EditValueChanged(object sender, EventArgs e)
        {
            Signals.RunnerLevelChanged.Send((int)tb_runnerLevel.EditValue);
        }
        private void tb_runnerRank_EditValueChanged(object sender, EventArgs e)
        {
            int rank = (int)tb_runnerRank.EditValue;
            Signals.RunnerRankChanged.Send(rank);
        }

        private void tb_actionCardLevel_EditValueChanged(object sender, EventArgs e)
        {
            int level = (int)tb_actionCardLevel.EditValue;
            Signals.XowerLevelChanged.Send(level);
        }

        private void b_createNewAction_Click(object sender, EventArgs e)
        {
            Debug.LogError("ActionCards must be created through code!");
            //DataManager.Inst.NewActionCard((string)tb_newActionType.EditValue);
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Control && e.KeyCode == Keys.S && !_saved)
            {
                _saved = true;
                Debug.LogWarning("Save!");
                DataManager.Inst.Save();
                FirebaseManager.Inst.Save();
            }
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.S)
            {
                _saved = false;
            }
        }

        private void tabFormContentContainer1_Click(object sender, EventArgs e)
        {

        }

        private void tabFormControl1_OuterFormCreated(object sender, DevExpress.XtraBars.OuterFormCreatedEventArgs e)
        {
            e.Form.CloseBox = false;
            e.Form.TabFormControl.ShowTabCloseButtons = false;
            e.Form.TabFormControl.ShowAddPageButton = false;
            e.Form.Icon = this.Icon;
            e.Form.Text = "(child) " + this.Text;
            e.Form.Size = this.Size;
        }

        private void b_createNewRunnerCard_Click(object sender, EventArgs e)
        {
            Debug.LogError("RunnerCards must be created through code!");
            //DataManager.Inst.NewRunnerCard(tb_newRunnerCard.Text);
        }

        private void b_createNewRunnerUnit_Click(object sender, EventArgs e)
        {
            Debug.LogError("RunnerUnits must be created through code!");
            //DataManager.Inst.NewRunnerUnit(tb_newRunnerUnit.Text);
        }

        private void b_changeStream_Click(object sender, EventArgs e)
        {
            Debug.LogError("Manifests aren't a thing anymore!");
         //   ChooseManifestForm cmf = new ChooseManifestForm();
         //   cmf.ShowDialog();
         //   if(!string.IsNullOrEmpty(cmf.Manifest))
         //   {
         //       l_currentStream.Text = cmf.Manifest;
         //       FirebaseManager.Inst.Manifest = cmf.Manifest;
         //       DataManager.Inst.SaveActiveManifest(cmf.Manifest);
         //   }
        }
    }
}