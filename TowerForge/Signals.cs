using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine;


public partial class Signals
{
    // Signals related to SunCore go here. Implementing projects can declare their own partial Signals class.
    public static Signal<string, string> ExampleSignal = new Signal<string, string>(nameof(ExampleSignal));

    public static Signal InitializationComplete = new Signal(nameof(InitializationComplete));
    
    // I'm putting signals in here for now for rapid prototyping reasons.

    public static Signal<string> LargeOperationBegin = new Signal<string>(nameof(LargeOperationBegin));
    public static Signal<string>  LargeOperationEnd = new Signal<string>(nameof(LargeOperationEnd));
    public static Signal ClearEverything = new Signal(nameof(ClearEverything));
    public static Signal<List<SunTree>> DisableSunTreesExcept = new Signal<List<SunTree>>(nameof(DisableSunTreesExcept));
    public static Signal EnableAllSunTrees = new Signal(nameof(EnableAllSunTrees));
    public static Signal<string> DebugMessage = new Signal<string>(nameof(DebugMessage));
    public static Signal<string> RunnerUpdate = new Signal<string>(nameof(RunnerUpdate));
    public static Signal<bool> ShowPowersNotTowers = new Signal<bool>(nameof(ShowPowersNotTowers));
    public static Signal<string> RunnerCardSelected = new Signal<string>(nameof(RunnerCardSelected));
    public static Signal<int> RankSelected = new Signal<int>(nameof(RankSelected));
    public static Signal<string> RunnerUnitSelected = new Signal<string>(nameof(RunnerUnitSelected));
    public static Signal<int> RunnerLevelChanged = new Signal<int>(nameof(RunnerLevelChanged));
    public static Signal<int> XowerLevelChanged = new Signal<int>(nameof(XowerLevelChanged));
    public static Signal<int> RunnerRankChanged = new Signal<int>(nameof(RunnerRankChanged));
    public static Signal<string, SunTree> ActionCardSelected = new Signal<string, SunTree>(nameof(ActionCardSelected));
    public static Signal<string> RunnerCardUpdated = new Signal<string>(nameof(RunnerCardUpdated));
    public static Signal<string> RunnerUnitUpdated = new Signal<string>(nameof(RunnerUnitUpdated));
    public static Signal<string> ActionCardUpdated = new Signal<string>(nameof(ActionCardUpdated));
    //public static Signal<string, int> RunnerAbilitySelected = new Signal<string, int>(nameof(RunnerAbilitySelected));
    public static Signal<string,int,int> RunnerAbilityEffectSelected = new Signal<string, int, int> (nameof(RunnerAbilityEffectSelected));
    public static Signal LibraryUpdated = new Signal(nameof(LibraryUpdated));
    public static Signal Save = new Signal(nameof(Save));
    public static Signal RoundsDataChanged = new Signal(nameof(RoundsDataChanged));
    public static Signal<string> ActionCardChangedLists = new Signal<string>(nameof(ActionCardChangedLists));
    public static Signal<int> RefreshArenaDailyDeals = new Signal<int>(nameof(RefreshArenaDailyDeals));
    public static Signal<string> AIValuesUpdated = new Signal<string>(nameof(AIValuesUpdated));
}

public delegate void Callback();
public delegate void Callback<T>(T arg1);
public delegate void Callback<T, U>(T arg1, U arg2);
public delegate void Callback<T, U, V>(T arg1, U arg2, V arg3); 

public abstract class SignalEngine
{
    protected static Dictionary<string, Delegate> s_events = new Dictionary<string, Delegate>();

    // internal constructor is so that classes outside SunCore can't inherit SignalEngine.
    internal SignalEngine() { }
    
    protected static bool CheckAddDelegate(string key, Delegate listener)
    {
        if (string.IsNullOrEmpty(key))
        {
            Debug.LogError("Error - Tried to use null or empty key to add a signal listener!");
            return false;
        }
        if (!s_events.ContainsKey(key)) { s_events.Add(key, null); }

        if (listener == null)
        {
            Debug.LogWarning($"Error - tried to add a null listener to signal {key}");
            return false;
        }

        Delegate del = s_events[key];
        // In the old signalmanager we had an option to allow duplicates by passing "safe = false". That's dumb.
        // If you really want that behavior, wrap your target listener in another bogus function, and go eat slop
        // with the pigs like the disgusting animal that you are.
        if (del != null)
        {
            foreach (Delegate eventHandler in del.GetInvocationList())
            {
                if (listener == eventHandler)
                {
                    Debug.LogWarning($"Error - tried to register listener {listener.Method.Name} twice with signal {key}");
                    return false;
                }
            }
        }

        // We don't have to validate signature types with this new method, since the signature is bound to the class
        // and the class name is guaranteed to be unique.
        return true;
    }

    protected static bool CheckRemoveDelegate(string key, Delegate listener)
    {
        if (string.IsNullOrEmpty(key))
        {
            Debug.LogError("Error - Tried to use null or empty key to remove a signal listener!");
            return false;
        }
        if (!s_events.ContainsKey(key)) { s_events.Add(key, null); }

        if (listener == null)
        {
            Debug.LogWarning($"Error - tried to remove a null listener to signal {key}");
            return false;
        }

        return true;
    }
}

public class Signal : SignalEngine
{
    private string _key;
    public Signal(string key)
    {
        _key = key;
    }
    public void Send()
    {
        if (string.IsNullOrEmpty(_key))
        {
            Debug.LogError("Error - Tried to use null or empty key to send a signal!");
        }
        if (!s_events.ContainsKey(_key)) { s_events.Add(_key, null); }
        
        Callback callback = (Callback)s_events[_key];
        if (callback == null)
        {
            Debug.LogWarning($"Warning - Sent signal {_key} but no listeners were listening!");
            return;
        }

        callback();
    }

    public void Register(Callback listener)
    {
        if (CheckAddDelegate(_key, listener))
        {
            s_events[_key] = (Callback)s_events[_key] + listener;
        }
    }

    public void Unregister(Callback listener)
    {
        if (CheckRemoveDelegate(_key, listener))
        {
            s_events[_key] = (Callback)s_events[_key] - listener;
        }
    }
}

public class Signal<T> : SignalEngine
{
    private string _key;
    public Signal(string key)
    {
        _key = key;
    }
    public void Send(T arg1)
    {
        if (string.IsNullOrEmpty(_key))
        {
            Debug.LogError("Error - Tried to use null or empty key to send a signal!");
        }
        if (!s_events.ContainsKey(_key)) { s_events.Add(_key, null); }
        
        Callback<T> callback = (Callback<T>)s_events[_key];
        if (callback == null)
        {
            Debug.LogWarning($"Warning - Sent signal {_key} but no listeners were listening!");
            return;
        }

        callback(arg1);
    }

    public void Register(Callback<T> listener) 
    {
        if (CheckAddDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T>)s_events[_key] + listener;
        }
    }

    public void Unregister(Callback<T> listener) 
    {
        if (CheckRemoveDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T>)s_events[_key] - listener;
        }
    }
}

public class Signal<T,U> : SignalEngine
{
    private string _key;
    public Signal(string key)
    {
        _key = key;
    }
    public void Send(T arg1, U arg2) 
    {
        if (string.IsNullOrEmpty(_key))
        {
            Debug.LogError("Error - Tried to use null or empty key to send a signal!");
        }
        if (!s_events.ContainsKey(_key)) { s_events.Add(_key, null); } 
        
        Callback<T,U> callback = (Callback<T,U>)s_events[_key];
        if (callback == null)
        {
            Debug.LogWarning($"Warning - Sent signal {_key} but no listeners were listening!");
            return;
        }

        callback(arg1,arg2);
    }

    public void Register(Callback<T,U> listener)
    {
        if (CheckAddDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T,U>)s_events[_key] + listener;
        }
    }

    public void Unregister(Callback<T,U> listener)
    {
        if (CheckRemoveDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T,U>)s_events[_key] - listener;
        }
    }
}

public class Signal<T,U,V> : SignalEngine
{
    private string _key;
    public Signal(string key)
    {
        _key = key;
    }
    public void Send(T arg1, U arg2, V arg3)
    {
        if (string.IsNullOrEmpty(_key))
        {
            Debug.LogError("Error - Tried to use null or empty key to send a signal!");
        }
        if (!s_events.ContainsKey(_key)) { s_events.Add(_key, null); }
        
        Callback<T,U,V> callback = (Callback<T,U,V>)s_events[_key];
        if (callback == null)
        {
            Debug.LogWarning($"Warning - Sent signal {_key} but no listeners were listening!");
            return;
        }

        callback(arg1,arg2,arg3);
    }

    public void Register(Callback<T,U,V> listener) 
    {
        if (CheckAddDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T,U,V>)s_events[_key] + listener;
        }
    }

    public void Unregister(Callback<T,U,V> listener)
    {
        if (CheckRemoveDelegate(_key, listener))
        {
            s_events[_key] = (Callback<T,U,V>)s_events[_key] - listener;
        }
    }
}
