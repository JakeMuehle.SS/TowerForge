using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class UnitsTree : SunTree
{
    private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
    private HashSet<string> _units = new HashSet<string>();
    private int _level = 0;
    // this is an int because the game will handle it as an int, and we don't want duplicate calls.
    private int _rank = 0;
    public UnitsTree() : base()
    {
        AddColumn("UnitName", 100, 100);
        AddColumn("HealthBase", 80, 80);
        AddColumn("HealthFinal", 80, 80);
        AddColumn("SpeedBase", 80, 80);
        AddColumn("SpeedFinal", 80, 80);
        AddColumn("Prefab", 150, 150,Editor_Prefab,Validator_Prefab,cellPipFunc:IsDefaultPip);
        AddColumn("AbilityCt",75,75);
        AddColumn("Threat",60,60);

        OnNodeSelect += (id) =>
        {
            Signals.RunnerUnitSelected.Send(id);
        };

        Signals.RunnerRankChanged.Register(HandleRankChange);
        Signals.RunnerLevelChanged.Register(HandleLevelChange);
        Signals.RunnerUnitUpdated.Register(RefreshUnit);
    }
    private object[] GetData(string id)
    {
        object[] data = new object[]
        {
            DataManager.Inst.DisplayName(id),
            DataManager.Inst.GetRunnerHealth(id,_level,0),
            DataManager.Inst.GetRunnerHealth(id,_level,DataManager.MAX_RANK_INDEX),
            DataManager.Inst.GetRunnerSpeed(id,_level,0),
            DataManager.Inst.GetRunnerSpeed(id,_level,DataManager.MAX_RANK_INDEX),
            DataManager.Inst.GetRunnerPrefab(id),
            DataManager.Inst.GetRunnerAbilityCount(id),
            DataManager.Inst.GetRunnerThreat(id,_level,_rank)
        };
        return data;
    }

    private RepositoryItem Editor_Prefab(string id)
    {
        if (!_units.Contains(id)) { return null; }
        return CreateEditorTextEdit();
    }

    private bool Validator_Prefab(string id, object value)
    {
        if(!_units.Contains(id)) { return false; }
        return DataManager.Inst.SetRunnerPrefab(id, (string)value);
    }

    private CellPip IsDefaultPip(string id, object value)
    {
        if (!_units.Contains(id)) { return new CellPip(false); }
        if(DataManager.Inst.IsRunnerDefaultPrefab(id))
        {
            return new SunTree.CellPip(3, System.Drawing.Color.DarkRed);
        }
        return new CellPip(false);
    }

    private void HandleLevelChange(int level)
    {
        _level = level;
        RefreshNodes();
    }

    private void HandleRankChange(int rank)
    {
        _rank = rank;
        RefreshNodes();
    }

    private void RefreshNodes()
    {
        foreach(string unit in _units)
        {
            RefreshUnit(unit);
        }
    }

    private void RefreshUnit(string id)
    {
        object[] data = GetData(id);
        if (!_nodes.ContainsKey(id))
        {
            // add unit
            TreeListNode node = AppendNode(data, null, id);
            _nodes.Add(id, node);
            _units.Add(id);
        }
        else
        {
            for (int i = 0; i < data.Length; i++)
            {
                _nodes[id].SetValue(i, data[i]);
            }
        }
    }

    protected override string GetTreeName()	{ return "UnitsTree"; }
    protected override List<string> ContextMenuItems(string id, bool multiSelect) 
    {
        List<string> menu = new List<string>();
        if(string.IsNullOrEmpty(id)) { return menu; }
        return menu; 
    }
    protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
    protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
    protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
    protected override void RuntimeInit() { }
    protected override void ClearEverything() { }
    protected override List<string> GetIDsToPopulate()
    {
        return DataManager.Inst.GetIds<RunnerUnitType>();
    }

    protected override void InitialPopulateAction(string id)
    {
        TreeListNode node = AppendNode(GetData(id), null, id);
        _nodes.Add(id, node);
        _units.Add(id);
    }
}