﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
public class AIValueTree : SunTree
{
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();

	private const string RUNNERS = "Runners";
	private const string TOWERS = "Towers";

	private HashSet<string> _runners = new HashSet<string>();
	private HashSet<string> _towers = new HashSet<string>();
	private HashSet<string> _headers = new HashSet<string>() { RUNNERS, TOWERS };

	private Dictionary<int, AIPropertyType> _propertyMap = new Dictionary<int, AIPropertyType>();
	public AIValueTree() : base()
	{
		AddColumn("ID", 100, 100);

		Signals.AIValuesUpdated.Register(AIValuesUpdated);
	}

	private void AIValuesUpdated(string id)
    {
		if(!_nodes.ContainsKey(id))
        {
			return;
        }

		object[] data = new object[0];
		if (DataManager.Inst.TryGetEnumFromId<RunnerCardType>(id, out RunnerCardType rtype))
		{
			data = GetRunnerData(rtype);
		}
		if (DataManager.Inst.TryGetEnumFromId<ActionCardType>(id, out ActionCardType atype))
		{

			data = GetTowerData(atype);
		}

		for(int i = 0; i < data.Length; i++)
        {
			_nodes[id].SetValue(i, data[i]);
        }

	}

	private object[] GetTowerData(ActionCardType type)
    {
		int preExistingColumns = 1;
		object[] data = new object[preExistingColumns + DataManager.Inst.GetAIValues().Count];
		data[0] = DataManager.Inst.DisplayName(DataManager.Inst.GetStringId<ActionCardType>(type));
		for (int i = preExistingColumns; i < data.Length; i++)
		{
			data[i] = DataManager.Inst.GetAIValueForCard(type, _propertyMap[i]);
		}
		return data;
	}

	private object[] GetRunnerData(RunnerCardType type)
    {
		int preExistingColumns = 1;
		object[] data = new object[preExistingColumns + DataManager.Inst.GetAIValues().Count];
		data[0] = DataManager.Inst.DisplayName(DataManager.Inst.GetStringId<RunnerCardType>(type));
		for (int i = preExistingColumns; i < data.Length; i++)
		{
			data[i] = DataManager.Inst.GetAIValueForCard(type, _propertyMap[i]);
		}
		return data;
	}
	protected override string GetTreeName() { return nameof(AIValueTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }
	protected override void RuntimeInit() { }
	protected override void ClearEverything() { }

    protected override void PrePopulate()
    {
        foreach(AIPropertyType type in DataManager.Inst.GetAIValues())
        {
			int col = AddColumn(type.ToString(), 100, 100, (id) =>
            {
				if(_towers.Contains(id) || _runners.Contains(id))
                {
					return CreateEditorTextEdit(DataEditorType.Float);
                }
				return null;
            }, (id, value) =>
            {
				if (_towers.Contains(id))
                {
					DataManager.Inst.TryGetEnumFromId<ActionCardType>(id, out ActionCardType atype);
					return DataManager.Inst.SetAIValueForCard(atype, type, (float)value);
                }
				if(_runners.Contains(id))
				{
					DataManager.Inst.TryGetEnumFromId<RunnerCardType>(id, out RunnerCardType rtype);
					return DataManager.Inst.SetAIValueForCard(rtype, type, (float)value);
				}
				return true;
            }).ColumnIndex;
			_propertyMap.Add(col,type);
        }
		TreeListNode runners = AppendNode(new object[] { RUNNERS }, GetRoot(), RUNNERS);
		_nodes.Add(RUNNERS, runners);
		TreeListNode towers = AppendNode(new object[] { TOWERS }, GetRoot(), TOWERS);
		_nodes.Add(TOWERS, towers);
    }
    protected override List<string> GetIDsToPopulate() 
	{
		List<string> ids = new List<string>();
		foreach(string id in DataManager.Inst.GetIds<ActionCardType>())
        {
			ids.Add(id);
        }
		foreach(string id in DataManager.Inst.GetIds<RunnerCardType>())
        {
			ids.Add(id);
        }
		return ids;

	}

    protected override bool IsHeader(string id)
    {
		return _headers.Contains(id);
    }

    protected override void InitialPopulateAction(string id)
	{
		if (DataManager.Inst.TryGetEnumFromId<RunnerCardType>(id, out RunnerCardType rtype))
        {
			TreeListNode rnode = AppendNode(GetRunnerData(rtype), _nodes[RUNNERS], id);
			_nodes.Add(id, rnode);
            _runners.Add(id);
			return;
        }
        if (DataManager.Inst.TryGetEnumFromId<ActionCardType>(id, out ActionCardType atype))
        {
			TreeListNode anode = AppendNode(GetTowerData(atype), _nodes[TOWERS], id);
			_nodes.Add(id, anode);
			_towers.Add(id);
			return;
        }
		Debug.LogError("HEY! This thing you tried to populate is neither a runner NOR a tower!!");
	}

}