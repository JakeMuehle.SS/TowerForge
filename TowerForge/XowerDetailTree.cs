﻿using System.Collections.Generic;
using DevExpress.Utils.DragDrop;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using System;
using System.Reflection;
using System.Linq;
using TowerForge.Properties;

public class PowerDetailTree : XowerDetailTree
{
	public PowerDetailTree() : base()
	{
		_xowerType = XowerType.Power;
	}
}

public class TowerDetailTree : XowerDetailTree
{
	public TowerDetailTree() : base()
	{
		_xowerType = XowerType.Tower;
	}
}

public class RunnerPowerDetailTree : XowerDetailTree
{
	public RunnerPowerDetailTree() : base()
	{
		_xowerType = XowerType.RunnerPower;
	}
}

public abstract class XowerDetailTree : SunTree
{
	private const string RATE = "Rate";
	private const string SUBINTERVAL = "SubInterval";
	private const string BURST_COUNT = "BurstCount";
	private const string BUILD_TIME = "BuildTime";
	private const string PROJECTILE_SPEED = "P Speed";
	private const string PROJECTILE_LOFT = "P Loft";
	private const string PROJECTILE_SCALE = "P Scale";
	private const string AIM_TYPE = "AimType";
	private const string AIM_PREFERENCE = "AimPreference";
	private const string MARK_STATI = "MarkStati";
	private const string STATI_INCLUSION_TYPE = "InclusionType";
	private const string MARK_TYPE = "MarkType";
	private const string PRIMARY_AIM_AREA = "PrimaryAimArea";
	private const string SPLASH_DISTANCE = "SplashDistance";
	private const string SPLASH_WIDTH = "SplashWidth";
	private const string SPLASH_TYPE = "SplashType";
	private const string DEPTH = "Depth";
	private const string COST = "Cost";
	private const string DURATION = "Duration";
	private const string UPGRADE_COST = "Upgrade Cost";
	private const string COSMETIC_EXTENSION = "CosmeticDurationExtension";
	private const string TOWER_FOOTPRINT = "Footprint";

	private const string CAT_AIM = "Aiming";
	private const string CAT_SPLASH = "Splash";
	private const string CAT_GENERAL = "General";
	private const string CAT_HITTING = "Hitting";
	private const string CAT_PROJECTILE = "Projectile";
	private const string CAT_TOWER = "z Tower Specific";
	private const string CAT_POWER = "z Power Specific";

	private int COL_VALUE;
	private int COL_FINAL;
	private int COL_SCALAR;
	private int COL_RANDCAP;
	private int COL_CURVE;

	private string _cardID = DataManager.Inst.GetStringId<ActionCardType>(ActionCardType.Fake_DeerPower);
	protected XowerType _xowerType = XowerType.None;
	private Dictionary<string, TreeListNode> _nodes = new Dictionary<string, TreeListNode>();
	/// <summary> Key : "Is it AimData?" </summary>
	private Dictionary<string,bool> _rankableFloats = new Dictionary<string,bool>();
	private HashSet<string> _ints = new HashSet<string>();
	private HashSet<string> _projectileFloats = new HashSet<string>();

	

	/// <summary> Key : enum type </summary>
	private Dictionary<string, Type> _enums = new Dictionary<string, Type>();
	/// <summary> Key : enum type </summary>
	private Dictionary<string, Type> _multiEnums = new Dictionary<string, Type>();
	/// <summary> item : category </summary>
	private Dictionary<string, string> _categories = new Dictionary<string, string>();

	private Dictionary<string, CellFunctions> _cellFuncs = new Dictionary<string,CellFunctions>();

	private Func<string, RankableFloat> _getDataRF;
	private Func<string, RankableFloat, bool> _setDataRF;
	private Func<HashSet<UnitStatus>> _getMarkStati;

	public XowerDetailTree() : base()
    {
		
		SetImages(new List<System.Drawing.Bitmap>() { Resources.collapse_16x16 });

        AddColumn("Property", 100, 100, cellPipFunc:LevelingTypePip);
		COL_VALUE = AddColumn("Value",      100,100,PrimaryEditor,PrimaryValidator).ColumnIndex;
		COL_FINAL = AddColumn("FinalValue", 80,80,FinalValueEditor,FinalValidator).ColumnIndex;
		COL_SCALAR = AddColumn("Scalar",     60,60,ScalarEditor,ScalarValidator).ColumnIndex;
		COL_RANDCAP =  AddColumn("RandomCap", 80, 80, RandomCapEditor, RandomCapValidator).ColumnIndex;
		COL_CURVE = AddColumn("CurveType",  80,80,CurveEditor,CurveValidator).ColumnIndex;

		_rankableFloats.Add(RATE, false);
		_rankableFloats.Add(SUBINTERVAL, false);
		_rankableFloats.Add(BUILD_TIME, false);
		_rankableFloats.Add(PRIMARY_AIM_AREA, true);
		_rankableFloats.Add(SPLASH_DISTANCE, true);
		_rankableFloats.Add(SPLASH_WIDTH, true);
		_rankableFloats.Add(DEPTH, true);
		// tower/power specific stuff:
		_rankableFloats.Add(COSMETIC_EXTENSION, false);
		_rankableFloats.Add(DURATION, false);

		_ints.Add(BURST_COUNT);
		_ints.Add(COST);
		_ints.Add(UPGRADE_COST);

		_projectileFloats.Add(PROJECTILE_SPEED);
		_projectileFloats.Add(PROJECTILE_LOFT);
		_projectileFloats.Add(PROJECTILE_SCALE);

		_enums.Add(AIM_TYPE, typeof(AimingType));
		_enums.Add(AIM_PREFERENCE, typeof(AimPreference));
		_enums.Add(STATI_INCLUSION_TYPE, typeof(StatiInclusionType));
		_enums.Add(MARK_TYPE, typeof(MarkType));
		_enums.Add(SPLASH_TYPE, typeof(SplashType));
		_enums.Add(TOWER_FOOTPRINT, typeof(TowerFootprint));

		_multiEnums.Add(MARK_STATI, typeof(UnitStatus));

		_categories.Add(RATE, CAT_HITTING);
		_categories.Add(SUBINTERVAL, CAT_HITTING);
		_categories.Add(BUILD_TIME, CAT_GENERAL);
		_categories.Add(SPLASH_DISTANCE, CAT_SPLASH);
		_categories.Add(SPLASH_WIDTH, CAT_SPLASH);
		_categories.Add(BURST_COUNT, CAT_HITTING);
		_categories.Add(DEPTH, CAT_HITTING);
		_categories.Add(AIM_TYPE, CAT_AIM);
		_categories.Add(AIM_PREFERENCE, CAT_AIM);
		_categories.Add(STATI_INCLUSION_TYPE, CAT_AIM);
		_categories.Add(MARK_TYPE, CAT_AIM);
		_categories.Add(PRIMARY_AIM_AREA, CAT_AIM);
		_categories.Add(SPLASH_TYPE, CAT_SPLASH);
		_categories.Add(MARK_STATI, CAT_AIM);
		_categories.Add(COST, CAT_GENERAL);
		_categories.Add(UPGRADE_COST, CAT_TOWER);
		_categories.Add(DURATION, CAT_POWER);
		_categories.Add(COSMETIC_EXTENSION, CAT_POWER);
		_categories.Add(PROJECTILE_SPEED, CAT_PROJECTILE);
		_categories.Add(PROJECTILE_LOFT, CAT_PROJECTILE);
		_categories.Add(PROJECTILE_SCALE, CAT_PROJECTILE);
		_categories.Add(TOWER_FOOTPRINT, CAT_TOWER);


	}

	private void SetActionCardFuncs()
	{
		Signals.ActionCardSelected.Register(SetCard);
		Signals.ActionCardUpdated.Register(UpdateCard);

		_getDataRF = (id) => { return DataManager.Inst.GetXowerRankableFloat(_cardID, _xowerType, _rankableFloats[id], id); };
		_setDataRF = (propertyName, rf) => { return DataManager.Inst.SetXowerRankableFloat(_cardID, _xowerType, propertyName, _rankableFloats[propertyName], rf); };
		_getMarkStati = () => { return DataManager.Inst.GetXowerMarkStati(_cardID, _xowerType); };
		_cellFuncs.Add(BURST_COUNT, new CellFunctions(
			() => { return DataManager.Inst.GetXowerBurstCount(_cardID, _xowerType); },
			(value) => { return DataManager.Inst.SetXowerBurstCount(_cardID, _xowerType, (int)value); }));
		_cellFuncs.Add(AIM_TYPE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerAimType(_cardID, _xowerType); },
			(value) => { return DataManager.Inst.SetXowerAimType(_cardID, _xowerType, GetEnum<AimingType>(value)); }));
		_cellFuncs.Add(AIM_PREFERENCE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerAimPreference(_cardID, _xowerType); },
			(value) => { return DataManager.Inst.SetXowerAimPreference(_cardID, _xowerType, GetEnum<AimPreference>(value)); }));
		_cellFuncs.Add(STATI_INCLUSION_TYPE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerMarkStatiInclusionType(_cardID, _xowerType); },
			(value) => { return DataManager.Inst.SetXowerMarkStatiInclusionType(_cardID, _xowerType, GetEnum<StatiInclusionType>(value));	}));
		_cellFuncs.Add(MARK_TYPE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerMarkType(_cardID, _xowerType); },
			(value) => { return DataManager.Inst.SetXowerMarkType(_cardID, _xowerType, GetEnum<MarkType>(value));	}));
		_cellFuncs.Add(SPLASH_TYPE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerSplashType(_cardID, _xowerType); }, 
			(value) => { return DataManager.Inst.SetXowerSplashType(_cardID, _xowerType, GetEnum<SplashType>(value)); }));
		_cellFuncs.Add(COST, new CellFunctions(
			() => {  return DataManager.Inst.GetActionCardCost(_cardID); },
			(value) => { return DataManager.Inst.SetActionCardCost(_cardID, (int)value); }));
		_cellFuncs.Add(UPGRADE_COST, new CellFunctions(
			() => { return DataManager.Inst.GetTowerUpgradeCost(_cardID); }, 
			(value) => { return DataManager.Inst.SetTowerUpgradeCost(_cardID, (int)value); }));
		_cellFuncs.Add(TOWER_FOOTPRINT, new CellFunctions(
			() => { return DataManager.Inst.GetTowerFootprint(_cardID); }, 
			(value) => { return DataManager.Inst.SetTowerFootprint(_cardID, GetEnum<TowerFootprint>(value)); }));
		_cellFuncs.Add(MARK_STATI, new CellFunctions(
			() => { return Utils.ListString<UnitStatus>(DataManager.Inst.GetXowerMarkStati(_cardID, _xowerType)); }, 
			(value) => { return DataManager.Inst.SetXowerMarkStati(_cardID, _xowerType, GetEnumList<UnitStatus>(value)); }));
		_cellFuncs.Add(PROJECTILE_LOFT, new CellFunctions(
			() => { return DataManager.Inst.GetXowerProjectileData(_cardID, _xowerType).Loft; }, 
			(value) => { return DataManager.Inst.SetXowerProjectileData(_cardID, _xowerType, GetProjectileData(loft:(float)value)); }));
		_cellFuncs.Add(PROJECTILE_SCALE, new CellFunctions(
			() => { return DataManager.Inst.GetXowerProjectileData(_cardID, _xowerType).Scale; },
			(value) => { return DataManager.Inst.SetXowerProjectileData(_cardID, _xowerType, GetProjectileData(scale: (float)value)); }));
		_cellFuncs.Add(PROJECTILE_SPEED, new CellFunctions(
			() => { return DataManager.Inst.GetXowerProjectileData(_cardID, _xowerType).Speed; },
			(value) => { return DataManager.Inst.SetXowerProjectileData(_cardID, _xowerType, GetProjectileData(speed: (float)value)); }));
	}
	protected override bool UseNodeImage(string id, out int index)
    {
		index = -1;
		return false;
    }

	private CellPip LevelingTypePip(string id, object _)
    {
		if(!_rankableFloats.ContainsKey(id))
        {
			return new CellPip(0, System.Drawing.Color.Transparent);
		}

		RankableFloat rf = GetRankableFloat(id);
		return Utils.GetRankablePip(rf);
	}

    protected override bool IsHeader(string id)
    {
        return _categories.ContainsValue(id);
    }

    private void SetCard(string cardName, SunTree tree)
    {
		if (DataManager.Inst.ActionCardIsRunnerPower(cardName) != (_xowerType == XowerType.RunnerPower))
		{ return; }
		_cardID = cardName;
		RefreshData();
    }

	private void UpdateCard(string cardId)
    {
		if(!string.IsNullOrEmpty(cardId) && cardId == _cardID)
        {
			RefreshData();
        }
    }
	#region primaryColumn
	private object[] GetData(string id)
	{
		object[] data = new object[]
		{
			id,
			"",
			"",
			"",
			"",
			""
		};

		try
        {
			AimingType aim = (AimingType)_cellFuncs[AIM_TYPE].Get();

			if (_rankableFloats.ContainsKey(id))
			{
				RankableFloat rf = _getDataRF(id);
				if (rf == null)
				{
					return data;
				}
				data[1] = rf.BaseValue;
				data[2] = rf.FinalValue;
				data[3] = rf.Scalar;
				data[4] = rf.RandomCap;
				data[5] = rf.Curve;
			}
			else
			{
				if (_cellFuncs.ContainsKey(id))
				{
					data[1] = _cellFuncs[id].Get();
				}
			}
		}
		catch(Exception e)
        {
			Debug.LogError($"Trouble creating the data for {id}");
        }
		return data;
	}
	

	private RepositoryItem PrimaryEditor(string id)
	{
		if (_enums.ContainsKey(id))
		{
			return CreateEditorDropDown(() =>
			{
				List<string> list = new List<string>();
				foreach (var item in _enums[id].GetEnumValues())
				{
					list.Add(item.ToString());
				}
				return list;
			});
		}
		if (_ints.Contains(id))
		{
			return CreateEditorTextEdit(DataEditorType.Integer);
		}
		if (_projectileFloats.Contains(id) && (AimingType)_cellFuncs[AIM_TYPE].Get() == AimingType.MarkProjectile)
		{
			return CreateEditorTextEdit(DataEditorType.Float);
		}
		if (_rankableFloats.ContainsKey(id))
		{
			return CreateEditorTextEdit(DataEditorType.Float);
		}
		if (id == MARK_STATI)
		{
			return CreateEditorCheckedComboBoxEdit(() =>
			{
				List<UnitStatus> statusList = new List<UnitStatus>((UnitStatus[])Enum.GetValues(typeof(UnitStatus)));
				Dictionary<string, bool> toReturn = new Dictionary<string, bool>();
				HashSet<UnitStatus> stati = _getMarkStati();
				foreach (UnitStatus status in statusList)
				{
					toReturn.Add(status.ToString(), stati.Contains(status));
				}
				return toReturn;
			});
		}
		return null;
	}

	private bool PrimaryValidator(string id, object value)
	{
        try
        {
			if (_rankableFloats.ContainsKey(id))
			{
				RankableFloat rfloat = GetRankableFloat(id);
				rfloat.BaseValue = (float)value;
				return _setDataRF(id, rfloat);
			}
			return _cellFuncs[id].Set(value);

        }
		catch(Exception e)
        {
			Debug.LogError("Error while trying to validate the first column.");
			return false;
        }
	}



	protected override List<string> GetErrors(string id, bool exitEarly)
	{
		List<string> errors = new List<string>();
		if (id == TOWER_FOOTPRINT && GetEnum<TowerFootprint>(_nodes[id].GetValue(1)) == TowerFootprint.None)
		{
			errors.Add("Tower footprint may not be 'None'!");
			if (exitEarly) { return errors; }
		}
		if (id == PROJECTILE_SPEED)
		{
			if ((AimingType)_cellFuncs[AIM_TYPE].Get() == AimingType.MarkProjectile && (float)_nodes[id].GetValue(1) <= 0)
			{
				errors.Add("Mark projectiles must have speed of more than zero!");
				if (exitEarly) { return errors; }
			}

		}
		if (id == AIM_TYPE)
		{
			AimingType type = (AimingType)_cellFuncs[AIM_TYPE].Get();
			if (type == AimingType.None)
			{
				errors.Add("Aiming type must not be 'None'!");
				if (exitEarly) { return errors; }
			}
			if (type != AimingType.Mark && type != AimingType.MarkProjectile && DataManager.Inst.GetXowerAtomicEffectCount(_cardID,_xowerType,0,false) == 0)
            {
				errors.Add("AimingType does not target a mark but ability has no secondary effects!");
				if(exitEarly) { return errors; }
            }
		}
		if (id == MARK_STATI)
		{
			int statiCount = DataManager.Inst.GetXowerMarkStati(_cardID, _xowerType).Count;

			if (statiCount > 0 && GetEnum<StatiInclusionType>(_nodes[STATI_INCLUSION_TYPE].GetValue(1)) == StatiInclusionType.None)
			{
				errors.Add("Specified stati but no stati inclusion type!");
				if (exitEarly) { return errors; }
			}
		}
		if(id == MARK_TYPE)
        {
			if(DataManager.Inst.GetXowerMarkType(_cardID,_xowerType) == MarkType.None)
            {
				errors.Add("MarkType cannot be 'None'!");
                if (exitEarly) { return errors; }
            }
        }
		return errors;
	}
	#endregion

	#region validators
	private ProjectileData GetProjectileData(float loft = -1, float speed = -1, float scale = -1)
    {
		ProjectileData pdata = new ProjectileData();
        try
        {
			pdata.Loft = loft >= 0? loft : (float)_nodes[PROJECTILE_LOFT].GetValue(1);
			pdata.Speed = speed >= 0? speed : (float)_nodes[PROJECTILE_SPEED].GetValue(1);
			pdata.Scale = scale >= 0? scale : (float)_nodes[PROJECTILE_SCALE].GetValue(1);

			return pdata;
        }
		catch(Exception e)
        {
			Debug.LogError(e.ToString());
			return pdata;
        }

    }

    private bool FinalValidator(string id, object value)
    {
		if(!_rankableFloats.ContainsKey(id))
        {
			return false;
        }
		RankableFloat rf = GetRankableFloat(id);
		rf.FinalValue = (float)value;
		return _setDataRF(id,rf);
    }

	private bool ScalarValidator(string id, object value)
	{
		if (!_rankableFloats.ContainsKey(id))
		{
			return false;
		}
		RankableFloat rf = GetRankableFloat(id);
		rf.Scalar = (float)value;
		return _setDataRF(id, rf);
	}

	private bool CurveValidator(string id, object value)
	{
		if (!_rankableFloats.ContainsKey(id))
		{
			return false;
		}
		RankableFloat rf = GetRankableFloat(id);
		rf.Curve = GetEnum<CurveType>(value);
		return _setDataRF(id, rf);
	}

	private bool RandomCapValidator(string id, object value)
    {
		if(!_rankableFloats.ContainsKey(id))
        {
			return false;
        }
		RankableFloat rf = GetRankableFloat(id);
		rf.RandomCap = (float)value;
		return _setDataRF(id, rf);
	}
    #endregion validators

	private void RefreshData()
    {
		List<string> nodeIds = _nodes.Keys.ToList();
		foreach(string id in nodeIds)
        {
			object[] data = GetData(id);
			for(int i = 0; i < data.Length; i++)
            {
				_nodes[id].SetValue(i, data[i]);
            }
        }
    }

	protected override bool IsDisabled(string id)
    {
		if(_categories.ContainsKey(id) && _categories[id] == CAT_PROJECTILE)
        {
			AimingType type = GetEnum<AimingType>(_nodes[AIM_TYPE].GetValue(1));
			return type != AimingType.MarkProjectile;
        }
		return false;
    }

	private RankableFloat GetRankableFloat(string id)
    {
		if (!_nodes.ContainsKey(id))
        {
			Debug.LogError($"Critical Error! {id} is not set to be a rankable float!");
			return null;
        }
		RankableFloat rf = new RankableFloat();
		try
		{
			rf.BaseValue = (float)_nodes[id].GetValue(COL_VALUE);
			rf.FinalValue = (float)_nodes[id].GetValue(COL_FINAL);
			rf.Scalar = (float)_nodes[id].GetValue(COL_SCALAR);
			rf.RandomCap = (float)_nodes[id].GetValue(COL_RANDCAP);
			rf.Curve = GetEnum<CurveType>(_nodes[id].GetValue(COL_CURVE));

		}
		catch(Exception e)
        {
			Debug.LogError("Error while getting rankable float from XowerDetailTree: " + e.Message);
        }
		return rf;
    }

	private RepositoryItem FinalValueEditor(string id)
    {
		if (!_rankableFloats.ContainsKey(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.Float);
    }

	private RepositoryItem ScalarEditor(string id)
    {
        if (!_rankableFloats.ContainsKey(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.Float);
    }

	private RepositoryItem CurveEditor(string id)
    {
		if (!_rankableFloats.ContainsKey(id)) { return null; }
		return CreateEditorDropDown(() =>
		{
			return Enum.GetNames(typeof(CurveType)).ToList();
		});
	}

	private RepositoryItem RandomCapEditor(string id)
    {
        if (!_rankableFloats.ContainsKey(id)) { return null; }
		return CreateEditorTextEdit(DataEditorType.Float);
    }

    protected override void RuntimeInit() { SetActionCardFuncs(); }

	protected override string GetTreeName() { return nameof(XowerDetailTree); }
	protected override List<string> ContextMenuItems(string id, bool multiSelect)
	{
		List<string> menu = new List<string>();
		if (string.IsNullOrEmpty(id)) { return menu; }
		return menu;
	}
	protected override void ContextMenuItemClicked(string operation, string id, List<string> otherNodes) { }
	protected override bool CheckMultiEditCompatability(string sourceID, string targetID, int column) { return false; }
	protected override void HandleSelfNodeDrop(TreeListNode targetNode, string droppingID, InsertType insertType) { }

	protected override void ClearEverything() { }
	protected override List<string> GetIDsToPopulate()
    {
        List<string> properties = new List<string>();
		//return properties;
        foreach (string key in _enums.Keys)
        {
			properties.Add(key);
        }
		foreach(string key in _multiEnums.Keys)
        {
			properties.Add(key);
        }
		foreach(string key in _rankableFloats.Keys)
        {
			properties.Add(key);
        }
		foreach(string key in _projectileFloats)
        {
			properties.Add(key);
        }
		foreach(string key in _ints)
        {
			properties.Add(key);
        }
		return properties;
	}
	protected override void InitialPopulateAction(string id)
	{
		if(!_categories.ContainsKey(id) || (_categories[id] == CAT_POWER && _xowerType == XowerType.Tower) || (_categories[id] == CAT_TOWER && _xowerType != XowerType.Tower))
		{ return; }
		TreeListNode parent = null;
		if(_categories.ContainsKey(id))
        {
            if (!_nodes.ContainsKey(_categories[id]))
            {
				parent = AppendNode(new object[] { _categories[id] }, null, _categories[id]);
                _nodes.Add(_categories[id],parent);
            }
			else
            {
				parent = _nodes[_categories[id]];
            }
        }
		TreeListNode node = AppendNode(GetData(id), parent, id);
		//node.StateImageIndex = 0;
		_nodes.Add(id, node);
	}

}

public class CellFunctions
{
	public Func<object> Get;
	public Func<object, bool> Set;

	public CellFunctions(Func<object> get, Func<object, bool> set)
    {
		Get = get;
		Set = set;
    }
}

public enum XowerType
{
	None,
	Tower,
	Power,
	RunnerPower
}
